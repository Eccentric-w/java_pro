
<map>
  <node ID="root" TEXT="java 进阶">
    <node TEXT="知识回顾" ID="3be0d88c1d0a79ae2a2c89a40b424199" STYLE="bubble" POSITION="right">
      <node TEXT="字符与字符串" ID="e2bb0853eeb403d1832c0ad3047c46a4" STYLE="fork">
        <node TEXT="&quot;字符串用双引号，用 String 声明&quot;" ID="0b41be432270f22437bd186460748c23" STYLE="fork"/>
        <node TEXT="&apos;字符用单引号，用 char 声明&apos;" ID="c7e9070a4ce4f6f7cd0130c87fbc3767" STYLE="fork"/>
      </node>
      <node TEXT="类" ID="ac9930bd30aaa8ba1e8610e852550a80" STYLE="fork">
        <node TEXT="命名：首字母大写，驼峰命名法" ID="cccffded3673e1b1c8d8165fe4532756" STYLE="fork"/>
        <node TEXT="成分（不是这些成分在类中就报错）" ID="59e73532a7463df732fbb7f275092ae3" STYLE="fork">
          <node TEXT="成员变量  Field" ID="ef1c8bf4e3ea09e8bcf1468b328a95e7" STYLE="fork"/>
          <node TEXT="成员方法 Method" ID="3fa20fe438dacc0e87f2f7ba9867cbfc" STYLE="fork"/>
          <node TEXT="构造器 Constructor 实例化一个对象并返回引用" ID="06cc11c1079b13a0e794142a2c86d600" STYLE="fork">
            <node TEXT="格式" ID="f1f6ec5a6c99086327e3791ad9e94b22" STYLE="fork"/>
            <node TEXT="一个类默认自带无参数构造器，如果类写了构造器（有参数），则无参构造器被覆盖" ID="6bc8f26118de80eb9900b8825282016f" STYLE="fork"/>
          </node>
          <node TEXT="代码块" ID="744a97253cd509546f6d4eb32b20565b" STYLE="fork"/>
          <node TEXT="内部类" ID="335540104da5becc119223e61558b36c" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="this关键字" ID="b15fd3ab2c234b1eed15446287d9d1be" STYLE="fork">
        <node TEXT="代表当前对象的引用" ID="bc572e64473fc1de894ae1db6ca68ede" STYLE="fork"/>
        <node TEXT="方法中：谁点用这个方法就代表谁" ID="1114abe0bc7afbef051b18d0612c64c9" STYLE="fork"/>
        <node TEXT="构造器中：代表了构造器正初始化的那个对象的引用" ID="3754cff1094973f45a2ed61016292db1" STYLE="fork"/>
      </node>
      <node TEXT="封装" ID="821a4f4dd0adeca41b7c9653b8049543" STYLE="fork">
        <node TEXT="作用" ID="d0525ed102a0957bcd83d0f545ea7cb9" STYLE="fork">
          <node TEXT="提高安全性" ID="b027d87b00c40db99b2b2f21e4e82451" STYLE="fork"/>
          <node TEXT="实现代码组件化" ID="275b2fcdfd59d5988a2b702238602b6e" STYLE="fork"/>
        </node>
        <node TEXT="规范" ID="711e6cb8f61e032c28c8727cd6c8ac1a" STYLE="fork">
          <node TEXT="成员变量都私有：private关键字（私有化）" ID="2aedbb0c43c1791fda8d944328b1904a" STYLE="fork">
            <node TEXT="private：被修饰的方法、成员变量、构造器只能在本类中被访问" ID="fb21475e8960e33fbe0abb898f32ae56" STYLE="fork"/>
          </node>
          <node TEXT="通过setter和getter方法暴露成员变量的肤质和取值" ID="8aeba0d6326914cebba8028d211b1e17" STYLE="fork"/>
        </node>
        <node TEXT="核心思想：合理隐藏，合理暴露" ID="15ed48f98a1949556b046baeb714cd9f" STYLE="fork">
          <node TEXT="不管代码有没有意义，都要进行封装规范" ID="e8c412108750b98bee1de4493d5b34b1" STYLE="fork"/>
        </node>
        <node TEXT="有参、无参、get成员变量、set成员变量" ID="0e4d4b1854796a6c26a182e7a2c36d68" STYLE="fork"/>
      </node>
      <node TEXT="static关键字：静态，修饰的成员（成员变量和成员方法）属于类本身" ID="d36ba985bfaa9b77b317bee1c4de6ed7" STYLE="fork">
        <node TEXT="成员变量" ID="9daa74393ea64bb6e30674553e9472cd" STYLE="fork">
          <node TEXT="静态变量：用static修饰的变量，属于类本身，通过类名直接访问" ID="543bcd58288a91e5234f972c268f1c01" STYLE="fork">
            <node TEXT="与类一起加载，只加载一次" ID="c453ecb528a747ddc81eed5174e664e7" STYLE="fork"/>
            <node TEXT="因为类只有一个，所以变量也只有一份，因此也可以通过对象访问（不推荐，没必要）" ID="3c95f58242c856ff850b52d750a0468f" STYLE="fork"/>
          </node>
          <node TEXT="实例变量：不用static修饰的变量，属于实例化的对象，必须通过对象名访问" ID="6e4499ffd439194ceb6f28aa3082cf97" STYLE="fork">
            <node TEXT="与类的实例化对象一起加载，类有多少对象就加载多少次" ID="69ae88e06c177ce5cfe41479622c51bc" STYLE="fork"/>
          </node>
          <node TEXT="内存机制" ID="4d4f7e916e6debad77bf349cd5942a8d" STYLE="fork">
            <node TEXT="" ID="3cfda615487c374ffe24d8ce192f359a" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="成员方法" ID="5c650eb59508a6a00451402ae69e47b3" STYLE="fork">
          <node TEXT="静态方法：static修饰，属于类本身，通过类名直接访问" ID="aefc6e7cd620a3079db711d4fd56902d" STYLE="fork"/>
          <node TEXT="实例方法：不用static修饰，属于类实例化出来的对象，必须通过对象名访问" ID="35c1221d10adfa1521744316cad8365a" STYLE="fork"/>
          <node TEXT="内存机制" ID="eefaae0d7575f3f329b4495ea44d788d" STYLE="fork">
            <node TEXT="方法栈是用来跑方法的" ID="207319c93cb386e6fdfd608995e3fc3f" STYLE="fork"/>
            <node TEXT="方法区用来存方法" ID="2ca070875dee328da377c7b8d300308f" STYLE="fork"/>
            <node TEXT="实例化的对象存在堆内存中" ID="e092866f2fa537c999f2a359449b71d1" STYLE="fork"/>
            <node TEXT="" ID="9ab2e84e08f38d6a3fcec8a17ea6b023" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="同一个类中访问静态成员可以省略类名不写" ID="7d6d79faf2bccd20dc338332be426d42" STYLE="fork"/>
        <node TEXT="常见问题（面试题）" ID="d268804d9c66e14a20dfb006739ad7c3" STYLE="fork">
          <node TEXT="实例方法是否可以直接访问实例成员变量？" ID="5cc251501e75f1c1fe167a78d63fde02" STYLE="fork">
            <node TEXT="可以，因为它们对属于对象" ID="e2c6eb0e98dccd687e17150e1a212e50" STYLE="fork"/>
          </node>
          <node TEXT="实例方法是否可以直接访问静态成员变量？" ID="640767b722cc14c322e75b96f260f34c" STYLE="fork">
            <node TEXT="可以，因为静态成员变量有共享性" ID="2f76fea05fdce167e7381b67bdce0067" STYLE="fork"/>
            <node TEXT="相当于通过 类名.变量名 的方式调用，&lt;类名.&gt;省略" ID="09aeacfee868212b8698a10d25ecf64c" STYLE="fork"/>
          </node>
          <node TEXT="实例方法是否可以直接访问实例方法？" ID="53fc2b390926613d4564ae23fb5a12d6" STYLE="fork">
            <node TEXT="可以，都属于对象。省略&lt;this.&gt;前缀" ID="100ec120584ad5ee5ec0b0c1752e792a" STYLE="fork"/>
          </node>
          <node TEXT="实例方法是否可以直接访问静态方法？" ID="8b807b18d92e8706180f72d58c7315be" STYLE="fork">
            <node TEXT="可以的，静态方法由共享性" ID="ed948e9c733a084975273d4a44d8b4e2" STYLE="fork"/>
            <node TEXT="相当于通过 类名.方法 的方式调用，&lt;类名.&gt;省略" ID="cb402f1fe3b314883d83a76831233bd3" STYLE="fork"/>
            <node TEXT="或者this也可以，&lt;this.&gt; 也表示当前类（对象可以调用静态方法）" ID="839093bf4fec39ef05ad9218609bf45f" STYLE="fork"/>
          </node>
          <node TEXT="静态方法是否可以直接访问实例变量？" ID="853dcd2e7077ce5bda69d7c70b299ef1" STYLE="fork">
            <node TEXT="不可以，静态方法属于类，实例变量属于对象" ID="86b7754449de14613958aab4c93c9a6b" STYLE="fork"/>
            <node TEXT="实例变量只能通过对象访问" ID="d73ec7541fd078736d30abfb9e1fae9e" STYLE="fork">
              <node TEXT="实例方法通过&lt;this&gt;关键字访问，this表示的是当前对象" ID="4c97e04d5fc08a653d885cf01d61ed57" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="静态方法是否可以直接访问静态变量？" ID="7b80c4c0b5d30997dc6c3e2533270b9a" STYLE="fork">
            <node TEXT="可以，静态方法和静态变量对属于类" ID="e8bb2a56c86c9867eb1f114e137b1263" STYLE="fork"/>
            <node TEXT="静态变量由有享性，可以被共享访问" ID="09293880416413f2679b3c2474a180b8" STYLE="fork"/>
          </node>
          <node TEXT="静态方法是否可以直接访问实例方法？" ID="a6898cdc8fe5c6c42dbdda6a91a59ba2" STYLE="fork">
            <node TEXT="不可以，实例方法只能被对象访问" ID="b732db9137b591f5deaec081025ac8bf" STYLE="fork"/>
          </node>
          <node TEXT="静态方法是否可以直接访问静态方法？" ID="5c6648b54326814d07093677a1de3fbf" STYLE="fork">
            <node TEXT="可以，静态方法有共享性，可以被共享访问、哦" ID="a6894e0258894fa5e47b68b45752a2e3" STYLE="fork"/>
            <node TEXT="都属于类 " ID="1adf2490ccd3b4d856e578450eb3fff8" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="构造器（构造方法）" ID="efe1d7a973a8561ce82209c908d5abaf" STYLE="fork">
        <node TEXT="方法名与类名相同" ID="25e18b6751a74ec78971593c686f0ddc" STYLE="fork"/>
        <node TEXT="没有返回类型" ID="5a3b31306f2a0cdc29f860722e43abda" STYLE="fork"/>
        <node TEXT="创建对象（new）时自动调用" ID="b223a65af07a4c2ffe9bfd28ab1240da" STYLE="fork"/>
        <node TEXT="类中必定有构造方法，若不写则自动创建无参构造方法；接口不允许实例化，所以没有构造方法" ID="8a807706ea6deb888ca8af136c091600" STYLE="fork"/>
        <node TEXT="不能被：static、final、  synchronized、abstract和native修饰。" ID="5b8e27b85578e94c4945ff95b50e9561" STYLE="fork"/>
      </node>
      <node TEXT="继承" ID="2517b43bacefdb99e3bd2296e909e6bb" STYLE="fork">
        <node TEXT="子类不能继承的" ID="968352bac23ffb681914bae07997bc40" STYLE="fork">
          <node TEXT="构造器不能继承，子类有自己的构造器" ID="98c61a3ccd94f32150bc9ba36314d98d" STYLE="fork"/>
          <node TEXT="子类继承的父类私有成员不能直接访问（争议观点）" ID="86858c7237a8b22d58873988405d167c" STYLE="fork"/>
          <node TEXT="子类不能继承父类的静态成员，但是可以共享访问，因为静态的只有一份，已经规定给了父类（争议观点）" ID="2926cea128c09a3de41057a5dc4d9db5" STYLE="fork"/>
        </node>
        <node TEXT="继承后成员访问特点" ID="e0f69094d2a4c91c96934ee93e97e62e" STYLE="fork">
          <node TEXT="成员变量" ID="bbe4a6639c585a0b24f1c5d09fd44511" STYLE="fork">
            <node TEXT="就近原则：先在方法内部找，没有就去子类找，子类没有找父类，父类没有就报错" ID="07f2c2b64a7200de8590f2f62d10bee5" STYLE="fork"/>
            <node TEXT="this 代表当前对象的引用，可用于访问子类对象的成员变量" ID="903bba3e868104b56480779b70457414" STYLE="fork"/>
            <node TEXT="super 代表父类对象的引用，可用于父类对象中的成员变量" ID="ca69de2a460a8306ae25860028977722" STYLE="fork"/>
            <node TEXT="" ID="1e203f71a13ecf8abaa0529dd26b2ad7" STYLE="fork"/>
          </node>
          <node TEXT="成员方法" ID="8e91c1566c3f8f2a640bad6ee9d70524" STYLE="fork">
            <node TEXT="就近原则：优先使用子类自己的方法" ID="dcf9adfc1facdb1e6894d079312a9d91" STYLE="fork"/>
            <node TEXT="子类有与父类相同名称的方法则父类的方法被覆盖" ID="c8d74c3b72b180dc97a6df09a73f030c" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="方法重写" ID="46a8aa85884b97cdda774f7b6bc567fb" STYLE="fork">
          <node TEXT="在重写的方法上面添加注解，重写注解：@override" ID="afc024f99a2a9b2b10f84567b4b47fc3" STYLE="fork"/>
          <node TEXT="要求（企业：申明不变，重新实现）" ID="e67142408de8b2a0082168699b54137f" STYLE="fork">
            <node TEXT="子类重写的方法名与形参列表要与父类一致" ID="c9957eadead3cc844c28c699c69c6d25" STYLE="fork"/>
            <node TEXT="子类重写的方法的返回值与父类相同，或范围小于父类" ID="80f42e12854b474d71baaec6f09be95b" STYLE="fork"/>
            <node TEXT="子类重写方法的修饰符与父类相同，或范围大于父类" ID="d716a2dec786dfef55115e09a09e4144" STYLE="fork"/>
            <node TEXT="子类重写方法申明抛出的异常与父类相同，或范围小于父类" ID="7f9a72c8e4b3e164579e8188a3630e13" STYLE="fork"/>
            <node TEXT="企业：申明等都不变，只修改方法内部的代码，重新实现" ID="65fb9b197d7b76a8e2ad71960714e86f" STYLE="fork"/>
          </node>
          <node TEXT="父类的私有方法和静态方法不能被重写" ID="efae688577372a781415b87e552ec64e" STYLE="fork"/>
        </node>
        <node TEXT="super关键字" ID="d55100a09a0d6ca84ec6e87f191f5759" STYLE="fork">
          <node TEXT="访问被重写的父类方法：" ID="3d04ba20b465195bca788ebc13e20285" STYLE="fork">
            <node TEXT="在子类定义方法，实现 super.被覆盖方法名，实现间接调用被覆盖方法" ID="25a756dea8b92ee2946066b7454cc7df" STYLE="fork"/>
            <node TEXT="实现父类与子类方法的综合使用" ID="14335dde708ded797fd1f34fa210366d" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="继承后的构造器" ID="75f568822854576ea6499b7ad0a58511" STYLE="fork">
          <node TEXT="子类的所有构造器默认会先访问父类的无参构造器，在执行子类自己的构造器" ID="b598276e9266778bcd724ddad71db171" STYLE="fork"/>
          <node TEXT="原因" ID="38434e6d5ab539ca051ca069d918322d" STYLE="fork">
            <node TEXT="子类构造器第一行默认有 super()；，写不写都存在，根据参数匹配调用父类的无参构造器" ID="08b33448e2f1a2e5f8784220743ac55b" STYLE="fork"/>
            <node TEXT="子类继承了父类的属性和行为，当调用子类构造器初始化子类对象时，必须先调用父类的构造器初始化继承自父类的属性和行为" ID="8b876008fcd441d913792026e6815ce5" STYLE="fork"/>
          </node>
          <node TEXT="内存图" ID="86ecfc299d0fde771821e11a4ac80e33" STYLE="fork">
            <node TEXT="" ID="da28f264f1e9fbc60dc26505a9e134ff" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="兄弟构造器" ID="aa732acdce0e9a1dbb17a6458ee34e17" STYLE="fork">
          <node TEXT="this（……）调用本类中其他构造器" ID="7ce58d9f454c1440ad87e165d61d4852" STYLE="fork"/>
          <node TEXT="super（……）调用父类构造器" ID="41511b0a604521a5958ecfb3f08a3893" STYLE="fork"/>
          <node TEXT="this（……）和super（……）都必须放在第一行，否则报错，所以this（……）和super（……）不会出现在同一个构造器中" ID="f32853db2b457757b6564d32b5a65066" STYLE="fork"/>
        </node>
        <node TEXT="继承的特点" ID="7b9c76e6fa573324e4b5ffb63b2306e2" STYLE="fork">
          <node TEXT="1. 单继承：每个类只能有一个直接父类" ID="b5fcc94da987654cee2211df45819a38" STYLE="fork">
            <node TEXT="问：为什么不能继承多个父类" ID="ffbca1fa3a504ea95623e381385ae66f" STYLE="fork">
              <node TEXT="用反证思路" ID="2c6fe2317f5c3d354c32b8c115f526ee" STYLE="fork">
                <node TEXT="若继承的多个父类中有相同的方法，子类继承调用时会出现二义性" ID="57eccfff68d6776bf33801fe6da5da41" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="2. 多层继承：每个类可以间接继承多个父类。" ID="8bd39feae42ad465386f55b817745e37" STYLE="fork"/>
          <node TEXT="3. 一个类可以有多个子类" ID="84fc93ecd6f9880213793ee8d0d6d43d" STYLE="fork"/>
          <node TEXT="4. 一个类默认继承object类，或者间接继承object类" ID="5283136e0d533b45e9b0b4b90270d197" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="引用类型" ID="417970dc16a26562a1de8f654d97d498" STYLE="fork">
        <node TEXT="除了基本数据类型，其他都是引用类型；引用类型也是一种数据类型" ID="819e0b3f17347b8e07ed927f849c4abd" STYLE="fork"/>
        <node TEXT="引用类型作为方法的参数和返回类型" ID="5e4d998f9230bf1058bddbfeeebfd64b" STYLE="fork">
          <node TEXT="引用类型一般存储的是地址" ID="7a34704c911f2aca67689edd794f2157" STYLE="fork"/>
        </node>
        <node TEXT="引用类型可以作为成员变量存储实例对象" ID="27a01872c4b99d402b352d5d552b4f44" STYLE="fork"/>
      </node>
    </node>
    <node TEXT="抽象类、接口、代码块、final、单例、枚举" ID="de533ec4740d2536ff864055e3a11329" STYLE="bubble" POSITION="right">
      <node TEXT="抽象类" ID="e71e785b88589baa7cef2b2ef4f99a51" STYLE="fork">
        <node TEXT="抽象" ID="a721e9dd40d3901ece27771ac1a5f8e6" STYLE="fork">
          <node TEXT="父类知道子类要实现某个功能，但是不同子类实现功能不同" ID="dcb41447067862133af4733be8009b76" STYLE="fork">
            <node TEXT="子类一定会重写父类的某个功能，该父类的方法就可以定义成抽象方法" ID="30b310a899c4fae5eabcb9539419283b" STYLE="fork"/>
          </node>
          <node TEXT="抽象方法" ID="38a7dbb8cfad624f7530bce877d4ffea" STYLE="fork">
            <node TEXT="没有方法体，只有方法签名" ID="0aa8acc54d9f7c48bbc4907449312399" STYLE="fork"/>
            <node TEXT="用abstract 修饰" ID="5aa74edb77c867d5315db16f8b030a0c" STYLE="fork"/>
          </node>
          <node TEXT="抽象类" ID="7ad0f788b2a76f016d7dc4aba91c6387" STYLE="fork">
            <node TEXT="子类集成了抽象类必须实现所有的抽象方法，否则也要定义成抽象类" ID="45751e9fa620007ab450445ff4695f95" STYLE="fork"/>
            <node TEXT="有抽象方法的类必须定义为抽象类" ID="ade7dc2d6c3e33197da79849c02d6bf2" STYLE="fork"/>
            <node TEXT="用 abstract 修饰" ID="ea3def1df4acdd5480d7a5ae3b6524a0" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="抽象类特征" ID="04f45d75aee550e636f708e9426437be" STYLE="fork">
          <node TEXT="核心：有得有失" ID="5b30d4f7aa345cd561d25781253707bd" STYLE="fork">
            <node TEXT="得到：创建抽象方法的能力" ID="c7bc387a0434b04de92975f777cee9ad" STYLE="fork"/>
            <node TEXT="失去：不能实例化对象" ID="440530654c4a25f2d4388029a244e3d3" STYLE="fork"/>
            <node TEXT="抽象类不能创建对象，除此之外，类的成分抽象类都具备" ID="dd2b9bda02621fab0691ba9d965a0520" STYLE="fork"/>
          </node>
          <node TEXT="面试题" ID="486aaf703001ea51a48a115a5dc7f6dd" STYLE="fork">
            <node TEXT="抽象类是否有构造器，是否可以创建对象，为什么？" ID="346af4dd87ee4eeb1cd2de66d01e9afc" STYLE="fork">
              <node TEXT="抽象类作为类类一定有构造器，为子类创建对象时调用父类构造器使用" ID="1e49643360b4d01e88c493c7bc44014f" STYLE="fork">
                <node TEXT="子类构造器默认有super()，需要访问父类构造器" ID="cedd670a68b4ce2ead4511be801d9f59" STYLE="fork"/>
              </node>
              <node TEXT="不可以创建对象" ID="f0e2132c0adb489ad8ff3045e4744305" STYLE="fork"/>
              <node TEXT="反证法：如果可以，那么实例化的对象调用没有方法体的抽象方法时，会出问题" ID="180cc25b5ffc205f5d72f988af7cec6e" STYLE="fork">
                <node TEXT="抽象意味不能有实体" ID="294fbc5740831545db61a1ffec0a6b77" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="抽象类存在意义（面试常问）" ID="ab9669562e2a22481bd80e7175b55083" STYLE="fork">
          <node TEXT="1. 被子类继承" ID="5fc8f72c149b243bfa6c16a697072363" STYLE="fork"/>
          <node TEXT="2. 模板思想，部分实现，部分抽象。可以设计模板设计模式" ID="40b1ca649642b7675e1439cd3ad3346f" STYLE="fork"/>
        </node>
        <node TEXT="模板设计模式" ID="7a91693f9ba5bb1b2efaf805f98d7f8a" STYLE="fork">
          <node TEXT="作用：优化代码架构，提高代码复用性。做到部分实现、部分抽象，抽象的部分给使用者重写实现" ID="23a7b51439d600b24b906f5517baa4b9" STYLE="fork"/>
        </node>
        <node TEXT="注意事项" ID="713cef5766b9dd08829e3cfd5fe7be35" STYLE="fork">
          <node TEXT="抽象的东西只能在本类中被访问" ID="08c4fa600cfd8eb76b1909710b87fb66" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="接口" ID="234d2d07e1eab0659c9abaa8fbcda16c" STYLE="fork">
        <node TEXT="概念" ID="66b3095321c95b9bdcfd1cbd214df66f" STYLE="fork">
          <node TEXT="JDK1.8之前，接口里只能是抽象方法和常量" ID="99d21102a641d17118f3771f832f7f87" STYLE="fork"/>
          <node TEXT="体现规范思想，实现接口的子类必须实现接口的所有抽象方法" ID="02806ff3dc987f4601bf110957eb2721" STYLE="fork"/>
        </node>
        <node TEXT="抽象方法" ID="f015409f0cbba79bea4498b667988cee" STYLE="fork">
          <node TEXT="接口里的方法可以省略 public abstract 不写，默认后加上" ID="fa6f75813b627b7d4e7f7c48bcc1274a" STYLE="fork"/>
        </node>
        <node TEXT="常量" ID="8e4ad57508ceced175f30b64aae7b855" STYLE="fork">
          <node TEXT="程序运行过程中不可改变" ID="7811f87da438d24af3942aafa532cf70" STYLE="fork"/>
          <node TEXT="修饰符为：public static final ，在接口中可以省略不写，默认会加上" ID="c53411babed1f8afc351316296845272" STYLE="fork"/>
          <node TEXT="名词字母全部大写，单词间用 _ 隔开" ID="706133da0fc946266b1d8f76f6a01760" STYLE="fork"/>
        </node>
        <node TEXT="基本实现" ID="9e36e9421800ea602105a2e69f2b9e23" STYLE="fork">
          <node TEXT="类与类是继承的关系，子类继承父类" ID="d7a18d91a86e5c508ce92f50655f8f38" STYLE="fork">
            <node TEXT="继承只能单继承" ID="e955211af27818463e9c7d19d50ee66a" STYLE="fork"/>
          </node>
          <node TEXT="类与接口是实现的关系，实现类实现接口" ID="950c1bd6aa7d4621dc12bbd53d401715" STYLE="fork">
            <node TEXT="实现可以多实现" ID="42da50d8d1137fc4ff678d3849e169c6" STYLE="fork"/>
          </node>
          <node TEXT="接口与接口之间可以多继承" ID="95cd0ae17718f80b37c4f2645fa01b48" STYLE="fork"/>
        </node>
        <node TEXT="JDK1.8及之后接口新增的三个方法" ID="26b43766faf9cf0b2292f693c50cddf4" STYLE="fork">
          <node TEXT="1. 默认方法：实例方法，必须用 default 修饰" ID="ec676ed7fa0b45d08671e3e4e2bb49ae" STYLE="fork">
            <node TEXT="默认会加 public 修饰" ID="7e9cc5505a83d4e84ad66dd58053b3e2" STYLE="fork"/>
            <node TEXT="必须由接口的实现类的对象调用" ID="ebc24d78d1ec7ed380528ff0e96fa5eb" STYLE="fork"/>
          </node>
          <node TEXT="2. 静态方法" ID="42fd36b54db32611aab89e864da6af51" STYLE="fork">
            <node TEXT="用 static 修饰" ID="09c2c378784ab6781d0493c33e24ec18" STYLE="fork"/>
            <node TEXT="默认用 public 修饰" ID="a6e058d6c3af1b47e8c90da7163c30e7" STYLE="fork"/>
            <node TEXT="只能用 接口名.方法名 调用" ID="c65b063e6e60d424b852ff7c2697baeb" STYLE="fork"/>
          </node>
          <node TEXT="3. 私有方法：私有的实例方法" ID="8d03916a5db22157bc602112772d7d69" STYLE="fork">
            <node TEXT="必须有 private 修饰" ID="8dd72337fb134f229ddfdf78abb9757d" STYLE="fork"/>
            <node TEXT="私有方法只能在本类中调用，但是接口中没有对象，所以私有方法是给默认方法和静态方法调用的" ID="e4f84ab051288b656ff70644d7a9751d" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="多接口实现的注意事项" ID="99479edb566e89442ca3e7bac62714bb" STYLE="fork">
          <node TEXT="1. 如果实现了多接口，所个接口中的同名静态方法不会冲突" ID="44454c254102acb622fbf01179f33865" STYLE="fork">
            <node TEXT="因为静态方法只能通过各自的接口名各自调用" ID="f15d3e53e6819dd420b697cbeda20b08" STYLE="fork"/>
          </node>
          <node TEXT="2. 当子类即继承父类又实现多个接口，且父类中的成员方法与接口中的默认方法重名时，就近选择执行父类的成员方法" ID="4c24bdb5915c0832bde9d828551a37ac" STYLE="fork"/>
          <node TEXT="3. 一个类实现多个接口时，多个接口中存在同名的默认方法，则实现类必须重写这个方法" ID="5b37de844edb06cd3d7f0a655e112a76" STYLE="fork"/>
          <node TEXT="4. 接口中没有构造器，不能创建对象" ID="b74a26013b603760149711b972ce5541" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="代码块" ID="c1e637fb52fbf6edd03557f6bb55bc33" STYLE="fork">
        <node TEXT="静态代码块" ID="ebc1f3be6986f35d819534766b7b1e47" STYLE="fork">
          <node TEXT="必须用static修饰" ID="eb5fb17464680cea40d6a32b0f1722ca" STYLE="fork"/>
          <node TEXT="属于类，与类一起优先加载（优先与mian方法），自动触发一次" ID="23287d0f58bd9602456b77cf2a6a3b73" STYLE="fork"/>
          <node TEXT="可以用与在执行类的方法前，进行静态资源的初始化操作" ID="70a163d203e097cb3abe41906ac71930" STYLE="fork"/>
        </node>
        <node TEXT="实例代码块" ID="9611c4fbf942474ae5ce63a8b3d5e825" STYLE="fork">
          <node TEXT="用的少" ID="770044ab35d479e3300539f229910d52" STYLE="fork"/>
          <node TEXT="不用static修饰" ID="91902e0dddba545d74db506f73e55dd1" STYLE="fork"/>
          <node TEXT="属于类的每个对象，与类的对象一起加载" ID="90dad803f54a730499e6f7936f0cc818" STYLE="fork"/>
          <node TEXT="每次创建对象时触发执行一次" ID="37ea11efa74297637f9f3fff2b6e769f" STYLE="fork"/>
          <node TEXT="用于初始化实例资源" ID="dae17ab7e8f92962ed4e6743251ee8b8" STYLE="fork"/>
          <node TEXT="实例代码块的代码实际上时提取到构造器中执行的" ID="091caeb1d4bca7d5c1c5cdcbfaf869b3" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="final关键字" ID="2439546536e88174ac0ebffc1fc798d1" STYLE="fork">
        <node TEXT="final修饰的类不能被继承" ID="06bdce379bf0cdce5e739307f168163f" STYLE="fork"/>
        <node TEXT="final修饰的方法不能被重写" ID="99062bac5b3550aa5ca509d2216203f2" STYLE="fork"/>
        <node TEXT="final修饰的变量有且仅能被赋值一次" ID="eb2bd5fa9bd127c68b5abcd3c9e46475" STYLE="fork">
          <node TEXT="修饰局部变量：保护起来，防止执行过程中被修改" ID="cf873fbe31ce21f42375d285ecda1570" STYLE="fork"/>
          <node TEXT="修饰成员变量" ID="7b9b86dcfb6cdb0ca8ffdd2cc33a4083" STYLE="fork">
            <node TEXT="静态：变量变成常量（public static final）" ID="a8e13ccfbf37ee132015de050e74bc28" STYLE="fork">
              <node TEXT="在哪里能赋值一次" ID="8ab66140dc9df4a00fc0acf4e939b8b7" STYLE="fork">
                <node TEXT="1. 定义的时候" ID="9279ce7edf5fa57ec9a6e8685bd722e8" STYLE="fork"/>
                <node TEXT="2. 静态代码块中" ID="09a7c70cb9ff4c8a963109b371f4bde0" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="实例：没有意义，对象创建后无法修改" ID="d7d7102f76cf0dc0c5be500c9bf43d59" STYLE="fork">
              <node TEXT="可以在哪里赋值一次" ID="1d890e7b3d71cecd2fde321484eb59c6" STYLE="fork">
                <node TEXT="1. 定义的时候赋值一次" ID="2d1835d8b561c9a0a843962c66ffb757" STYLE="fork"/>
                <node TEXT="2. 实例代码块中" ID="b8f256a3d47359c20322df0537f3b2a5" STYLE="fork"/>
                <node TEXT="3. 构造器中（实例代码块中代码实际就是提取到构造器中执行的）" ID="624713e1ad3cbbbf9c168a615076d82c" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="变量" ID="8e60993adb3017e3f49515ad0b071d9f" STYLE="fork">
          <node TEXT="成员变量" ID="e5f14e92264b897e91054073d999fa9f" STYLE="fork">
            <node TEXT="静态成员变量：有static修饰，属于类，只加载一份" ID="9be0a73595664e6231c95a9a7d663d8d" STYLE="fork"/>
            <node TEXT="实例成员变量：无static修饰，属于对象，与对象一起加载" ID="ba235c67ee20cf441566d8e095501a2c" STYLE="fork"/>
          </node>
          <node TEXT="局部变量" ID="dd06d16a60dd30c43d284a4a0bbc736a" STYLE="fork">
            <node TEXT="在方法、循环、构造器、代码块中，作用范围内用完就消失了" ID="3d5262cec9b10f93e3036812cce1864f" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="单例设计模式（面试重点）" ID="ce3f569bb3dcfac5e45ea2e1750b2746" STYLE="fork">
        <node TEXT="一个类永远只用一个对象，不能创建多个对象" ID="3f7d663927381721aff3eb7dc7a41c93" STYLE="fork"/>
        <node TEXT="为什么用单例" ID="166e33776b5cc4f9c2511126f21141b3" STYLE="fork">
          <node TEXT="1. 开发中用的对象只需要一个，例如：虚拟机对象、windows的任务管理器" ID="5f0e04582fc800b411d68f70c0159533" STYLE="fork"/>
          <node TEXT="2. 对象越多越占内存，有时只需要一个对象就可以实现业务，单例可以节省资源，提高性能" ID="0a19d5833657d589a867c5212471cbe0" STYLE="fork"/>
        </node>
        <node TEXT="实现方法（目前有两种）" ID="7083480a3bd53ed0c77a0b32299f43b3" STYLE="fork">
          <node TEXT="懒汉单例设计模式" ID="7ffebcc577d54df9ccc7aaca068bc296" STYLE="fork">
            <node TEXT="通过类获取对象时，发现没有对象才去创建" ID="9788829f7a3930b5fa9d6934909bd393" STYLE="fork"/>
            <node TEXT="" ID="ebf746781dccb5e4b75ce77f029477b4" STYLE="fork"/>
          </node>
          <node TEXT="饿汉单例设计模式" ID="75ac727c5f0db3caf00ed1461473c4bd" STYLE="fork">
            <node TEXT="调用类时，对象已经创建好了" ID="a67d0f67ee6cfa8e460fdd9769dd2ccf" STYLE="fork"/>
            <node TEXT="步骤" ID="f7733af64f57e72a4f32a78b4fd3ba2e" STYLE="fork">
              <node TEXT="1. 第一单例类" ID="7eb7d1290d760635735df8ba368ca444" STYLE="fork"/>
              <node TEXT="2. 把类的构造器私有" ID="fab87b645c87d317683a0b9afba934d3" STYLE="fork"/>
              <node TEXT="3. 实例化一个对象，并赋值给一个静态成员变量" ID="0c06cd8efe7b28a9e94774ac92bb3e6e" STYLE="fork"/>
              <node TEXT="4. 定义一个方法反回创建好的单例对象" ID="66b551f2c5720403eb9358f6005d7387" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="枚举类" ID="787b02a6a3f6092477a33e87c9e0ac2c" STYLE="fork">
        <node TEXT="作用" ID="693bb0d3f12a7aa6f690d38fbc6141f6" STYLE="fork">
          <node TEXT="信息标志和信息分类" ID="d7d1f3d86cc89e03d975bb411bb8a68d" STYLE="fork"/>
        </node>
        <node TEXT="特点" ID="5d16977ab7528499950ab77280615e37" STYLE="fork">
          <node TEXT="1. 枚举类用final修饰，不能被继承" ID="180afbb4dc6371708d34eb84a81e1267" STYLE="fork"/>
          <node TEXT="2. 枚举类默认继承了枚举类型 java.lang.Enum" ID="be6a3b9bf013a398af01c9b818109f23" STYLE="fork"/>
          <node TEXT="3. 第一行罗列的时枚举类的对象，用常量存储" ID="76082fd47965992714670f97372d115d" STYLE="fork"/>
          <node TEXT="4. 枚举类的构造器是私用的" ID="f08f37c496e92bb7d92be3526e4d4318" STYLE="fork"/>
          <node TEXT="5. 枚举类是多例模式" ID="3df0b6d4e66c2772e7707d26aaf4e524" STYLE="fork"/>
        </node>
      </node>
    </node>
    <node TEXT="多态、内部类、包、object类" ID="4dff7809842dbede22ffed6e32476a01" STYLE="bubble" POSITION="right">
      <node TEXT="多态" ID="3f8528620315a20caadde65ecfc72233" STYLE="fork">
        <node TEXT="概念：同一类型的对象，执行同一行为，在不同的状态下会表现出不同的行为特征" ID="bb95d1c950f2ad5adb015ca14b58b40c" STYLE="fork"/>
        <node TEXT="形式" ID="524feb4217e9f3b149011fcfd8d62b12" STYLE="fork">
          <node TEXT="父类类型  对象名称  =  new  子类构造器" ID="d5104c552ba0835111a38c7f354a0e73" STYLE="fork"/>
          <node TEXT="接口  对象名称  =  new  实现类构造器" ID="7c07c30d18d3d823f58f214506063f41" STYLE="fork"/>
          <node TEXT="对于方法的调用：编译看左边，运行看右边" ID="58f895e819c1dc382ecfeeec6c617753" STYLE="fork"/>
          <node TEXT="对于变量的调用：编译看左边，运行看左边" ID="d7bf966cf15f63bbce250f2c6a7fb893" STYLE="fork"/>
        </node>
        <node TEXT="使用前提" ID="3fffb138ce6c713d0d9ef2e3795fa528" STYLE="fork">
          <node TEXT="1. 存在继承或实现关系" ID="05af8d61a022555d8a2d9dd23b2702b2" STYLE="fork"/>
          <node TEXT="2. 必须存在父类类型的变量实例化子类类型的对象" ID="c0962e8c3fe4571c0083b00b17964975" STYLE="fork"/>
          <node TEXT="3. 必须存在方法重写" ID="ca1d43bfd45012609bc46d562a05123d" STYLE="fork"/>
        </node>
        <node TEXT=" 优势" ID="36ffdff2b7eb909ebdfe9af24dc86221" STYLE="fork">
          <node TEXT="1. 右边对象可以实现组建化切换，功能也随之改变" ID="8554d43bf4d9d7d8e67213ddaf8a7ba7" STYLE="fork">
            <node TEXT="便于维护和拓展，实现类与类之间的解耦" ID="a9d047ae3a06a24f67fba75ad11a823c" STYLE="fork"/>
          </node>
          <node TEXT="2. 父类类型可以作为方法形参，将子类对象传递给方法" ID="55b3903e1f89b862c863d907f2aa6dbf" STYLE="fork"/>
        </node>
        <node TEXT="缺点" ID="31017cad16298d8df03c1921326abafb" STYLE="fork">
          <node TEXT="多态形式下，子类不能直接调用自己的独有功能。" ID="f620e89d5fc4b42e11ad1a99f5bfbba2" STYLE="fork">
            <node TEXT="编译看左边，父类没有的方法，子类不能直接调用，编译阶段直接报错" ID="a76fd73656d84c8913d723cfe41fd866" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="类型转换" ID="9ed0d55984155bac53320bd20d59d094" STYLE="fork">
          <node TEXT="引用类型自动类型转换" ID="369a6ca6dabedc99e823cfcadad7b2b7" STYLE="fork">
            <node TEXT="小范围（子类类型）的对象或变量可以自动类型转换赋值给大范围（父类类型）的变量" ID="f88d07c29a892a98f9c8dc823d880ec1" STYLE="fork"/>
          </node>
          <node TEXT="引用类型强制类型转换" ID="e8f01394ebd213183cf0add20fc97e0d" STYLE="fork">
            <node TEXT="转换后可以调用子类自己特有的方法" ID="35301a22d9e55306ae6dbb42c5157a6a" STYLE="fork"/>
          </node>
          <node TEXT="多态类型下转换异常问题" ID="f14309c74c9b8f95a39ea4bcbebc4401" STYLE="fork">
            <node TEXT="有继承或实现关系的两个类型可以进行强制转换，编译阶段不会报错" ID="2875e4d1512491e91c933d4ff5577111" STYLE="fork"/>
            <node TEXT="判断类型：" ID="52268c7c676cb6f0d234004d1de130ad" STYLE="fork">
              <node TEXT="变量  intanceof  类型，相同则返回true" ID="4f3eb9a030ad22ac80a1cf221fbbbbcd" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="内部类（前三个用处不大）" ID="90ef3208c22834441cec7ffa02752360" STYLE="fork">
        <node TEXT="有更好的封装性，内部类有更多的修饰符，体现组建化思想" ID="3a0b14a92b168edf473663a3461a11a8" STYLE="fork"/>
        <node TEXT="静态内部类" ID="f791133251d1b7630dbd7c44bf5dad5d" STYLE="fork">
          <node TEXT="属于外部类本身，只会加载一份" ID="013ec9716801b4ede885641605ed5250" STYLE="fork"/>
          <node TEXT="与外部类没有区别，只是位置不同" ID="6a53e5b410a9a55ddb01b520967740c1" STYLE="fork"/>
        </node>
        <node TEXT="实例内部类" ID="b545165e39242b3d38385a804c3394e4" STYLE="fork">
          <node TEXT="属于外部类的对象（创建时需要先创建外部类对象在创建内部类对象），随对象一起加载" ID="ed57c7d92a7e44dbf4e2f323f4e1e817" STYLE="fork"/>
          <node TEXT="不能定义静态成员" ID="4cbc4341ff830b6cdf787c657a2248d0" STYLE="fork"/>
        </node>
        <node TEXT="局部内部类" ID="916177d3263df010732039e369b9f0b0" STYLE="fork">
          <node TEXT="只能定义实例成员，不能定义静态成员" ID="ae7b69d193e6d12e5dd6a6bb4f8a5440" STYLE="fork"/>
          <node TEXT="可以定义常量" ID="a4378ffed318f248edfcde653e9dc07d" STYLE="fork"/>
        </node>
        <node TEXT="匿名内部类" ID="7def623b9258254888c7ffedb690aa64" STYLE="fork">
          <node TEXT="没有名字的局部内部类" ID="aad0a851297aba86ff9d31da9c5860b6" STYLE="fork"/>
          <node TEXT="目的：简化代码" ID="17d4fb3e9377fee8ebd2f032872703df" STYLE="fork"/>
          <node TEXT="格式：new 类名|抽象类|接口（形参）{ 方法重写 }" ID="d979f3b383c3f22f6c500251ee3c56e1" STYLE="fork">
            <node TEXT="是一个匿名内部类也是一个对象" ID="e220cb0a09c3c3520988ba791d41c18b" STYLE="fork"/>
          </node>
          <node TEXT="特点：" ID="6cfd47357453d1a67c16f675108d3bde" STYLE="fork">
            <node TEXT="1. 不用定义子类" ID="a03fa0a3c686502fe0fa1e52342d694e" STYLE="fork"/>
            <node TEXT="2. 没有名字的内部类" ID="99c8b3fed55dc036025b1e76111d5bfc" STYLE="fork"/>
            <node TEXT="3. 匿名内部类写出来，就会立即创建匿名内部类的对象返回" ID="4ddcd7409e87d197d49aa3b0f3d808e5" STYLE="fork"/>
            <node TEXT="4. 匿名内部类的对象类型就是new 后面的类名的子类类型" ID="d9d00f3eca137252a1ced645fe327a49" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="包" ID="a477ecad8d806d141c00e570f260061b" STYLE="fork">
        <node TEXT="命名规则" ID="43cdebd1a9c4369dcba0fb5060176b71" STYLE="fork">
          <node TEXT="公司域名倒写+技术名称" ID="77489327cf23a4d117f27df9392d6312" STYLE="fork"/>
          <node TEXT="全部英文，多个单词用“.”连接" ID="38cb0703085095d310cc712d841d1e83" STYLE="fork"/>
        </node>
        <node TEXT="用法" ID="8d45b3d9545564550b198855794c8a99" STYLE="fork">
          <node TEXT="相同包下的类可以直接访问" ID="569d7f603bf307d5b9a99c94c6056ba4" STYLE="fork"/>
          <node TEXT="不同包下类想访问必须导包" ID="ddb451cbd0e9565cddb998278900ca59" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="权限修饰符" ID="c023604f9d76fad8a1e159e32a6636db" STYLE="fork">
        <node TEXT="作用：private &lt; 缺省 &lt; protected &lt; public " ID="66cd462687d299b3043d3ae4f076cd1f" STYLE="fork"/>
        <node TEXT="作用范围" ID="11548e159e25f3c5c4c26901a35bea27" STYLE="fork">
          <node TEXT="其他包的子类：其他包中本类的子类" ID="5e795bf3b71b37412351aade1b73e91f" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="object类" ID="aa4b1f312fb572316431caedfe2f2551" STYLE="fork">
        <node TEXT="toString()方法：返回对象在内存中的地址" ID="fac726b61c9b9d1a38721034c9076afa" STYLE="fork">
          <node TEXT="直接输出对象默认会调用toString()方法，所以toString()方法可以默认不写" ID="3cbc0fd6df4dacb1162706b4ec9b7901" STYLE="fork"/>
          <node TEXT="开发中看到对象的地址意义不大，更多的用处是提供给子类重写（可直接生成）" ID="1c7241bddd1b674238c0908f696c30a7" STYLE="fork"/>
          <node TEXT="重写后查看对象内容" ID="b17f7099a68e1be8e634dbe012127b20" STYLE="fork"/>
        </node>
        <node TEXT="equals(boject o)方法：比较两个对象的地址是否相同" ID="d78ffb7ae31d8b15d52d5f5fddada9e2" STYLE="fork">
          <node TEXT="用处不大，比较地址可以直接用 &quot;==&quot; 符合代替" ID="47cacc7b359820e7b58445dd32073640" STYLE="fork"/>
          <node TEXT="通常是用来比较两个对象的内容是否相同" ID="fd322000d8bf7463dbfe21b184f310d9" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="objects类" ID="c5cae55c85dd79d8fdd17ccae45bb9df" STYLE="fork">
        <node TEXT="equals方法：比较两个对象地址（更安全）" ID="bad6e9c06b0292e54ec90851402fb5ed" STYLE="fork">
          <node TEXT="此时第二个输出会报空指针异常，第一个输出则正常输出false（底层进行非空判断）" ID="e94e2ce419e87376ca28d8bedf9defd6" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="date日期类" ID="ce93284f86c47da305c9feb8217cc74d" STYLE="fork">
        <node TEXT="有许多已经过时的方法" ID="a2d79b2bb9eb71b64697374a6db380a6" STYLE="fork">
          <node TEXT="企业中不会用过时的方法" ID="23d2f8a73e5cd2636b23c1bf3a4fc117" STYLE="fork"/>
          <node TEXT="过时：现在可以用，但是已经放弃，在不久的将来不能用" ID="7de159a57724a1cbe1d2bf9ae7b294a9" STYLE="fork"/>
        </node>
        <node TEXT="时间形式" ID="031aac835c3a11b802ae9b6edcca66eb" STYLE="fork">
          <node TEXT="获取系统当前时间的日期" ID="c3b03cb5e4aa6123453b57df79dcf1d6" STYLE="fork">
            <node TEXT="创建Data对象直接输出是日期类型，说明重写了toString方法" ID="5be2c41fe93cbb50d1e83701e88047f8" STYLE="fork"/>
          </node>
          <node TEXT="从1970年1月1日00:00至今的毫秒" ID="94f662769cdf436b0ca433a064aaa73d" STYLE="fork">
            <node TEXT="getTime方法：返回毫秒值" ID="bc5a0f081d3c7815d909a0dfe36acafe" STYLE="fork"/>
            <node TEXT="时间毫秒值可以放入有参构造器中转换成日期对象" ID="6c5d68c6620ac99a2f22c4bce828ace0" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="SimpleDateFormat类格式化日期类型" ID="24811b533cf9c97fa2d52758e20f63ec" STYLE="fork">
          <node TEXT="format方法格式化" ID="a628c603ec86f5bfa34b46936cd62138" STYLE="fork">
            <node TEXT="支持格式化类型" ID="ae326c3ae15c292031e2daceb000519b" STYLE="fork">
              <node TEXT="日期型" ID="d68a5163a5776df04efd87778f992784" STYLE="fork"/>
              <node TEXT="毫秒值" ID="32a31f6da6b16a0069f90606f19f8235" STYLE="fork"/>
            </node>
            <node TEXT="将日期类型转换成指定格式的字符串" ID="9ca7c9cb8ab8f9bdb3c8b625fd0dfbfe" STYLE="fork"/>
          </node>
          <node TEXT="parse方法解析字符串类型" ID="53aae482575a3e9ce44c572fac9e430d" STYLE="fork">
            <node TEXT="创建SimpleDateFormat对象时构造器的参数必须与被解析的时间格式一致" ID="317e4218cddcb6291ec125992ba823dc" STYLE="fork"/>
            <node TEXT="按照指定格式解析字符串，转换成指定的日期类型" ID="2b801c482f26aea8a2291ff491ab2670" STYLE="fork"/>
          </node>
          <node TEXT="符号代表（区分大小写）" ID="1202b78117a7c72ce303e034376b33b9" STYLE="fork">
            <node TEXT="yyyy：年" ID="0907d2df0a0721e9de25e049acc03569" STYLE="fork"/>
            <node TEXT="MM：月" ID="2de512e4aefbbf2f436eedea5ad262a2" STYLE="fork"/>
            <node TEXT="dd：日" ID="2571f60444160bd89081417a624ce285" STYLE="fork"/>
            <node TEXT="HH：小时" ID="46aec3b101c761eb500c1388d7850d24" STYLE="fork"/>
            <node TEXT="mm：分钟" ID="b13273159a7da8caa34cf41dcd1e41f4" STYLE="fork"/>
            <node TEXT="ss：秒" ID="4a9168dd4473aff4dad4646b51290071" STYLE="fork"/>
            <node TEXT="EEE：星期" ID="e3043991b94b8477517abb862e35f3a9" STYLE="fork"/>
            <node TEXT="a：上午/下午" ID="3878f13f8df9d692762f65f626f4a141" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="获取当前时间戳：System.currentTimeMillis();" ID="ff8f551562bd2c6bd43b7049d8bf8a6e" STYLE="fork">
          <node TEXT="用long类型的变量接收" ID="798a290624a8e3f9b619a9226854a7cb" STYLE="fork"/>
        </node>
      </node>
    </node>
    <node TEXT="各种API的调用" ID="06ceaf978ed23c69773031823f27ddbc" STYLE="bubble" POSITION="right">
      <node TEXT="Calendar日历类" ID="b3c56454ae6f4d48ceb72cb3ba5dd609" STYLE="fork">
        <node TEXT="创建：Calender calendar = Calendar.getInstance ()" ID="bd6a67a319b9d9c8849448a5a9405561" STYLE="fork">
          <node TEXT="通过类名调用方法直接创建对象" ID="3461167b01dced1afa111cdd53340672" STYLE="fork"/>
        </node>
        <node TEXT="get方法获取指定字段的值" ID="65a4f1d889eebab663d90066400e44df" STYLE="fork">
          <node TEXT="例如：对象名.get(Calendar.字段名)" ID="cb091b3cf00d6c9c8bf7cb01bf0ca5a1" STYLE="fork"/>
        </node>
        <node TEXT="add（Calendar.字段名，数值），将指定字段增加多少值" ID="61092688c3cd60fc643b050c8066dccd" STYLE="fork">
          <node TEXT="例如：年数增加5" ID="65843fa3e7e59a18320d624fdb2f070e" STYLE="fork"/>
        </node>
        <node TEXT="注意" ID="9c306068c33a81a16dec2af345147bd5" STYLE="fork">
          <node TEXT="字段名均为大写，例如：MONTH，DAY_OF_MONTH，DAY_OF_YEAR等" ID="beee4f6b03ac858b22e413952265412f" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="Math类" ID="0d5dc307a4054191c169c54c4225a83b" STYLE="fork">
        <node TEXT="最终类，无法创建对象，都是静态方法，直接通过类名调用" ID="a99d1a333a45b3af745568c69b4be571" STYLE="fork"/>
      </node>
      <node TEXT="BigDecimal大数据类" ID="68a50fa3400aeae47e0cd81fc83afb83" STYLE="fork">
        <node TEXT="普通浮点数运算存在失真问题，大数据类可以解决这个问题" ID="a2e642a0ac5babc741efb6e60b32e6e9" STYLE="fork"/>
        <node TEXT="创建对象方法" ID="b0befffb57b2e5ca7642c19c1c2862c0" STYLE="fork">
          <node TEXT="BigDecimal a1 = new BigDecimal.value(double型数值)" ID="81cf80b829e056844b7b122ff077e451" STYLE="fork"/>
          <node TEXT="将浮点数据类型封装成大数据对象，从而确保运算准确性" ID="c6788d278d8942e0c8d6e406c4d96299" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="正则表达式" ID="6f8b7828827e6a4bb2ed8e96b9053375" STYLE="fork"/>
      <node TEXT="包装类" ID="a7d48ef44ea628241397822ab313bc06" STYLE="fork"/>
      <node TEXT="泛型" ID="6ec92edbd78dfefdf5c60f51012933bf" STYLE="fork">
        <node TEXT="泛型就是一个标签：&lt;数据类型&gt;" ID="bb454b810bd6d2a811b9e2b1c057f023" STYLE="fork">
          <node TEXT="可以约束在编译阶段只能操作某种数据类型" ID="97469c56a985b3bbb095476b56e3a093" STYLE="fork"/>
          <node TEXT="jdk1.7之后可以申明省略不写" ID="ce3795eea911b346b00f203dd65ac32d" STYLE="fork"/>
        </node>
        <node TEXT="类有继承关系，ArrayList没有继承关系，泛型没有继承关系" ID="556a0adfcecad0184e56eca454eaf31f" STYLE="fork"/>
        <node TEXT="泛型和集合只能支持引用数据类型，不支持基本数据类型" ID="29daa33820d41d70f4bf3638f2812d8b" STYLE="fork"/>
        <node TEXT="好处：在编译阶段约束只能操作某种数据类型，从而不会出现数据类型转换异常" ID="7ac546d13089ebfb80ffa69859ca731e" STYLE="fork">
          <node TEXT="泛型只在编译阶段有效，泛型信息不会进入到运行阶段，运行阶段不再能约束" ID="540d898bd6821fd32bc729e58c5c38cb" STYLE="fork"/>
        </node>
        <node TEXT="自定义泛型核心思想：把出现泛型变量的地方全部替换成传输的真实数据类型" ID="2e6048ed52838f810f09ddf2abc8a256" STYLE="fork"/>
        <node TEXT="通配符：？" ID="0ecb03fce8e596a38643f90bcb368d24" STYLE="fork">
          <node TEXT="再使用泛型时可以代表一切类型" ID="a3c574d51db1a9a307fd043d67f5939a" STYLE="fork"/>
          <node TEXT="泛型的上下限（约束通配符）" ID="16b61c4ea26776fd0d5d8b622fcb048d" STYLE="fork">
            <node TEXT="？ extends A 代表？只能代表A类及其子类（上限）" ID="50c624b40c2bac452a72a6938053b926" STYLE="fork"/>
            <node TEXT="？ super A 代表？只能代表A类及其父类（下限，用的少）" ID="fb724efea3473703cbcc71205ffba1da" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="集合" ID="6a2079324d5a185cda757a8e0fc07d29" STYLE="fork">
        <node TEXT="特点：大小不固定，类型不确定。集合有多种，不同场景用不同集合" ID="24a9f9a2358f9776d585413fe632de3d" STYLE="fork">
          <node TEXT="数组：定义之后类型和长度都固定了" ID="04de5f8f71eebfdcd5e41e69b321725c" STYLE="fork"/>
        </node>
        <node TEXT="集合重写了toString方法，输出集合内容" ID="42212a49ac1f32b531047d21252440c4" STYLE="fork"/>
        <node TEXT="collection集合（是接口）是所有集合的祖宗类" ID="001f0c1f20c90c988250461fecbd6509" STYLE="fork">
          <node TEXT="体系" ID="2da8521f7a723d89e0ea88ee19327316" STYLE="fork"/>
          <node TEXT="各个集合特点" ID="50658299c614c0e069f1db387eea6e3f" STYLE="fork">
            <node TEXT="ps：" ID="e5ee476ff9f8f47c6241a3e83ab2b651" STYLE="fork">
              <node TEXT="无序：不按添加的顺序排列 " ID="076f5b013be762f1e3e4f75935756ef8" STYLE="fork"/>
              <node TEXT="有序：按添加进去的顺序排列" ID="3262962fc039af724d1c6e9a05e59c63" STYLE="fork"/>
            </node>
            <node TEXT="Set接口：无序，不重复，无索引" ID="1641cc6aea6e4fa46da3e8b89b7efe04" STYLE="fork">
              <node TEXT="HashSet实现类：无序，不重复，无索引" ID="b3cb8283868493fd8bcd5608f2745c8d" STYLE="fork">
                <node TEXT="LinkedHashSet实现类：有序，不重复，无索引" ID="cc342b66aa4aecb1fef6f6b896800241" STYLE="fork"/>
              </node>
              <node TEXT="TreeSet实现类：不重复，无索引，默认升序排序" ID="d31cfb33be179c6bb2db4bcdfe5dbfd7" STYLE="fork"/>
            </node>
            <node TEXT="List接口：有序，可重复，有索引" ID="9612bb7c5879c068a77564d7c79ecc15" STYLE="fork">
              <node TEXT="LinkedList实现类：有序，可重复，有索引" ID="0f0077e81313a52782db1d8ac37d97d0" STYLE="fork"/>
              <node TEXT="ArrayList实现类：有序，可重复，有索引" ID="a9d80739520367afc96bbccda100e838" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="基本数据类型包装类" ID="337e0c85a76951495314a1e8559fde36" STYLE="fork">
        <node TEXT="short与long是数值型" ID="5adce3bfee34b684ec7112795f6faa69" STYLE="fork"/>
        <node TEXT="把字符串转换成基本数据类型的包装类" ID="2fd1343b91378e03cca63598130791ee" STYLE="fork">
          <node TEXT="调用需要转换的基本数据类型对应包装类的 parseXXX(String  s)；" ID="45422e4b402347cce97043247f8d27ed" STYLE="fork">
            <node TEXT="其中 XXX 表示基本类型，参数为可以转成基本类型的字符串" ID="6474e9791343e7ccc17b262c5cefafd0" STYLE="fork"/>
          </node>
          <node TEXT="必须保证传入字符串可以转换为基本数据类型,不能包含空格" ID="dd589915c8d947b290ff6abc559ecd0f" STYLE="fork"/>
        </node>
        <node TEXT="自动拆箱与封箱" ID="8d3852874472f174e193ce8361d745ae" STYLE="fork">
          <node TEXT="jdk1.5之后的特性" ID="be688cf80f817b3a83c576a0e86c8066" STYLE="fork"/>
          <node TEXT="自动封箱：在需要的时候，程序会自动将基本数据类型转换成对应的封装类型" ID="ce009a08296c1e0c6188455de888aeeb" STYLE="fork"/>
          <node TEXT="自动拆箱：在需要的时候，程序会自动将封装类型转换成对应的基本数据类型" ID="d8374522bfe9e3a32ba8b9cfdf4a552e" STYLE="fork"/>
        </node>
      </node>
    </node>
    <node TEXT="迭代器、数据结构、集合、collection工具类" ID="84e7922c05ce130d21abe9326d0f32fe" STYLE="bubble" POSITION="right">
      <node TEXT="collection集合遍历方式" ID="61d37c217b2191cf8ec8afed6401e373" STYLE="fork">
        <node TEXT="迭代器" ID="414d211ef941588f93e4efa219329209" STYLE="fork">
          <node TEXT="Iterator iterator =  集合名.iterator() ：获取集合对应的迭代器" ID="6ea0cef207d41edfa62452672e99dba6" STYLE="fork"/>
          <node TEXT="next：获取下一个元素，并将指针向后移一位" ID="03853c7c687c8667745ac44a1792e21a" STYLE="fork"/>
          <node TEXT="hasNext（）：判断有没有下一个元素，有则返回true，说明可以迭代" ID="49ad7d886d9ff6eb1a8711899a773657" STYLE="fork"/>
          <node TEXT="数组没有迭代器" ID="a4573a088df62ce589b07f599cd470e0" STYLE="fork"/>
        </node>
        <node TEXT="foreach（增强for循环）" ID="592af5773ce070be980ae73d5ac873f8" STYLE="fork">
          <node TEXT="可以遍历集合和数组" ID="40c4c85e3f40675c4ba5ef4b2eefb237" STYLE="fork"/>
          <node TEXT="缺点：" ID="d669a851f7db16f5e2e7677c335c8485" STYLE="fork">
            <node TEXT="没有索引，不知道遍历到了哪个元素，只能从头遍历到尾" ID="56615f7032176e9f0cc2b1457eddfd3b" STYLE="fork"/>
          </node>
          <node TEXT="用for循环遍历需要有索引，而foreach不需要索引" ID="6b802c7e6f4ae4f9c2c0dbef1e860c74" STYLE="fork"/>
        </node>
        <node TEXT="lambda表达式（jdk1.8之后有的）" ID="28e9a0ed706ead9a6178902d5c89aa71" STYLE="fork">
          <node TEXT="为简单的循环简化代码" ID="de6a45f312b6c437b3734cacc62d49e1" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="数据结构（面试热点）" ID="6b15107003101e5250cb641d2d242926" STYLE="fork">
        <node TEXT="集合的底层" ID="cb71bebdb4d64376644b8542b22d23ae" STYLE="fork">
          <node TEXT="常见的结构：栈，队列，数组，链表和红黑树" ID="491b1f344d361b9d4d401811551161d2" STYLE="fork"/>
          <node TEXT="栈（stack）" ID="3b4940663e46206adfa228b8ec87dfc2" STYLE="fork">
            <node TEXT="先进后出" ID="0ddebd223dc2672dea5ca94ee15ef77e" STYLE="fork"/>
            <node TEXT="压栈=如栈" ID="6ce8d508bfd0f354ea580719c3de3667" STYLE="fork"/>
            <node TEXT="弹栈=出栈" ID="a3b0df572a3bfc3fd1daef09bf5b1402" STYLE="fork"/>
          </node>
          <node TEXT="队列（queue）" ID="a03e831dbb0a7caadac71ef5562a8ac8" STYLE="fork">
            <node TEXT="先进先出" ID="24140481af8f6520c513a67fd03a8120" STYLE="fork"/>
          </node>
          <node TEXT="数组" ID="9996ecb1ed0f50dc7cc4f14a538e1ef1" STYLE="fork">
            <node TEXT="元素有索引" ID="204f1245c5dfd51b9514629bf265c544" STYLE="fork"/>
            <node TEXT="查询元素快（根据索引计算目标地址），增删元素慢（创建新数组，迁移元素）" ID="a1b08870f920d2ba03d35a488efe8cab" STYLE="fork"/>
          </node>
          <node TEXT="链表" ID="e234aedf8392cd66c9b3e908fde5bc89" STYLE="fork">
            <node TEXT="元素游离存储" ID="b883f4fec6a7a526c315fb0325f9909f" STYLE="fork"/>
            <node TEXT="查询速度慢，增删速度快" ID="8be764458206242e11aaa3e94442465b" STYLE="fork"/>
            <node TEXT="java大多是双链表，首尾定位速度最快" ID="1bbc7cb9b502814c034db9e94cc8d1b0" STYLE="fork"/>
          </node>
          <node TEXT="红黑树（平衡二叉B树，平衡的二叉查找树）" ID="b40f6c84322e00173bfd2925df70c701" STYLE="fork">
            <node TEXT="每个节点是红色或黑色" ID="4fddacc9b7d894fa689b4f23180f0d82" STYLE="fork"/>
            <node TEXT="根节点必须是黑色，叶子节点必须是黑色" ID="6ff9ddfae5522fe782b887d99818d545" STYLE="fork"/>
            <node TEXT="如果某节点是红色，则其子节点必须是黑色（不能出现两个红色节点相连）" ID="e467725cf127f7616e6fbffe93b8adbd" STYLE="fork"/>
            <node TEXT="对于每个黑色节点，从该节点到其所有后代节点的路径上，都包含有相同数量的黑色节点" ID="d2bd6a890e824fc1589c61b13ca71338" STYLE="fork"/>
            <node TEXT="增删改查性能都高" ID="68a92137a23b6408db8aee982e231693" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="集合" ID="0e5197f94600c181a5278b8e98f3b7ec" STYLE="fork">
        <node TEXT="List接口" ID="ab62772ea870bdfc3748e57bf93b7796" STYLE="fork">
          <node TEXT="元素有序，可重复，有索引" ID="78020433091fe6d4505f079f9a60aa45" STYLE="fork"/>
          <node TEXT="多一种遍历方式：for循环，因为相比Collection集合多了索引" ID="b7c506f0dc2cc44ff0e48fbe7560943c" STYLE="fork"/>
          <node TEXT="ArrayList" ID="5644c04c1c2acb7bf667ead706610174" STYLE="fork">
            <node TEXT="重写了toString方法" ID="25b8733b38097aa142a145d39c11c23a" STYLE="fork"/>
            <node TEXT="有索引，基于数组存储，查询快，增删慢" ID="4c56f70e9caa3200ac71d6eb8a0965ba" STYLE="fork"/>
            <node TEXT="" ID="5cb2edb3ae79318c7cb1f80efe2b72ed" STYLE="fork"/>
          </node>
          <node TEXT="LinkedList" ID="45be5bb288f91cdda4716e995e918407" STYLE="fork">
            <node TEXT="有索引，基于链表，支持双链表，查询慢，增删快" ID="973cd9f22b995608a6489abf18e5b37c" STYLE="fork"/>
            <node TEXT="独有的API可以用来做栈和队列" ID="208800da0efc8a3a7b6092d426fd0cb9" STYLE="fork"/>
            <node TEXT="" ID="0c36e637e92ad1c548f63e3b4ed22153" STYLE="fork"/>
          </node>
          <node TEXT="subList（）：获取指定集合指定范围的子集合，子集合的元素会随指定集合的元素变化而变化" ID="44ff26242984efbc7407d243c92efc9b" STYLE="fork"/>
        </node>
        <node TEXT="Set接口" ID="20f14df1a5ad27364546ce0d2e707be2" STYLE="fork">
          <node TEXT="元素无序，不重复，无索引" ID="051178a9912244f47430f89bbb2243a7" STYLE="fork"/>
          <node TEXT="判断是否重复" ID="b61d085b9462522fc23846ac4b70849c" STYLE="fork">
            <node TEXT="重写hashCode方法和equals方法，类中右键重写" ID="2c6f01fd3b911d177028f5f697e27ef6" STYLE="fork"/>
            <node TEXT="equals：只要对象内容相同就认定相同返回true" ID="9a7d3e0ea25ed5dee9c27a7ca10a41e9" STYLE="fork"/>
          </node>
          <node TEXT="Set系列集合添加元素无序的原因（面试重点）" ID="f9a0c194470842294999c2f35a57f98c" STYLE="fork">
            <node TEXT="底层采用了哈系表存储元素，采用这种结构导致插入的元素无序" ID="09052a5cc469e25b61f2257be0abc6b0" STYLE="fork">
              <node TEXT="哈希表：哈希表的增删改查性能都很好" ID="834ff9f2f0081702b57c41e3943e810f" STYLE="fork">
                <node TEXT="jdk1.8 之前：哈希表 = 数组 + 链表 + 哈希算法" ID="146be070082a3cd1943a0c67b7bb0e24" STYLE="fork"/>
                <node TEXT="jdk1.8 之后：哈希表 = 数组 + 链表 + 红黑树 + 哈希算法" ID="bbbaf7f4f34d8daee0c87cfbc8efbc1e" STYLE="fork">
                  <node TEXT="加了红黑树之后，链表过长就会转换成红黑树，增加效率和性能" ID="baab232f691e3d02db8463c62e270bfb" STYLE="fork"/>
                </node>
              </node>
            </node>
          </node>
          <node TEXT="HashSet集合" ID="85b3ee000bb2542972eb9362bce60804" STYLE="fork">
            <node TEXT="完全继承Set集合：元素无序，不重复，无索引" ID="b1b1de42cac0b6722d5699a66ee7869b" STYLE="fork"/>
          </node>
          <node TEXT="LinkedHashSet集合" ID="6d8aa78b280fc10fbead37543803afd8" STYLE="fork">
            <node TEXT="有序，不重复，无索引" ID="a6c46f29f4810dca7868de31b6d3884b" STYLE="fork"/>
            <node TEXT="底层依旧是使用哈希表存储元素，但是每个元素会额外带一个链来维护添加的数据" ID="0c41c9068df96eab8aecf31ba827e2b0" STYLE="fork"/>
            <node TEXT="缺点：" ID="64f52c2b9c3ce35bea829ca0ef2e1054" STYLE="fork">
              <node TEXT="多的存储顺序的链会额外占空间，" ID="8eee9127e63cd2c2e5f94ea4569291d8" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="TreeSet集合（可排序集合）" ID="c0d204799cbdaa9647dc4ccaf06e815e" STYLE="fork">
            <node TEXT="不重复，无索引，默认升序排序" ID="75d684a343d10cf99c53bffcb7597de7" STYLE="fork"/>
            <node TEXT="自排序方式" ID="4594f0bc04562b1c9be43195d98c1617" STYLE="fork">
              <node TEXT="1. 有值特性（整型、double等）的直接按照值大小排序" ID="7e09341cdbea349c7a538d4dc6eb7201" STYLE="fork"/>
              <node TEXT="2. 字符串根据 ASCII表逐个对比排序" ID="615b0656794bce4b669c887a0ece8470" STYLE="fork"/>
              <node TEXT="3. 自定义的引用数据类型无法直接默认排序" ID="1b579e7c96443ed0190fc3dde58c8347" STYLE="fork"/>
            </node>
            <node TEXT="自定义引用类型的排序实现" ID="a763ed9a12fdfb7a7aeddc7e058ae0c2" STYLE="fork">
              <node TEXT="1. 让对象的类实现比较器规则接口Comparable，重写比较方法" ID="d19534d646b4cfea4d753da51704fa8e" STYLE="fork">
                <node TEXT="默认会调用对比，this是比较者，o是被比较者" ID="3c95dd6db8683e5dbe1f074d5ccc9434" STYLE="fork"/>
              </node>
              <node TEXT="2. 直接使用集合自带的比较器对象（添加元素时比较器自动对比）" ID="22142a7429b502053407e985d6bd9264" STYLE="fork"/>
              <node TEXT="如果类和集合都带有比较规则，则按照集合自带的比较器规则" ID="5500aec70055e5e4ecfeaffd9e1c5b63" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="总结" ID="2231e8fa7b8e4dfdcb1d5c2beed987ec" STYLE="fork">
          <node TEXT="需要元素可以重复，有索引，查询快-------用ArrayList集合（用的最多）" ID="ea7bf1b9da1d9d410925cad899454b7f" STYLE="fork"/>
          <node TEXT="可以重复，有索引，增删快--------------------用LinkedList集合（适用元素比较少的查询，经常操作首尾元素的情况）" ID="c95ce5355222535ab46fb39385962f91" STYLE="fork"/>
          <node TEXT="不重复、无序、无索引，增删改查快-------用HashList集合" ID="835adba16d2fdb992a8a7615174c6065" STYLE="fork"/>
          <node TEXT="不重复、无索引，增删改查快且有序-------用LinkedHashList集合 " ID="b50b49a78323b7895b182e9fbe2c78cb" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="collections操作集合的工具类" ID="97f6bc0660613d95611a6687418d1e31" STYLE="fork">
        <node TEXT="都是static方法" ID="11a2d29a358d0c116f6ceaf7bdc0bf14" STYLE="fork"/>
        <node TEXT="addAll（）：批量加入元素，可以加入0或多个元素" ID="04caf420f1ed6de2bd2c808b882ef0f8" STYLE="fork"/>
        <node TEXT="shuffle（）：打乱元素顺序" ID="4c01013c4310b77a934ff1129c59156f" STYLE="fork">
          <node TEXT="只能打乱List集合，Set集合位置是计算出固定的。" ID="f08bb3358374fb60c15faafb7288c874" STYLE="fork"/>
        </node>
        <node TEXT="sort（）：排序" ID="d3c155943f60b49432f4762926a80298" STYLE="fork">
          <node TEXT="只能对List集合排序，原理同上" ID="b79fb33a004c3709b392adc253bd17c4" STYLE="fork"/>
          <node TEXT="不能默认只能排序简单类型，不能排序自定义类型" ID="b7013d47f5432ff5568728400f837074" STYLE="fork">
            <node TEXT="需要定义比较方法，定义的位置" ID="91cbc8eff556a4a7e3510a04044644d9" STYLE="fork">
              <node TEXT="1. 可以用类实现Comparable&lt;类类型&gt;接口，并且重写compareTo方法" ID="3f74c9faa0f77adf0f164c5f8db693e9" STYLE="fork"/>
              <node TEXT="2. 在sort方法的第二个参数new一个Comparator对象，并重写compare方法" ID="d08cc0a44deb2f2cd70bd4c72b1b4793" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="可变参数" ID="2ef2b4f496c230030f6dbf781d314ee4" STYLE="fork">
        <node TEXT="定义格式：方法名（类型...形参名）" ID="a7e80311c09f5aab6e1a88ec1cb0a9dd" STYLE="fork"/>
        <node TEXT="可以使方法接收0个或多个参数，数组也可以" ID="4d213d09ffd87cbe86212f0a06b6ea89" STYLE="fork"/>
        <node TEXT="可变参数在方法内部本质就是一个数组" ID="87712f6be47439061eb81dc465db41b3" STYLE="fork"/>
        <node TEXT="注意事项：" ID="ca8ca953f375c558703d248d4119d557" STYLE="fork">
          <node TEXT="1. 一个形参列表中，只能有一个可变参数" ID="5973226f265dfa856235a7a05d2d0784" STYLE="fork"/>
          <node TEXT="2. 可变参数只能放在形参列表的末尾" ID="0a3c083c20edf885dac984a19aefe7e3" STYLE="fork"/>
        </node>
      </node>
    </node>
    <node TEXT="Map集合，排序，查找" ID="37beb3905b17e3aaf74666edc1668369" STYLE="bubble" POSITION="right">
      <node TEXT="Map集合（祖宗类）" ID="04ed12970364ad097cbb192857678b04" STYLE="fork">
        <node TEXT="键值对集合" ID="3206505ad7d958effe13e3e87ecb4818" STYLE="fork"/>
        <node TEXT="集合体系" ID="2f2223b849a23ef52418dcd6ce6d73e5" STYLE="fork">
          <node TEXT="底层和Set集合是一样的" ID="98315c60db78001090baec5517a57be9" STYLE="fork"/>
        </node>
        <node TEXT="Map集合特点" ID="59c2f84c4f718fe7d9341b2a327da012" STYLE="fork">
          <node TEXT="特点由键决定" ID="3b329dc4a8516fe64378d85c812ce26c" STYLE="fork"/>
          <node TEXT="无序、不重复、无索引" ID="9e37fc488ae8af51da0f7e9e4d3483b3" STYLE="fork">
            <node TEXT="重复键的值，后面的会覆盖前面的" ID="1e7fd1215466a214880063133b5dd4a5" STYLE="fork"/>
          </node>
          <node TEXT="对值无要求" ID="33d0f5384495e8139d7136b229601c47" STYLE="fork"/>
          <node TEXT="键值都可以为null" ID="f809d35d81a1044d0e204d3a863d4bc8" STYLE="fork"/>
          <node TEXT="键值都可以存储自定义类型" ID="3de52c3fdd7e0d7ee1faf44549970f8c" STYLE="fork"/>
        </node>
        <node TEXT="HashMap集合：无序、不重复、无索引（与Map集合一致）" ID="edf8e6e66fa318957e4cd376ca27d428" STYLE="fork">
          <node TEXT="实现类，经典，用的最多" ID="443e7e966ed420e212432e98fbd5d525" STYLE="fork"/>
          <node TEXT="HashSet的底层是HashMap，只是只保留键" ID="079a1408be13c67cd6df249f84702a8a" STYLE="fork"/>
        </node>
        <node TEXT="LinkedHashMap集合：有序、不重复、无索引，值无要求" ID="5a10196418191e9ab573d8013c11e7bb" STYLE="fork">
          <node TEXT="LinkedHashSet集合是LinkedHashMap集合去除值只保留键" ID="2da8c5c77d9c57a536765a9a86ea9808" STYLE="fork"/>
        </node>
        <node TEXT="TreeMap集合" ID="3da591fbc48839bcd48cd86774a96dfe" STYLE="fork">
          <node TEXT="TreeSet集合底层基于TreeMap集合" ID="7bfcc941f57fb36e9ffb46ff49d22c6a" STYLE="fork"/>
          <node TEXT="可排序（默认升序），不重复" ID="565ab1173358988985882c9810dff13e" STYLE="fork">
            <node TEXT="自动删除重复的键值对，相同的键，后面的值会覆盖前面的" ID="84a6740c08a174860439dab0ce499bc6" STYLE="fork"/>
          </node>
          <node TEXT="按照键排序，特点与TreeSet集合相同" ID="23906eadfdefe2b1db926ccf9b0f4201" STYLE="fork"/>
        </node>
        <node TEXT="遍历方式" ID="b00dbcb21025769d10727e5e21a7dce2" STYLE="fork">
          <node TEXT="1. 键找值的方式（面向过程）" ID="61f91720faf7abb99e609fb5f64e827b" STYLE="fork">
            <node TEXT="先获取键的集合" ID="cfd86a25da8ea4f3309bab300a97cc9f" STYLE="fork"/>
            <node TEXT="在根据键循环获取值" ID="3709f1b1666a5c66437abd764714d0ba" STYLE="fork"/>
          </node>
          <node TEXT="2. 键值对 的方式遍历（更面向对象）" ID="a0cd73f019fa85af176b51fe1a42c07d" STYLE="fork">
            <node TEXT="将Map集合转换成Set集合：Set&lt;Map.Entry&lt;K类型, V类型&gt;&gt; entries = Map集合.entrySet();" ID="8d160f84308de6991b5a19c9fbb2cd05" STYLE="fork"/>
            <node TEXT="Set存储的就是Map键值对的实体类型" ID="714c1324bdde94c995887c379b771438" STYLE="fork"/>
            <node TEXT="通过foreach遍历" ID="a99ec5611494d0a329451fc5fe85996a" STYLE="fork"/>
            <node TEXT="分别取键和值" ID="66d24e66cb8eb557a1a8c46aaa933fc2" STYLE="fork"/>
          </node>
          <node TEXT="3. lambda表达式（jdk1.8之后）" ID="049ab8a98e2204eb433d48b751974afd" STYLE="fork">
            <node TEXT="Map集合名.forEach((k,v)-&gt;{ System.out.println(k+&quot;=&quot;+v); });" ID="ee4bf6b266a47a34ee726d99a0f6d162" STYLE="fork"/>
            <node TEXT="拓展：jdk1.8是TSC（Long Time Support 长久支持版），下一个LTS版本是jdk11" ID="39cfe8189344bfeb39900af00054ea09" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="冒泡排序" ID="f10a8af55fea33de7f9515f7b0c734b1" STYLE="fork">
        <node TEXT="循环位置减1细节，计算机循环：小于几就循环几次（从0开始遍历）" ID="f2530c46ab7a40735b4e37cabb51aa73" STYLE="fork"/>
      </node>
      <node TEXT="选择排序" ID="841b836333a3b250b28ebbd3dcab1c99" STYLE="fork"/>
      <node TEXT="二分查找" ID="5f20572eb9574e19bf8b10cf05441af7" STYLE="fork"/>
    </node>
    <node TEXT="异常、线程" ID="4870c2a37fa28e7b6fee0389e580549d" STYLE="bubble" POSITION="left">
      <node TEXT="异常" ID="8eafad92ac04ed418cbbcd949e54d85b" STYLE="fork">
        <node TEXT="体系结构" ID="f44d74529fdb95d240157884a3930a67" STYLE="fork">
          <node TEXT="Throwable（根类）" ID="181925b16ce63cdafdc08502758776d6" STYLE="fork">
            <node TEXT="Error：错误，不是程序员引起，系统级错误，例如：内存崩溃等" ID="a627aa9fb4cc826c265fb63b3a71f59d" STYLE="fork"/>
            <node TEXT="Exception：异常，需要研究和解决" ID="f9ba6fe645c8aa1269e7934e242dd03a" STYLE="fork">
              <node TEXT="编译时异常" ID="0e46a04dfa2ac862871abfa2cb9c8d5f" STYLE="fork">
                <node TEXT="作用：提醒程序员这里可能出现bug，注意检查" ID="72e29688ceb8795aa9360ef45785c7ba" STYLE="fork"/>
              </node>
              <node TEXT="RuntimeException：运行时异常" ID="c108d10645526f215ff3e373e1be76d1" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="异常产生和处理的默认机制（出现异常，程序直接挂掉）" ID="f4ea4d494a8c12dc53f91f433d407867" STYLE="fork">
          <node TEXT="1.  在出现异常的代码部分创建一个对应类型的异常对象" ID="f6772755bf446af1c1aba9de3b8d5757" STYLE="fork"/>
          <node TEXT="2. 异常从出现的地方抛给调用者，调用者最终抛给JVM虚拟机" ID="2da1abe9c98439962382710b76fbbc0b" STYLE="fork"/>
          <node TEXT="3. 虚拟机接收异常后，在控制台输出异常栈信息" ID="23c834895e4514c14caf15ab398ddcc3" STYLE="fork"/>
          <node TEXT="4. 在处在异常的代码位置干掉程序，后续代码不再执行" ID="e509032b9d7863243e530a9973079382" STYLE="fork"/>
        </node>
        <node TEXT="编译时异常" ID="68543d8473b4934941b2c4e4bf3ae6c4" STYLE="fork">
          <node TEXT="抛出异常：throws " ID="bc357621557f1b0feb104c19f88da9a8" STYLE="fork">
            <node TEXT="建议抛 Exception，可以接所有异常" ID="019926eb25d52c23fae3ca550b4fa6b0" STYLE="fork"/>
            <node TEXT="可以解决编译时错误，但是运行时有异常还是会干掉程序" ID="c0fab133d33cfb00e5531bc596070607" STYLE="fork"/>
          </node>
          <node TEXT="捕获异常：try{……}catch（异常类型，可以用|叠加，建议用Exception ）{……}" ID="b4e5acc53b60f6dfcb0442a8fbb452d2" STYLE="fork"/>
          <node TEXT="底层把异常一层层抛给上层，最上层捕获异常" ID="956015af11e60805a6024a0941689716" STYLE="fork"/>
        </node>
        <node TEXT="运行时异常" ID="9910e38199c72e68bcafc71be70950a9" STYLE="fork">
          <node TEXT="异常部分（底层）自动抛出，只需要在最外层捕获异常即可" ID="9d91c05f7410356327139b9ef82bb756" STYLE="fork"/>
          <node TEXT="常见异常" ID="2c0OKo0kz3" STYLE="fork">
            <node TEXT="索引越界异常:IndexOutOfBoundsException" ID="CwOG8NRDVq" STYLE="fork"/>
            <node TEXT="数组索引越界异常:ArrayIndexOutOfBoundsException" ID="HjgflfT1xQ" STYLE="fork"/>
            <node TEXT="字符串索引越界异常:StringIndexOutOfBoundsException" ID="1RdTZzZSuw" STYLE="fork"/>
            <node TEXT="空指针异常: NullPointerException" ID="RPnWOJqCPX" STYLE="fork"/>
            <node TEXT="算术异常:ArithmeticException*/" ID="ZDpDl3fQn9" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="finally关键字" ID="a6c99b1a9557262d1d4bc38cf915c465" STYLE="fork">
          <node TEXT="只能出现1次或不写" ID="4f0b7196874acda556ff93294c5b1159" STYLE="fork"/>
          <node TEXT="写了finally可以不写catch" ID="4f80741782dddcb7f88a531096885847" STYLE="fork"/>
          <node TEXT="不管有没有异常，都一定执行finally内的代码" ID="28353c19504e7a51902845b3e2885446" STYLE="fork">
            <node TEXT="finally中的return会覆盖前面所有的return" ID="d3b004b0af503b5889258defa9a32e16" STYLE="fork"/>
          </node>
          <node TEXT="作用：用于资源的回收操作" ID="a8ed3f963597fcce4488ca6bfd94acf7" STYLE="fork"/>
        </node>
        <node TEXT="注意事项" ID="03053f813f0fc65414ec9abd2a3577c6" STYLE="fork">
          <node TEXT="1. 子类重写方法的异常应该与父类被重写方法的异常一样或者范围更小" ID="4667c3f541b85b80cce201bae25c4fe2" STYLE="fork"/>
          <node TEXT="2. 方法都默认抛出运行时异常，因此运行时异常底层自动抛出，只需要在最外层捕获" ID="39d0a5c3daf86de6144bac800def7ab6" STYLE="fork"/>
          <node TEXT="3. 当捕获多个异常时（多个catch），前面捕获的异常类不能是后面捕获的异常类的父类" ID="8152bdae89b8eadb32aaeca67dd54382" STYLE="fork"/>
        </node>
        <node TEXT="自定义异常类型" ID="322a0245d3c7206dba36b0048b0ba51d" STYLE="fork">
          <node TEXT="自定义编译时类型" ID="bb73ad548e07e66df63c2265e32090a0" STYLE="fork">
            <node TEXT="1. 继承Exception类" ID="a414fdecaa6a1166e1fadfe36326bde8" STYLE="fork"/>
            <node TEXT="2. 重写所有构造器" ID="8709e93ce0affae43a5d7d4bf4f5d026" STYLE="fork"/>
            <node TEXT="3. 在异常的位置使用 throw new 将自定义对象抛出" ID="770e7d31d42e7d70974428d19fd9d5fb" STYLE="fork"/>
          </node>
          <node TEXT="自定义运行时异常" ID="6ae59d004fef8038233d35a9fdd0fefa" STYLE="fork">
            <node TEXT="1. 继承RuntimeException类" ID="22be57fb84a5f9605c21e7121bc650aa" STYLE="fork"/>
            <node TEXT="2. 重写所有构造器" ID="18baa7e2c6e6a63adf6e44c543427add" STYLE="fork"/>
            <node TEXT="3. 在异常的位置使用 throw new 将自定义对象抛出" ID="65ade537127942ff14060b2ea0e00766" STYLE="fork"/>
          </node>
          <node TEXT="throw：用在异常出现的地方，创建异常对象并立即从此处抛出" ID="8fa41f26f1b8823883ec74f08bc19b17" STYLE="fork"/>
          <node TEXT="thwors：用在方法处，用于抛出异常" ID="a927e2a174eeb9eeac7d3f37920b164b" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="多线程（并发编程，JUC）" ID="036bd4f6f6daba4ccbd7dd0d37266932" STYLE="fork">
        <node TEXT="进程" ID="3ef99d408cb6986c93fcbc91bcaf8fa6" STYLE="fork">
          <node TEXT="程序时静止的，运行中的程序就是进程" ID="65dc23f05fd9f0c5930fbb9f0266375b" STYLE="fork"/>
          <node TEXT="进程的特征：" ID="4c2f5d298f544cf931c5e6822b9fd391" STYLE="fork">
            <node TEXT="动态性：动态的占用资源" ID="060d5a3e27f2ba4c767b3c824931d5d3" STYLE="fork"/>
            <node TEXT="独立性：多个进程之间时相互独立的" ID="37465a8d4c5e9513ad1b829c57d43efe" STYLE="fork"/>
            <node TEXT="并发性：多个进程在单核之间快速来回切换" ID="92bf3cef6510daaf4982aac59af994d3" STYLE="fork">
              <node TEXT="并行：多个进程同一时刻同时进行（多核CPU）" ID="d87d95c5abff75374206199baa68b683" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="线程" ID="bb6f702c248557ff78351ddd935ed259" STYLE="fork">
          <node TEXT="作用" ID="a92ede60709aa07b62c9e87b5bcf636c" STYLE="fork">
            <node TEXT="提高程序效率，多线程有更多的机会得到CPU资源" ID="ff6d57c4eb25b6a6f7d4da27d8b876b0" STYLE="fork"/>
            <node TEXT="解决很多任务模型" ID="28aaace2b21af87b5e77f52d5204a035" STYLE="fork"/>
            <node TEXT="大型高并发技术" ID="bebb35f864a04a9f503a5511ba20851b" STYLE="fork"/>
          </node>
          <node TEXT="创建方法" ID="87ee4b7a894aa68ea61e72979a369198" STYLE="fork">
            <node TEXT="1. 自定义线程类继承Thread类，重写run方法，调用对象的start方法起点线程" ID="1df1a65440a42c02eebc2a7bef5c518e" STYLE="fork">
              <node TEXT="优点：代码简单" ID="e5c5203fc8c2035756d379ab3753ab9c" STYLE="fork"/>
              <node TEXT="缺点：继承了Thread类，不能再继承别的类（单继承，多接口）" ID="c732f0f7d905757ea644a7607aa2a6f0" STYLE="fork"/>
            </node>
            <node TEXT="2. 自定义线程任务类实现Runnable接口" ID="24f9db90004c497459d2dce0683707c0" STYLE="fork">
              <node TEXT="重写run方法" ID="3ceb0418595c113916e898c335122723" STYLE="fork"/>
              <node TEXT="创建线程任务类的对象" ID="072c329939a2f101965ef09796ca1d36" STYLE="fork"/>
              <node TEXT="把线程任务类对象封装成线程对象" ID="e55f2a011573f31a45232cfbc6e70e34" STYLE="fork">
                <node TEXT="Thread的构造器" ID="d583b0c9875e3f40d5da33925e8386d5" STYLE="fork"/>
              </node>
              <node TEXT="调用线程对象的start方法启动线程" ID="837d3bd6c58d4497cbdb19cf746a58b2" STYLE="fork"/>
              <node TEXT="优点：" ID="32952a079ac0131ca0933e2fa1b1d1dc" STYLE="fork">
                <node TEXT="避免单继承的局限性" ID="2c746d15d71bc0eada3902a23bb0b0e5" STYLE="fork"/>
                <node TEXT="一个线程线程任务对象可以封装成多个线程对象，用于多个线程共享一个资源" ID="29b9e7ba2401e4e3d7fe6ec553808c4f" STYLE="fork"/>
                <node TEXT="解耦操作，线程任务代码可以被多个线程共享，线程任务代码和线程独立" ID="134b99fd2cc4d7547ecaa9ae803240f1" STYLE="fork"/>
              </node>
              <node TEXT="匿名内部类写法" ID="a709e12c59cf9d9a7fe0035e4fb9f2af" STYLE="fork">
                <node TEXT="把线程任务类对象直接在封装过程中创建" ID="aa5f7a0e04a5e3dc419d45ecd1489933" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="3. 实现Callable接口" ID="c38b806a857d2e0caa255c5d45500d99" STYLE="fork">
              <node TEXT="（1）创建线程任务类实现Callable接口，通过泛型申明返回值的类型" ID="3133c6a78e2210cde8d85a931e3f5e01" STYLE="fork"/>
              <node TEXT="（2）重写线程任务类的call方法" ID="8cebc24572a5c3b49e19a78230556f80" STYLE="fork"/>
              <node TEXT="（3）创建Callable的线程任务类对象（多态）" ID="889d275de6ab6a23c4c7cbf920990b75" STYLE="fork"/>
              <node TEXT="（4）把Callable任务类对象封装成未来任务对象" ID="97441be8346a209824bed61ef1212a91" STYLE="fork">
                <node TEXT="未来任务对象" ID="2a2e7336bdb92f8963fbfcd712e8a791" STYLE="fork">
                  <node TEXT="Runnable接口的子类，可以被包装成线程对象" ID="07cfddfc50ed0e70f7308bce02e989cb" STYLE="fork"/>
                  <node TEXT="未来任务对象可以在线程结束后，得到线程执行的结果（call方法）" ID="e4a04e1c86f75e5e881a8b301eb65beb" STYLE="fork"/>
                </node>
              </node>
              <node TEXT="（5）把未来任务对象包装成线程对象" ID="921dd021ce80de4914c871f4ba2773cc" STYLE="fork"/>
              <node TEXT="（6）启动线程" ID="0a4b0ca73a52592264f7b381f7df8315" STYLE="fork"/>
              <node TEXT="通过未来任务对象的get方法获取线程执行的结果" ID="1030c8dff7fde10e2ce40b7fb9c7d2b2" STYLE="fork">
                <node TEXT="可能有异常结果，所以要try…catch…" ID="ebecce74c29ee984e4482f2664cdd61c" STYLE="fork"/>
                <node TEXT="如果在获取线程执行结果时，线程还没有执行完，则会让出CPU资源，让线程先执行" ID="f8b23b13bd73d423b052ef819f8e04f0" STYLE="fork"/>
              </node>
              <node TEXT="优点很多" ID="bceab48768183856feb81cda68f8f27c" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="main方法由主线程执行，可以理解为主线程" ID="f643766878f9ecd9c319f4817dac4efb" STYLE="fork"/>
          <node TEXT="注意事项" ID="41c1b0439f30fe46badff5b123ea1acb" STYLE="fork">
            <node TEXT="启动线程必须用线程对象的start方法，直接用run方法不候启动线程而是调用主线程执行此方法。start方法的底层是将子线程注册到CPU。" ID="e4ee032f23272c41e37ef9f27bf2fbdd" STYLE="fork"/>
            <node TEXT="建议先创建子线程，主线程人物放到后面，否则主线程总是先执行完的" ID="200eada063a0591761330c6a23a5cabc" STYLE="fork"/>
          </node>
          <node TEXT="Thread类的API" ID="781f674d4ed748208271fe84886f68ed" STYLE="fork">
            <node TEXT="注意方法的实例还是静态，调用者不同" ID="18e61e20e60cb48bc5b2dd66e99b16a0" STYLE="fork"/>
            <node TEXT="线程睡眠：sleep方法，形参是毫秒 " ID="cfa1e097aeafc303473806b854a3aea9" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="线程调度" ID="5d5f4835492494b9245e0d170d98498b" STYLE="fork">
          <node TEXT="分时调度" ID="309d602097eb6a7659a63e4d71b9346a" STYLE="fork">
            <node TEXT="所有线程轮流使用 CPU 的使用权，平均分配每个线程占用 CPU 的时间。" ID="3ef995b4c82c9d60aad9d490836b9a86" STYLE="fork"/>
          </node>
          <node TEXT="抢占式调度" ID="b3026c9fb1684f898cb71e651a890d11" STYLE="fork">
            <node TEXT="优先让优先级高的线程使用 CPU，如果线程的优先级相同，那么会随机选择一个(线程随机性)，Java使用的为抢占式调度。" ID="ab227a92a0ad315de66de9a0976b62d4" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="线程安全" ID="9b1487fd273c27769581ac30346bdd3d" STYLE="fork">
          <node TEXT="线程同步（上锁）的方式" ID="c174711e9faee0e55cc416030ead5348" STYLE="fork"/>
          <node TEXT="1. 同步代码块" ID="18ae981e221d9a7c52f40842f9c0c42c" STYLE="fork">
            <node TEXT="格式：synchronized（锁对象） { 共享资源的核心代码 }" ID="2cb02908eedf8e890f61e50d5ae0a695" STYLE="fork"/>
            <node TEXT="锁对象：“唯一”的资源对象" ID="67e241edc59f9ed1e6b304d47887a716" STYLE="fork">
              <node TEXT="实例方法中，使用 this 作为锁对象，此时 this 正是共享资源" ID="16d1432ef51a8ecc68f83413686113dd" STYLE="fork"/>
              <node TEXT="静态方法中，使用 类名.class 作为锁对象" ID="47581f03bab95bc27e5d8cd6af5afc41" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="2. 同步方法" ID="bb2d5a99ea5de0e0c9a811e98d191633" STYLE="fork">
            <node TEXT="给方法上锁，加上synchronized 关键字" ID="4e142f2882bb85ede4296433bfd882cf" STYLE="fork">
              <node TEXT="synchronized：排他（互斥）锁机制" ID="365357da853930811a668011879a7bd4" STYLE="fork"/>
            </node>
            <node TEXT="同一时刻，只有一个线程能访问该方法的资源" ID="6066787d3736991083f3a64cf8da8c92" STYLE="fork"/>
          </node>
          <node TEXT="3. lock显示锁" ID="c888de77d673e0d36bcd1a43bc3813a5" STYLE="fork">
            <node TEXT="同步锁" ID="758ee588c8807fda02e0cbe38e6e1674" STYLE="fork"/>
            <node TEXT="加锁：public void lock（）" ID="2a3946fb1ea718043157e5ab0a006e25" STYLE="fork"/>
            <node TEXT="释放锁：public void unlock（）" ID="0cd4a43d4f8bc8057c3bbda3f1ad990c" STYLE="fork"/>
          </node>
          <node TEXT="总结：" ID="23ac8ccbbb3537f50ba467330a372264" STYLE="fork">
            <node TEXT="线程安全，性能差（加锁导致性能差）" ID="2e73fe7d927b96c7651a1c4d88ce9e7d" STYLE="fork"/>
            <node TEXT="线程不安全性能好，不存在多线程安全时，使用不安全代码" ID="c74cd18588d41d61d7efafba15f1466b" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="线程通信" ID="e7ab6f43b2579c59a6c014d8c818fc2c" STYLE="fork">
          <node TEXT="注意事项" ID="2330f54b30dc94a7e5c7e12480caa3fd" STYLE="fork">
            <node TEXT="多线程访问同一资源时才需要线程通信" ID="d2180a6349c71c5f5ac83861ba84b5f1" STYLE="fork"/>
            <node TEXT="线程通信必须保证线程安全，否则没有意义" ID="9a70cda9993d70e23455c9d228734e25" STYLE="fork"/>
          </node>
          <node TEXT="核心方法，都必须由锁对象调用" ID="fdbe7414b73906fdf2686996e4529180" STYLE="fork">
            <node TEXT="public void wait（）：让当前线程进入等待状态，会把锁释放出去" ID="80ba7db348ccfcf16fc39181800476db" STYLE="fork"/>
            <node TEXT="public void notify（）：唤醒当前锁对象上某个等待状态的线程" ID="612d833341a8b4ba6dcdfb294f18d893" STYLE="fork"/>
            <node TEXT="public void notifyAll（）：唤醒当前锁对象上所有等待状态的线程" ID="d8b4fe1a372c5a261201274ae46c230b" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="线程状态（面试热点）" ID="9aeb9b948c8e2b47cbc3ca618420d272" STYLE="fork">
          <node TEXT="各个状态转换关系" ID="fe49e3d7a848699eb72137ee14109c2b" STYLE="fork"/>
          <node TEXT="状态" ID="14d9b887c930109ef4a28c682fabee5f" STYLE="fork">
            <node TEXT="NEW(新建)：线程刚被创建，但是并未启动。还没调用start方法。MyThread t = new MyThread只有线程对象，没有线程特征。" ID="cc8dedef6a7dc40cc1082b9408151057" STYLE="fork"/>
            <node TEXT="Runnable(可运行)：线程可以在java虚拟机中运行的状态，可能正在运行自己代码，也可能没有，这取决于操作系统处理器。调用了t.start()方法   ：就绪（经典教法）" ID="1b4180bac1dc65ed56ba50c80fc0981b" STYLE="fork"/>
            <node TEXT="Blocked(锁阻塞)：当一个线程试图获取一个对象锁，而该对象锁被其他的线程持有，则该线程进入Blocked状态；当该线程持有锁时，该线程将变成Runnable状态。" ID="1c69b58c54717b4f3ef39805e5b7d85f" STYLE="fork"/>
            <node TEXT="Waiting(无限等待)	：一个线程在等待另一个线程执行一个（唤醒）动作时，该线程进入Waiting状态。进入这个状态后是不能自动唤醒的，必须等待另一个线程调用notify或者notifyAll方法才能够唤醒。" ID="c192714df64d1ecbda8b8e0cdd234f59" STYLE="fork"/>
            <node TEXT="Timed Waiting(计时等待)：同waiting状态，有几个方法有超时参数，调用他们将进入Timed Waiting状态。这一状态将一直保持到超时期满或者接收到唤醒通知。带有超时参数的常用方法有Thread.sleep 、Object.wait。" ID="6052af3d3e3b9d3c634ab891b75dbc20" STYLE="fork"/>
            <node TEXT="Teminated(被终止)：因为run方法正常退出而死亡，或者因为没有捕获的异常终止了run方法而死亡。" ID="5c723f71a7ed30ded487d9b6c56ae2a7" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="线程池" ID="f38aa78d2fe786260b03024bc3269345" STYLE="fork">
          <node TEXT="作用" ID="09e8c22d84349909971390fdcd85ba30" STYLE="fork">
            <node TEXT="存储多个线程，其中的线程可以反复使用，提高管理性（可以约束最多能有多少线程，不会因为线程过多而死机）" ID="b61c9e0aacef881dc68a67c88188fa2d" STYLE="fork"/>
            <node TEXT="无需反复创建销毁线程，从而节省资源、提高响应速度" ID="11b3c2e3f55535c26c92f2acd19cd98a" STYLE="fork"/>
          </node>
          <node TEXT="常用方法" ID="d75fdce50e2fb00e05fcee6467d991b0" STYLE="fork">
            <node TEXT="线程池.submit（任务）" ID="68654910cf4e337ea7752089f466fa71" STYLE="fork">
              <node TEXT="提交任务" ID="89a0e3412aeb97d322e6a734cc777ea8" STYLE="fork">
                <node TEXT="Callable做线程池的任务可以得到线程执行的结果" ID="459da46ec5bcc85cf6817e3b4ff61437" STYLE="fork"/>
              </node>
              <node TEXT="如果线程池不满则创建新线程，线程池满则调用之前创建的线程来处理这个任务" ID="dfd544c7304db51bf06e7f1e5cf15efc" STYLE="fork"/>
              <node TEXT="返回值是未来对象类型" ID="72ae3af75d251022c3346facd63276e8" STYLE="fork"/>
            </node>
            <node TEXT="  void execute（Runnable command）" ID="b672833ae60de242adb8c031c84ee48e" STYLE="fork">
              <node TEXT="提交任务但是返回值为空" ID="b0dd6eb80226f444a31d4e609c1c876c" STYLE="fork"/>
              <node TEXT="方便抛出异常，但是无法捕捉错误，submit方法相反" ID="fb244c95ca83191a62f4c0d17c3fcc07" STYLE="fork"/>
            </node>
            <node TEXT="关闭线程池" ID="438a037dd7b00b66502a228ab17d08d2" STYLE="fork">
              <node TEXT="线程池.shotdown（）：任务都执行完成后，关闭线程池" ID="1269f16d9175c18539e2104d715d91f4" STYLE="fork"/>
              <node TEXT="线程池.shutdownNow（）：立即关闭线程池，无论任务是否执行完成" ID="0ebd1d9e5a77b8370c45978863866751" STYLE="fork"/>
            </node>
            <node TEXT="创建可以存储3个线程的线程池" ID="4cb0952a015a92f6dda8ab74b5f9880a" STYLE="fork">
              <node TEXT="ExecutorService pools = Executors.newFixedThreadPool(3);" ID="7b19438fada10f8534ea89543578963a" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="死锁（面试）" ID="2a55545290f6aeaa924878b9be460fbf" STYLE="fork">
          <node TEXT="产生条件（打破任一条件，死锁即可消失）" ID="aec3ed638cd46290824ada1cd23872e4" STYLE="fork">
            <node TEXT="资源互斥使用" ID="7b6c30495433fccf22d96cc0a1eaa046" STYLE="fork"/>
            <node TEXT="不可抢占资源" ID="a942ca247bab086e1114e67e9c6a4410" STYLE="fork"/>
            <node TEXT="请求和保持：在请求其他资源时，对原有资源保持占用" ID="12e50391c841d644dda8bcf3ec045771" STYLE="fork"/>
            <node TEXT="循环等待：存在循环等待队列（a等b的资源，b等a的资源）" ID="048065c46e8eba32fc89c81ec250b09b" STYLE="fork"/>
          </node>
          <node TEXT="形成死锁通常需要锁的嵌套" ID="4e723fdf73f2283d00aaeda7fd0a81a4" STYLE="fork"/>
        </node>
        <node TEXT="volatile关键字" ID="3443e6bfec84d095aa432f2ded7e8e8f" STYLE="fork">
          <node TEXT="只能修饰实例变量和类变量，不能修饰私有变量" ID="e55f014f6f658536405a8f539a61fbd6" STYLE="fork"/>
          <node TEXT="保证数据可见性，不保证原子性（线程安全问题）" ID="390dbda8b13339b136db1fdc8c26bf10" STYLE="fork"/>
          <node TEXT="并发编程中，多线程修改变量，会出现线程间变量的不可见性" ID="5b1b8ba8cbb607281fcb398d38166443" STYLE="fork">
            <node TEXT="不可见性：多线程访问共享变量，一个线程修改变量后，其他线程看不到最新的变量值" ID="bdfa2668df2d4e1ac9b3ef9c5dc73406" STYLE="fork"/>
          </node>
          <node TEXT="解决不可见性方法" ID="d7b6650163dfa5ed8fa1e7571dbeb500" STYLE="fork">
            <node TEXT="加锁" ID="3562182245e4bd03e19b3f64e690430c" STYLE="fork">
              <node TEXT="对访问不到最新变量值部分的代码加锁" ID="54d631c994d5bc80203dc26979a41214" STYLE="fork"/>
              <node TEXT="线程获得锁会清空工作内存，将主内存的最新变量值拷贝到工作内存" ID="4f6c656d17e6014432ef4689af6d0781" STYLE="fork"/>
            </node>
            <node TEXT="对共享的变量加 volatile 关键字" ID="775511f2e9a310dbd1d13f13febca10b" STYLE="fork">
              <node TEXT="一旦有线程修改了 volatile 修饰的共享变量，其他线程就到主内存会读取最新的变量值" ID="d325b04ee90d4cdabbfce977e1fb5cbc" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="原子性" ID="4562613a4731c0974bec16d973a6fb8e" STYLE="fork">
            <node TEXT="原子性：一批操作是一个整体，要么一起都实现，要么都不实现" ID="14f93cf952173be77a9387352c41e510" STYLE="fork"/>
            <node TEXT="原子性的保证" ID="e5db4cc8dcb4dce3c0b3f414734c028e" STYLE="fork">
              <node TEXT="1. 加锁：对关键代码加锁" ID="01ee08c1c2ea340f4ffbc97d1edadb87" STYLE="fork">
                <node TEXT="加锁机制性能较差：同一时刻只能有一个线程在运行" ID="7d66484babf765677b091b1bb062b4eb" STYLE="fork"/>
              </node>
              <node TEXT="2. 原子类：性能高，线程安全" ID="bf4d6c33dc1a85b71d02a713c22e5f7e" STYLE="fork">
                <node TEXT="常用方法" ID="a8b58ea018af8c074b580c03928b4150" STYLE="fork"/>
                <node TEXT="实现：CAS(check and swap)机制" ID="c779e38d8238e0514da8161b3062d539" STYLE="fork">
                  <node TEXT="读取值-&gt;对值进行操作-&gt;提交值" ID="713071cce7c107087e8acb97eb906024" STYLE="fork">
                    <node TEXT="旧值与读取时的值相同-&gt;用新值替换旧值" ID="29227ca62b0f1e53ba2f7a09fa662b5a" STYLE="fork"/>
                    <node TEXT="旧值与读取时的值不同-&gt;再读取值，重复操作" ID="eb68a07c4b3c301f0e5ecb5521448615" STYLE="fork"/>
                  </node>
                </node>
              </node>
              <node TEXT="乐观锁和悲观锁" ID="161f6593878f827453f482f722fab25a" STYLE="fork">
                <node TEXT="synchronized 悲观锁：总是假设最坏的情况，每次去拿数据的时候都认为别人会修改，所以每次在拿数据的时候都会上锁，这样别人想拿这个数据（读或写）就会阻塞直到它拿到锁。因此悲观锁性能差。" ID="c76f658f16204d0af1c44f91eb4bbac2" STYLE="fork">
                  <node TEXT="ReentrantLock 也是悲观锁" ID="4204bf7e29a7620ce4c0e3d55d6528d5" STYLE="fork"/>
                  <node TEXT="CAS机制 乐观锁：总是假设最好的情况，每次去拿数据的时候都认为别人不会修改，所以不会上锁，但是在更新的时候会判断一下在此期间别人有没有去更新这个数据。由此性能较好." ID="3f5130a62318d46f6acc6fed2f7eefef" STYLE="fork"/>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="并发（面试热点）" ID="70baf8be172ba483a1c32f08c765d679" STYLE="fork">
          <node TEXT="HashMap集合线程不安全，性能最好" ID="72fe2a780efce35190ebb263f9223e84" STYLE="fork"/>
          <node TEXT="Hashtable 集合线程安全，性能较差（已经被淘汰）" ID="ccb9cd7052f5a698655bc7ff1f16cbc3" STYLE="fork">
            <node TEXT="该集合的所有方法都加锁（悲观锁）" ID="0d19f68185cfe05f3fda6449082cc993" STYLE="fork"/>
          </node>
          <node TEXT="ConcurrentHashMap 集合保证了线程安全，综合性能好" ID="f18c12d32a1a667521885f96034ba8b0" STYLE="fork">
            <node TEXT="分段锁（CAS机制+局部锁定）" ID="42ffd744b2f7115afa630037170d0240" STYLE="fork"/>
            <node TEXT="只锁操作的元素位置" ID="3274c92f7d61af13749a183923b7061a" STYLE="fork"/>
          </node>
          <node TEXT="总结：并发存在安全问题时，用ConcurrentHashMap 集合" ID="7ecb60e1ae04c849b95a10c6307b31d4" STYLE="fork"/>
          <node TEXT="线程对象.join（）：主线程需要等该线程执行完，在继续执行，不能抢占该线程的CPU" ID="d263a623c4e3c35818b5668880c5ae1c" STYLE="fork"/>
        </node>
        <node TEXT="并发包：" ID="63823ce7fdd5f4c0259729112e91998d" STYLE="fork">
          <node TEXT="CountDownLatch：计数器" ID="296eeaaab53e70e938198fc4552d80cf" STYLE="fork">
            <node TEXT="通过一个计数器来实现的，每当一个线程完成了自己的任务后，可以调用countDown()方法让计数器-1" ID="b6d26209b8e2e2795e7f2bc7ca59756e" STYLE="fork"/>
            <node TEXT="await()方法的使线程进入阻塞状态" ID="7c13bb586c598d89d1825f44baa38cc9" STYLE="fork"/>
          </node>
          <node TEXT="CyclicBarrier：循环屏障" ID="995de04a3a3dce4b2e48789ceca47a06" STYLE="fork">
            <node TEXT="一组线程到达一个屏障（也可以叫同步点）时被回收，直到最后一个线程到达屏障时，屏障才会开门，所有被屏障拦截的线程才会继续运行。" ID="6b4c7b362fcba931c60077b26987dd30" STYLE="fork">
              <node TEXT="每个线程调用await方法告诉CyclicBarrier我已经到达了屏障，然后当前线程被回收" ID="528dd8b46a2ea36ca6093da582f6e8ff" STYLE="fork"/>
              <node TEXT="多组线程执行多个屏障" ID="b94e951d31b6194f30015f75da516e6b" STYLE="fork"/>
            </node>
            <node TEXT="构造方法" ID="d755e308b64c15e15c4347665fd2909a" STYLE="fork">
              <node TEXT="CyclicBarrier ( nt parties , Runnable barrierAction ) " ID="74021a857dd17567a416b3fd858b9e28" STYLE="fork"/>
              <node TEXT=" 用于在线程到达屏障时，优先执行barrierAction" ID="961a26f78cc28ddfd56afe7f8934800a" STYLE="fork"/>
              <node TEXT="参数一：多少个线程触发屏障，参数二：达到屏障后执行的线程任务" ID="8b586977f885a8cc33ddcaccfd6ffb84" STYLE="fork"/>
            </node>
            <node TEXT="底层有线程池机制" ID="b79debac4c67f7990e7c1259aadb0b72" STYLE="fork"/>
          </node>
          <node TEXT="Semaphore：控制线程的并发数量" ID="d5b2db002bbe979f4ae29e7bc6820fb5" STYLE="fork">
            <node TEXT="控制一段时间内允许多个线程执行" ID="05852536bc193d63171447fd1dfe9cb0" STYLE="fork">
              <node TEXT="synchronized：同一时间只允许一个线程执行" ID="0a4970e838208d884b3fddc45f88515f" STYLE="fork"/>
            </node>
            <node TEXT="构造方法" ID="78c918df33f1415b82573945927c6a10" STYLE="fork">
              <node TEXT="public Semaphore(int permits)" ID="2ba758738e6e69ca7def2e6deaf93a7d" STYLE="fork">
                <node TEXT="参数表示允许执行的线程的数量" ID="58b5c93a410552b34373150875a8a488" STYLE="fork"/>
              </node>
              <node TEXT="public Semaphore(int permits, boolean fair)" ID="bcaf1ba2e1927ada88b45e1704ef9ae0" STYLE="fork">
                <node TEXT="参数二 表示公平性，true时 下次执行等待最久的线程" ID="efb4f5f0100b415fcf64a4399b9b340b" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="方法" ID="ae0c203a7a19006df3b10c5559f691d2" STYLE="fork">
              <node TEXT="acquire()：获得锁许可" ID="626a3cdc2654af60ed685a05442ac055" STYLE="fork"/>
              <node TEXT="release()：释放锁许可" ID="7e16ef9a90881ddedf8572a3f6151aa6" STYLE="fork"/>
              <node TEXT="两个方法范围内的代码，只能同时执行指定数量个线程" ID="28e174aeb1ebcf78d5d9e8f8898539e4" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="Exchanger：多线程间交换数据" ID="8d970a928fec53fb390d48762ed778ae" STYLE="fork">
            <node TEXT="某个线程等不到对方的数据，就会一直等待，不会进行数据交换" ID="a2d38152ed08c79c57e5919162d9a533" STYLE="fork"/>
          </node>
        </node>
      </node>
    </node>
    <node TEXT="lambda、Stream、File类、IO流" ID="dea247da3194594ddebf4be91c80b439" STYLE="bubble" POSITION="left">
      <node TEXT="Lambda" ID="11d59a5fef43c6ce9494ecc27021f05a" STYLE="fork">
        <node TEXT="目的：简化匿名内部类代码" ID="62b15946da82f73f752e4a5bc0c2ea84" STYLE="fork"/>
        <node TEXT="标准格式" ID="e9dccfbe6968e6d29e86af1969fbcfc6" STYLE="fork">
          <node TEXT="(参数类型 参数名称) -&gt; { 代码语句 }" ID="5335f070434acf47fff61aa7a0516562" STYLE="fork"/>
        </node>
        <node TEXT=" 只能简化函数式接口" ID="0bbdf21694329483d1c7b32e3ddaa4e2" STYLE="fork">
          <node TEXT="1. 必须是接口" ID="094d6b94ab0614ba0b29a1382e918fb5" STYLE="fork"/>
          <node TEXT="2. 接口中只能有一个抽象方法" ID="c7191d960fc0bf0a199e435fd7a96be3" STYLE="fork"/>
        </node>
        <node TEXT="@FunctionalInterface，函数式代码的注解" ID="feff22fc4f14dd31b377e5d792d47ecc" STYLE="fork"/>
        <node TEXT="代码省略规则" ID="63a19ee64473727d18e9d503c83f940f" STYLE="fork">
          <node TEXT="1. 方法体代码只有一行。可以省略大括号和分号不写" ID="f3ccbdd28c6d777c337296095b53634c" STYLE="fork"/>
          <node TEXT="2. 方法体代码只有一行代码。可以省略大括号和分号不写。如果这行代码是return语句，必须省略return不写" ID="c1792e673c210fae1b07bc09e4d4c364" STYLE="fork"/>
          <node TEXT="3. 参数类型可以省略不写" ID="35903667a44854ca9154e3f74020e400" STYLE="fork"/>
          <node TEXT="4. 如果只有一个参数，参数类型可以省略，同时形参括号也可以省略。" ID="dcd40de123d76c61114c852ae5912433" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="方法引用（可遇不可求）" ID="d911f721816b76cbd634f453dc9e6435" STYLE="fork">
        <node TEXT="1. 静态方法的引用" ID="0f7f42c4906bfb7da8a5ea83f962d370" STYLE="fork">
          <node TEXT="引用格式：类名::静态方法" ID="9f2d6ded46d704dcae9f90653a799b14" STYLE="fork"/>
          <node TEXT="被引用的方法的参数列表要和函数式接口中的抽象方法的参数列表一致。则两个参数列表都可以省略" ID="069d6bf7b5129575416470ecef3dfe3f" STYLE="fork"/>
        </node>
        <node TEXT="2. 实例方法的引用" ID="0818cf1d4249062a1a5050b466cb0f4c" STYLE="fork">
          <node TEXT="引用格式：对象::实例方法" ID="e20e3a79d3f6929f557fb43fa521288b" STYLE="fork"/>
          <node TEXT="用法要求与静态方法相同，只是调用者不同" ID="a656620e958a03d5ef856396dab079e3" STYLE="fork"/>
        </node>
        <node TEXT="3. 特定类型方法的引用" ID="51a5300016b7be7cae1f177c68d98cdc" STYLE="fork">
          <node TEXT="特定类型：String等java自带的" ID="14fc51843668bf43d94a31b0afcabbdf" STYLE="fork"/>
          <node TEXT="特定类型::方法" ID="bd00cda398059280ba1721d870ccbaf1" STYLE="fork"/>
          <node TEXT="用法" ID="9467c11c1b558fd2091a81423dcfb9f3" STYLE="fork">
            <node TEXT="如果第一个参数列表中的第一个形参作为了后面的方法的调用者，并且其余参数作为后面方法的形参，那么就可以用特定类型方法引用" ID="e003ab36614341933f31c8e286a75296" STYLE="fork"/>
            <node TEXT="与实例方法相似，只是调用者由对象变成了特点类型" ID="bba1f0a3ceec1fb291ec2c3c5d25aab3" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="4. 构造器引用" ID="50f4331d159207836834c0c0240c2764" STYLE="fork">
          <node TEXT="引用格式：类名::new" ID="5875534f36772828b69c911b8f04bafd" STYLE="fork"/>
          <node TEXT="前后参数一致的情况下，又在创建对象就可以使用构造器引用" ID="b2e634da5f4419b04f9cc33969d3d7f1" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="Stream流" ID="314a15d4320f4edbef2ad22c8155d535" STYLE="fork">
        <node TEXT="简化集合和数组API，弱化它们的弊端" ID="610d618d1b45989003e448a082a0bf6e" STYLE="fork"/>
        <node TEXT="获取Stream流" ID="06e83650a94114d96b47314891f574cf" STYLE="fork">
          <node TEXT="集合：集合.stream()" ID="45869ff3b91a7931e8d2b9072e50320e" STYLE="fork">
            <node TEXT="Map集合" ID="7393c6c582404849d7ba2d54a1724f14" STYLE="fork">
              <node TEXT="获取键Stream流：map.keySet().stream()" ID="f1ae2ff5737ebdfd4a57523a088c3c9f" STYLE="fork"/>
              <node TEXT="获取值Stream流：map.values().stream()" ID="63da738d752e8eac3114e5b6aa2d3338" STYLE="fork"/>
              <node TEXT="获取键值对Stream流：Stream&lt;Map.Entry&lt;String,String&gt;&gt; stringStringStream = map.entrySet().stream()" ID="8b3a5a8a79079ba69b41232f0daedcfb" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="数组：Stream.of（数组） 或者 Arrays.stream（数组）" ID="c2c26b0cebe324497c1bb3a9ed8de2c8" STYLE="fork"/>
        </node>
        <node TEXT="Stream流常用API" ID="e0717c3859dc3ef9ac059edd897b88ec" STYLE="fork">
          <node TEXT="forEach : 逐一处理(遍历)，终结方法（之后不能再用了，只能创建新的流）" ID="0edb93d45eb8b818ab33e561b860f6e7" STYLE="fork"/>
          <node TEXT="count：统计个数，返回值是long型，终结方法" ID="4b20b36815c700301dca3d94ba229648" STYLE="fork"/>
          <node TEXT="filter：过滤元素，返回值是Stream型，以下均为非终结方法（需要用Stream变量接收，支持链式编程）" ID="72013f4b6d94ea9d516c279c593a49b8" STYLE="fork"/>
          <node TEXT="limit：取前几个元素" ID="d59ed4167a6df29d040011edcd3cf3d2" STYLE="fork"/>
          <node TEXT="skip：跳过前几个" ID="3c78cf2b2a3b6ac38b26b64a3395d5b7" STYLE="fork"/>
          <node TEXT="map：加工方法" ID="a0383844b781e19333ca2ac1b6be53f1" STYLE="fork"/>
          <node TEXT="concat：合并流，一次只能合并两个" ID="ed22c689aa6b433bf16f38551a18acc1" STYLE="fork"/>
        </node>
        <node TEXT="收集Stream流" ID="8f6918499c502086bebbe9f89fd940d9" STYLE="fork">
          <node TEXT="流的手段，集合才是目的" ID="723f51624130b06e1ec959bbdc235e77" STYLE="fork"/>
          <node TEXT="把流的数据转换会集合" ID="0103b3e84d373b6b825acc7281323460" STYLE="fork"/>
          <node TEXT="方法" ID="571080da5d4c2d8efc1725f00c0c197a" STYLE="fork">
            <node TEXT="转换成Set集合" ID="70ae3375c01574d0519ab34db38c6bba" STYLE="fork">
              <node TEXT="Stream流.collect(Collectors.toSet())" ID="778fedf7c09fb5343b3da9afc92a511c" STYLE="fork"/>
            </node>
            <node TEXT="转换成List集合" ID="5cbf0979470c450077d8ad6848129f29" STYLE="fork">
              <node TEXT="Stream流.collect(Collectors.toList())" ID="cc8a0952fdcd28df181ccfe7aca58b3d" STYLE="fork"/>
            </node>
            <node TEXT="转换成数组" ID="5831a267b26c0ee6557531b4a8c6c3de" STYLE="fork">
              <node TEXT="Stream流.toArray()    //只能转换成Object数组类型" ID="76593da45dd3baf3ed9ccb91bef72dd9" STYLE="fork"/>
              <node TEXT="借用toArray的构造器" ID="595f69663796b649e272a391b1adc72f" STYLE="fork">
                <node TEXT="Stream流.toArray(String[]::new)       //可以转换成只能数据类型" ID="606576fa2111674c9f65a30430ab7a12" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="File类" ID="512368118ee1c5edfb6fa8edf50467a2" STYLE="fork">
        <node TEXT="分隔符" ID="32ff6ba87cb97cd2b137301e027e9d28" STYLE="fork">
          <node TEXT="正斜杠：/" ID="2a9880e3454f06bdafd9339700da29ba" STYLE="fork"/>
          <node TEXT="反斜杠：\\" ID="6581b74a7fead8181223ac646d4e4cfa" STYLE="fork"/>
          <node TEXT="分隔符API：File.separator" ID="caac6cb8a897472c0cf7fa058c6ca6f5" STYLE="fork"/>
        </node>
        <node TEXT="构造方法" ID="833418cd63a6fc3d72d22cb895315632" STYLE="fork">
          <node TEXT="File（路径+文件名）" ID="d0fd153a42095a33bc04f18773a9dc97" STYLE="fork"/>
          <node TEXT="File（路径，文件名）" ID="f745341b233df0973b9660a64bee952f" STYLE="fork"/>
          <node TEXT="File（File路径对象，文件名）" ID="33f8cc15560c54c33cb9ec32f3b029e7" STYLE="fork"/>
        </node>
        <node TEXT="注意事项" ID="d2eb1b8053c824bad82a92bbf00b66a8" STYLE="fork">
          <node TEXT="一个File对象代表硬盘中实际存在的一个文件或者目录" ID="da8bdf148f243e4cdca9d49f1c5828cf" STYLE="fork"/>
          <node TEXT="无论该路径下是否存在文件或者目录，都不影响File对象的创建。" ID="0bf0fba17a413f11d0e38e4c2b777865" STYLE="fork"/>
        </node>
        <node TEXT="常用API" ID="63ae7b06f822c786ca1943000f362a13" STYLE="fork">
          <node TEXT="public String getAbsolutePath()：返回绝对路径名字符串" ID="8364fbb8ac1728debce0f8a2bbea2a16" STYLE="fork"/>
          <node TEXT="public String getPath()：返回创建文件时用的路径" ID="ffb27093df15ce9fb3dad3d298139a0b" STYLE="fork"/>
          <node TEXT="public String getName()：返回目录和文件（带后缀）名称" ID="a646c19b0644343241749b301f145e87" STYLE="fork"/>
          <node TEXT="public long length()：返回文件的长度（字节数）" ID="bfff143193d7cb01758b95fb2284a994" STYLE="fork"/>
          <node TEXT="public boolean exists()：判断文件或目录是否存在" ID="e7ea154c268f32756da3ba20a9b71f54" STYLE="fork"/>
          <node TEXT="public boolean isDirectory()：判断是否是目录" ID="dd1a0af072a4a5681d62ff9605340227" STYLE="fork"/>
          <node TEXT="public boolean isFile()：判断是否是文件" ID="46aa08b3bf4a2934ff6501e632d51200" STYLE="fork"/>
          <node TEXT="public boolean createNewFile()：该名称的文件不存在时，创建一个（不常用）" ID="05856929a4fb9f59da2cf51404d971fa" STYLE="fork"/>
          <node TEXT="public boolean delete() ：删除这个文件或目录，只能删除空目录" ID="46db6543b132049c0abf569df225aa5c" STYLE="fork"/>
          <node TEXT="public boolean mkdir()：创建目录，只能创建，只能创建一级目录" ID="4bcf2e8254c0e0ef99058ea77e4c5f2a" STYLE="fork"/>
          <node TEXT="public boolean mkdirs()：创建目录，能创建多级目录，推荐用" ID="bc91e564569c799b0c295cbe3644c629" STYLE="fork"/>
          <node TEXT="public String[] list()：返回当前目录下所有一级文件名称字符串数组" ID="cb2e9ed385bdf6a027c2b21e5fd21ed3" STYLE="fork"/>
          <node TEXT="public File[] listFiles()：返回当前名录下一级的文件或目录" ID="25a48cc5e40da1e833a21ea3a66c77df" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="递归" ID="22dc26c2592d2dbc30254820a742a041" STYLE="fork">
        <node TEXT="三要素‘" ID="65dfd2829ea1d5af6566239ef01900f1" STYLE="fork">
          <node TEXT="递归的终结点" ID="9314008a96f29c9315c30ae82c1203ed" STYLE="fork"/>
          <node TEXT="递归的公式" ID="bd11c5cf66647dec25b7843a98d23d78" STYLE="fork"/>
          <node TEXT="递归的方向：必然走向终结点" ID="b81b935bc8f34569159d1505d65c3e4b" STYLE="fork"/>
        </node>
        <node TEXT="公式转换" ID="89b63ac78546c25912080595372d547d" STYLE="fork"/>
      </node>
      <node TEXT="IO流" ID="17ad6b77553f5e38b0b5dd0adbe00e50" STYLE="fork">
        <node TEXT="注意事项" ID="8e02506fdbc33b3a4d915ad841b66f2c" STYLE="fork">
          <node TEXT="计算机中最小单位是字节" ID="c4380c926a70a26992198328c939c586" STYLE="fork"/>
          <node TEXT="File类只能操作文件对象，读写文件要用IO流" ID="3c386f050578fd9f8b4ad7837178ae6f" STYLE="fork"/>
          <node TEXT="如果代码编码和读取的文件编码不一致。字符流读取的时候会乱码。" ID="e5d0b457425baedfa5cd24ce90293e88" STYLE="fork">
            <node TEXT="对字母和数字没有影响，对中文有影响" ID="fd305f54cc96128fd1a6818de02e5589" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="IO流体系" ID="cc199d6c204e78723226058b14fc9021" STYLE="fork">
          <node TEXT="字符流只适用于文本文件，图片等文件需要用字节流" ID="bf4f87714a190a997c92f875d31dd704" STYLE="fork"/>
          <node TEXT="抽象类用于多态定义变量类型，实现类用来创建对象" ID="f2185191aab9b9baeaa3f834a1c6f15b" STYLE="fork"/>
          <node TEXT="字节流" ID="8fc703e317921a09c9e9322e4b6ac25b" STYLE="fork">
            <node TEXT="字节缓冲区：一个字节数组，用来临时存储字节数据。" ID="e18dd42a748b059f13dea991a6e573c2" STYLE="fork"/>
            <node TEXT="字节输入流：磁盘读取到内存" ID="1bc9ac60cd6c3a12ea968817923288f1" STYLE="fork">
              <node TEXT="一个一个字节读取英文和数字没有问题。" ID="39017f7367d200877aaacc336d3140b0" STYLE="fork"/>
              <node TEXT="但是一旦读取中文输出无法避免乱码，因为会截断中文的字节。" ID="0d3f45b95060a5a6c6f84c3780abe88c" STYLE="fork"/>
              <node TEXT="一个一个字节的读取数据，性能也较差，所以禁止使用此方案！" ID="552deee3c82ac9279652a3f2be447c8f" STYLE="fork"/>
              <node TEXT="方法" ID="b56ae07dbfdb9f1251cdebeefaab1dfd" STYLE="fork">
                <node TEXT="read" ID="7d34c7c581c0175f0ed166a2f4146159" STYLE="fork">
                  <node TEXT="字节数组，数据读取到字节数组中，返回读取的字节数量，没有字节可读返回-1。" ID="45e1ab347a9e6f87f1334b0b93a070c5" STYLE="fork"/>
                  <node TEXT="每次读取一个字节返回！读取完毕会返回-1。" ID="638a1a958c3fe674ebdfa94d824a4c22" STYLE="fork"/>
                </node>
              </node>
            </node>
            <node TEXT="字节输出流：内存写入到磁盘" ID="9ac9648f7fa09c7fd1e178afd9055f07" STYLE="fork">
              <node TEXT="构造方法" ID="d5b291db399271f582977bf89cb2b89f" STYLE="fork">
                <node TEXT="FileOutputStream(String file)" ID="68d79db5d46966924c43e4f6c3a3ac35" STYLE="fork">
                  <node TEXT="普通文件路径" ID="aeb202b02117eb73569518206f6a79fd" STYLE="fork"/>
                </node>
                <node TEXT="FileOutputStream(File file , boolean append)" ID="06827b11f5bdc96b4bdc303252131bba" STYLE="fork">
                  <node TEXT="向普通文件追加字节（第二个参数为true）" ID="8a7f340745afcad66052252ea495e2c8" STYLE="fork"/>
                </node>
                <node TEXT="FileOutputStream(String file , boolean append)" ID="c6b8f050fb36f46af725f25d223aaca5" STYLE="fork">
                  <node TEXT="与第二个构造方法相同" ID="a1fd413a2793fe458d56900af384b971" STYLE="fork"/>
                </node>
              </node>
              <node TEXT="方法" ID="8a38eea0f4f139e7be244911965b932a" STYLE="fork">
                <node TEXT="write" ID="c7550464562c63e59fc6ebb16e26d8c5" STYLE="fork">
                  <node TEXT="写一个字节" ID="ce4ca13ffff30c532f6b744d384bd979" STYLE="fork"/>
                  <node TEXT="字节数组" ID="79bfb67e192a33e6ccea2674bc40cf3a" STYLE="fork"/>
                  <node TEXT="字节数组，其实索引位置，字节长度" ID="77ad0402322d0d6715c17516f5046f35" STYLE="fork"/>
                </node>
              </node>
              <node TEXT="字节输出流默认是覆盖数据管道。" ID="c9952763e0884b3d22dab69611a0222a" STYLE="fork"/>
              <node TEXT="关闭和刷新：刷新流可以继续使用，关闭包含刷新数据但是流就不能使用了" ID="84d4d2e84bbaf581e583341e331486f8" STYLE="fork">
                <node TEXT="public abstract void close()" ID="95d3cb6c13f2d84f320e0942e88c205d" STYLE="fork"/>
                <node TEXT="public abstract void flush()" ID="abeb113cf9e67585cbe0e22fdf928629" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="字符流" ID="f4b7a6051888f095881ab634be7dc49a" STYLE="fork">
            <node TEXT="按字符读取，可以解决中文编码错乱的问题，适合文本文件的操作，但是性能较差" ID="42ec96bdff3951adb5fc90ea1928598e" STYLE="fork"/>
            <node TEXT="字符输入流" ID="321071409a68782c23115bf1c687b20c" STYLE="fork">
              <node TEXT="构造方法" ID="f91569bc5003c66026993db24c66fe5e" STYLE="fork">
                <node TEXT="FileReader(File file)：给定File对象" ID="8fca1141e99eb568e1fb5cf4b58aa115" STYLE="fork"/>
                <node TEXT="FileReader(String fileName)：给定文件路径和文件名" ID="20796ae80f3e6e1655ead4468ba99cd0" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="字符输出流" ID="a324e9db23fb216fd263df916310a22c" STYLE="fork">
              <node TEXT="构造方法参数：如果文件不存在直接创建文件" ID="39f9b2a00b793deda307c27ad906ddf4" STYLE="fork">
                <node TEXT="File对象" ID="957acb5a2d3ea6f0d91e974ba9ee0c6c" STYLE="fork"/>
                <node TEXT="文件路径及名称" ID="e7a0eed86f0ac7646f700bd17dbd0d88" STYLE="fork"/>
              </node>
              <node TEXT="方法" ID="015d459364a577c6b2d40984f2278f32" STYLE="fork">
                <node TEXT="public void write()" ID="57656231400680a10af99e5b88ed89bc" STYLE="fork">
                  <node TEXT="写一个字符" ID="48fb0fc5c9b167fa62b580239a386426" STYLE="fork"/>
                  <node TEXT="写一个字符数组" ID="d67ae5f4787219fe02c51a695d3f13d9" STYLE="fork"/>
                  <node TEXT="写一个字符串" ID="d75e0f2732d2aa740c8ca09a022e36f9" STYLE="fork"/>
                </node>
                <node TEXT=" public abstract void write(char[] b, int off, int len)" ID="cee17d0100491b119ffe334c3bb72f03" STYLE="fork"/>
              </node>
              <node TEXT="必须关闭资源通道，如果不关闭,数据只是保存到缓冲区，并未保存到文件。" ID="99023a83ebfbf376fabab726bfc160fa" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="关闭和刷新" ID="1d5b3e41e6d4cae79ec657c71501ad5b" STYLE="fork">
            <node TEXT="既想写出数据，又想继续使用流" ID="4fa700ac693c5fd65a986a3276d75f81" STYLE="fork"/>
            <node TEXT="flush ：刷新缓冲区，流对象可以继续使用。" ID="1ab8cef2499bd1a54f17ccad40453b8c" STYLE="fork"/>
            <node TEXT="close ：关闭流，释放系统资源。关闭前会刷新缓冲区。" ID="8ec01a8c509079da0b5d2c93a7e0239f" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="流关闭的原则：先开后关，后开先关" ID="8d6ba908f79d4e274d30d16140595939" STYLE="fork"/>
        <node TEXT="try(只能定义资源对象)" ID="5b4d2774db3a2bc40cfb2690ddd2bdc4" STYLE="fork">
          <node TEXT="会自动关闭资源" ID="58d016e2cf6afa08241a7abafc3e6628" STYLE="fork"/>
        </node>
        <node TEXT="单位长度" ID="d33fb10abfbf3c9de93eb48d0f757195" STYLE="fork">
          <node TEXT="1字=2字节(1 word = 2 byte)，一个字的字长为16" ID="f09297793246929b9705b1a73f285530" STYLE="fork"/>
          <node TEXT="1字节=8位(1 byte = 8bit)，一个字节的字长是8" ID="962f3b9d1b3e2b7754860bb3a47c3a6a" STYLE="fork"/>
        </node>
        <node TEXT="字符集" ID="3ced886991b093f1991b9fbbe2d050d0" STYLE="fork">
          <node TEXT="ASCII" ID="05432268ad734bca3e6c5f2aba89c47b" STYLE="fork">
            <node TEXT="数字和英文在底层，占用一个字节" ID="c8b1b53756b1a4b8450d7056cac2d3db" STYLE="fork"/>
          </node>
          <node TEXT="GBK" ID="a05c7d3056dd0cd8c683812adbe1d18e" STYLE="fork">
            <node TEXT="中文占用2个字节" ID="463533305e821c3643d01af55deb22b2" STYLE="fork"/>
          </node>
          <node TEXT="UTF8（开发用）" ID="c7fed2907fda1eb7ebd24d12e2428b60" STYLE="fork">
            <node TEXT="中文占用3个字节" ID="09d0199127aabd4c1a2729ea1a3da2ce" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="缓冲流" ID="8237b1412e836ed835340ef32b832fca" STYLE="fork">
          <node TEXT="（1）BufferedInputStream：字节缓冲输入流，可以提高字节输入流读数据的性能。" ID="0d9c242d0c1bcddd5c8757cb6e976be5" STYLE="fork">
            <node TEXT="构造方法：把低级的字节输入流包装成一个高级的缓冲字节输入流管道，功能无变化" ID="5c6ec7bf9ef2cff9c4bcc63644ab47c1" STYLE="fork">
              <node TEXT="public BufferedInputStream(InputStream in)" ID="95fc95b6e468599c90e75085e8399ae1" STYLE="fork"/>
            </node>
            <node TEXT="原理" ID="81c4d64dea06962779609eeb52ee0e8c" STYLE="fork">
              <node TEXT="缓冲字节输入流管道自带了一个8KB的缓冲池，每次可以直接借用操作系统的功能最多提取8KB的数据到缓冲池中去" ID="03f71e81e8a19a063f114d936996307a" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="（2）BufferedOutStream：  字节缓冲输出流，可以提高字节输出流写数据的性能。" ID="56fcbe919879ca2ad09912d95eb4e6ca" STYLE="fork"/>
          <node TEXT="（3）BufferedReader：  字符缓冲输入流，可以提高字符输入流读数据的性能。" ID="e552f74086f456527b5ffd6ce572deb9" STYLE="fork">
            <node TEXT="多了一个方法" ID="18fbd6587dad416cf128d368a92e2cfd" STYLE="fork">
              <node TEXT="public String readLine(): 读取一行数据返回，读取完毕返回null;" ID="6a3806a4bee78b43d2d4f8e62ce6e76c" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="（4）BufferedWriter：  字符缓冲输出流，可以提高字符输出流写数据的性能。" ID="a41efa21892d7e6d319729bd0442e42a" STYLE="fork">
            <node TEXT="多个一个方法" ID="e9b81622552ca39d4c03d43a8a5c70cc" STYLE="fork">
              <node TEXT="public void newLine()：新建一行。（换行）" ID="ca331480a43c8a89d35beed53280735d" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="字符转换流（对字符流的操作）" ID="c823e26d45e2b7f9fe20fe299f305961" STYLE="fork">
          <node TEXT="作用：解决不同编码读取乱码的问题，将字节流转换成字符流" ID="69181fb7cf4086a4365c59d47be7378d" STYLE="fork"/>
          <node TEXT="字符输入转换流" ID="96f7b05f90ee92cc58f4ff45e4a7acbc" STYLE="fork">
            <node TEXT="构造方法" ID="279d23b51d45c216c717b93e40a5a743" STYLE="fork">
              <node TEXT="public InputStreamReader(InputStream is,String charset)" ID="219c17988ae310523ec7060fc799e22e" STYLE="fork"/>
              <node TEXT="输入流，转换成指定的编码" ID="4de1ed2e84aa98e68ec1d4bbf4941789" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="字符输出转换流" ID="e007d9f0139f71b179c8f7d0e831606e" STYLE="fork">
            <node TEXT="作用：" ID="39ee3671030cab03a429ea80f3db1640" STYLE="fork">
              <node TEXT="可以指定编码把字节输出流转换成字符输出流" ID="2e60eb8345a259c554978e65ddad5fc7" STYLE="fork"/>
              <node TEXT="可以指定写出去的字符的编码" ID="b41702c236b694edebca7b6488090dfa" STYLE="fork"/>
            </node>
            <node TEXT="构造方法" ID="9c2245943536b5fc82fbd0ddca1b1343" STYLE="fork">
              <node TEXT="public OutputStreamWriter(OutputStream os , String charset)" ID="570c7ffdfc8955e6a544ad8ccc0fd1da" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="序列化（对字节流的操作）" ID="2ab80120d812e2bc2ddceeefd7ea0ecb" STYLE="fork">
          <node TEXT="可以对存储对象的集合直接进行操作" ID="1683042d69be4881d14c0fc63c394ecf" STYLE="fork"/>
          <node TEXT="对象序列化：对象 =&gt; 文件中" ID="19bd7e5fb619735a27f0d60daeb1799e" STYLE="fork">
            <node TEXT="就是把Java对象数据直接存储到文件中去" ID="4449adbaea9137364aa13d52e8b76dc3" STYLE="fork"/>
            <node TEXT="对象字节输出流，ObjectOutputStream" ID="4144cb537b6808b8505b6803c1b18c21" STYLE="fork"/>
            <node TEXT="构造方法" ID="dd36b0bdcf36696c1a7c69a4d84fc06e" STYLE="fork">
              <node TEXT="public ObjectOutputStream(OutputStream out)" ID="0142fb773532e908135dfbd68f6e64cf" STYLE="fork"/>
            </node>
            <node TEXT="序列化方法" ID="7da0276485de9290b49264d7b8194176" STYLE="fork">
              <node TEXT="public final void writeObject(Object obj)" ID="2cfb78ce57b321a0de7443df0589b7b0" STYLE="fork"/>
            </node>
            <node TEXT="序列化的条件" ID="95ce79343cbb9cc28249f707d0b15891" STYLE="fork">
              <node TEXT="序列化的对象必须实现序列化接口 implements Serializable" ID="5134097d104b8a0e0cb5d7711cdf633c" STYLE="fork"/>
              <node TEXT="类的属性序列化，transient（瞬态）修饰的属性不进行序列化" ID="fdc075c5f453f9ac508724468a39c07c" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="对象反序列化：文件中 =&gt; 对象" ID="fc646f4ca2fe31804bf9861086670c32" STYLE="fork">
            <node TEXT="把序列化的数据恢复成对象" ID="14ad2e48aa42327441e38767e5d001c6" STYLE="fork"/>
            <node TEXT="构造器" ID="733d95494bfd35d16a3636dfea425a48" STYLE="fork">
              <node TEXT="public ObjectInputStream(InputStream is)" ID="af89d1df90090042a8a8f9db45f7d84a" STYLE="fork"/>
            </node>
            <node TEXT="反序列化方法" ID="6aa60bfa06be14d326fb2fcfef93196e" STYLE="fork">
              <node TEXT="public final Object readObject()" ID="4c1a5c7549bac265830ef45cd3215803" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="打印流" ID="c9ddf5cf5d3a21be80f9df8a5cf91def" STYLE="fork">
          <node TEXT="方便、高效（底层基于缓冲流）打印各种数据" ID="f25f95cf5584276530df53537f9fe390" STYLE="fork"/>
          <node TEXT="构造方法" ID="b223ccc4419ce90d059bc3b21d86c994" STYLE="fork">
            <node TEXT="public PrintStream(OutputStream os)" ID="673c01b067bf8812f41c1306910a387f" STYLE="fork"/>
            <node TEXT="public PrintStream(String filepath)" ID="91403f4ee636b908f9245a194862d9d1" STYLE="fork"/>
            <node TEXT="PrintWriter不光可以打印数据，还可以写字符数据出去" ID="07ede7b0f549f210a17bc3d6cf8bbb09" STYLE="fork"/>
            <node TEXT="PrintStream不光可以打印数据，还可以写字节数据出去" ID="5734a194040462cf2afdea04fe4d38a6" STYLE="fork"/>
            <node TEXT="两个用法相同" ID="7775517d0fbb24a3319204ca75bfd8b5" STYLE="fork"/>
          </node>
          <node TEXT="通过打印流输出的数据，会写入打印流的文件中" ID="806497a26067160e09c252c8810a3c78" STYLE="fork"/>
          <node TEXT="重定向" ID="dca036f4a5248e89b2ab2ccc2d337c4c" STYLE="fork">
            <node TEXT="让系统的输出流向打印流" ID="d34773c2e90bb57c4f8fadccf4fe4958" STYLE="fork"/>
            <node TEXT="将系统输出的数据打印到打印流的文件中" ID="f52efbb272eff4f58da344d9dbdb96b1" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="Properties属性集合对象" ID="7c92a34e06adf96a3c738a9f773851bc" STYLE="fork">
        <node TEXT="保存属性的文件" ID="97423e1694a59b41473aea9a952204b7" STYLE="fork"/>
        <node TEXT="大型框架底层实现" ID="4a607798b3a56ba7308f56f1c1ecee4c" STYLE="fork"/>
        <node TEXT="属于Map集合，以键值对的形式存储系统属性文件信息" ID="b7e47960eda1627955fda7d982114bf0" STYLE="fork"/>
        <node TEXT="方法" ID="a837fd478eccf7bdc9d8895dd973137e" STYLE="fork">
          <node TEXT="存储（输出）常用" ID="6c76f5bc6f1236c178e0fde254a3d597" STYLE="fork"/>
          <node TEXT="store方法参数" ID="09e7692458e3731be76d531962bb262b" STYLE="fork">
            <node TEXT="参数一：被保存数据的输出管道" ID="f35bd42d2f0ef32e8454b91d36276dde" STYLE="fork"/>
            <node TEXT="参数二：记录日志。就是对象保存的数据进行解释说明！" ID="ada498357f3ef561028129001928dbec" STYLE="fork"/>
          </node>
          <node TEXT="读取（输入）方法" ID="6e59cdb94346f63848f1f79c0dfe5437" STYLE="fork">
            <node TEXT="参数可以是字节输入流/字符输入流" ID="4786181bf13d5e2cf0135dbeda7a2523" STYLE="fork"/>
          </node>
        </node>
      </node>
    </node>
    <node TEXT="网络编程" ID="7e2a1e50edaa8b2e95325d6faec592b0" STYLE="bubble" POSITION="left">
      <node TEXT="概述" ID="adb5280956a0c1ba4a1a3d6c934d76c1" STYLE="fork">
        <node TEXT="网络编程三要素" ID="e698f4cbc2eb6cb1d580342b948ec277" STYLE="fork">
          <node TEXT="协议" ID="9d049db66431ee9cbe4b35c1885cff86" STYLE="fork">
            <node TEXT="TCP协议：传输控制协议" ID="f0afaf0af1bf5aafb4cf167ee115b7a0" STYLE="fork">
              <node TEXT="面向连接的协议，保证数据传输的安全性，可靠的协议" ID="0f2c9f913db9ecfe820d7f9ca10a6c40" STYLE="fork"/>
              <node TEXT="使用场景：文件下载，浏览网页" ID="ae8276f06be59350ca32de9b4b739f50" STYLE="fork"/>
              <node TEXT="三次握手，第四次挥手断开连接" ID="8366754185cf644ec21dae470bd60fad" STYLE="fork">
                <node TEXT="1. 客户端-&gt;服务端，确认服务器正常" ID="4c9179a578c7d813604287b6fe87f653" STYLE="fork"/>
                <node TEXT="2. 服务器-&gt;客户端，服务器回复自己正常" ID="fc3f71e08ab676e41d0cdd1b983bd405" STYLE="fork"/>
                <node TEXT="3. 客户端-&gt;服务器连接请求，确认连接" ID="371c09fc444572e7bff47b97e04afaaa" STYLE="fork"/>
              </node>
              <node TEXT="基于IO流进行数据传输" ID="223266f45174a565a3925f7e03024a46" STYLE="fork"/>
              <node TEXT="传输数据大小没有限制" ID="c529bae897a6a43d67b2fcb187684e5f" STYLE="fork"/>
            </node>
            <node TEXT="UDP协议：用户数据报协议(User Datagram Protocol)" ID="241f95775d77250306820494b2828c15" STYLE="fork">
              <node TEXT="面向无连接协议，不建立连接，不可靠协议" ID="f39c64576fb52d3c5653afae11f61bb8" STYLE="fork"/>
              <node TEXT="不管服务器是否启动，直接发送数据包（大小限制64K）" ID="9089e406fe78cc336d1dff32ec3efefd" STYLE="fork"/>
              <node TEXT="速度快，容易丢数据" ID="c6bd769e2de61e672c70bbba00362c67" STYLE="fork"/>
              <node TEXT="应用场景：视频会议，QQ聊天" ID="77901e54e4301ff40e57fa9595e3f396" STYLE="fork"/>
            </node>
            <node TEXT="架构" ID="7925e3b383f270a195d70ce3b1d0ee8b" STYLE="fork"/>
          </node>
          <node TEXT="IP地址" ID="f904db3ef2ba9eca9ab8d186da81c11e" STYLE="fork">
            <node TEXT="IP分类" ID="7f70b40209384556d27d16ce0830c2c6" STYLE="fork">
              <node TEXT="IPV4" ID="86b8a681cc64efff5fae9bd520109146" STYLE="fork">
                <node TEXT="32位二进制数" ID="907f57770c69585a7604f8432dc819e7" STYLE="fork"/>
              </node>
              <node TEXT="IPV6" ID="962a1d57a6cae1de99377c2fdfb9ec11" STYLE="fork">
                <node TEXT="128位二进制数" ID="ee4348e193c3359769bb65f8d8f39840" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="端口号" ID="34d13299a993ee38106fbeb0117e5d6e" STYLE="fork">
            <node TEXT="唯一标识设备中的进程（应用程序）" ID="f2f02fcd5081570c9a658b742997de97" STYLE="fork"/>
            <node TEXT="用两个字节表示的整数，取值范围是0~65535，前1024个用于知名的服务和应用" ID="aedb7a3b1bc010867ddbafc70974c562" STYLE="fork"/>
          </node>
          <node TEXT="利用`协议`+`IP地址`+`端口号` 三元组合，使网络中的进程可以进行通信" ID="484c04cc8d5c653ea3859222d02e6221" STYLE="fork"/>
        </node>
        <node TEXT="InetAddress类" ID="0d8f629401e4621947840902daddb1c8" STYLE="fork">
          <node TEXT="该类的对象代表IP地址对象" ID="8e4fb4f8570b9a746c9ac126199c1823" STYLE="fork"/>
          <node TEXT="方法" ID="64d9d8e5ea1687b8a9103363f3829367" STYLE="fork">
            <node TEXT="String getHostName();获得主机名" ID="40146c60d2e68f41dfcb7780e228614d" STYLE="fork"/>
            <node TEXT="String getHostAddress();获得IP地址字符串" ID="34afe0f600fe5a0b866458761ea8e80c" STYLE="fork"/>
            <node TEXT="getLocalHost() ：获取本地主机IP地址" ID="48cfc2cc992d6e425ccc487ff745ef1e" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="UDP通信" ID="bab10fb81f3a29c2350351d62de3152c" STYLE="fork">
        <node TEXT="基于数据包" ID="96c28756e8135dc6dd05158b53bb53d0" STYLE="fork"/>
        <node TEXT="相关类" ID="f81f937b248edab10bb01c081c9502f3" STYLE="fork">
          <node TEXT="DatagramPacket" ID="c12c91a8815e833de8c0da6a9b028c0e" STYLE="fork">
            <node TEXT="数据包对象" ID="831a168af37ae5950d3e2fb1a1fa622f" STYLE="fork"/>
            <node TEXT="作用：用来封装要发送或要接收的数据，比如：集装箱" ID="0aa4db297403304e34431111fb27fe02" STYLE="fork"/>
            <node TEXT="构造方法" ID="087b13b8a4f43a1262f25566bb5ee98f" STYLE="fork">
              <node TEXT=" DatagramPacket(byte[] buf, int length, InetAddress address, int port)" ID="cef37d69215069404965bd30b8093f14" STYLE="fork">
                <node TEXT="创建发送端数据包对象" ID="6aa7dde42fa59c505acbb9f358029657" STYLE="fork"/>
                <node TEXT="buf：要发送的内容，字节数组" ID="7394590989d5d246bfd9e2e44370e9c7" STYLE="fork"/>
                <node TEXT="length：要发送内容的长度，单位是字节" ID="a4dbc0a7f4b5cfab7d5cee83706202c8" STYLE="fork"/>
                <node TEXT="address：接收端的IP地址对象" ID="d8e00c713790fd51ae8d75811e8811e0" STYLE="fork"/>
                <node TEXT="port：接收端的端口号" ID="f1d95eee698b9c8efa7db71099bf2ce6" STYLE="fork"/>
              </node>
              <node TEXT="DatagramPacket(byte[] buf, int length)" ID="8ac69efa220d650ea7751125b0c7ce80" STYLE="fork">
                <node TEXT="创建接收端的数据包对象" ID="5cd090f4e2bb21eb5f563d739c81ef3f" STYLE="fork"/>
                <node TEXT="buf：用来存储接收到内容" ID="5972e056f990d9093c648d7ab817dd0f" STYLE="fork"/>
                <node TEXT="length：能够接收内容的长度" ID="1ad5b0463179000b01c7cba610f43950" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="常用方法" ID="ab67fc26f8f8f83087243cfbedc6e2f1" STYLE="fork">
              <node TEXT="int getLength() 获得实际接收到的字节个数" ID="702cec0c0dede693b0cbecc98507c401" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="DategramSocket" ID="4c08dcc020621740db5e6c55cb443b79" STYLE="fork">
            <node TEXT="发送对象" ID="a0c668ccad78bf1f5460ed6515cd1f51" STYLE="fork"/>
            <node TEXT="作用：用来发送或接收数据包，比如：码头" ID="93f6a05cd29f422ae309a30047a7bc4f" STYLE="fork"/>
            <node TEXT="构造方法" ID="b932ab061b1bf21325259661571382b3" STYLE="fork">
              <node TEXT="DatagramSocket() " ID="11f3c87b45e3e2fec1fa5ab708306496" STYLE="fork">
                <node TEXT="创建发送端的Socket对象，系统会随机分配一个端口号。" ID="fbafe4c6492358fb1366145cfde191cb" STYLE="fork"/>
              </node>
              <node TEXT="DatagramSocket(int port) " ID="b490f3ddfdceb843ac6af56932c80ecc" STYLE="fork">
                <node TEXT="创建接收端的Socket对象并指定端口号" ID="a4f5a3649e7524379f88ab8c25820722" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="成员方法" ID="37c6ab09ac8aad30e46fe30989b37a09" STYLE="fork">
              <node TEXT="void send(DatagramPacket dp) " ID="f42bf1476e20e04738ce3e184604faa4" STYLE="fork">
                <node TEXT="发送数据包" ID="ce559f5c119c24660f9b46b942f32057" STYLE="fork"/>
              </node>
              <node TEXT="void receive(DatagramPacket p) " ID="6d4dbaf7666b87df8b42e3380467ae9b" STYLE="fork">
                <node TEXT="接收数据包" ID="70f439f1e7f5e7570c8fa4f5724b04a4" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="服务端运行后一直等待接收数据，知道有数据发送过来" ID="670c6b1060a9f59628e0623d56baa1b9" STYLE="fork"/>
        <node TEXT="客户端可以直接发送数据，不管有没有接收，程序运行结束" ID="93c46f55c932bfe1aed3c5d0cf77660c" STYLE="fork"/>
      </node>
      <node TEXT="TCP通信" ID="6046555fb3014124945cfdc1e5cf1213" STYLE="fork">
        <node TEXT="基于通信管道" ID="300b3399ba6eccae703f68d7eba7146d" STYLE="fork"/>
        <node TEXT="相关类" ID="4693c4fe0562d099751dfe5adeb1ce10" STYLE="fork">
          <node TEXT="Socket" ID="f1090cf79b01c9201bc72070db3f09a6" STYLE="fork">
            <node TEXT="该类的对象代表一个客户端程序" ID="55e8e2e03caa2e37fe98c068a6f5dae3" STYLE="fork"/>
            <node TEXT="构造方法" ID="62d994d3f3bbb5058646059f70612985" STYLE="fork">
              <node TEXT="Socket(String host, int port)" ID="ce5df5eec2a1db187aaafdf0829b314c" STYLE="fork">
                <node TEXT="ip地址字符串，端口号" ID="a4293c578d0a4416ddb2fca1dfff4d4d" STYLE="fork"/>
                <node TEXT="只要执行该方法，就会立即连接指定的服务器程序，如果连接不成功，则会抛出异常，如果连接成功，则表示三次握手通过。" ID="a88096cb39c1ced55694cd630ffbed94" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="常用方法" ID="6f82324588d25e765856f82bbf951db0" STYLE="fork">
              <node TEXT="OutputStream getOutputStream（）" ID="7b233b313d12ecc9d587d954e6492e06" STYLE="fork">
                <node TEXT="获得字节输出流对象" ID="3ac68f433b17e423912da0c465a601d1" STYLE="fork"/>
              </node>
              <node TEXT="InputStream getInputStream（）" ID="5839780d350e729f3126112831bedfb8" STYLE="fork">
                <node TEXT="获得字节输入流对象" ID="522f652fdb2cf6985fd410f85baa05e1" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="ServerSocket" ID="3b95e52a477df7661d622a3e7b58f785" STYLE="fork">
            <node TEXT="该类的对象代表一个服务器端程序" ID="57646b5805a1cbcd63a0e00782717959" STYLE="fork"/>
            <node TEXT="构造方法" ID="d1e2cc0081087d4ba9016ae0f9aed2bd" STYLE="fork">
              <node TEXT="ServerSocket（int port）" ID="a43f4fd3d0409afc6b540adae0351bd9" STYLE="fork">
                <node TEXT="指定端口号开启服务器" ID="a81cf5a074f1d24c21b83df3fcb91301" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="常用方法" ID="9e3bbea4c0ace88cafdbbe408ef1cdec" STYLE="fork">
              <node TEXT="Socket accept（）" ID="a0d78cabe59bfd2748eddbaeba74cda9" STYLE="fork">
                <node TEXT="等待客户端连接" ID="ffe2f69132c086fcbcd1701060b51c24" STYLE="fork"/>
                <node TEXT="返回与客户端连接的Socket对象" ID="9d1c0a09575f278d5f1624833dd494b5" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="客户端开发流程" ID="f9442cf6d35808bbfa3e3d4fd88e40be" STYLE="fork">
          <node TEXT="1. 创建请求服务器的Scoket管道连接" ID="4abb22693489483385a776cd17e4e5ba" STYLE="fork"/>
          <node TEXT="2. 通过socket通信管道得到字节输出流" ID="0e63db66297cb462dd0e88a98855d102" STYLE="fork"/>
          <node TEXT="3. 通过字节输出流，向服务器发送数据" ID="f0ea508e995d48e7e51cbfb05bea1869" STYLE="fork"/>
          <node TEXT="注意事项" ID="bbb16c71d35ea0137bceae597450ba0a" STYLE="fork">
            <node TEXT="1. 通信管道与传输流不同，通信管道要调用 socket.shutdownOutput（）方法，通知服务器已经传输完了，否则服务器会在读取数据位置一直等待，造成系统死在那里" ID="0fde95a44a0c90071d304280bdef1236" STYLE="fork"/>
            <node TEXT="2. UUID.randomUUID().toString()" ID="a9b6ec228b37eeb0497637cb281e6492" STYLE="fork">
              <node TEXT="数字和字母混合创建的字符串" ID="b1f585e63fcf834ef21bc35d9778ac68" STYLE="fork"/>
            </node>
            <node TEXT="3. main方法只能由main方法调用，其他方法不能调用" ID="83992e4fe0eb0ad50cc79b3205e897ae" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="服务端（服务器）开发流程" ID="68b4d341b456ff9df9a7701e80bb29ae" STYLE="fork">
          <node TEXT="1. Socket注册端口" ID="eb5c6ccab31421b6d7a786b258c74649" STYLE="fork"/>
          <node TEXT="2. 接收客户端的Socket管道连接" ID="0f00c0fa3eea5670e7bd24e88a532162" STYLE="fork">
            <node TEXT="serverSocket.accept（）方法" ID="fb3f7830adbbff2e611f16c54d569c01" STYLE="fork"/>
          </node>
          <node TEXT="3. 从socket通信管道中获得字节输入流" ID="d53709450f64b46dcfb36bf00bceea27" STYLE="fork"/>
          <node TEXT="4. 通过字节输入流，读取客户端发送的消息" ID="150567db96b5806610e2a8dce7b2be34" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="基本通信模型" ID="f2846a413eb0e2ef2a7b51f007263de6" STYLE="fork">
        <node TEXT="同步阻塞" ID="9ba3cf233b89bc2a29a149e7e938e755" STYLE="fork">
          <node TEXT="1. 同步：当前线程要自己进行数据的读写操作" ID="12c29af630215b247bf1ff3bb381f40b" STYLE="fork"/>
          <node TEXT="2. 异步：当前线程可以去做其他事情" ID="77e5ab1799291cbf9c612b9ecc0f4423" STYLE="fork"/>
          <node TEXT="3. 阻塞：在数据没有的情况下，还是要继续等待着读。（排队等待）" ID="024bd753642063551acbc679e947d93d" STYLE="fork"/>
          <node TEXT="4.非阻塞：在数据没有的情况下，会去做其他事情，一旦有了数据再来获取。" ID="30bb0dd03fc21f18c8fbb5eaf51f69a2" STYLE="fork"/>
        </node>
        <node TEXT="1. BIO通信模式：同步阻塞式通信" ID="4f5e159bb17bc9b6306b82ed09409444" STYLE="fork">
          <node TEXT="socket编程（UDP通信，TCP通信）就是" ID="7d937e944c2115ec668a943c7f9c84f5" STYLE="fork"/>
          <node TEXT="服务器：一个连接一个线程，如果连接不做事情，就造成线程资源的浪费" ID="aad1ad74ad72b0a3d6613b962ac6c9be" STYLE="fork"/>
          <node TEXT="性能差：大量线程，大量阻塞" ID="647f92a4e28f85e65da1f02b3152f716" STYLE="fork"/>
          <node TEXT="应用场景：" ID="e9b26ff25f2f797b9e365542889fee6a" STYLE="fork">
            <node TEXT="连接数目比较小且固定的框架" ID="8f954561b3ca8c53aba9aadeb14bef88" STYLE="fork"/>
            <node TEXT="对服务器资源高" ID="5e7742e7d8a9c098dc6ccd6fc9c72242" STYLE="fork"/>
            <node TEXT="jdk1.4 前只能用这个" ID="d4db508964aed1ff7d3c973c1269758b" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="2. 伪异步通信" ID="b5a04f0d3f791ef46d8991242639c26f" STYLE="fork">
          <node TEXT="引入线程池，一个线程负责多个连接" ID="2c1a5dc69aeef13f6e1c7d52d489dd5c" STYLE="fork"/>
          <node TEXT="可控制线程数量，避免系统死机" ID="4d4cbd8bf70bd439f2c67c50c12d12f5" STYLE="fork"/>
          <node TEXT="高并发下性能差：线程数量少，依然存在大量阻塞" ID="e603e46aedcfc276b03b6e48718705a7" STYLE="fork"/>
        </node>
        <node TEXT="3. NIO通信，同步非阻塞IO" ID="6be8326418273a37b790b3da03d11475" STYLE="fork">
          <node TEXT="请求对应一个连接，发送的连接都会注册到多路复用器" ID="da8ca27dc18c4bad0526de9616a8b34c" STYLE="fork">
            <node TEXT="一个线程专门负责接收连接" ID="c664bc8520a22cf586d70d32b7bb4af4" STYLE="fork"/>
            <node TEXT="一个线程负责轮流询问多路复用器中的连接，哪个有数据就开启处理哪个线程" ID="a4a676404d0bdc0b469f8bb375b4730c" STYLE="fork"/>
          </node>
          <node TEXT="性能较好" ID="862ea3ecff3717ef4440d2452d7e2b98" STYLE="fork"/>
          <node TEXT="同步：不断接收连接，以及处理数据" ID="02eb3b763b18781b8f2fcc3fa249fff3" STYLE="fork"/>
          <node TEXT="非阻塞：一个管道没有数据不需要等待，去遍历其他连接，处理有数据的连接" ID="73dd7aad58626a40b805b9b07e6deb10" STYLE="fork"/>
          <node TEXT="应用场景：" ID="2893a9442ae1cdbf57f6b8c2410dfa59" STYLE="fork">
            <node TEXT="连接数目多且轻操作的架构" ID="cb7b8089281e46be966cf687750c1164" STYLE="fork"/>
            <node TEXT="聊天服务器等" ID="98e9133b8928fc011ad9e4979179c36b" STYLE="fork"/>
            <node TEXT="jdk1.4 之后支持" ID="d68ec2a89f3d50b6423684c04bd33ba0" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="4. AIO通信，异步非阻塞IO" ID="d54b754a4cde61ff7f66670e68857df3" STYLE="fork">
          <node TEXT="一个有效请求占用一个线程" ID="dde8a2f7532bd07af2a5151a982dfd39" STYLE="fork"/>
          <node TEXT="异步：服务端线程接收到了客户端管道以后就交给底层处理它的io通信，自己可以做其他事情。" ID="acf221a4da9fbfb51afed9ce8754cf7d" STYLE="fork"/>
          <node TEXT="非阻塞：底层也是客户端有数据才会处理，有数据以后通知服务器应用来启动线程进行处理" ID="dfff543d95d5c9a18abfa76c22bd1a96" STYLE="fork"/>
          <node TEXT="应用场景：" ID="ac06ba2f80d7ef8576ae3f2eea0bb3c7" STYLE="fork">
            <node TEXT="连接数目多且重操作的架构，如相册服务器等，充分调用操作系统参与并发操作" ID="7dd96d68c29cb378621038d21558d295" STYLE="fork"/>
            <node TEXT="jdk1.7 之后开始支持" ID="80b03aa351c15ed3a4b3ddf4658a3d88" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="NIO通信" ID="9fc95cc594688d4596c372df38981a26" STYLE="fork">
        <node TEXT="组成部分" ID="5f74886554e8da1eda225ec21432a140" STYLE="fork">
          <node TEXT="1. Channel 通道" ID="f2673fa388da7ef80d87e58298b5e315" STYLE="fork">
            <node TEXT="特性" ID="e2b00f0198246bbced508e37cabadc2b" STYLE="fork">
              <node TEXT="1. Channel通道是双向的，既能读又能写，而流是单向的" ID="86dfaebd8fac1541ec946bffb5509b6d" STYLE="fork"/>
              <node TEXT="2. 可以进行异步的读写" ID="af256b4168f5ed0da7df0b9202bdcf3e" STYLE="fork"/>
              <node TEXT="3. 读写必须通过Buffer对象" ID="9c961745b9cf7f7e858454f6f312a1ee" STYLE="fork">
                <node TEXT="读和写不会直接读写到Channel通道，而是通过Buffer传递数据，先把数据读写到Buffer中" ID="07a4541d13e4be62b45c596fac6698e2" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="类型" ID="9aa5085fa9823f4c699a79d735556995" STYLE="fork">
              <node TEXT="1. FileChannel：对文件读写" ID="847dd5d7643166e3ff607852a1c69ee8" STYLE="fork"/>
              <node TEXT="2. DatagramChannel：读写UDP网络协议数据" ID="ac61750b813e2ca10752c3f6e2f51a4a" STYLE="fork"/>
              <node TEXT="3. SocketChannel：读写TCP网络协议数据" ID="bbe20aa853cd0f8bc33834bf1ca70d5a" STYLE="fork"/>
              <node TEXT="4. ServerSocketChannel：监听TCP网络连接" ID="e7da4d2eff10c66733da7c675ae4f798" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="2. Buffer 缓冲区" ID="6c9b09784328ec32db50f3fc98823942" STYLE="fork">
            <node TEXT="Buffer实质是数组（缓冲区不是），可以是各种类型" ID="c83eae71accf4b0f2afb1ec5d58cf121" STYLE="fork"/>
            <node TEXT="读写数据的步骤" ID="610a97f89b8f68cd7456c204f2c8d8b2" STYLE="fork">
              <node TEXT="1. 写入数据到Buffer" ID="2fa4257608db865b059259d5890ef4ac" STYLE="fork">
                <node TEXT="写入数据是，Buffer会记录写了多少数据" ID="cfa72e01e885f36bbdce45cd7c185e79" STYLE="fork"/>
              </node>
              <node TEXT="2. 调用flip（）方法" ID="366ad434161e697fcca9f3ac02d4b03f" STYLE="fork">
                <node TEXT="写模式切换到读模式" ID="0c8d93201d53f827620beb10a9680792" STYLE="fork"/>
                <node TEXT="让指针恢复到起始位置 " ID="8e73aa8989c2fdd51c87fbe90f437673" STYLE="fork"/>
              </node>
              <node TEXT="3. 从Buffer中读数据" ID="b1ee5b3ccd15c8b5efa4bc99d5acb348" STYLE="fork"/>
              <node TEXT="4. 调用clear（）方法或compact（）方法" ID="4884e77e19a1e1d92b21501dc6531104" STYLE="fork">
                <node TEXT="读完数据，要清除缓存区，使其可以再次被写入" ID="2770186504313fc6fec1c58dffa71770" STYLE="fork"/>
                <node TEXT="clear方法：清除整个缓冲区" ID="c8f925dd72e742c5af4a6cc162b3f412" STYLE="fork"/>
                <node TEXT="compact方法：清除已读数据，未读数据移动到缓冲区起始处，新写的数据放到未读数据后面" ID="1a464aa9763c93ef9f50a4d81449a1e0" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="Buffer种类" ID="85ef8853334bcc3877d2f07ca5709431" STYLE="fork"/>
          </node>
          <node TEXT="3. Selector 选择器" ID="152804387bdee6ec3cfd2cc7c194e854" STYLE="fork">
            <node TEXT="Selector是一个对象，可以注册到多个Channel中，监听Channel上发生的事件，根据事件决定Channel的读写。通过一个线程管理多个线程" ID="a82b25873bb35e9495d5c97b79bc8ea4" STYLE="fork"/>
            <node TEXT="操作" ID="2c8c89c1924d59e93da32266aea555eb" STYLE="fork">
              <node TEXT="创建Selector" ID="b65e5879d4eb5e28f084d21dd1107ae7" STYLE="fork">
                <node TEXT="Selector selector = Selector.open（）" ID="fb1b9a57d831f88fe99ac28e792d2b5c" STYLE="fork"/>
              </node>
              <node TEXT="注册Channel到Selector" ID="e114a4af927a3138e25713b0a43c7210" STYLE="fork">
                <node TEXT="channel.configureBlocking(false)" ID="6f398ac25e572da94144808051eacc8c" STYLE="fork">
                  <node TEXT="Channel 必须设置成异步模式" ID="0f460123709e894b1b90165b47d31f57" STYLE="fork"/>
                  <node TEXT="SocketChannel可以（由异步模式），FileChannel不行（没有异步模式）" ID="971521b91f3ab5cd71e5fc23c958b222" STYLE="fork"/>
                </node>
              </node>
              <node TEXT="SelectionKey" ID="d51f3f177a8fed956fc773dffabcc6e5" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="NIO2（AIO）通信" ID="9be61111560ed4b96a93f2217bcbeb3d" STYLE="fork"/>
      </node>
    </node>
    <node TEXT="单元测试、反射、注解、动态代理" ID="84bbd428c922613ad82719e70689a9f7" STYLE="bubble" POSITION="left">
      <node TEXT="单元测试框架：Junit" ID="5b5af5de3b0393e94169ad295a5c5e50" STYLE="fork">
        <node TEXT="作用" ID="24220a1992254d87080ec4ca8140fe7b" STYLE="fork">
          <node TEXT="能够独立的测试某个方法或者所有方法的预期正确性" ID="41f60ff76e51543fc76987c7ad5ee66c" STYLE="fork"/>
        </node>
        <node TEXT="单元" ID="0756641c6cd1eab755856f7679ad15a2" STYLE="fork">
          <node TEXT="java中，一个类是一个单元" ID="b2e79717f0dcbd34abae300545cc1df0" STYLE="fork"/>
        </node>
        <node TEXT="测试方法" ID="408bdc26c87656382c3dd668ceffe4c4" STYLE="fork">
          <node TEXT="模拟运行代码" ID="f9f54bbf70f6412d92ae2c2601c34219" STYLE="fork">
            <node TEXT="测试类" ID="4bcf114d69e5218190d650a6dc92c31e" STYLE="fork">
              <node TEXT="命名规范" ID="2024b3399e02ebbb8ed7dabda4e8359c" STYLE="fork">
                <node TEXT="以Test开头，以业务类类名结尾，使用大驼峰命名法" ID="2ac0ac7770a7e7a82165c69554b20fda" STYLE="fork"/>
                <node TEXT="Test写在末尾也可以，通常写在开头" ID="c6eaa5784f485a4eeb679175a6b9a36c" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="测试方法" ID="3c23245681675d0fa056f0e11ee059b5" STYLE="fork">
              <node TEXT="命名规范" ID="a1557553c3b2935eddc7dd8818df0c1d" STYLE="fork">
                <node TEXT="以test开头，以业务方法名结尾" ID="48e327ba9bd149d7036ec38ed019c8eb" STYLE="fork"/>
                <node TEXT="小驼峰命名法" ID="61721d08e199b00a36511b5948121bb6" STYLE="fork"/>
              </node>
              <node TEXT="注意事项" ID="c486714d21d14a1aff7fdb6e9ea070d1" STYLE="fork">
                <node TEXT="必须用public修饰，没有返回值（void），没有参数" ID="62e9513f315b77b2c454f062a847fd08" STYLE="fork"/>
                <node TEXT="必须用@Test修饰" ID="5edebdb97dd40f5b241b28202c9f8e7d" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="运行测试代码" ID="5e6784711a5df0cf94dc02678c3932b3" STYLE="fork">
            <node TEXT="选中方法名 --&gt; 右键 --&gt; Run &apos;测试方法名&apos;  运行选中的测试方法" ID="50514e3766856bef8a7e70cb99885d23" STYLE="fork"/>
            <node TEXT="选中测试类类名 --&gt; 右键 --&gt; Run &apos;测试类类名&apos;  运行测试类中所有测试方法" ID="6e204116cc66a04d80fa820decff7c89" STYLE="fork"/>
            <node TEXT="选中模块名 --&gt; 右键 --&gt; Run &apos;All Tests&apos;  运行模块中的所有测试类的所有测试方法" ID="0375f3b4e444f4a003f3e6f0947412eb" STYLE="fork"/>
          </node>
          <node TEXT="测试结果" ID="11819729b5a4767d701fa8433cd25cb9" STYLE="fork">
            <node TEXT="绿色：测试通过" ID="3daccc513a77799575371fc693f899bc" STYLE="fork"/>
            <node TEXT="红色：测试失败，存在问题" ID="6922ecc1e2e812c91f627d0bcea0099e" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="注解" ID="579ef66e8f9291ea6976b2e10a859e64" STYLE="fork">
          <node TEXT="Junit4" ID="4f2e0a84649ee0ea657157faf79f0588" STYLE="fork">
            <node TEXT="@Test 测试方法" ID="89a94c36beeaf3e7d12e7f7fee1b6175" STYLE="fork"/>
            <node TEXT="@Before：用来修饰实例方法，该方法会在每一个测试方法执行之前执行一次" ID="eebb5f92f1f272e5a06cab4c7a1acb43" STYLE="fork"/>
            <node TEXT="@After：用来修饰实例方法，该方法会在每一个测试方法执行之后执行一次" ID="36fa99441d7cb905ec51d866be117ae0" STYLE="fork"/>
            <node TEXT="@BeforeClass：用来静态修饰方法，该方法会在所有测试方法之前只执行一次" ID="d213f3666719f1fe57641535a1277d4d" STYLE="fork"/>
            <node TEXT="@AfterClass：用来静态修饰方法，该方法会在所有测试方法之后只执行一次" ID="37b32dcca4022cc2b4613bd573ea30c3" STYLE="fork"/>
          </node>
          <node TEXT="Junit5" ID="64195cf92c8d9e31b9b0f78d286231d6" STYLE="fork">
            <node TEXT="@Test 测试方法" ID="75a582783164f52130e27837265154a7" STYLE="fork"/>
            <node TEXT="@BeforeEach：用来修饰实例方法，该方法会在每一个测试方法执行之前执行一次" ID="bda4b465e0ff66440ed1b62ffeb0683d" STYLE="fork"/>
            <node TEXT="@AfterEach：用来修饰实例方法，该方法会在每一个测试方法执行之后执行一次" ID="89e67661487cce77b4d6a29401f905cb" STYLE="fork"/>
            <node TEXT="@BeforeAll：用来静态修饰方法，该方法会在所有测试方法之前只执行一次" ID="bdc16e4609969004d486f5d6bff4b304" STYLE="fork"/>
            <node TEXT="@AfterAll：用来静态修饰方法，该方法会在所有测试方法之后只执行一次" ID="611e4c262e681ee411265b28d70a7622" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="反射" ID="7a2d0c6cc3cdfdb5e97aedc9e00de5fb" STYLE="fork">
        <node TEXT="概述" ID="9514a235c5c237e3e5828e72b317cd06" STYLE="fork">
          <node TEXT="作用" ID="48791575a157dbb5eced7e5bf0e0c7f7" STYLE="fork">
            <node TEXT="可以在程序运行过程中对类进行解剖并操作类中的所有成员(成员变量，成员方法，构造方法)" ID="354bd81118e8d24104871e0bcd661e83" STYLE="fork"/>
          </node>
          <node TEXT="前提" ID="a586f044a1766dad0e9e38a56ec469c3" STYLE="fork">
            <node TEXT="要获得该类字节码文件对象，就是Class对象（核心思想和关键）" ID="f42287807bfd7c95399bdf732224b6aa" STYLE="fork"/>
          </node>
          <node TEXT="应用" ID="3956c13ab6fe104007d99306b8480277" STYLE="fork">
            <node TEXT="开发IDE" ID="a424a7330aee57286539cabd325fd2e7" STYLE="fork"/>
            <node TEXT="设计框架" ID="33071e7e0fc51d64532ea9ff6ba98159" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="作用" ID="285b8d14aae9d8f18d1115f0f8b3e561" STYLE="fork">
          <node TEXT="破坏封装性，破坏泛型约束性，运行时得到类的全部成分" ID="1f4a696b5fe01577ce9ea55e70edb3e4" STYLE="fork"/>
          <node TEXT="做java高级框架" ID="d9bcd263763ca8488b6f5c7f33984cb5" STYLE="fork"/>
        </node>
        <node TEXT="获取Class对象" ID="369f0105d1cb80f6a3313e9b6b56667f" STYLE="fork">
          <node TEXT="三种方式" ID="3bf9ec916cc3218f3a45827ff735e06c" STYLE="fork">
            <node TEXT="1. 通过 类名.class" ID="e6732cafebefc4a48533f1378b92a0a1" STYLE="fork"/>
            <node TEXT="2. 对象名.getClass（）方法" ID="b06908140d4ecc4c2dbc46251d102d9d" STYLE="fork"/>
            <node TEXT="3. Class类.forName（类全名）方法" ID="36a592a14c480c5a5d10aebc4ec02a49" STYLE="fork">
              <node TEXT="直接加载该类的Class文件" ID="762f123a5cf44663ab53a03ed32234d8" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="Class类方法" ID="8586726ec969dc7818dd0eb890d4015c" STYLE="fork">
            <node TEXT="String getSimpleName(); 获得类名字符串：类名" ID="d5ca68f791a5d020ac2a56f004990fc7" STYLE="fork"/>
            <node TEXT="String getName();  获得类全名：包名+类名" ID="38eda4699cb4dd4f115fb1d3de12fece" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="获取构造器" ID="084156ab14b22e4939b9998cb80f9ca7" STYLE="fork">
          <node TEXT="Constructor类" ID="d3ef8df648c4f99d5ee3e29515f72f12" STYLE="fork">
            <node TEXT="类中的一个构造方法就是一个Constructor类对象" ID="9820ff2b92bdb14dc39378af4a0310a8" STYLE="fork"/>
            <node TEXT="常用方法" ID="765bd161df84e44bdd04f310033b7faf" STYLE="fork">
              <node TEXT="T newInstance(Object... initargs)" ID="4c564e767dbeec84b46e5b93fb98bcb6" STYLE="fork">
                <node TEXT="根据指定参数创建对象" ID="7f04ddfa5d795287a919897866453b8f" STYLE="fork"/>
                <node TEXT="T 代表 类对象" ID="a88d583d90838d72f8d8c6dbf2e9a093" STYLE="fork"/>
              </node>
              <node TEXT="void setAccessible(true)" ID="0467b0082fec3cdbeca093caf9f5e40c" STYLE="fork">
                <node TEXT="打开私有构造器的访问权限，暴力反射" ID="c834007ad9bab6541cc57e486678dc76" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="方法" ID="d8ec82bf95d6456d3257ae7fb7624f05" STYLE="fork">
            <node TEXT="getConstructor(Class... parameterTypes)" ID="45974c96f94f8b8ffd00645431b12efc" STYLE="fork">
              <node TEXT="根据参数获取一个构造器" ID="414b6894d91662242e3c53fa8205ab28" STYLE="fork"/>
              <node TEXT="只能获取public修饰的" ID="05e35904588d59dfeddf175e1858a4b2" STYLE="fork"/>
              <node TEXT="用的不多" ID="bd06be50310cd8882ee667a146f456c1" STYLE="fork"/>
            </node>
            <node TEXT="getDeclaredConstructor(Class... parameterTypes)" ID="3c6901451fb4e502cf7d4beadb1516ae" STYLE="fork">
              <node TEXT="根据参数匹配某个构造器" ID="0a5f2d0d36c43781395644ed570155ea" STYLE="fork"/>
              <node TEXT="没有权限限制，常用" ID="3a882778320a16d4ee8e46ca8eb35aa3" STYLE="fork"/>
            </node>
            <node TEXT="getConstructors()" ID="2c3f2723117c094a28d4b1f4d710d32f" STYLE="fork">
              <node TEXT="获取所有构造器" ID="bf5d17c7e48617d348048a54534d114b" STYLE="fork"/>
              <node TEXT="只能获取public修饰的" ID="b17d41a071b4d8452e5ea10a7b8574ea" STYLE="fork"/>
              <node TEXT="几乎不用" ID="63d1a36abcb75ce52e9e2e6e13aaf537" STYLE="fork"/>
            </node>
            <node TEXT="getDeclaredConstructors()" ID="366d8b9eb6aad965dd85fa918a5f324b" STYLE="fork">
              <node TEXT="获取所用构造器" ID="7fe05a0cf64cda084d7aa690fa8e5e38" STYLE="fork"/>
              <node TEXT="没有权限限制，常用" ID="875a9cbf272cf1db0a572910b85ee482" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="获取成员方法" ID="907fa8ab296f8cec1006fe6cc0c6c43f" STYLE="fork">
          <node TEXT="Method类" ID="5c776c36bf3f519cd3b5a7ec6c596504" STYLE="fork">
            <node TEXT="一个Method类对象，代表一个成员方法" ID="e2f013b27cade3423953b06480713694" STYLE="fork"/>
            <node TEXT="常用方法" ID="e9cca57bc0d2c32b35c99a27d0aadcf6" STYLE="fork">
              <node TEXT="Object invoke(Object obj, Object... args)" ID="6257e7d9c901850e14b9a1131101fe28" STYLE="fork">
                <node TEXT="方法对象调用，参数是类对象" ID="d5d4905af897ec1363588b6acffd5a5f" STYLE="fork"/>
                <node TEXT="调用类对象中的方法，返回值是方法的返回值" ID="14b4d745983538a05842ef4f37a1b91b" STYLE="fork"/>
              </node>
              <node TEXT="void setAccessible(true)" ID="2ef1dd002ce9660fd984c7eab6cb760e" STYLE="fork">
                <node TEXT="对于私有成员方法，用此方法打开访问权限" ID="a9cf2378a5af1c3cd1410b31803fb051" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="获取方法" ID="faa669099415e6d253806c4ee7f1a392" STYLE="fork">
            <node TEXT="Method getMethod(String name,Class...args);" ID="2619a21d38e60c1de91a11f26fc21983" STYLE="fork">
              <node TEXT="根据方法名和参数获得一个方法" ID="8e83d3207de1b1bedde5c89d47f88b9d" STYLE="fork"/>
              <node TEXT="只能获得public修饰的" ID="8b6d84814faaf5e44a5d867931a27ad0" STYLE="fork"/>
            </node>
            <node TEXT="Method getDeclaredMethod(String name,Class...args);" ID="43c4ef77169ec7a261369702f149c9cc" STYLE="fork">
              <node TEXT="根据方法名和参数获得一个方法" ID="06ac821793cabe7ddc24de9ad178a970" STYLE="fork"/>
              <node TEXT="不受修饰符限制" ID="cf664602373fd5816ec5e4d05e66de4b" STYLE="fork"/>
            </node>
            <node TEXT="Method[] getMethods();" ID="eb64954ef1b1d4b1f4d9f8b8d5a677e0" STYLE="fork">
              <node TEXT="获得所有方法" ID="3be56612ae54462c5c06d1ec8cdd1dda" STYLE="fork"/>
              <node TEXT="受public限制" ID="c3e71e47a5a9bfaec1319e6515588473" STYLE="fork"/>
            </node>
            <node TEXT="Method[] getDeclaredMethods();" ID="f91562576367bb6d5654afc56cc5199c" STYLE="fork">
              <node TEXT="获得所有方法，不受限制" ID="c12c49c9a8947ddce993a2eb564aea11" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="获取成员变量" ID="1f5ee9108388a7857d573b93c4efbf0f" STYLE="fork">
          <node TEXT="Field类" ID="cb4b13ef3f1fd339b5cdac682e5f80b4" STYLE="fork">
            <node TEXT="一个Field类对象代表一个成员变量" ID="cecfb0da3ff4e5a0b14d96d1c7370e76" STYLE="fork"/>
            <node TEXT="常用方法" ID="23fd6a261ba49b40bf1727be843afe64" STYLE="fork">
              <node TEXT="Class getType()" ID="05f3dfbdfd0447e0eaa074144b38b4b2" STYLE="fork">
                <node TEXT="获取属性类型，返回Class对象" ID="11dd7312d531ff264e1d3afacb1df151" STYLE="fork"/>
              </node>
              <node TEXT="给对象obj的属性赋值，针对不同的类型选取不同的方法" ID="b8e32aaf7d52f2f2ab477e1212c82d29" STYLE="fork"/>
              <node TEXT="获取对象obj对应的属性值的，针对不同的类型选取不同的方法" ID="edcd1797ac17b8ec601c869b4620161b" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="获取方法" ID="f7894830d411136ec2001776a277968b" STYLE="fork">
            <node TEXT="Field getField(String name)" ID="7d62eef07626c83ba508ec0c79092f68" STYLE="fork">
              <node TEXT="根据变量名获取" ID="6f1ddc90ca0e82f712b508056e1e0a94" STYLE="fork"/>
              <node TEXT="public限制" ID="49fb281e652db17ec47d83b0b1755186" STYLE="fork"/>
            </node>
            <node TEXT="Field getDeclaredField(String name)" ID="07d4203bf55fa58905d6fc4ce0abb610" STYLE="fork">
              <node TEXT="根据变量名获取" ID="cf35e694c2139412d8d9a0697e5f0948" STYLE="fork"/>
              <node TEXT="不受限制" ID="01c22013f4df2bf1bef1828926b597f4" STYLE="fork"/>
            </node>
            <node TEXT="Field[] getFields()" ID="3b4a85e4927fa73dcaad3e3716255dc5" STYLE="fork">
              <node TEXT="获取所有变量" ID="22f3129a2bf77320fcfd5e2e813777ef" STYLE="fork"/>
              <node TEXT="public限制" ID="e1add9dad583008d9835cf7b014fcf79" STYLE="fork"/>
            </node>
            <node TEXT="Field[] getDeclaredFields()" ID="abc24494fc2c9f0542a7edd92cdf9871" STYLE="fork">
              <node TEXT="获取所有变量" ID="c48a8b4d03aad039ca89aed34e16df14" STYLE="fork"/>
              <node TEXT="不受限制" ID="39487e5d83d15043c81531374398eb9a" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="注解" ID="157d012935c3b54d28cc5cc60900f69b" STYLE="fork">
        <node TEXT="注解是给编译器或JVM看的，编译器或JVM可以根据注解来完成对应的功能" ID="850a923090e261e0082a3f792eb78d07" STYLE="fork"/>
        <node TEXT="标记可以加在包、类，属性、方法，方法的参数以及局部变量上" ID="dacf00b2f3a3976c162ee477a73f6fe3" STYLE="fork"/>
        <node TEXT="常用注解" ID="546d57e846141e1e119335a48babe045" STYLE="fork">
          <node TEXT="@author：用来标识作者姓名" ID="f1aa3040bd442951a1185b2e2c64609f" STYLE="fork"/>
          <node TEXT="@version：用于标识对象的版本号，适用范围：文件、类、方法" ID="df598502efae2995a29750fc1fe13025" STYLE="fork"/>
          <node TEXT="@Override：重写父类方法" ID="101f959de89d99631b2147db520fe4c7" STYLE="fork"/>
          <node TEXT="@FunctionalInterface：函数式接口，只有一个方法的接口" ID="50250f82f0f17f1fa86355286a4a11d5" STYLE="fork"/>
        </node>
        <node TEXT="自定义注解" ID="77d67881cca296c83c5431a69722e758" STYLE="fork">
          <node TEXT="定义格式" ID="d74d4129252b216532ef8f3a65d57a32" STYLE="fork">
            <node TEXT="@interface  注解名 {}" ID="45896f8bd63f17d40d63f9ffd53e3fd3" STYLE="fork"/>
          </node>
          <node TEXT="属性格式" ID="80b42c5d3d6f537b77fa0554bdc51ef7" STYLE="fork">
            <node TEXT="数据类型 属性名()    &lt; default 默认值&gt;" ID="768cb07499203c252b4c2348dff28ec1" STYLE="fork"/>
            <node TEXT="如果属性有默认值，则使用注解时，不必须赋值；如果没有默认值则必须赋值" ID="521713bbcbcbcee7939d968bc81d2a5a" STYLE="fork"/>
          </node>
          <node TEXT="特殊属性 Value" ID="442c6520a6e0a9dc6dbdb2f31adba49f" STYLE="fork">
            <node TEXT="如果注解中只有一个属性时，一般都会将该属性名命名为value" ID="160ac72404ccb281f09b5253d5c89304" STYLE="fork"/>
            <node TEXT="如果注解中只有一个属性且名字叫value，则在使用该注解时可以直接给该属性赋值，不需要写属性名" ID="3a9731ae9c5730e0a875f061e3d53332" STYLE="fork"/>
            <node TEXT="如果注解中除value属性之外还有其他属性且只要有一个属性没有默认值，则在给属性赋值时必须写出属性名value" ID="dec9900f9270e043f692788e8b0462a5" STYLE="fork"/>
          </node>
          <node TEXT="元注解" ID="c9dc46bb52e1c1a50fa3b3c57dd71994" STYLE="fork">
            <node TEXT="注解自定义注解" ID="e4c4928e0519353671b3e442c8fca36a" STYLE="fork"/>
            <node TEXT="@Target：用来标识注解使用的位置" ID="e866ab1c3d81b08353946c8358d20b0e" STYLE="fork">
              <node TEXT="使用方法" ID="8d6a6337864b4adeeb5d3d1846d8aecf" STYLE="fork">
                <node TEXT="@Target（ElementType.TYPE），只能用在类和接口" ID="0aae09aea3b6aa7a251b5827d42b26eb" STYLE="fork"/>
              </node>
              <node TEXT="可使用的值定义在ElementType枚举类中" ID="b986f792de7412cd6ce209ab85fe0ebd" STYLE="fork">
                <node TEXT="TYPE，类，接口" ID="33e2748459aa64a1527925ff6ae986ea" STYLE="fork"/>
                <node TEXT="FIELD, 成员变量" ID="81d8a0ac4ffa78ed3dff62b62ba92b90" STYLE="fork"/>
                <node TEXT="METHOD, 成员方法" ID="131d2a5782075bf4f7bb7b0c9fd8f9d4" STYLE="fork"/>
                <node TEXT="PARAMETER, 方法参数" ID="6483534d1e5a47f3255621ddf659a0e8" STYLE="fork"/>
                <node TEXT="CONSTRUCTOR, 构造方法" ID="c1dde2adb43134df938eb50646523b4e" STYLE="fork"/>
                <node TEXT="LOCAL_VARIABLE, 局部变量" ID="8240692349429e91e749094f70309279" STYLE="fork"/>
              </node>
            </node>
            <node TEXT="@Retention：用来标识注解的生命周期(有效范围)" ID="1a1ec69430be798939fa98623f41738d" STYLE="fork">
              <node TEXT="使用方法" ID="f0bd5d9cd57a0f0e60192503169bbf31" STYLE="fork">
                <node TEXT="@Retention（RetentionPolicy.RUNTIME），在所有阶段都能用" ID="ac05b07b269381e3753b4c9c0f1f9c10" STYLE="fork"/>
              </node>
              <node TEXT="可使用的值定义在RetentionPolicy枚举类中" ID="775d830b0184ae6a64aa7bc5ea75d049" STYLE="fork">
                <node TEXT="SOURCE：注解只作用在源码阶段，生成的字节码文件中不存在" ID="184867f4b68f9f41386e4e5c173171b3" STYLE="fork"/>
                <node TEXT="CLASS：注解作用在源码阶段，字节码文件阶段，运行阶段不存在，默认值" ID="2a6adf084809639625513dafb12357cd" STYLE="fork"/>
                <node TEXT="RUNTIME：注解作用在源码阶段，字节码文件阶段，运行阶段" ID="a1eb39609b103f9d91730f4627723fe3" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
        <node TEXT="注解解析" ID="531855a387496ddd3f52bdc3e4abe527" STYLE="fork">
          <node TEXT="Annotation: 注解类，该类是所有注解的父类" ID="d8acfed1210fb5e1302d39cc1f23b33d" STYLE="fork"/>
          <node TEXT="AnnotatedElement:该接口定义了与注解解析相关的方法" ID="49a25af27792029deac42a9241c8d39a" STYLE="fork">
            <node TEXT="常用方法" ID="f891b11d138af1fa372704007e93ebdb" STYLE="fork">
              <node TEXT="T getAnnotation(Class annotationClass)" ID="1382e7d606b0970daae679c0e383e52c" STYLE="fork">
                <node TEXT="根据注解类型获得对应注解对象" ID="d33ead82c261742d4c5e6f73c59cb9b5" STYLE="fork"/>
              </node>
              <node TEXT="Annotation[]	getAnnotations()" ID="3a5b0fd30d80051a919aa1a023f8dcb7" STYLE="fork">
                <node TEXT="获得当前对象上使用的所有注解，返回注解数组，包含父类继承的" ID="8c82f49215abb85dcd101b13b5bcf091" STYLE="fork"/>
              </node>
              <node TEXT="Annotation[]	getDeclaredAnnotations()" ID="d2112a2d082ffed28162a8d4d775b4e5" STYLE="fork">
                <node TEXT="获得当前对象上使用的所有注解，返回注解数组,只包含本类的" ID="dc7f819b244dba4da5daf9ba26780f54" STYLE="fork"/>
              </node>
              <node TEXT="boolean isAnnotationPresent(Class annotationClass)" ID="0beff77c346a71d950eb64d3c25f7b77" STYLE="fork">
                <node TEXT="判断当前对象是否使用了指定的注解，如果使用了则返回true，否则false" ID="8deb0507313720dca2b6126b832df77f" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="解析流程" ID="844603fc642ef4ed261a1a0fedbe2eda" STYLE="fork">
            <node TEXT="1. 获得类/方法/变量的对象" ID="d65c0250378d4911cea70e9bf7178a9d" STYLE="fork"/>
            <node TEXT="2. 判断是否用某个注释修饰" ID="d659c90a9aefa478665d086f9c1456f2" STYLE="fork"/>
            <node TEXT="3. 获取注释对象" ID="7116a0229c6792ec2f77f0b452ea5113" STYLE="fork"/>
          </node>
        </node>
      </node>
    </node>
    <node TEXT="XML、Dom4j" ID="715c91d1d99bf1e9347e37a4db1d53d3" STYLE="bubble" POSITION="left">
      <node TEXT="XML" ID="7f0cb04155819b85ea05e7cd7673ccfb" STYLE="fork">
        <node TEXT="作用" ID="d98633694fa018d0fe13790b08dab5c5" STYLE="fork">
          <node TEXT="1. 存储数据，作为数据交换的载体" ID="c4fd2100d0eb586f19eab88c87597996" STYLE="fork"/>
          <node TEXT="2. 作为配置文件" ID="86b15037ab3db5c63b925b49cb042bf2" STYLE="fork"/>
        </node>
        <node TEXT="构成部分" ID="e4f475606a3cd7e679f67ef833a977d0" STYLE="fork">
          <node TEXT="声明" ID="7ab462deda5d280fc9a5869f705c0977" STYLE="fork">
            <node TEXT="" ID="6f54697e2180961121554d38f2902959" STYLE="fork"/>
          </node>
          <node TEXT="元素（标签）" ID="497ac6a7e0f49cb61325ff9bae6c47f9" STYLE="fork">
            <node TEXT="空元素只有标签，而没有结束标签，但元素必须自己闭合，例如：&lt;sex/&gt;" ID="3f313fdf1299b3d9eca0b50bcba1d266" STYLE="fork"/>
            <node TEXT="命名规则" ID="a777fed62c912f8f73e7cf920a0cffd1" STYLE="fork">
              <node TEXT="区分大小写" ID="d00c7ef1660cfad67243453f04328b43" STYLE="fork"/>
              <node TEXT="不能使用空格，冒号" ID="f0f7b6085cbdc0adbc5f0c39947e3e24" STYLE="fork"/>
            </node>
            <node TEXT="有且仅有一个跟元素" ID="528b604ba543ca389bba656982510fe9" STYLE="fork"/>
          </node>
          <node TEXT="属性" ID="955e08b0e8b5bf027183d451dbe1fd3b" STYLE="fork">
            <node TEXT="元素的一部分" ID="a531cb9b75529d6cc904245b852f55d7" STYLE="fork"/>
            <node TEXT="属性名=“属性值”" ID="8823a5e054cff0dbc7e0e55ef2ee2281" STYLE="fork"/>
            <node TEXT="一个元素可以有0~N个属性，但一个元素中不能出现同名属性" ID="1c519bac8ebc28546504c6d4ab041140" STYLE="fork"/>
          </node>
          <node TEXT="注释" ID="fd535e962fe4cbf1a71cf0cdeeddafaf" STYLE="fork">
            <node TEXT="以&lt;!--开始，以--&gt;结束" ID="ae70f16557adf93756091e9efd6aee4f" STYLE="fork"/>
            <node TEXT="&lt;!-- 注释内容 --&gt;" ID="9d059e8e91cfbc30805e816b41432836" STYLE="fork"/>
          </node>
          <node TEXT="转义字符" ID="426e90a11ddd95f9c1b7b3a30c5f7c34" STYLE="fork">
            <node TEXT="" ID="93836a2cca5058f4437e828412f6a254" STYLE="fork"/>
          </node>
          <node TEXT="字符区" ID="1a3c83002baaf19a7fc7559e284e50cf" STYLE="fork">
            <node TEXT="不由 XML 解析器进行解析的文本数据" ID="a93a0f11fea0df4cb211fd538245d4c9" STYLE="fork"/>
            <node TEXT="由 &quot;&lt;![CDATA[&quot; 开始，由 &quot;]]&gt;&quot; 结束" ID="1a3f9e7d99ccd0709212be0d3617ded2" STYLE="fork"/>
            <node TEXT="当大量的转义字符出现在xml文档中时，会使XML文档的可读性大幅度降低。这时需要使用CDATA段" ID="7c32300a8dd8acecc607f675c928d3e1" STYLE="fork"/>
            <node TEXT="注意事项" ID="b0cacd3cb154837d97bc5e2b331ecff6" STYLE="fork">
              <node TEXT="CDATA 部分不能包含字符串 &quot;]]&gt;&quot;。也不允许嵌套的 CDATA 部分" ID="dcb1a7eca6a721b51978ae949d84471d" STYLE="fork"/>
              <node TEXT="结尾的 &quot;]]&gt;&quot; 不能包含空格或折行" ID="23d679560a6a004e51e8d620fc11fba2" STYLE="fork"/>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="XML约束" ID="0c84547f25ee6aa2e42fa5b8b32c9bd7" STYLE="fork">
        <node TEXT="DTD：文档类型定义" ID="e0201414619c08846ad0c9a612e3b99a" STYLE="fork">
          <node TEXT="定义在 XML 文档中出现的元素的次序、元素如何相互嵌套以及XML文档结构的其它详细信息" ID="44560ff3602c731a4ac6ab4384fdbcf3" STYLE="fork"/>
          <node TEXT="DTD约束无法对具体数据类型进行约束,所以开发工具没有任何错误提示" ID="3f9830653b924d9078bf97dfc7b8d275" STYLE="fork"/>
          <node TEXT="语法" ID="f600d6d09b3f977be5e5a999315722ce" STYLE="fork">
            <node TEXT="使用范围" ID="54d712f1e3d8d6cd38f839384cb8a402" STYLE="fork">
              <node TEXT="内部DTD，在XML文档内部嵌入DTD，只对当前XML有效" ID="887b8477257e39222732ae501be3aaec" STYLE="fork"/>
              <node TEXT="外部DTD—本地DTD，DTD文档在本地系统上，企业内部自己项目使用" ID="cd07dfba2f5f0f728b0a38e952ac8604" STYLE="fork"/>
              <node TEXT="外部DTD—公共DTD，DTD文档在网络上，一般都有框架提供 , 用的最多" ID="1f5caaabd076538d5452f5ca3dc22215" STYLE="fork"/>
            </node>
            <node TEXT="元素" ID="b7e514d72fea88c775c4ab75ab633a04" STYLE="fork">
              <node TEXT="嵌套层级" ID="6192b4918922d0b2ccf1acbd263a2f7c" STYLE="fork">
                <node TEXT="语法" ID="e84f57c0fa6d6ff7c92e9e6b76fc76a8" STYLE="fork"/>
                <node TEXT="代码" ID="b12910411eed310a978bdd09d8591411" STYLE="fork"/>
              </node>
              <node TEXT="元素数据" ID="d33c343c138b5e47cf12df6532b5b58d" STYLE="fork">
                <node TEXT="语法" ID="9d7b3d42e11ff3255682aac5b963edf8" STYLE="fork"/>
                <node TEXT="标签类型" ID="07a728f943ee441106c5eceba6139382" STYLE="fork"/>
                <node TEXT="代码" ID="cd803f1c97506287f286bfca6a05693c" STYLE="fork"/>
              </node>
              <node TEXT="数量表示" ID="1f7de990fc697bd97141ddbdbed89b8a" STYLE="fork">
                <node TEXT="含义" ID="b47bf9c47228d276a59bcee3a19cce0b" STYLE="fork"/>
              </node>
              <node TEXT="属性声明" ID="af66a464ff99f6195a46d6eee7ea03ec" STYLE="fork">
                <node TEXT="语法" ID="e4f6396cf3857fed14430b6e1810c6bc" STYLE="fork"/>
                <node TEXT="类型" ID="604cfd6ecbf919ec27e50d8589965aec" STYLE="fork"/>
                <node TEXT="属性要求" ID="7ca510305612942e436da0d0eb634a3c" STYLE="fork"/>
                <node TEXT="代码实例" ID="2c1367c1a1ebbbfb7af3dae15c6bf012" STYLE="fork"/>
              </node>
            </node>
          </node>
          <node TEXT="使用" ID="a480bfd20cf696806efdb2c3a57be37a" STYLE="fork">
            <node TEXT="编写dtd文档，并引入（一般不会自己编写，只要会用就行）" ID="ef24afaa4729bd55ae32a0c3726732f4" STYLE="fork"/>
            <node TEXT="约束内容" ID="e51a9d9ada075681149f6a082fe60f2d" STYLE="fork"/>
            <node TEXT="指定使用的dtd约束文件" ID="eaafb14dc2ebbb64fd9a45ab5dbcbe3b" STYLE="fork"/>
            <node TEXT="不按约束会报错" ID="3127ec67552387382f155af3242761f8" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="schema：XSD，比DTD强大" ID="16a291c61eaa179c28f356c4f75ae928" STYLE="fork">
          <node TEXT="本身也是XML文档，单Schema文档扩展名为xsd" ID="d535138d1215eb7667d724c2814d0510" STYLE="fork"/>
          <node TEXT="使用" ID="83b361eca0be37f4503c0e1ca148032f" STYLE="fork">
            <node TEXT="引入" ID="e871ea9f402d7861162595ab46b2b401" STYLE="fork"/>
          </node>
          <node TEXT="命名空间：解决元素和属性的名称冲突问题" ID="d035f50f932b386a2fce0b91425018b7" STYLE="fork">
            <node TEXT="一个XML文档只能使用一个DTD文件,但一个XML文档可以使用多个Schema文件" ID="05b61581272967771b72c746f653cae6" STYLE="fork"/>
            <node TEXT="若使用多个Schema文件，则使用相同的元素名称就会冲突" ID="0f30193e18624cb392f87fe8f9379d14" STYLE="fork"/>
            <node TEXT="实例" ID="7b71543c7a99fb10be4568bf542a9620" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="XML解析（DOM4j）" ID="09dfab9289c4c3b2b6111a303a78688f" STYLE="fork">
        <node TEXT="使用DOM4j需要导入dom4j-版本号.jar包" ID="bc9d21ee15072585e7040f08410145a5" STYLE="fork"/>
        <node TEXT="解析方式" ID="62f07c1191dd9e4b2c17703da42dabbb" STYLE="fork">
          <node TEXT="DOM解析" ID="3735862e8f70a057e4dabf5c7b4f355e" STYLE="fork">
            <node TEXT="优点：将整个XML文件加载到内存中，生成一棵DOM树。随意访问、修改和删除树上任何一个节点，适合程序开发，纯面向对象" ID="8c75c24311d1e26b15c328f9e646e940" STYLE="fork"/>
            <node TEXT="缺点：占内存" ID="ccd05f424834d53b2b4a9f63ee058dc1" STYLE="fork"/>
            <node TEXT="java中的DOM解析开发包" ID="42bac7964410ed1ac69d39806b0cfb58" STYLE="fork"/>
          </node>
          <node TEXT="SAX解析" ID="aeaa262c66afb3970cedc8bd3dd689e3" STYLE="fork">
            <node TEXT="优点：事件驱动型解析方式，读取一行就解析一行，可以读取任一大小的XML文件" ID="fadb6a1e04323616e424a2bbec354b3f" STYLE="fork"/>
            <node TEXT="缺点：访问过得元素不能再访问，不能修改，只能查找" ID="01529a3687bb073b0c37da92bfcfbe7b" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="DOM解析" ID="2244c220c189716dbd9997b792b6bf4b" STYLE="fork">
          <node TEXT="生成DOM树" ID="80af3cb46015f2323d1d390c3740e46a" STYLE="fork">
            <node TEXT="过程" ID="3c7011d23af54ebab4c6b46a3adfddd0" STYLE="fork">
              <node TEXT="XML DOM 和 HTML DOM一样，将整个XML文档加载到内存" ID="11ba3423cd9ad9dd31a53def27b56edb" STYLE="fork"/>
              <node TEXT="成一个DOM树，并获得一个Document对象，通过Document对象对DOM进行操作" ID="ceed3d2e172d1465ecbc25d92d646efc" STYLE="fork"/>
            </node>
            <node TEXT="示例" ID="c557a6bb146206a8be19e92db2f6d18b" STYLE="fork">
              <node TEXT="代码" ID="3a71db466f991f7d1ea9cbc8c29e65e6" STYLE="fork"/>
              <node TEXT="DOM树" ID="a819abea3e2a3d3d95f5a0d0a89b6270" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="DOM树中的API（DOM4j是第三方框架，需要提前导入）" ID="49019100b41bfb29ca7fd42bc66732f9" STYLE="fork">
            <node TEXT="Document：当前解析的XML文档对象" ID="eccd87aeace662d1e1f3e1df51992588" STYLE="fork"/>
            <node TEXT="Node：XML中节点，它是其它所有节点对象的父接口" ID="ae4f77224e9cdc2c5c5c1dfa32dd6490" STYLE="fork"/>
            <node TEXT="Element：代表一个元素(标签)" ID="9bcf93aa8e2a72255d2acfbf59670bc5" STYLE="fork"/>
            <node TEXT="Attribute：代表一个属性" ID="fcb385042f035e4b69940de2daf79247" STYLE="fork"/>
            <node TEXT="Text：代表标签中文本" ID="a4b1f35cf6275e65412bb43379a7fa4e" STYLE="fork"/>
          </node>
          <node TEXT="获取Document对象" ID="0cf5796bac30dc0061bd4e609cb15263" STYLE="fork">
            <node TEXT="1. 创建一个SAXReader对象（dom4j的解析器对象：代表整个dom4j框架）" ID="4e477b29d2ac8c743ae95c5f50021e65" STYLE="fork"/>
            <node TEXT="2. 从类路径下加载xml文件，得到输入流对象" ID="0e22de29c99f9e2ee7954ef6aa2f5fce" STYLE="fork"/>
            <node TEXT="3. 通过 SAXReader对象的read(InputStream in )方法，从输入流中读取，返回文档对象" ID="ada3ea992a6e510628272138be312940" STYLE="fork"/>
          </node>
          <node TEXT="获取根元素" ID="ba90d8a4a7ffdfb8008046103857c928" STYLE="fork">
            <node TEXT="Element getRootElement()" ID="772bead7b2f2dcfb0dba5cd246dc9d87" STYLE="fork">
              <node TEXT="通过文档对象得到根元素" ID="6fa034b726bb3532b44e1340e87af902" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="获取子元素对象" ID="83654593dedd5750fad87514299d4129" STYLE="fork">
            <node TEXT="常用方法" ID="afa45b3f6a54d820163f92b7cc0d7aab" STYLE="fork"/>
          </node>
          <node TEXT="获取属性对象" ID="753f899f7a5adf1a16949dad820879d2" STYLE="fork">
            <node TEXT="Attribute对象" ID="89900ab52f9e9b5b9ebfed0bb33023d8" STYLE="fork">
              <node TEXT="代表一个标签的属性，如：id=&quot;1&quot;" ID="4a8f32fc7477db3ea75e8ad6719af0a6" STYLE="fork"/>
            </node>
            <node TEXT="方法" ID="17e6885db8a7a2eb19ce4e23469eed1a" STYLE="fork">
              <node TEXT="Element中与属性相关方法" ID="064285c7319ac251f965e29526a4b525" STYLE="fork"/>
              <node TEXT="Attribute常用方法" ID="7a3b53541022373cb2fa4832a091c347" STYLE="fork"/>
            </node>
          </node>
          <node TEXT="获取元素中文本内容" ID="b7881c2286541a852b6c71286154e4ce" STYLE="fork">
            <node TEXT="方法" ID="c90c2c68413ee47345006714f00e26b8" STYLE="fork">
              <node TEXT="String getText()" ID="7222963056ddf696abbc61153e7792c0" STYLE="fork">
                <node TEXT="得到元素中文本" ID="c83b3680d34443448953e43d5a559b30" STYLE="fork"/>
              </node>
              <node TEXT="String elementText(元素名)" ID="75065a373d342e403366a0a17da633eb" STYLE="fork">
                <node TEXT="得到子元素中文本" ID="06433832225843437bceffccb5c6bfaa" STYLE="fork"/>
              </node>
              <node TEXT="String elementTextTrim(元素名)" ID="b752a2eaebe3ee164e8152a4ae484aca" STYLE="fork">
                <node TEXT="得到子元素中文本，去掉先后空格" ID="5d4170ae86e394685cf4fb854cf0b64e" STYLE="fork"/>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node TEXT="XPath表达式" ID="b79a43625e0929d4d75348d58e3822e3" STYLE="fork">
        <node TEXT="需要导入 jaxen-1.1.2.jar包" ID="6e07e061a134842dbca57f6b813b7101" STYLE="fork"/>
        <node TEXT="常用方法" ID="f00cfbab89bde92e72cbc7b5cc4cc9c7" STYLE="fork">
          <node TEXT="Node   selectSingleNode(String xpath)" ID="7359d8e2f84f7383f1e7ba32264e6ca8" STYLE="fork">
            <node TEXT="查找指定节点（元素、属性、文本等）" ID="b01de75c0c94f35fef5c21ffa34bb341" STYLE="fork"/>
          </node>
          <node TEXT="List   selectNodes(String xpath)" ID="2f8586456b3bbf6a2b27a951cb00a656" STYLE="fork">
            <node TEXT="查找符合条件的多个节点" ID="09572c7a92c9fb05e6be970eef04a914" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="路径" ID="bd97e64528953134293d621bdc63410b" STYLE="fork">
          <node TEXT="绝对路径" ID="c0a8020440bf043488c762516232e9e4" STYLE="fork">
            <node TEXT="/根元素/子元素/孙元素" ID="21371adcdcac00dc2a2f277a9b12d9f2" STYLE="fork"/>
            <node TEXT="从根元素开始，不能跨级" ID="fd84b4f429c5baece954ba1bfdc3fbd7" STYLE="fork"/>
          </node>
          <node TEXT="相对路径" ID="287922e1a8b291164aa33aecf44a6acb" STYLE="fork">
            <node TEXT="./子元素/孙元素" ID="41be30b2862f824e19a4162ea5b99a30" STYLE="fork"/>
            <node TEXT="从当前元素开始" ID="bb8c6de09c9c3e6be409bcc14d537ed7" STYLE="fork"/>
          </node>
        </node>
        <node TEXT="全局搜索" ID="86a5da95f0de8d03d4797031ac113375" STYLE="fork">
          <node TEXT="语法" ID="0a8ee82a96c1175941e7d289c3728efd" STYLE="fork">
            <node TEXT="//contact" ID="267c26d7a1335714b4ad7e136e0e869c" STYLE="fork">
              <node TEXT="找contact元素，无论元素在哪里" ID="730103d67b57fb6d9a73f0a6bc69fc4e" STYLE="fork"/>
            </node>
            <node TEXT="//contact/name" ID="bd744ad3fd4bc270ca76b0076aed1d3c" STYLE="fork">
              <node TEXT="找contact，无论在哪一级，但name一定是contact的子节点" ID="fa31a23fefff270025313717768f4061" STYLE="fork"/>
            </node>
            <node TEXT="//contact//name" ID="bee89a04fd9b3a12647e2ae6154862ff" STYLE="fork">
              <node TEXT="contact无论在哪一种，name只要是contact的子孙元素都可以找到" ID="b81c56b538be870ce3372c92476d2339" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="属性查找" ID="62528888502e2f24d40ce85c8b26c72d" STYLE="fork">
          <node TEXT="语法" ID="f5d71c39eeaab34d2810f6379b41108f" STYLE="fork">
            <node TEXT="//@属性名" ID="c0e3d08ed367ee9df13d8a33d9aba95e" STYLE="fork">
              <node TEXT="查找属性，不论那个元素，有这个属性即可" ID="53c780fe29658263af5a080b0889c371" STYLE="fork"/>
            </node>
            <node TEXT="//元素[@属性名]" ID="76a8948f7e2f066ceec0236615504bee" STYLE="fork">
              <node TEXT="只用元素和属性名查找" ID="9e7eca09be9bc749c7f65eca4babe308" STYLE="fork"/>
            </node>
            <node TEXT="//元素[@属性名=属性值]" ID="902b78e0e3a756a697e8bdc1505f7dd5" STYLE="fork">
              <node TEXT="" ID="70c23012b1d7b1c1a382854dd20666b9" STYLE="fork"/>
            </node>
          </node>
        </node>
        <node TEXT="总结" ID="15b19a2dae85348a019781ebc9cc509b" STYLE="fork">
          <node TEXT="绝对路径" ID="57a8164aed4b69c91e225b5c3e97f81b" STYLE="fork">
            <node TEXT="/xxx/xxx/xxx" ID="706b14723236353da57cb4fce073a56b" STYLE="fork"/>
          </node>
          <node TEXT="相对路径" ID="4270bb9c9b3c4ba3bb8bcf620d12c916" STYLE="fork">
            <node TEXT="./xx/xx" ID="58ea371dae16cb1eebb1a26ad56ce47b" STYLE="fork"/>
          </node>
          <node TEXT="全文路径" ID="aa315bb216327d8ace3da5433355c2a6" STYLE="fork">
            <node TEXT="//xxx" ID="9a1d6f700bda9b9fff2ee61ffed92b06" STYLE="fork"/>
            <node TEXT="//xxx/xxx" ID="176d8af86a69086463728b3f9a916a1e" STYLE="fork"/>
            <node TEXT="//xxx//xxx" ID="62c6ba063667041845607c271d49be9f" STYLE="fork"/>
          </node>
          <node TEXT="属性查找" ID="24e183c55c14713133ebee042cf7e2a5" STYLE="fork">
            <node TEXT="//@属性名" ID="f1a1c42f251ebebfca0350dceca26206" STYLE="fork"/>
            <node TEXT="//元素[@属性名]" ID="978bacdbb14927858bc5f76da8b970d1" STYLE="fork"/>
            <node TEXT="//元素[@属性名=值]" ID="e313f2fb59a45cbd15f4a31397dcb4c1" STYLE="fork"/>
          </node>
        </node>
      </node>
      <node TEXT="工厂模式" ID="772b73ac952c6aa64edcb0e7fdc467f8" STYLE="fork">
        <node TEXT="用一个类专门用来创建对象，类似于工厂生产" ID="4efd58cd06bce221394cf93b53999095" STYLE="fork"/>
        <node TEXT="作用" ID="912f346336b1c6577871bf39bbcfd44d" STYLE="fork">
          <node TEXT="解决类与类之间的耦合问题" ID="9ee025cf51769846f8fd2d4d27d12385" STYLE="fork"/>
        </node>
        <node TEXT="示例步骤" ID="82a0cb4d4e168c4c0cdc6ca7a48d8a86" STYLE="fork">
          <node TEXT="" ID="a1b43c0b820d839d7b4f7b4ef1434777" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="装饰模式" ID="cdd813413dbdcaed11a50e20fbc14943" STYLE="fork">
        <node TEXT="不改变原类的基础上对类中的方法进行扩展增强" ID="7a50163c1737f718f7cfaef5bdb0308f" STYLE="fork"/>
        <node TEXT="步骤" ID="2e9bd5a4c201a469db7d6c52714f228f" STYLE="fork">
          <node TEXT="1. 装饰类和被装饰类实现相同的接口（继承相同父类的方式也可以）" ID="04aca36320045449a242fd00dcbf6b4e" STYLE="fork"/>
          <node TEXT="2. 在装饰类中传入被装饰类的对象" ID="a2049593228d01e95e332f07a86bf5b4" STYLE="fork"/>
          <node TEXT="3. 在装饰类中对需要扩展的方法进行方法重写" ID="82e9396fe203842dcae2f9d91092bdce" STYLE="fork"/>
          <node TEXT="4. 在装饰类中不需要扩展的方法通过被装饰类调用" ID="562db0ca3241222f16952218103aa5ae" STYLE="fork"/>
        </node>
      </node>
      <node TEXT="commons-io工具包" ID="e3aabf1010c027abefdb53327575e2e8" STYLE="fork">
        <node TEXT="先导入 commons-io-版本.jar包" ID="f740dfa2f3d8ca27d7f641bd7ad4ee42" STYLE="fork"/>
        <node TEXT="提供了一组有关IO操作的类库，可以挺提高IO功能开发的效率" ID="9f3367f75d4aa5a9ff586c6c4d5facd9" STYLE="fork"/>
        <node TEXT="java自带也有相关类库：Files" ID="88515fbc8a902d3b6717a3ac9c46c37d" STYLE="fork"/>
      </node>
      <node TEXT="Base64" ID="403bb7863079591950d840322372ec00" STYLE="fork">
        <node TEXT="三种静态编解码器" ID="9d055175410f03f1d8712911be737ae3" STYLE="fork">
          <node TEXT="基本：适用普通文件" ID="16df465bbb18180cf3043e14daf233c3" STYLE="fork"/>
          <node TEXT="URL：适用于网络地址，参数等" ID="553795648c5fe57d075e539b905d7761" STYLE="fork"/>
          <node TEXT="MIME：适用于大文件" ID="62b9c88964af5c8664f3e7a748737ca1" STYLE="fork"/>
        </node>
      </node>
    </node>
  </node>
</map>