package 并发包CyclicBarrier;

/**
 循环屏障
 */

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args) {
//                                              第一个参数是触发屏障的线程数，第二参数是触发屏障后执行的线程任务
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3 , new Meeting());

        for (int i = 1 ; i <= 6 ; i++) {
            new Employ(cyclicBarrier,"线程"+i).start();
        }
    }
}

//触发屏障后，执行的线程任务
class Meeting implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"组织主持会议");
    }
}

class Employ extends Thread {
    private CyclicBarrier cyclicBarrier ;

    public Employ(CyclicBarrier cyclicBarrier , String name) {
        super(name);
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {


        try {
//            去会议室的路上需要2s
            Thread.sleep(2000);

//        员工线程执行的操作
            System.out.println(Thread.currentThread().getName()+"进入会议室");

//            告诉循环屏障执行完成，然后当前线程被回收
            cyclicBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}