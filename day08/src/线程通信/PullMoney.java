package 线程通信;

public class PullMoney extends Thread {

    private String name ;
    private Acount acount ;

    public PullMoney() {}

    public PullMoney(String name, Acount acount) {
        super(name);
        this.acount = acount;
    }


    @Override
    public void run() {
//        一直取钱，有钱就取
        while (true) {
//            取钱：10000
            acount.pullMoney(10000);
            try {
//                当前线程睡眠3s
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
