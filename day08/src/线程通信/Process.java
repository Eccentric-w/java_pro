package 线程通信;

public class Process {

//    账户是共享且唯一的
    private static Acount acount = new Acount("12306",0);

    public static void main(String[] args) {

//        创建三个存钱的线程
        new PushMoney("岳父",acount).start();
        new PushMoney("亲爸",acount).start();
        new PushMoney("干爹",acount).start();

//        创建两个取钱的线程
        new PullMoney("小红",acount).start();
        new PullMoney("小明",acount).start();

    }

}
