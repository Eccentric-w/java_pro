package 线程通信;

public class PushMoney extends Thread {

    private String name ;
    private Acount acount ;

    public PushMoney() {
    }

    public PushMoney(String name, Acount acount) {
        super(name);
        this.acount = acount;
    }

    @Override
    public void run() {
//        一直存钱，账户没钱就存
        while (true){
//            存钱：10000
            acount.pushMoney(10000);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
