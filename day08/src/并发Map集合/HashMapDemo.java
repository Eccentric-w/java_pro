package 并发Map集合;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo {

    public static Map<String,Integer> hashMap = new HashMap<>();

    public static void main(String[] args) {

//        创建线程任务
        MyRunnable myRunnable = new MyRunnable();

        Thread thread0 = new Thread(myRunnable,"线程0");
        Thread thread1 = new Thread(myRunnable,"线程1");

        thread0.start();
        thread1.start();

        try {
//            主线程遇到这条语句，会让出CPU资源，并且不再和线程0、线程1争夺资源，直到线程0、1执行结束
            thread0.join();
            thread1.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        join方法确保线程执行完，再最后统计集合中的元素
        System.out.println("元素个数是:"+hashMap.size());

    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0 ; i < 500000 ; i++) {

//            通过类名调用集合，添加数据
//            键需要唯一，用线程名加数字，可以保证键的唯一性
            HashMapDemo.hashMap.put(Thread.currentThread().getName()+i , i);
        }
    }
}