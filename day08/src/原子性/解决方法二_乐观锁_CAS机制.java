package 原子性;

import java.util.concurrent.atomic.AtomicInteger;

public class 解决方法二_乐观锁_CAS机制 {
    public static void main(String[] args) {

        Test2 test = new Test2();

        for (int i = 0 ; i < 100 ; i++) {
            new Thread(test).start();
        }
//        乐观锁：数字打印出来不是有序的

    }
}

class Test2 implements Runnable {

//    不设置值默认为0
    AtomicInteger count = new AtomicInteger();

    @Override
    public void run() {
        for (int i = 0 ; i < 100 ; i++) {
//            自增1，返回自增后的值
            System.out.println("count ==> "+count.incrementAndGet());
        }
    }
}