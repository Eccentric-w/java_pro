package 原子性;

public class 原子性问题 {
    public static void main(String[] args) {

        Test test = new Test();

        for (int i = 0 ; i < 100 ; i++) {
            new Thread(test).start();
        }

    }
}

class Test implements Runnable {
//    保证可见性
    private volatile int count = 0;
    @Override
    public void run() {
        for (int i = 0 ; i < 100 ; i++) {
            System.out.println("count ==> "+count++);
        }
    }
}