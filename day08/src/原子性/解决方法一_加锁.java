package 原子性;

public class 解决方法一_加锁 {
    public static void main(String[] args) {

        Test1 test = new Test1();

        for (int i = 0 ; i < 100 ; i++) {
            new Thread(test).start();
        }
//        打印出来的时有序的数字排列

    }
}

class Test1 implements Runnable {
    //    保证可见性
    private volatile int count = 0;
    @Override
    public void run() {
//       关键代码加锁保证原子性
        synchronized ("atom"){
            for (int i = 0; i < 100; i++) {
                System.out.println("count ==> " + count++);
            }
        }
    }
}