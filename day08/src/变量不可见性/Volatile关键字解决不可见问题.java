package 变量不可见性;

public class Volatile关键字解决不可见问题 {

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.start();

        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (test1.isFlag()) {
                System.out.println("线程开始进入工作");
            }
        }
    }


}

class Test1 extends Thread {

//    给共享资源添加关键字即可，一旦有线程更改值，其他线程就会取最新值
    volatile boolean flag = false ;

//    boolean类型特殊名称，与get方法相同


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {

//        在修改flag值之前先休眠一会，确保主线程把变量读取到工作内存之后，再修改变量
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        flag = true ;
        System.out.println("子线程的flag="+flag);
    }
}
