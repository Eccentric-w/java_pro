package 线程池;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runnable任务线程池 {

    public static void main(String[] args) {
//        创建可以存储3个线程的线程池
        ExecutorService pools = Executors.newFixedThreadPool(3);

//        创建线程任务对象
        Runnable target = new MyRunnable();

        pools.submit(target);   //提交任务，创建一个线程
        pools.submit(target);   //提交任务，创建第二个线程
        pools.submit(target);   //提交任务，创建第三个线程
        pools.submit(target);   //提交任务，不会创建线程，复用前面的线程执行这个任务

//            线程池默认不会关闭，程序会一直运行
        pools.shutdown();   //等线程任务都执行完后，关闭线程池

    }

}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0 ; i < 5 ; i++) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
        }
    }
}