package 课后习题;

public class 第四题 {
    public static void main(String[] args) {
        HWPhone hwPhone = new HWPhone();
        hwPhone.type = "p30";
        hwPhone.color = "奶奶灰";
        hwPhone.price = 3999.0 ;
        HWPhone.brand = "华为";
        hwPhone.call();
    }

}

class HWPhone {
    public String type ;
    public String color ;
    public double price ;
    public static String brand ;

    public void call() {
        System.out.println(brand+","+type+","+price+","+color);
    }
}