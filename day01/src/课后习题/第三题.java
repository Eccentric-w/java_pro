package 课后习题;

public class 第三题 {
    public static void main(String[] args) {
        Cat cat = new Cat("波斯猫");
        cat.eat();
        cat.catchMouse();
        Dog dog = new Dog("旺财狗");
        dog.eat();
        dog.lookHome();
    }
}

class Animal {
    public String name ;
    public String color ;
    public int price ;

    public void eat() {
        System.out.println(name+"吃饭");
    }
}

class Dog extends Animal {
    public Dog(String name) {
        this.name = name;
    }

    public void lookHome() {
        System.out.println(name+"看家");
    }
}

class Cat extends Animal {
    public Cat(String name) {
        this.name = name;
    }

    public void catchMouse() {
        System.out.println(name+"抓老鼠");
    }
}