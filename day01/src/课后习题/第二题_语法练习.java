package 课后习题;

public class 第二题_语法练习 {
    public static void main(String[] args) {
        Coder coder = new Coder("马化腾");
        coder.eat();
        coder.sleep();
        coder.coding();
        Teacher teacher = new Teacher("马云");
        teacher.eat();
        teacher.sleep();
        teacher.teach();
    }
}

class Person {
    public String name ;
    public int age ;

    public void eat() {
        System.out.println(name+"吃饭");
    }

    public void sleep() {
        System.out.println(name+"睡觉");
    }
}

class Coder extends Person {
    public Coder(String name) {
        this.name = name;
    }
    public void coding() {
        System.out.println(name+"敲代码");
    }
}

class Teacher extends Person {
    public Teacher (String name) {
        this.name = name;
    }
    public void teach() {
        System.out.println(name+"上课");
    }
}