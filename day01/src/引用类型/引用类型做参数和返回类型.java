package 引用类型;

public class 引用类型做参数和返回类型 {
    public static void main(String[] args) {
        Data data = createData();
//        将引用类型作为参数传入方法中，方法调用实例化方法
        put(data);
    }

    //    用方法实例化对象并返回，可以在创建对象时对对象进行初始化的一些操作
    private static Data createData() {

//        返回初始化的对象
        return new Data();
    }

//    接收Data数据类型作为参数
    public static void put(Data data) {

//        方法回调
        data.output();
    }
}


class Data {
    public void output() {
        System.out.println("数据持续输出中~~");
    }
}