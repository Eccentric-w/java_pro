package 引用类型;

public class 引用类型做成员变量 {
    public static void main(String[] args) {
        Address address = new Address();
        Address add = new Address();
        address.setName("地址");
        address.setAge(23);
        address.setAddress(add);
        System.out.println(address.getName());
        System.out.println(address.getAddress());
        System.out.println(address);
    }
}



class Address {
    private String name ;
    private int age ;
    private char sex ;
    private Address address ;

    public Address() {
    }

    public Address(String name, int age, char sex, Address address) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.address = address;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return sex
     */
    public char getSex() {
        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(char sex) {
        this.sex = sex;
    }

    /**
     * 获取
     * @return address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * 设置
     * @param address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    public String toString() {
        return "Address{name = " + name + ", age = " + age + ", sex = " + sex + ", address = " + address + "}";
    }
}
