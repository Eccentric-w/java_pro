package one_知识回顾;

public class MemberMethodDemo_04 {
    private String name ;
    private  int age ;

    static void inAddr() {
//        静态方法不能访问同一类中的成员变量
        System.out.println("hello world");
    }

    void hello() {
        System.out.println("我是实例化方法");
    }

    public static void main(String[] args) {
//        此处省略类名，因为在同一类中
//        静态方法可以通过类名调用
        inAddr();
//        实例化方法不能通过类名调用
//        MemberMethodDemo_04.hello();

        MemberMethodDemo_04 member_01 = new MemberMethodDemo_04();
        member_01.name = "孙悟空" ;
        member_01.age = 12 ;

//        实例化的对象访问静态方法
        member_01.inAddr();
//        对象访问实例化方法（不建议）
        member_01.hello();
        System.out.println("他的名词是"+member_01.name+"他已经"+member_01.age+"岁了");
    }
}
