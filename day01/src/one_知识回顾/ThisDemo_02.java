package one_知识回顾;

public class ThisDemo_02 {
    public static void main(String[] args) {
        Animal animal_01 = new Animal();
        animal_01.setName("恶犬");
//        此时setName方法中的this表示 animal_01
        System.out.println(animal_01.getName());

//        此时定义参数
        Animal animal_02 = new Animal("dog",4,"male");
        System.out.println(animal_02.toString());

/*        不能直接用‘，’逗号连接多个属性输出
      System.out.println(animal_02.getName(),animal_02.getAge(),animal_02.getSex());？*/
    }
}

class Animal{
/*    private 关键字定义的变量只能在类内部访问
    外部如果想访问通过 set 和 get 方法，确保数据的安全性*/
    private String name ;
    private int age ;
    private String sex ;

    public Animal() {}

    public Animal(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }

}


