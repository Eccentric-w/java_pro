package one_知识回顾;

public class ClassDemo_01 {
    public static void main(String[] args) {
        test test_01 = new test();
        test_01.test();
    }
}

class test {
    void test(){
//        代码语句必须写在方法中，因为不属于类的五大成分
        System.out.println("hello world,the idea is good");
    }
}
