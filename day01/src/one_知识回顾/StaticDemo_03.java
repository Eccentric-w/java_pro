package one_知识回顾;

public class StaticDemo_03 {
    public static void main(String[] args) {
//        访问静态成员变量
        System.out.println(test_01.testName);


        test_01 swk = new test_01();
//        通过类名访问静态变量，不推荐
        System.out.println(swk.testName);
        swk.name = "孙悟空";
//        因为age的属性是private，私有，仅在其所在类中可以访问，所以这里不可以赋值和取值
        System.out.println(swk.name);
    }
}

class test_01 {
    public static String testName = "this is test's name";
    public String name ;
    private int age ;
//    私有化，仅在当前类中可以访问age
}