package 继承后;

public class 成员变量的特点 {
//    采用就近原则
public static void main(String[] args) {
    Cat cat = new Cat();
    cat.WhatName();
}

}

class Animal {
    public String name = "父类名称";
}

class Cat extends Animal {
    String name = "子类名称";
    void WhatName() {
        String name = "方法内部名称";
        System.out.println(name);    //方法内部名称
        System.out.println(this.name);  //子类名称
        System.out.println(super.name); //父类名称
    }
}