package 继承后;

public class 调用父类构造器 {
    public static void main(String[] args) {
        GoldMonkey goldMonkey = new GoldMonkey("金丝猴",12,'男');
        goldMonkey.eat();
    }
}

class GoldMonkey extends Monkey {

    public GoldMonkey(String name, int age, char sex) {
        super(name,age,sex);
    }

    public void eat() {
//        快速移动某行位置快捷键：shift+alt+↑/↓
        System.out.println("name:"+getName()+"age"+getAge()+"sex:"+getSex());
    }
}

class Monkey {
    private String name ;
    private int age ;
    private char sex ;


    public Monkey() {
    }

    public Monkey(String name, int age, char sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {

        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {

        this.age = age;
    }

    /**
     * 获取
     * @return sex
     */
    public char getSex() {

        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(char sex) {

        this.sex = sex;
    }

    public String toString() {

        return "Monkey{name = " + name + ", age = " + age + ", sex = " + sex + "}";
    }
}


