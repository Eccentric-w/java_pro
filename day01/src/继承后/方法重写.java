package 继承后;

public class 方法重写 {
    public static void main(String[] args) {
        City city = new City();
        city.run(); //子类重写的方法
        city.go();  //间接调用父类方法

    }
}

class Country {
    public void run() {
        System.out.println("这个国家很富有");
    }
}

class City extends Country {
    @Override
    public void run() {
        System.out.println("这个城市富可敌国~~");
    }

    public void go() {
        super.run();
    }
}