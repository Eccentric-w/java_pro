package 继承后;

public class 构造器 {
    public static void main(String[] args) {
        B a = new B();
//        实例化对象时自动调用构造方法创建
    }
}

class A {
    public A() {
        System.out.println("==父类的无参数构造方法==");
    }
}

class B extends A {
    public B() {
//        super(); 此语句默认存在，所以先运行父类的构造方法
        System.out.println("==子类的无参构造方法==");
    }
}
