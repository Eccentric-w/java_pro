package 继承后;

public class 兄弟构造器 {
    public static void main(String[] args) {
        BigMonkey bigMonkey = new BigMonkey("大猴子",13);
        System.out.println(bigMonkey.toString());
    }
}

class BigMonkey extends Monkey {

    public BigMonkey(String name, int age) {
        this(name, age, '雄');
    }

    public BigMonkey(String name, int age, char sex) {
        super(name, age, sex);
    }
}