package 异常;

/**
 底层自动抛出
 只需要在最外层捕获即可
 */

public class 运行时异常处理机制 {
    public static void main(String[] args) {

        try {
            System.out.println(test());
        }catch (Exception e ) {
            System.err.println("最外层捕获到异常，被除数不能为0");
        }

    }

    public static int test() {

        int a = 10 ;
        int b = 0 ;

        return a / b ;

    }
}
