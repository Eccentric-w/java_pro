package 异常;

public class 常见运行时异常类型 {
    public static void main(String[] args) {
        System.out.println("======================程序开始======================");
//        1. 数组越界
        int[] intList = {3,4,5};
        System.out.println(intList[2]); //是正确的
//        System.out.println(intList[3]); //越界错误
//        Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3

//        2.空指针
        String zifu = null;
        System.out.println(zifu);   //没问题
//        System.out.println(zifu.length());  //有异常
//        Exception in thread "main" java.lang.NullPointerException

//        3.类型转换异常
        Object a = "23";
        Integer b = (Integer) a;
        System.out.println(b);
//        Exception in thread "main" java.lang.ClassCastException: class java.lang.String cannot be cast to class java.lang.Integer (java.lang.String and java.lang.Integer are in module java.base of loader 'bootstrap')

//        4.迭代器遍历越界
        int[] list = {1,2,3,4,5,6};
//        遍历越界，最后一个元素不存在
        for (int i = 0 ; i< list.length+1;i++) {
            System.out.println(list[i]);
        }

//        5. 数学操作异常
        System.out.println(10/0);

//        6. 数字转换异常
        String c = "34";
        String e = "34ee";
        int d = Integer.valueOf(c);     //没问题
        int f = Integer.valueOf(e);     //报异常

        System.out.println("======================程序结束======================");
    }
}
