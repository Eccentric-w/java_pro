package 异常;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class 编译时异常的处理方式 {

    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) {

        try {
            test3();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

//    1. 抛出异常
//    直接在方法处抛出异常，谁调用这个方法再继续抛出（默认的处理机制）
    static public void test1() throws ParseException {
        String s1 = "1234-3-4";
        Date date1 = simpleDateFormat.parse(s1);
    }


//    2. 在出现异常的位置，try……catch……异常
    public static void test2() {
        String s2 = "1234-5-6";
        try {
            Date date2 = simpleDateFormat.parse(s2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


//    3. 异常抛出给main方法，main方法再try……catch……对应的语句
    public static void test3() throws ParseException {
        String s3 = "3456-6-7";
        Date date3 = simpleDateFormat.parse(s3);
    }



}
