package 异常;

public class 自定义编译时异常 {
    public static void main(String[] args) {
        try {
            System.out.println(test(10, 0));
        } catch (CompileAbnormal compileAbnormal) {
//            打印异常栈
            compileAbnormal.printStackTrace();
            System.err.println("被除数不能是0");
        }

    }

    public static int test(int a  , int b ) throws CompileAbnormal {
        if (b == 0) {
//            在异常位置抛出异常
            throw new CompileAbnormal("被除数不能是0");
        }
        return a / b ;

    }

}


class CompileAbnormal extends Exception {

//    重写所有构造器
    public CompileAbnormal() {
    }

    public CompileAbnormal(String message) {
        super(message);
    }

    public CompileAbnormal(String message, Throwable cause) {
        super(message, cause);
    }

    public CompileAbnormal(Throwable cause) {
        super(cause);
    }

    public CompileAbnormal(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}