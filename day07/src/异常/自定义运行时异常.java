package 异常;

public class 自定义运行时异常 {
    public static void main(String[] args) {
        test();

    }

    public static void test() throws RunAbnormal{
        int a = 10 ;
        int b = 0 ;
        if (b == 0 ) {
            throw new RunAbnormal("运行时，被除数不能是0");
        }

        System.out.println(a / b);

    }
}

class RunAbnormal extends RuntimeException {
//    重写所有构造方法
    public RunAbnormal() {
    }

    public RunAbnormal(String message) {
        super(message);
    }

    public RunAbnormal(String message, Throwable cause) {
        super(message, cause);
    }

    public RunAbnormal(Throwable cause) {
        super(cause);
    }

    public RunAbnormal(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
