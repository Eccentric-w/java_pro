package 异常;

public class 异常注意事项 {
    public static void main(String[] args) {
        try {
            test();
        }catch (Exception e) {
            System.out.println("Exception能捕获所有异常类型");
        }
//        catch (ArithmeticException arithmeticException) {  //编译时异常，因为这个捕获没有意义了
//            System.out.println();
//        }

    }

//   当catch捕获多个异常时，前面的异常不能时后面异常的父类，否则后面的异常捕获没有意义
    public static void test() {
        int a = 10 ;
        int b = 0 ;
        int c = a / b ;
    }
}
