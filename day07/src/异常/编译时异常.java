package 异常;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 提供给开发者提醒可能出现的错误
 */

public class 编译时异常 {
    public static void main(String[] args) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        获取当前时间（美式）
        Date date =  new Date();
        System.out.println(date);

        String s = simpleDateFormat.format(date);

        String s1 = "1999-05-19";
//        这里就是编译时异常
        try {
            Date date1 = simpleDateFormat.parse(s1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
