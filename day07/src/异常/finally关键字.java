package 异常;

public class finally关键字 {
    public static void main(String[] args) {

        try {
            System.out.println(test1());
        }catch (Exception e) {
            System.err.println("An error occurred");
        }finally {
            System.out.println("finally关键字一定会执行");
            System.out.println("写了finally可以不写catch");
        }

    }

    public static int test1() {
        return 10 / 0 ;
    }
}
