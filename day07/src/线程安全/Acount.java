package 线程安全;

public class Acount {
    private double money ;

    public void takeOut(double money) {
//        this.money是本Acount对象的money属性
//        money是传入的参数，表示要取出的钱数
        if (this.money >= money) {
            System.out.println(Thread.currentThread().getName()+"取钱:"+money);
//            更新账户余额
            this.money -= money;
            System.out.println(Thread.currentThread().getName()+"取钱后，账户剩余金额:"+this.money);
        }else {
            System.out.println(Thread.currentThread().getName()+"取钱，余额不足");
        }

    }


    public Acount() {
    }

    public Acount(double money) {
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
