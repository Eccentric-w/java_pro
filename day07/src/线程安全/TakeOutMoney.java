package 线程安全;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class TakeOutMoney {
    public static void main(String[] args) {
//        创建共享账户对象，存储10000元
        Acount acount = new Acount(10000);

//        创建线程对象
        Thread user1 = new User("用户1", acount);
        user1.start();

        Thread user2 = new User("用户2",acount);
        user2.start();

    }
}
