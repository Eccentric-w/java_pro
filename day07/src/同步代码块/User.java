package 同步代码块;

public class User extends Thread {
    public Acount acount ;

    @Override
    public void run() {
        acount.takeOut(10000);
    }

    public User() {
    }

    public User(Acount acount) {
        this.acount = acount;
    }

    public User(Runnable target, Acount acount) {
        super(target);
        this.acount = acount;
    }

    public User(String name, Acount acount) {
        super(name);
        this.acount = acount;
    }

    public User(Runnable target, String name, Acount acount) {
        super(target, name);
        this.acount = acount;
    }
}
