package 显示锁;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Acount {
    private double money ;

//    创建一把单独的锁
    private final Lock lock = new ReentrantLock();

    public void takeOut(double money) {

        lock.lock();    //上锁
        try{//        this.money是本Acount对象的money属性
//        money是传入的参数，表示要取出的钱数
            if (this.money >= money) {
                System.out.println(Thread.currentThread().getName() + "取钱:" + money);
//            更新账户余额
                this.money -= money;
                System.out.println(Thread.currentThread().getName() + "取钱后，账户剩余金额:" + this.money);
            } else {
                System.out.println(Thread.currentThread().getName() + "取钱，余额不足");
            }
        }catch (Exception e) {

        }finally {
//            确保出现异常时，一定会解锁，否则资源就永久锁死了
            lock.unlock();  //解锁
        }

    }


    public Acount() {
    }

    public Acount(double money) {
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
