package practice.线程安全.关卡一;

/**
 * 思路：
 *  定义一个线程任务类，创建一个对象
 *  定义两个线程对象，实现同一个线程任务对象
 */

public class 案例二 {
    public static void main(String[] args) {
        Shop shop = new Shop();
        new Thread(shop,"实体店").start();
        new Thread(shop,"线上店").start();
    }
}

class Shop implements Runnable {
    volatile static int sum = 0 ;
    @Override
    public void run() {
        while (sum < 100) {
            sum++;
            synchronized (this) {   // 在循环下面上锁，否则会一个线程直接循环完
            System.out.println(Thread.currentThread().getName()+"卖出第"+sum+"个商品，剩余"+(100-sum)+"个");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        }
    }
}