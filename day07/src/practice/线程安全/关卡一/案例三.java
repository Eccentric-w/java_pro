package practice.线程安全.关卡一;

public class 案例三 {
    public static void main(String[] args) {
        Driver driver = new Driver();
        new Thread(driver,"前门").start();
        new Thread(driver,"中门").start();
        new Thread(driver,"后门").start();
    }
}

class Driver implements Runnable {
    volatile int place = 80 ;
    @Override
    public void run() {
        while ( true ) {
            synchronized (this) {
                if (place == 0) {
                    break;
                }
                place--;
                System.out.println(Thread.currentThread().getName()+"上车,剩余"+place+"个座位");
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}