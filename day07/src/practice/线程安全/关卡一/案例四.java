package practice.线程安全.关卡一;

public class 案例四 {
    public static void main(String[] args) {
        MyRunnable myRunnable = new MyRunnable();
        new Thread(myRunnable,"a").start();
        new Thread(myRunnable,"b").start();
        new Thread(myRunnable,"c").start();
        new Thread(myRunnable,"d").start();
    }
}

class MyRunnable implements  Runnable {
    Test test = new Test();

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (test.getCount() == 0) {
                    break;
                }
                test.setCount(test.getCount()-1);
                System.out.println("当前窗口为：窗口"+Thread.currentThread().getName()+"卖了一张票，剩余票数为"+test.getCount());
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}

class Test extends Thread {
    public int count = 100 ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}