package practice.线程安全.关卡一;

public class 案例一 {
    public static void main(String[] args) {
        for (int i = 0 ; i < 10 ; i++) {
            Thread thread = new MyThread();
//            给线程名称赋值   获取线程名称
            thread.setName(thread.getName()+i);
            thread.start(); //  启动线程
        }
    }
}

class MyThread extends Thread {
    @Override
    public void run() { // 对run方法上锁不管用，线程创建时就启动了
        synchronized (案例一.class) {  // 对代码块上锁，同一时刻只能用一个线程访问

//        获取当前线程的名称
        System.out.println(this.getName()+"通过山洞");
        try {
//            线程休眠5秒钟
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        }
    }
}