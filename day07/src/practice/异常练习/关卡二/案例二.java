package practice.异常练习.关卡二;

import java.util.Scanner;

public class 案例二 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        String name = scanner.nextLine();
        try{

            String password = scanner.nextLine();
            if (!name.equals("root")) {
                throw new LoginException("用户名错误");
            }else if (!password.equals("1234")) {
                throw new LoginException("密码错误");
            }else {
                System.out.println("欢迎回来");
            }
        }catch (LoginException e) {
            e.printStackTrace();
        }
    }
}

class LoginException extends RuntimeException {
    public LoginException() {
        super();
    }
    public LoginException(String message) {
        super(message);
    }
}