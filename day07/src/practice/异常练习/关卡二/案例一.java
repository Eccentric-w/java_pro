package practice.异常练习.关卡二;

public class 案例一 {
    public static void main(String[] args) {
        Player player = new Player("qwe",34);
        System.out.println(player);
        player.setHP(-23);
        System.out.println(player);
    }
}

class HPException extends RuntimeException {
    public HPException() {
        super();
    }
    public HPException(String message) {
        super(message);
    }
}

class Player {
    String name ;
    int HP ;

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", HP=" + HP +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        if (HP < 0 ) {
            throw new HPException("HP不能是负数:"+HP);
        }
        this.HP = HP;
    }

    public Player(String name, int HP) {
        this.name = name;
        this.HP = HP;
    }
}