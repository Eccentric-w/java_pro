package practice.异常练习.关卡三;

import com.sun.security.jgss.GSSUtil;

public class 案例三 {
    public static void main(String[] args) {
        new Thread(new BlackGroundMusic()).start();
        new Thread(new Frames()).start();

    }
}

class BlackGroundMusic implements Runnable {

    @Override
    public void run() {
        System.out.println("播放背景音乐");
    }
}

class Frames implements Runnable {

    @Override
    public void run() {
        System.out.println("显示画面");
    }
}