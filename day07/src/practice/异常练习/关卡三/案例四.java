package practice.异常练习.关卡三;

public class 案例四 {
    public static void main(String[] args) {
        new Thread(new Factorial(10)).start();
        new Thread(new Factorial(5)).start();
        new Thread(new Factorial(8)).start();
    }
}

class Factorial implements Runnable {
    private int num ;
    public Factorial(int num) {
        this.num = num;
    }

    @Override
    public void run() {
        int max  = factorial(num);
        System.out.println(max);
    }

    public int factorial(int num) {
        int sum = 1 ;
        for (int i = 1; i < num+1 ; i++) {
            sum *= i;
        }
        return sum;
    }
}