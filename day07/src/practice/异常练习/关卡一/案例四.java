package practice.异常练习.关卡一;

public class 案例四 {
    public static void main(String[] args) {

//1. 请说出throw的使用位置，作用是什么?
//2. 请说出 throws的使用位置，作用是什么?
        /**
         * throw：用在定义类时，继承父类
         * throws：使用在代码中，用来抛出异常
         *
         *
         * throw：在代码中，放在异常出现的地方，创建异常对象并将其从此处抛出
         * throws：用在方法声明，告诉调用者，可能会抛出异常
         */

    }
}
