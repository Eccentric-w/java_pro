package practice.异常练习.关卡一;

public class 案例六 {

    public static void main(String[] args) {

        Student student = new Student("li","n",-12);
        System.out.println(student.getScore());
        student.setScore(-132);
        System.out.println(student.getScore());


    }

}

// 定义运行时异常
class ScoreException extends RuntimeException {

//    无参构造器
    public ScoreException() {
        super();
    }
//    有参构造器，将异常信息传递给父类
    public ScoreException(String message) {
        super(message);
    }

}

class Student {
    private String ID ;
    private String name ;
    private int score ;

    @Override
    public String toString() {
        return "Student{" +
                "ID='" + ID + '\'' +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        if (score < 0 ) {
            throw new ScoreException("分数不能是负数："+score);
        }
        this.score = score;
    }

    public Student(String ID, String name, int score) {
        this.ID = ID;
        this.name = name;
        this.score = score;
    }
}