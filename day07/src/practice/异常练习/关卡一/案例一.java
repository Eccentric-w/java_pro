package practice.异常练习.关卡一;

public class 案例一 {
    public static void main(String[] args) {

//        1. 异常的继承体系
//        最顶层是Throwable类，Error和Exception继承Throwable类
//        Runtimeable继承Exception类
        /**
         * Error是系统级别的严重错误，无法处理，只能修改代码
         *Exception是编译时异常，在编写阶段会提醒，必须修改，如：日期格式化异常等
         * Runtimeable是运行时异常，编译阶段不会报错，会在运行时出错，如：除零错误
         */




    }
}
