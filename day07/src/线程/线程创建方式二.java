package 线程;

public class 线程创建方式二 {
    public static void main(String[] args) {

//        普通方式
//        创建线程任务类对象
        MyRunnable myRunnable = new MyRunnable();
//        将线程任务类对象封装成线程对象
        Thread t1 = new Thread(myRunnable,"线程1");
        t1.start();


//        匿名内部类写法方式
        Thread t2 = new Thread(new MyRunnable(),"线程2");
        t2.start();


//        主线程
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
        }
    }
}


//创建线程任务类
class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
        }
    }

}