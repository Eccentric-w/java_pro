package 线程;

public class 创建线程方式一 {
    public static void main(String[] args) throws InterruptedException {
        Thread myThread = new MyThread("thread01");
//        先启动子线程，否则如果先启动主线程就先执行完主线程再执行子线程
        myThread.start();
        System.out.println(myThread.getName());

        for (int i = 0 ; i < 10 ; i++ ) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
            Thread.sleep(1000);
        }
        System.out.println(Thread.currentThread().getName());
    }
}

//继承Thread类
class MyThread extends Thread {
    //调用有参构造器命名
    public MyThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0 ; i < 10 ; i++ ) {
//            获取当前线程对象，并获取名称
            System.out.println(Thread.currentThread().getName()+"==>"+i);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName());
    }
}