package 线程;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class 线程创建方法三 {
    public static void main(String[] args) {

//        3. 创建线程任务类对象
        Callable myCalllable = new MyCalllable();

//        4. 将线程任务类对象封装成未来任务对象，后期可用于获取线程执行的结果
        FutureTask task = new FutureTask(myCalllable);

//        5. 未来任务对象是Runnable的子类，根据多态，这里可以放入未来任务对象类型
        Thread thread1 = new Thread(task,"线程1");

//        6. 启动线程
        thread1.start();


//        主线程
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
        }



        try {
//            获取线程的执行结果，并输出
            System.out.println(task.get());
//            返回结果可能是正确的，也可能是异常
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("线程执行过程中出现了错误");
        }

    }
}

//1. 创建线程任务类，实现Callable接口
class MyCalllable implements Callable<String> {

//    2. 重写call方法
    @Override
    public String call() throws Exception {
        int sum = 0 ;
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(Thread.currentThread().getName()+"==>"+i);
            sum += i;
        }
        return Thread.currentThread().getName()+"执行的结果是:"+sum;
    }
}