package 练习题.关卡三;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class 案例二 {

    static Map<StringBuilder,String> fruitHashMap = new HashMap<>();
    static Random random = new Random();
    static String[] fruitList = {"香蕉","西瓜","橘子","苹果"};

    public static void main(String[] args) {
        for (String s : fruitList) {
            fruitHashMap.put(createRandom(),s);
        }
        System.out.println(fruitHashMap);
    }


    public static StringBuilder createRandom() {
        StringBuilder id = new StringBuilder();
        for (int i = 0 ; i < 8 ; i++) {
            id.append(random.nextInt(10));
        }
        if (fruitHashMap.containsKey(id)) {
            return createRandom();
        }
        return id;
    }
}
