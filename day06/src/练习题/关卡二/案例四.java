package 练习题.关卡二;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 案例四 {

    static Map<Character,Integer> characterIntegerMap = new HashMap<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        String input = scanner.nextLine();
        for (int i = 0 ; i < input.length() ; i++ ) {
            if (  characterIntegerMap.containsKey(input.charAt(i))) {
                int value = characterIntegerMap.get(input.charAt(i)) + 1 ;
                characterIntegerMap.put(input.charAt(i),value);
            }else {
                characterIntegerMap.put(input.charAt(i),1);
            }
        }
        System.out.println(characterIntegerMap);
    }

}
