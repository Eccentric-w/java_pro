package 练习题.关卡二;

import org.w3c.dom.ls.LSOutput;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 案例五 {

    static Map<String,Integer> charHashMap = new HashMap<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        String input = scanner.nextLine();
        char[] inputs = input.toCharArray();

        for (char c : inputs) {
            if (c == ' ') {
                addCheck("空格");
            }else if ( c < 58 && c > 47) {
                addCheck("数字");
            }else if ( (c < 91 && c > 64) || ( c < 123 && c > 96 )) {
                addCheck("字母");
            }else {
                addCheck("其他");
            }
        }
        System.out.println(charHashMap);

    }

    public static void addCheck(String s) {
        if (charHashMap.containsKey(s)) {
            charHashMap.put(s,charHashMap.get(s) + 1);
        }else {
            charHashMap.put(s,1);
        }
    }

}
