package 练习题.关卡二;

import java.util.HashMap;
import java.util.Map;

public class 案例三 {

    static Map<Integer,String> hashMap = new HashMap<>();

    static {
        hashMap.put(1,"张三丰");
        hashMap.put(2,"周芷若");
        hashMap.put(3,"汪峰");
        hashMap.put(4,"灭绝师太");
    }

    public static void main(String[] args) {

        System.out.println(hashMap);

        hashMap.put(5,"李晓红");

        hashMap.remove(1);

        hashMap.put(2,"周林");

        System.out.println(hashMap);

    }


}
