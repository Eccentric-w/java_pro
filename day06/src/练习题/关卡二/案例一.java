package 练习题.关卡二;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 案例一 {

    static Scanner scanner = new Scanner(System.in);
    static Map<String,Integer> studentHashMap = new HashMap<>();

    public static void main(String[] args) {

        while (studentHashMap.size() < 5) {
            String studentMessage = scanner.nextLine();
            String[] student = studentMessage.split(",");
            studentHashMap.put(student[0],Integer.valueOf(student[1]));
        }
        System.out.println(studentHashMap);
    }
}
