package 练习题.关卡二;

import java.util.HashMap;
import java.util.Map;

public class 案例二 {

    static Map<String,Integer> hashMap = new HashMap<>();

    static {
        hashMap.put("柳岩",2100);
        hashMap.put("张亮",1700);
        hashMap.put("诸葛亮",1800);
        hashMap.put("灭绝师太",2600);
        hashMap.put("东方不败",3800);
    }

    public static void main(String[] args) {

        int value = hashMap.get("柳岩");
        value = value + 300;
        hashMap.put("柳岩",value);

        System.out.println(hashMap);

    }

}
