package 练习题.关卡一;

import java.util.HashMap;
import java.util.Map;

/**

 Map接口常用的方法

 */

public class 案例一 {

    public static void main(String[] args) {

        Map<String,Integer> stringIntegerMap = new HashMap<>();

//        1. 添加元素
        stringIntegerMap.put("jack",34);
        stringIntegerMap.put("nick",23);

//        修改元素
        stringIntegerMap.put("jack",21);

//        2. 根据键获取值
        System.out.println(stringIntegerMap.get("jack"));

//        3.删除元素
        stringIntegerMap.remove("nick");

        System.out.println(stringIntegerMap);
    }

}
