package 练习题.关卡一;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class 案例三 {
    public static void main(String[] args) {
        Map<String,Integer> stringIntegerMap = new HashMap<>();

        stringIntegerMap.put("jack",21);
        stringIntegerMap.put("nick",43);
        stringIntegerMap.put("adi",34);

        Collection<Integer> valueList = stringIntegerMap.values();

        for (Integer integer : valueList) {
            System.out.println(integer);
        }

        Iterator iterator = valueList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
