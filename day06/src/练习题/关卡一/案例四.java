package 练习题.关卡一;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class 案例四 {
    static Map<String,String> nameHashMap = new HashMap<>();

    static {
        nameHashMap.put("邓超","孙俪");
        nameHashMap.put("李晨","范冰冰");
        nameHashMap.put("刘德华","柳岩");
        nameHashMap.put("黄晓明","Baby");
        nameHashMap.put("谢霆锋","张柏芝");
    }

    public static void main(String[] args) {

        Set<String> nameKeyList = nameHashMap.keySet();

        for (String s : nameKeyList) {
            System.out.println(s+"==>"+nameHashMap.get(s));
        }

    }
}
