package 练习题.关卡一;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class 案例二 {

//    Map集合无序
    static Map<String,Integer> stringIntegerMap = new HashMap<>();

    public static void main(String[] args) {

        stringIntegerMap.put("jack",21);
        stringIntegerMap.put("nick",43);
        stringIntegerMap.put("adi",34);

//        获取所有的key
        Set<String> keyList = stringIntegerMap.keySet();

//        增强for循环
        for (String s : keyList) {
            System.out.println(s);
        }

        Iterator iterator = keyList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }



    }

}
