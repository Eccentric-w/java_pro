package 排序;

import java.util.ArrayList;
import java.util.List;

public class 冒泡排序 {
    public static void main(String[] args) {
        int[] list = new int[]{4,3,4,56,7644,3,2,4654,32,4,5,6,6,4534};
        for (int i : list) {
            System.out.print(i+", ");
        }
        System.out.println("\b");

        int[] orderList = bubblingOrder(list);
        for (int i : orderList) {
            System.out.print(i+", ");
        }
        System.out.println("\b");

    }

    public static int[] bubblingOrder(int[] list) {

        int temp ;
        /**
         这里的减1是细节，不写则会超出范围，报错
         最后一个不用排序，因为只剩它自己了
         循环：参数条件小于几次就会遍历几次
         */
        for (int i = 0 ; i < list.length-1 ; i++) {
            for (int j = 0 ; j < list.length-i-1 ; j++) {
                if (list[j] > list[j+1]) {
                    temp = list[j];
                    list[j] = list[j+1];
                    list[j+1] = temp;
                }
            }
        }
        return list;
    }

}
