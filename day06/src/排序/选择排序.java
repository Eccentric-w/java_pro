package 排序;

public class 选择排序 {
    public static void main(String[] args) {
        int[] list ={4,3,4,56,7644,3,2,4654,32,4,5,6,6,4534};
        for (int i : list) {
            System.out.print(i+", ");
        }
        System.out.println("\b");

        int[] orderList = choiceOrder(list);
        for (int i : orderList) {
            System.out.print(i+", ");
        }
        System.out.println("\b");
    }

    public static int[] choiceOrder(int[] list) {

        int temp ;
        for (int i = 0 ; i < list.length-1 ; i++) {
//            不管前面遍历多少，第二个循环都要比较到最后一个元素
            for (int j = i+1 ; j < list.length ; j++ ) {
                if (list[i] > list[j]) {
                    temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }
        return list;
    }

}
