package 图书管理系统;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LibraryManager {

    static {
        final Map<String, List<Book>> bookMap = new HashMap<>();
    }

    private final static Scanner systemScanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("====================欢迎来到图书馆====================");
        System.out.println("添加图书：add");
        System.out.println("查看图书：show");
        System.out.println("删除图书：delete");
        System.out.println("修改图书：update");
        System.out.println("退出系统：exit");
        System.out.print("请输入命令:");
        String command = systemScanner.nextLine();

    }
}
