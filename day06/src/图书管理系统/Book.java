package 图书管理系统;

/**
 图书信息
 书名、作者、价格
 封装
 */

public class Book {
    private String bookName ;   //书名
    private int bookPrice ;     //价格
    private String bookAuther ; //作者

    public Book(String bookName, int bookPrice, String bookAuther) {
        this.bookName = bookName;
        this.bookPrice = bookPrice;
        this.bookAuther = bookAuther;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(int bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getBookAuther() {
        return bookAuther;
    }

    public void setBookAuther(String bookAuther) {
        this.bookAuther = bookAuther;
    }

}
