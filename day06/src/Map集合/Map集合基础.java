package Map集合;

import java.util.HashMap;
import java.util.Map;

public class Map集合基础 {
    public static void main(String[] args) {
//        HashMap集合特性与Map完全一致
        Map<String,Integer> list1 = new HashMap<>();
        list1.put("xiaomi",1999);
        list1.put("huawei",4399);
        list1.put("iphone",12999);

//        相同的键，后面的会覆盖前面的
        list1.put("huawei",699);

        System.out.println(list1);
    }
}
