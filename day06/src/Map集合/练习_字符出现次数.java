package Map集合;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class 练习_字符出现次数 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String chars = scanner.nextLine();

        Map<Character,Integer> list = new TreeMap<>();

        for (int i = 0;i<chars.length();i++) {
            char ch = chars.charAt(i);
            if (list.containsKey(ch)) {
                list.put(ch,list.get(ch)+1);
            }else {
                list.put(chars.charAt(i),1);
            }

        }
        list.forEach((k,v)->{
            System.out.println(k+"="+v);
        });
    }
}