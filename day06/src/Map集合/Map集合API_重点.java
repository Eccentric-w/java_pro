package Map集合;

import java.awt.im.spi.InputMethod;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Map集合API_重点 {

    public static void main(String[] args) {

    Map<String,Integer> list1 = new HashMap<>();

//    1.添加元素
    list1.put("李明",23);
    list1.put("小红",12);
        System.out.println(list1);

//    2.清空集合
    list1.clear();
        System.out.println(list1);

//    3. 判断是否为空
        boolean list1Empty = list1.isEmpty();
        System.out.println(list1Empty);

//    4.根据键取值
        list1.put("李明",23);
        list1.put("小红",12);
        System.out.println(list1.get("李明"));

//    5.根据键删除元素
        list1.remove("小红");
        System.out.println(list1);

//    6.判断是否包含某个键
        System.out.println(list1.containsKey("李明"));

//    7. 判断是否包含某个值
        System.out.println(list1.containsValue(23));

//    8. 取所有键的集合
        list1.put("小刚",12);
        list1.put("丹尼",55);
        Collection<String> keySetList = list1.keySet();
        System.out.println(keySetList);

//    9. 取所有值的集合
        Collection<Integer> valueSetList = list1.values();
        System.out.println(valueSetList);

//    10.集合大小
        System.out.println(list1.size());

//    11. 合并其他集合
        Map<String ,Integer> list2 = new HashMap<>();
        list2.put("nick",35);
        list2.put("frank",31);
        System.out.println(list2);
        list1.putAll(list2);
//        Map集合是无序的
        System.out.println(list1);

        Set<Map.Entry<String, Integer>> entries = list1.entrySet();
        list1.forEach((k,v)->{ System.out.println(k+"="+v); });

    }



}

