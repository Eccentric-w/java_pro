package Map集合;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Map集合遍历方式 {
    public static void main(String[] args) {

        Map<String ,Integer> list = new HashMap<>();
        list.put("Nick",31);
        list.put("Frank",32);
        list.put("horse",28);
        list.put("LuoTuo",25);

        System.out.println("------------------Map集合------------------");
        System.out.println(list);

        System.out.println("------------------键找值的方式------------------");
//        1.键找值：先获取键集合，在根据键获取对应的值
        Collection<String> keyList = list.keySet();
        System.out.println(keyList);
        for (String s : keyList) {
            System.out.print(s+"="+list.get(s)+", ");
        }
        System.out.println("\b\b");



//        2. 键值对形式
        System.out.println("------------------键值对方式------------------");

        Set<Map.Entry<String,Integer>> entries = list.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.print(entry+", ");
        }
        System.out.println("\b\b");

//        3. lmabda表达式方式
        System.out.println("------------------lambda方式------------------");
        list.forEach((k,v) ->{
            System.out.print(k+"="+v+", ");
        });
        System.out.println("\b\b");

    }
}
