package Map集合;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class TreeMap集合 {
    public static void main(String[] args) {

        Map<Pig,String> pigList = new TreeMap<>(new Comparator<Pig>() {
            @Override
            public int compare(Pig o1, Pig o2) {
//                专业比较浮点数类型
//                价格升序排列
                return Double.compare(o1.price,o2.price);
            }
        });

        pigList.put(new Pig("配齐",26,500),"粉色");
        pigList.put(new Pig("乔治",12,300),"蓝色");
        pigList.put(new Pig("假乔治",12,100),"假蓝色");
        pigList.put(new Pig("猪八戒",5,1000),"粉色");
        pigList.put(new Pig("天棚元帅",99,2000),"黄色");

//        System.out.println(pigList);
            pigList.forEach((k,v)->{
            System.out.println(k+"="+v);
        });

    }
}

class Pig implements Comparable<Pig>{
    String name ;
    double price ;
    double weight ;

    public Pig(String name, double price, double weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Pig o) {
//        按照价格升序排列
        return Double.compare(this.weight,o.weight);
    }
}