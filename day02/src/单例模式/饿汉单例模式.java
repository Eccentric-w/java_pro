package 单例模式;

public class 饿汉单例模式 {
    public static void main(String[] args) {
        Ehan ehan01 = Ehan.getEhan();
        Ehan ehan02 = Ehan.getEhan();
//        验证Ehan类的对象只有一个
        System.out.println(ehan01 == ehan02);
    }


}

class Ehan {
//    私有化构造器
    private Ehan() {
        System.out.println("我是饿汉构造器，唯一的对象");
    }

    static Ehan ehan = new Ehan();

//    定义成静态，通过类名调用
    public static Ehan getEhan() {
        return ehan;
    }
}