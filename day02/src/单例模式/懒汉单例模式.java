package 单例模式;

public class 懒汉单例模式 {
    public static void main(String[] args) {
        Lhan test = Lhan.getLhan();
        Lhan test1 = Lhan.getLhan();
//        只输出了一次”i am Lhan“说明对象只创建了一个


//        返回true说明二者地址相同，是同一个对象
        System.out.println(test == test1);
    }

}

class Lhan {
    private Lhan() {
        System.out.println("i am Lhan");
    }
    public static Lhan lhan;

    public static Lhan getLhan() {
        if (lhan == null) {
            lhan = new Lhan();
        }
        return lhan;
    }
}
