package final关键字;

public class 修饰成员变量 {

}

class E {
    public static final String NAME;
//    经过修饰后，变成了常量
//    只能在定义或者静态代码块中修改
    static {
        NAME = "acd";
    }

    public final String ad;
    {
//        意义不大，创建对象时触发调用
        ad = "adc";
    }

}