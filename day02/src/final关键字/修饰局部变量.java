package final关键字;

public class 修饰局部变量 {
    public static void main(String[] args) {
        D d = new D();

    }

}

class D {
    void run() {
        final String name = "adc";
//        报错，只能被修改一次
//        name = "xyy";
//        name = "abb";
    }
}
