package 抽象类;

public class 模板设计模式 {
    public static void main(String[] args) {
        Student student = new Student();
        student.write();
    }
}

class Student extends Article {

    @Override
    public String writecontent() {
        return "这是正文";
    }
}


abstract class Article {
    private String title = "《这是题目》";
    private String end = "这是结尾的内容";

    public void write() {
        System.out.println(title);
//        就近原则调用子类重写的方法
        System.out.println(writecontent());
        System.out.println(end);
    }

    public abstract String writecontent();
}