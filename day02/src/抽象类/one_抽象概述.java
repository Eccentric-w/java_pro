package 抽象类;

public class one_抽象概述 {
    public static void main(String[] args) {
        B b = new B();
        b.run();
    }
}

abstract class A {
//    抽象方法不需要方法体
    public abstract void run();
}

class B extends A {
//    子类重写抽象方法
    @Override
    public void run() {
        System.out.println("hello world");
    }
}