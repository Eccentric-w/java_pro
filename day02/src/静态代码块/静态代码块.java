package 静态代码块;

import java.util.*;

public class 静态代码块 {
    public static List<String> cards = new ArrayList<>();
    static {
        System.out.println("执行静态代码块");
        cards.add("黑桃4");
    }

    public static void main(String[] args) {
        System.out.println("执行main方法");
        System.out.println(cards);

    }
}
