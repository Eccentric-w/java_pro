package 静态代码块;

/**
 实例代码块用的很少
 */

public class 实例代码块 {
    public static void main(String[] args) {
        System.out.println("执行mian方法");
        new democode(); //创建对象但是不赋给变量名，相当于垃圾变量，会被回收
    }
}

class democode {
    {
        System.out.println("这是里实例代码块，会跟随对象的创建执行一次");
    }
}
