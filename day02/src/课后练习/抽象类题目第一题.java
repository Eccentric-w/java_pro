package 课后练习;

//        2岁的红色的公鸡会吃饭(啄米)和打鸣
//        1岁的黑色的鸭子会吃饭(吃鱼)和游泳
public class 抽象类题目第一题 {
    public static void main(String[] args) {

        Duck duck = new Duck("鸭子",1,"黑色");
        Cock cock = new Cock("公鸡",2,"红色");
        cock.eat();
        cock.call();
        duck.eat();
        duck.swim();

    }
}

class Cock extends Poultry {
    String name ;
    int age ;
    String color ;

    public Cock(String name,int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    @Override
    public void eat() {
        System.out.println(age+"岁的"+color+"的"+name+"会啄米");
    }

    public void call() {
        System.out.println(age+"岁的"+color+"的"+name+"会打鸣");
    }
}

class Duck extends Poultry {

    String name ;
    int age ;
    String color ;

    public Duck(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    @Override
    public void eat() {
        System.out.println(age+"岁的"+color+"的"+name+"会吃鱼");
    }

    public void swim() {
        System.out.println(age+"岁的"+color+"的"+name+"会游泳");
    }
}

abstract class Poultry {

    int age;
    String color;

    public  abstract void eat();

}
