package 课后练习;

/**
 1. 经理
 成员变量:工号,姓名,工资
 成员方法:工作(管理其他人),吃饭(吃鱼)
 2. 厨师
 成员变量:工号,姓名,工资
 成员方法:工作(炒菜),吃饭(吃肉)

 */

public class 抽象类提名第二题 {
    public static void main(String[] args) {
        Manager manager = new Manager("m110","老王",10000);
        manager.eat();
        manager.work();
        Cook cook = new Cook("c110","小王",6000);
        cook.eat();
        cook.work();
    }
}

class Manager extends Staff {
    String id;
    String name;
    double salary;

    public Manager(String id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    void work() {
        System.out.println("工号为:"+id+",姓名为:"+name+",工资为:"+salary+"的经理在工作，管理其他人");
    }

    @Override
    void eat() {
        System.out.println("工号为:"+id+",姓名为:"+name+",工资为:"+salary+"的经理在吃鱼");
    }
}

class Cook extends Staff {
    String id;
    String name;
    double salary;

    public Cook(String id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    void work() {
        System.out.println("工号为:"+id+",姓名为:"+name+",工资为:"+salary+"的厨师在工作，炒菜");
    }

    @Override
    void eat() {
        System.out.println("工号为:"+id+",姓名为:"+name+",工资为:"+salary+"的厨师在吃肉");
    }
}

abstract class Staff {
    String id ;
    String name ;
    double salary ;

    abstract void work ();
    abstract void eat ();
}
