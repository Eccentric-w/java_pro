package 课后练习;

public class 接口枚举单例第三题 {
    public static void main(String[] args) {
        BB bb = new BB();
        bb.showA();
        bb.showB10();
        bb.showC10();

    }
}

interface AA {
    void showA();
    private static void show10(String str) {
        for (int i=0;i<10;i++) {
            System.out.print(str+" ");
        }
        System.out.println("\b");
    }

    default void showB10() {
        show10("BBBB");
    }

    default void showC10() {
        show10("CCCC");
    }
}

class BB implements AA {

    @Override
    public void showA() {
        System.out.println("AAAA");
    }
}