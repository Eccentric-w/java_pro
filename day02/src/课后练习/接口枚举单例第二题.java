package 课后练习;

public class 接口枚举单例第二题 {
    public static void main(String[] args) {
        B b = new B();
        b.showA();
        b.showB();
    }
}


interface A {
    void showA();
    default void showB() {
        System.out.println("BBBB");
    }
}

class B implements A {
    @Override
    public void showA() {
        System.out.println("AAAA");
    }
}