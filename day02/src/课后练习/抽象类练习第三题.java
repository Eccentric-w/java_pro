package 课后练习;

/**
 1. 在传智播客有很多员工(Employee),按照工作内容不同分教研部员工(Teacher)和行政部员工(AdminStaff)
 2. 教研部根据教学的方式不同又分为讲师(Lecturer)和助教(Tutor)
 3. 行政部根据负责事项不同,又分为维护专员(Maintainer),采购专员(Buyer)
 4. 公司的每一个员工都编号,姓名和其负责的工作
 5. 每个员工都有工作的功能,但是具体的工作内容又不一样,在向上抽取的时候定义为抽象方法
 */


public class 抽象类练习第三题 {
    public static void main(String[] args) {
        Lecturer lecturer = new Lecturer("666","符红学");
        lecturer.work();
        Tutor tutor = new Tutor("668","guqi");
        tutor.work();
        Maintainer maintainer = new Maintainer("686","paoding");
        maintainer.work();
        Buyer buyer = new Buyer("888","jytm");
        buyer.work();

    }

}

class Buyer extends AdminStaff {
    String id ;
    String name ;

    public Buyer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    void work() {
        System.out.println("工号为 "+id+" 的采购专员 "+name+" 在采购音响设备");
    }

}

class Maintainer extends AdminStaff {
    String id ;
    String name ;

    public Maintainer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    void work() {
        System.out.println("工号为 "+id+" 的维护专员 "+name+" 在解决不能共享屏幕问题");
    }

}

class Tutor extends Teacher {
    String id ;
    String name ;

    public Tutor(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    void work() {
        System.out.println("工号为 "+id+" 的助教 "+name+" 在帮助学生解决问题");
    }

}

class Lecturer extends Teacher {
    String id ;
    String name ;

    public Lecturer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    void work() {
        System.out.println("工号为 "+id+" 的讲师 "+name+" 在讲课");
    }

}

abstract class AdminStaff extends Employee {}

abstract class Teacher extends Employee {}

abstract class Employee {
    String id ;
    String name ;
    abstract void work();
}