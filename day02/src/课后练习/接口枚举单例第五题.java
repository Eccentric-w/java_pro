package 课后练习;

public class 接口枚举单例第五题 {
    public static void main(String[] args) {

//        枚举类型通过枚举.的方式调用
        Car car = new Car("benz",Orientation.EAST);
        car.run();
    }
}

enum Orientation {
    EAST, SOUTH, WEST, NORTH;
}

class Car {
    String brand ;
    Orientation orientation ;

    public Car(String brand, Orientation orientation) {
        this.brand = brand;
        this.orientation = orientation;
    }

    void run() {
        System.out.println(brand+"牌的车正在往"+orientation+"方向行驶");
    }
}