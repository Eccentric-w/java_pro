package 接口;

public interface 概述 {
//    public abstract 修饰符是灰色，表示可以省略
    public abstract void run();

//    静态常量
    public static final String STUDENT_NAME = "我是学生的名词";
//    public static final 是灰色，表示可以省略
//    常量字母全部大写，单词用下划线隔开
}
