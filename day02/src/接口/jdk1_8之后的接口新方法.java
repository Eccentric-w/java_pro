package 接口;

public class jdk1_8之后的接口新方法 {
    public static void main(String[] args) {
        Achieve achieve = new Achieve();
//        接口的静态方法只能用接口名调用
        achieve.defaultMethod();
        NewMethod.staticMethod();

    }
}

/**
 * 这是文档注释
 */

class Achieve implements NewMethod {

}

interface NewMethod {
    default void defaultMethod() {
        System.out.println("调用了接口新增的默认方法\n接下来要调用私用方法了");
        privateMethod();
    }

    static void staticMethod() {
        System.out.println("调用了新增的静态方法");
    }

    private void privateMethod() {
        System.out.println("调用了私有方法");
    }
}