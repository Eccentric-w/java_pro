package 接口;

public class 基本实现 {
    public static void main(String[] args) {
        PingpongAthlete pingpongAthlete = new PingpongAthlete("张即可");
        System.out.println("这位运动员的名字是"+pingpongAthlete.name);
        pingpongAthlete.run();
        pingpongAthlete.train();
    }

}

class PingpongAthlete implements athlete {

    String name ;

     PingpongAthlete(String name) {
        this.name = name ;
    }

    @Override
    public void run() {
        System.out.println(name+"正在跑步");
    }

    @Override
    public void train() {
        System.out.println(name+"正在训练");
    }
}


interface athlete {
    void run();
    void train();
}