package 收集Stream流;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class 收集Stream流 {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("黄四郎");
        list.add("张麻子");
        list.add("汤师爷");
        list.add("小六子");
        list.add("张麻子");
        list.add("胡万");

        Stream<String> stringStream = list.stream();

        System.out.println("--------------------------------------------");

//        收集为List集合
        List<String> arrayList = list.stream().collect(Collectors.toList());
        arrayList.forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        收集为Set集合
        Set<String> stringSet = list.stream().collect(Collectors.toSet());
        stringSet.forEach(System.out::println);
        System.out.println("--------------------------------------------");

//      收集为数组
//        普通收集只能转换为Object类型
        Object[] objects = list.stream().toArray();
        for (Object object : objects) {
            System.out.println(object);
        }
        System.out.println("--------------------------------------------");

//        调用构造器可以转换成指定数据类型
        String[] strings = list.stream().toArray(String[]::new);
        for (String string : strings) {
            System.out.println(string);
        }
        System.out.println("--------------------------------------------");




    }
}
