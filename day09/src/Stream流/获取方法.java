package Stream流;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Stream;

public class 获取方法 {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("abc");
        list.add("def");

        String[] strings = {"adc","ap","ad"};

        Map<String ,String> map = new HashMap<>();
        map.put("abc","a");
        map.put("def","d");
        map.put("cde","b");

//        集合获取Stream流
        Stream<String> listStream = list.stream();

//        Map集合获取Stream流
//        获取键Stream流
        Stream<String> keyString = map.keySet().stream();
//        获取值Stream流
        Stream<String> valueStream = map.values().stream();
//          获取键值对Stream流
        Stream<Map.Entry<String,String>> stringStringStream = map.entrySet().stream();

//        获取数组Stream流
        Stream<String> arraryStream = Stream.of(strings);
        Stream<String> arraryStream1 = Arrays.stream(strings);

        System.out.println(arraryStream);




    }
}
