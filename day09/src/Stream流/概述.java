package Stream流;

import java.util.ArrayList;
import java.util.List;

public class 概述 {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        list.add("f");
        list.forEach( System.out::println );

//        filter:过滤器
//        以“a”为首，长度为1的元素
        list.stream().filter(s -> s.startsWith("a")).filter(s -> s.length() == 1).forEach(System.out::println);

    }
}
