package Stream流常用API;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class 常用API {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("黄四郎");
        list.add("张麻子");
        list.add("汤师爷");
        list.add("小六子");
        list.add("张麻子");
        list.add("胡万");

//        list集合获取Stream流
        Stream<String> stringStream = list.stream();
        System.out.println("--------------------------------------------");

//        1. 遍历,终结方法
        list.stream().forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        2. 过滤元素，只要三个字的名字
        list.stream().filter(s -> s.length() == 3).forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        3. 取前几个元素
        list.stream().limit(3).forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        4. 统计个数,终结方法
        System.out.println(list.stream().count());
        System.out.println("--------------------------------------------");

//        5. 跳过前几个元素
        list.stream().skip(2).forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        6. 加工方法，把每个名字封装成学生对象
//        stringStream2.map(s -> new Student(s));     // 可以简化写法
        list.stream().map(Student::new).forEach(System.out::println);
        System.out.println("--------------------------------------------");

//        7. 合并流
        Integer[] integers = {12,23,3,445,56,45};
        Stream<Integer> integerStream = Stream.of(integers);

//        由于两个流泛型不同，因此只能用Object类型接
        Stream<Object> objectStream = Stream.concat(list.stream(),integerStream);
        objectStream.forEach(System.out::println);

    }

}
