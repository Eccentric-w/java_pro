package practise.File和递归.关卡一;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

public class 案例一 {
    public static void main(String[] args) throws IOException {

        /**
         * 相对路径：从当前路径开始
         * 绝对路径：磁盘路径
         */

//        相对路径创建文件
        File file = new File("./file01.txt");   // 获取文件对象
        if (!file.exists()) {   //　如果文件不存在
            file.createNewFile();   // 就根据File对象创建文件
        }
        file.delete();  // 删除文件
        String name = file.getName(); // 文件名
        long size = file.length();  // 文件大小
        String path = file.getAbsolutePath(); // 文件绝对路径
        String parentPath = file.getParent();   // 父文件夹路径
        File parentFile = file.getParentFile(); // 父文件夹文件对象


        File file1 = new File("/file/file00");
        file1.mkdir();  // 创建文件夹

        File file2 = new File("./file/file/file");
        file2.mkdirs(); // 创建多级文件夹

        File file3 = new File("/file");
        if (file3.isFile()) {
            System.out.println("是一个文件");
        }else if (file3.isDirectory()){
            System.out.println("是一个文件夹");
        }


    }
}
