package practise.线程池和lambca和stream流;

public class 案例七 {
    public static void main(String[] args) {
//        原始方式
        invokeCalc(130, 120, new Calculator() {
            @Override
            public int calc(int a, int b) {
                return a-b;
            }
        });

//        lambda标准方式
        invokeCalc(130, 120,(int a,int b) -> {
            return a - b;
        });

//        省略方式
        invokeCalc(130,120,(a,b) -> a-b);

    }

    private static void invokeCalc(int a, int b, Calculator calculator) {
        int result = calculator.calc(a, b);
        System.out.println("结果是：" + result);
    }
}

interface Calculator {
    int calc(int a, int b);
}