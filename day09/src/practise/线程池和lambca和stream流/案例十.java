package practise.线程池和lambca和stream流;

public class 案例十 {
    public static void main(String[] args) {
        Person person = new Person();
        new Thread(new InputThread(person)).start();
        new Thread(new OutputThread(person)).start();

    }
}

class InputThread implements Runnable {
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public InputThread(Person person) {
        this.person = person;
    }

    private Person person ;

    @Override
    public void run() {
        while (true) {
            synchronized (person) {
                if (person.isFlage()) {
                    person.setName("张三");
                    person.setAge(10);
                }else {
                    person.setName("李四");
                    person.setAge(20);
                }
                person.setFlage(!person.isFlage());
                person .notify();
                try {
                    person.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

class OutputThread implements Runnable {
    private Person person ;

    @Override
    public String toString() {
        return "OutputThread{" +
                "person=" + person +
                '}';
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public OutputThread(Person person) {
        this.person = person;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (person) {
                System.out.println(Thread.currentThread().getName()+person);
            person.notify();
            try {
                person.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            }
        }

    }
}

class Person {
    private String name ;
    private int age ;
    private boolean flage ;

    public Person() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", flage=" + flage +
                '}';
    }

    public Person(String name, int age, boolean flage) {
        this.name = name;
        this.age = age;
        this.flage = flage;
    }

    public boolean isFlage() {
        return flage;
    }

    public void setFlage(boolean flage) {
        this.flage = flage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}