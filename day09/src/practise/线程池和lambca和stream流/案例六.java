package practise.线程池和lambca和stream流;

public class 案例六 {
    public static void main(String[] args) {
//        最原始方式
        invokeDirect(new Director() {
            @Override
            public void makeMovie() {
                System.out.println("导演拍电影");
            }
        });

//        标准方式
        invokeDirect(() -> {
            System.out.println("导演拍电影");
        });

//        极简方式
        invokeDirect(()->System.out.println("导演拍电影"));
    }


    private static void invokeDirect(Director director) {   //　接受参数是一个接口对象
        director.makeMovie();
    }
}

interface Director {
    void makeMovie();
}