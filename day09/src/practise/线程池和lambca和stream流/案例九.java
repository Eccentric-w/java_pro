package practise.线程池和lambca和stream流;

import java.util.*;

public class 案例九 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list,1,2,3,4,5,6);
        list.forEach(integer -> System.out.println(integer));
        System.out.println("-------------------------------");

        Set<Integer> set = new HashSet<>();
        Collections.addAll(set,1,2,3,4,45,56);
        set.forEach(integer -> System.out.println(integer));
        System.out.println("-------------------------------");
        set.forEach(System.out::println);   // 极简写法
        System.out.println("-------------------------------");

        Map<Integer,Integer> map = new HashMap<>();
        map.put(1,2);
        map.put(2,2);
        map.put(4,2);
        map.forEach((t1,t2) -> System.out.println(t1+":"+t2));
    }
}
