package practise.线程池和lambca和stream流;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.Random;

public class 案例十一 {
    public static void main(String[] args) {

        MyRunnable myRunnable = new MyRunnable();

        new Thread(myRunnable,"前门").start();
        new Thread(myRunnable,"后门").start();

    }
}

class MyRunnable implements Runnable {

    private int number = 100;
    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        int count = 0 ;
        while (true) {
            synchronized (this) {
                if (number > 0) {
                    String ball = DoubleColorBallUtil.create();
                    System.out.println("编号为" + number + "的员工从" + name + "进场，彩票号是:" + ball);
                    number--;
                    count++;
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println(name + "入场的人数是:" + count + "人");
                    break;
                }
            }
        }
    }
}


class DoubleColorBallUtil {
    // 产生双色球的代码
    public static String create() {
        String[] red = {"01","02","03","04","05","06","07","08","09","10",
                "11","12","13","14","15","16","17","18","19","20","21","22","23",
                "24","25","26","27","28","29","30","31","32","33"};
        //创建蓝球
        String[] blue = "01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16".split(",");
        boolean[] used = new boolean[red.length];
        Random r = new Random();
        String[] all = new String[7];
        for(int i = 0;i<6;i++) {
            int idx;
            do {
                idx = r.nextInt(red.length);//0‐32
            } while (used[idx]);//如果使用了继续找下一个
            used[idx] = true;//标记使用了
            all[i] = red[idx];//取出一个未使用的红球
        }

        all[6] = blue[r.nextInt(blue.length)];
        Arrays.sort(all);
        return Arrays.toString(all);
    }
}