package practise.线程池和lambca和stream流;

import java.util.ArrayList;
import java.util.Comparator;

public class 案例八 {
    public static void main(String[] args) {

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("nick",78));
        students.add(new Student("frank",95));
        students.add(new Student("lucy",98));
        students.add(new Student("white",89));
        System.out.println(students);
        System.out.println("-------------------------------");

//        原始方式
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getScore() - o2.getScore();
            }
        });
        System.out.println(students);
        System.out.println("-------------------------------");

//        标准方式
        students.sort((Student t1 , Student t2) -> {
            return t2.getScore() - t1.getScore();
        });
        System.out.println(students);
        System.out.println("-------------------------------");

//        简略方式
        students.sort((t1,t2) -> t1.getScore()-t2.getScore() );
        System.out.println(students);

    }
}


class Student {
    private String name ;
    private int score ;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }
}