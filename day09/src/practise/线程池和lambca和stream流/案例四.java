package practise.线程池和lambca和stream流;

/*
线程池，计算数字阶乘
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class 案例四 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        executorService.submit(new MyRunable(10),"10的阶乘");
        executorService.submit(new MyRunable(8),"8的阶乘");
        executorService.submit(new MyRunable(5),"5的阶乘");

        executorService.shutdown();

    }
}

class MyRunable implements Runnable {
    private int number;

    public MyRunable(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        int sum = 1 ;
        for (int i = 1 ; i < (number+1) ; i++) {
            sum *= i ;
        }
        System.out.println(Thread.currentThread().getName()+"执行的结果是:"+sum);
    }
}