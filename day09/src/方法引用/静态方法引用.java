package 方法引用;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class 静态方法引用 {
    public static void main(String[] args) {

        List<Student> studentList = new ArrayList<>();

        Student student0 = new Student("lucy",23);
        Student student1 = new Student("tony",12);
        Student student2 = new Student("seven",13);

        Collections.addAll(studentList,student0,student1,student2);

//        Collections.sort(studentList, ( s1,  s2) -> s1.getAge() - s2.getAge() );  //  将这个方法封装到Student类中

        Collections.sort(studentList,( s1,  s2) -> Student.sort(s1,s2));    //封装调用

//        被引用的方法的参数列表要和函数式接口中的抽象方法的参数列表一致
//        进一步简写为
        Collections.sort(studentList,Student::sort);

    }
}

class Student {
    private String name ;
    private int age ;

    public static int sort(Student student1 , Student student2) {
        return student1.getAge() - student2.getAge();
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", age=" + age;
    }
}