package 方法引用;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class 实例方法调用 {
    public static void main(String[] args) {
        List<Student01> studentList = new ArrayList<>();

        Student01 student0 = new Student01("lucy",23);
        Student01 student1 = new Student01("tony",12);
        Student01 student2 = new Student01("seven",13);

        Collections.addAll(studentList, student0, student1, student2);

//        简化写法
//        Collections.sort(studentList,(s1,s2) -> s1.sort(s2));

//       实例方法调用写法
        /**
         前面参数列表的第一参数是后面方法的调用者
         前面参数列表的其余参数全部是后面方法的参数
         */
        Collections.sort(studentList,Student01::sort);

        for (Student01 student01 : studentList) {
            System.out.println(student01);
        }
    }
}

class Student01 {
    private String name ;
    private int age ;

//    排序方法封装到类中，实例方法
    public int sort(Student01 student) {
        return this.getAge() - student.getAge();
    }

    public Student01(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", age=" + age;
    }
}