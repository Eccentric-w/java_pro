package 递归;

import java.io.File;

public class 寻找文件 {

    public static void main(String[] args) {
        searchFile(new File("E:/project"),"java");

    }

    public static void searchFile(File file, String name) {
//        判断路径是否存在且是否是文件夹
        if (file.exists() && file.isDirectory()) {
//            获取所以一级文件及文件夹对象
            File[] files = file.listFiles();
            if (files != null && files.length >0){
                for (File f : files) {
                    if (f.isFile()) {
//                        模糊匹配，包含name字符串的文件名都匹配成功
                        if (f.getName().contains(name)) {
//                            输出搜索到的文件的绝对路径
                            System.out.println(f.getAbsolutePath());
                        }
                    }else if (f.isDirectory()) {
                        searchFile(f,name);
                    }
                }
            }
        }
    }
}
