package 递归;

public class 猴子吃桃 {
    public static void main(String[] args) {
        System.out.println(eat(1));
    }

    /*
    f(x+1) = f(x)/2 - 1
    2f(x+1) = f(x) -2
    f(x) = 2(f(x)+1)
    */
    public static int eat(int day) {
        if (day == 10) {
            return 1 ;
        }else {
            return 2*(eat(day+1)+1);
        }
    }
}
