package File类;

import java.io.File;

public class 常用API {
    public static void main(String[] args) {
        File file = new File("day09/src/File类/name");

        System.out.println("------------------------------------");
//        1. 返回绝对路径
        System.out.println(file.getAbsolutePath());
        System.out.println("------------------------------------");
//        2. 返回创建时的路径
        System.out.println(file.getPath());
        System.out.println("------------------------------------");
//        3. 返回文件名称
        System.out.println(file.getName());
        System.out.println("------------------------------------");

        System.out.println(file.length());
        System.out.println("------------------------------------");

        System.out.println(file.exists());
        System.out.println("------------------------------------");

        System.out.println(file.isFile());
        System.out.println("------------------------------------");

        System.out.println(file.isDirectory());
    }
}
