package File类;

import java.io.File;

public class File创建方法 {
    public static void main(String[] args) {
        String lujy = "./day09/src/File类/name";
//        构造方法一：路径+文件名
        File file = new File(lujy);
//        获取文件创建时的路径
        System.out.println(file.getPath());
        System.out.println(file.length());
        System.out.println("-----------------------------------");

//      构造方法二：路径，文件名
        String l1 = "day09/src/File类";
        String name = "name";
        File file1 = new File(l1,name);
//        获取文件的绝对路径
        System.out.println(file1.getAbsolutePath());
        System.out.println("-----------------------------------");

//        构造方法三：File类对象，文件名
        File file2 = new File(l1);
        File file3 = new File(file2,"name");
//        返回路径和文件名称
        System.out.println(file3.getName());

    }
}
