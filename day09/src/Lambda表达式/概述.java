package Lambda表达式;

public class 概述 {
    public static void main(String[] args) {

//        只能简化函数式接口，Thread内有多个方法，不属于函数式代码
        Thread thread0 = new Thread(){
            @Override
            public void run() {
                System.out.println("hello world");
            }
        };
        thread0.start();

//        Runnable是函数式接口，可也简化写
        new Thread(() -> {
                System.out.println("hello world Runnable 是函数式接口");
        }).start();

//        进一步简化成一行代码
//        省略大括号和分号
        new Thread(() -> System.out.println("hello world Runnable 是函数式接口") ).start();
    }
}
