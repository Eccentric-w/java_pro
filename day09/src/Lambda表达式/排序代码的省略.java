package Lambda表达式;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class 排序代码的省略 {
    public static void main(String[] args) {


        List<Student> studentList = new ArrayList<>();

        Student student0 = new Student("lucy",23);
        Student student1 = new Student("tony",12);
        Student student2 = new Student("seven",13);
//        批量添加元素进集合，第一个参数是集合，第二个参数是可变参数(T…)
        Collections.addAll(studentList,student0,student1,student2);

        /**
         Comparator是函数式接口,使用lambda写法
         省略形参类型
         省略return，省略大括号和分号
         */
//        按照升序排序
        Collections.sort(studentList, ( s1,  s2) -> s1.getAge() - s2.getAge() );

//        高级for循环
        for (Student student : studentList) {
            System.out.println(student);
        }

//        forEach原始写法，Consumer是函数式接口，可以使用Lambda表达式
        studentList.forEach(new Consumer<Student>() {
            @Override
            public void accept(Student student) {
                System.out.println(student);
            }
        });

//        简化后一行代码
        /**
            只有一个形参，可以省略形参括号
         */
        studentList.forEach(student -> System.out.println(student));

    }
}

class Student {
    private String name ;
    private int age ;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", age=" + age;
    }
}