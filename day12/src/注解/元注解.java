package 注解;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//@Class("12306")   // 不用用于类
public class 元注解 {

    @ClassTest("name") // 只能用于成员变量
    String name ;
}

@Target(ElementType.FIELD)  // 只能用在成员变量
@Retention(RetentionPolicy.RUNTIME) // 全阶段都能起作用
@interface ClassTest {
    String value();
    int age() default 123;
}