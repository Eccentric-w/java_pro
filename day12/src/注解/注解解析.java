package 注解;

import java.lang.annotation.*;

public class 注解解析 {
    @org.junit.Test
//    解析类的注释
    public void fjus() {
        Class c = Qwe.class;
        if (c.isAnnotationPresent(File.class)) {
            File file = (File) c.getDeclaredAnnotation(File.class);
            System.out.println(file.size());
            System.out.println(file.value());
            System.out.println(file.isFile());
        }

    }
}


@File(size = 2048,isFile = false)
class Qwe {
    @File(value = "test",size = 1024, isFile = true)
    String test ;
}


@Target({ElementType.TYPE,ElementType.FIELD})   // 可以在类、接口，和成员变量上声明
@Retention(RetentionPolicy.RUNTIME) // 永久有效
@interface File {
    String value() default "新建文件";
    long size() ;
    boolean isFile() ;
}