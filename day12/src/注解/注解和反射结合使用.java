package 注解;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

/**
 * 只执行有MyTest注解的方法
 */

public class 注解和反射结合使用 {

    public static void main(String[] args) throws Exception{

        TestDemo testDemo = new TestDemo(); // 得到对象
        Class testDemoClass = TestDemo.class;   // 获取类
        Method[] methods = testDemoClass.getDeclaredMethods();  // 获取中所有的方法
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyTest.class)) { //如果这个方法有MyTest注解，则执行
                method.invoke(testDemo);    //执行testDemo对象的此方法
            }
        }
    }
}



class TestDemo {

    @MyTest
    public void test1() { System.out.println("test1"); }

    @MyTest
    public void test2() { System.out.println("test2"); }

    public void test3() { System.out.println("test3"); }

    @MyTest
    public void test4() { System.out.println("test4"); }

    @MyTest
    public void test5() { System.out.println("test5"); }
}



@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyTest {

}