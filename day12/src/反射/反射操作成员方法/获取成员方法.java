package 反射.反射操作成员方法;

import org.junit.Test;

import java.lang.reflect.Method;

public class 获取成员方法 {
    static Class user = User.class; // 先获取类对象

    @Test
//    根据方法名参数获取方法，受public限制
    public void testOne() throws Exception {
        Method test = user.getDeclaredMethod("test1");  //获取方法
        System.out.println(test.getName()+":"+test.getReturnType());    // 方法名和类型
    }

    @Test
    public void testAll() throws Exception {
        Method[] methods = user.getDeclaredMethods();   // 不受限制，获得所有方法
        for (Method method : methods) {
            System.out.println(method.getName()+":"+method.getReturnType());
        }
    }

    @Test
    public void testInvoke() throws Exception {
        Method method = user.getDeclaredMethod("test1");
//        method.setAccessible(true);   // test1 是私用方法，所以要打开方法权限，暴力反射
        User u = new User();
        method.invoke(u);    // 调用u对象的test1方法

    }


}
