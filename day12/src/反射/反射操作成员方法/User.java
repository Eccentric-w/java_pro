package 反射.反射操作成员方法;

public class User {
    private String name ;
    private int age ;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    private void test1() {
        System.out.println("this is test1");
    }
    public void test2(String name) {
        System.out.println("this is test2");
    }
    public void test3(int age) {
        System.out.println("this is test3");
    }
    public void test4() {
        System.out.println("this is test4");
    }
    private void test5(String naem ,int age ) {
        System.out.println("this is test5");
    }
}
