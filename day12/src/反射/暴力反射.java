package 反射;

import org.junit.Test;
import java.lang.reflect.Constructor;

public class 暴力反射 {

    @Test
//    通过有参构造器，得到类的对象，并返回
    public void testCreateObject() throws Exception {
//        创建Student对象
        Student student = new Student("nick","01233");
        Class s = student.getClass();   // 得到Student类

//        得到有参构造器
        Constructor constructor = s.getDeclaredConstructor(String.class, String.class);
//        constructor.setAccessible(true);    //打开私有构造器的访问权限权限
        Student student1 = (Student)constructor.newInstance("frank","12345");

        System.out.println(student1);
    }



    @Test
//    通过无参构造器，创建对象
    public void testCreateObject1() throws Exception{
        Class s = Teacher.class;
        Constructor constructor = s.getDeclaredConstructor(String.class,String.class);
        constructor.setAccessible(true);    // 打开私有构造器的权限

        Teacher teacher = (Teacher)constructor.newInstance();
        System.out.println(teacher);

    }
}
