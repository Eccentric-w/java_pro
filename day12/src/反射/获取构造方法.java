package 反射;

import org.junit.*;

import java.lang.reflect.Constructor;

public class 获取构造方法 {

    static Student student = new Student();
    static Class s = student.getClass();

    @Test
//    指定参数，只能获取public修饰的,一个构造器
    public void testGetConstructor() throws NoSuchMethodException {
        System.out.println("-----------------一个public---------------------------");

        Constructor constructor = s.getConstructor(String.class);
//                                构造器名字             构造器参数
            System.out.println(constructor.getName()+":"+constructor.getParameterCount());
    }

    @Test
//    指定参数，匹配一个，不受修饰符限制
    public void testGetDeclaredConstructor() throws NoSuchMethodException {
        System.out.println("--------------------一个不限------------------------");

        Constructor constructor = s.getDeclaredConstructor(String.class,String.class);
//                         构造器全名
        System.out.println(constructor+":"+constructor.getParameterCount());

    }

    @Test
//      不指定参数，获得所有public修饰的构造器，
    public void testGetConstructors() {
        System.out.println("-------------------public所有-------------------------");

        Constructor[] constructors = s.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getName()+":"+constructor.getParameterCount());
        }
    }

    @Test

    public void testGetDeclaredConstructors() {
        System.out.println("------------------不限所有--------------------------");

        Constructor[] constructors = s.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getName()+":"+constructor.getParameterCount());
        }
    }
}
