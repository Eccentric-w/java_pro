package 反射;

public class Teacher {
    private String name ;
    private String ID ;

    private Teacher() {
    }

    public Teacher(String name, String ID) {
        this.name = name;
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", ID='" + ID + '\'' +
                '}';
    }
}
