package 反射;

public class 获得Class方法 {
    public static void main(String[] args) throws ClassNotFoundException {

//        1. 类名.class
        Class student = Student.class;
        System.out.println(student);

//        2. 对象.getclass方法
        Class student2 = Student.class;
        System.out.println(student2);

//        3. Class.forname()方法
//        直接加载改类的class文件
        Class student3 = Class.forName("反射.Student");
        System.out.println(student3);

        System.out.println(student.getName());  // 该类的全名
        System.out.println(student.getSimpleName());    // 类名

    }
}
