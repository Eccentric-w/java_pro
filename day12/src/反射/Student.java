package 反射;

public class Student {
    private String name ;
    private String ID ;

    public Student(String name, String ID) {
        this.name = name;
        this.ID = ID;
    }

    public Student(String name) {
        this.name = name;
    }

    public Student() {}

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", ID='" + ID + '\'' +
                '}';
    }
}
