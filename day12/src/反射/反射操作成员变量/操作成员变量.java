package 反射.反射操作成员变量;

import org.junit.Test;

import java.lang.reflect.Field;

public class 操作成员变量 {

    @Test
    public void testName() throws Exception {
        Cat cat = new Cat("miao",1,"man");
        Class catClass = cat.getClass();
        System.out.println(cat.getName());

        Field nameField = catClass.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(cat,"hwhw");  // 给类对象指定变量赋值
        nameField.get(cat); // 获取指定对象的name变量值
        System.out.println(cat.getName());  // cat对象的name变量属性发上变化

    }
}
