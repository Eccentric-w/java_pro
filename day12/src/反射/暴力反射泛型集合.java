package 反射;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class 暴力反射泛型集合 {

    @Test
    public void test() throws Exception {

        List<Integer> list = new ArrayList<>();
        list.add(1);    // 无修饰符
        list.add(1);    // List集合可重复
        list.add(1);
        list.add(1);
        System.out.println(list);

        System.out.println("--------------------进入暴力反射--------------------");
//        泛型只能在编译阶段工作（约束），运行阶段不能约束了
//        反射在运行阶段工作

        Class listClass = list.getClass();
        Method listRun = listClass.getDeclaredMethod("add", Object.class);
        listRun.invoke(list,"hello world");

//        通过迭代器遍历
        Iterator iterator = list.listIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next()+", ");
        }
        System.out.println("\b");
    }
}
