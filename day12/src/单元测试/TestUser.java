package 单元测试;

import org.junit.*;

public class TestUser {

    @Test
    public void testUser() {
        User user = new User("admin","123456");
        System.out.println(user.login());
    }

    @Test
    public void testFunction() {
        User.function();
    }

    @Before
    public void before() {
        System.out.println("在实例方法前执行");
    }

    @After
    public void after() {
        System.out.println("在实例方法之后执行");
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("在静态方法之前执行");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("在静态方法之后执行");
    }

}
