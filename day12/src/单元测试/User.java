package 单元测试;

public class User {
    private String name ;
    private String password ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }
    public boolean login() {
        if (name == "admin" && password == "123456") {
            return true;
        }
        return false;
    }

    public static void function() {
        System.out.println("这是静态方法");
    }
}
