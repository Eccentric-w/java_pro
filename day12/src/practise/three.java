package practise;

import java.lang.reflect.Constructor;

public class three {
    public static void main(String[] args) throws Exception{
//        获取类对象
//        在同一文件中的类是并列关系，不是嵌套，Student类属于在practise文件夹下
        Class student = Class.forName("practise.Student");    // 路径中有中文可能会报错，尽量不用中文
//        1. 通过Class对象创建
//        这样写会覆盖无参构造器，因此类中一定要存在无参构造器
        Student student1 = (Student) student.newInstance(); // 划线说明方法已经过时了
//        Object s = student.newInstance("nick","09",23);
        System.out.println(student1);
//        Student student = new Student("nick","09",12);
//        Student student1 = Student.class;

//        2. 通过Constructor对象的方法创建
        Constructor constructor = student.getConstructor(String.class,String.class,int.class);
        Student student2 = (Student) constructor.newInstance("nick","09",23);
        System.out.println(student2);

    }
}

class Student {
    String name ;
    String ID ;
    int age ;

    public Student() {
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", ID='" + ID + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student(String name, String ID, int age) {
        this.name = name;
        this.ID = ID;
        this.age = age;
    }
}