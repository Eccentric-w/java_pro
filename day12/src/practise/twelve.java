package practise;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

// 解析获得该成员方法上使用注解的属性值。
public class twelve {

    @Book(value = "西游记",price = 12.5,authors = {"孙悟空", "猪八戒"})
    public void sell() {}

    public static void main(String[] args) throws Exception{
        Class c = twelve.class;
        Method method = c.getDeclaredMethod("sell");
        System.out.println(method.isAnnotationPresent(Book.class));
        if (method.isAnnotationPresent(Book.class)) {   // 如果method有Book修饰，则获取Book对象，输出Book对象信息
            Book book = method.getAnnotation(Book.class);
            System.out.println(book);
        }
    }
}

@Target(ElementType.METHOD) // 成员方法使用注解
@interface Book {
    String value() ; // 书名
    double price() ; // 价格
    String[] authors(); // 多位作者
}