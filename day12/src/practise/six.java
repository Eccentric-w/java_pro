package practise;

import java.lang.reflect.Field;

public class six {

    public static void main(String[] args) throws Exception {
        B b = new B();
        setProperty(b,"propertyName","hello world");
        System.out.println(getProperty(b,"propertyName"));
    }
//                                对象        成员变量名            要赋给变量的值
    public static void setProperty(Object obj, String propertyName, Object value) throws Exception {
        Class c = obj.getClass();   // 获取类对象
        Field field = c.getDeclaredField(propertyName); // 根据变量名获取不受修饰的变量
        field.set(obj,value);   // 给指定对象的变量赋值
    }

    public static Object getProperty(Object obj, String propertyName) throws Exception{
        Class c = obj.getClass();
        Field field = c.getDeclaredField(propertyName);
        Object value  = field.get(obj); // 获取变量的值
        return value;
    }
}

class B {
    String propertyName ;
    public void showPropertyName() {
        System.out.println(propertyName);
    }
}