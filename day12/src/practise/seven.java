package practise;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class seven {
    public static void main(String[] args) throws Exception{
        Class c = Class.forName("practise.Person"); // 获取类对象分
        Constructor constructor = (Constructor)c.getConstructor(String.class,int.class);    // 获取Constructor对象，类的也有参构造方法
        Person person = (Person)constructor.newInstance("nick",23); // 实例化有参的Person对象
        System.out.println("========================");
        System.out.println(person); // 打印初始的persion对象信息
        Method setNameMethod = c.getDeclaredMethod("setName", String.class);    // 反射获取setName方法
        setNameMethod.invoke(person,"frank");    // 反射成员方法的方式对name进行赋值，参数（对象，值）
        System.out.println("========================");
        System.out.println(person); // 打印改变名字后的person对象
        Field field = c.getDeclaredField("age");    // 获取指定变量名，不受修饰符限制的成员变量对象
        field.set(person,37);   // 反射赋值为37 ，参数（对象，赋值的值）
        System.out.println("========================");
        System.out.println(person); // 打印修改age后的对象信息
        System.out.println("========================");

    }
}

class Person {
    String name ;
    int age ;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}