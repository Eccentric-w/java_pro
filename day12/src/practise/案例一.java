package practise;

import org.junit.*;

public class 案例一 {  // 测试方法只能写在此类中，注意测试类命名
    Calculator calculator ;
    @Before // 在每一个实例方法运行前执行
    public void init() {    // 测试方法前初始化对象
        if (calculator == null) {
            calculator = new Calculator();
        }
    }
    @After  // 在每一个是实例方法运行后执行
    public void close() {   // 测试完一个方法，讲对象设置为null，防止影响下一个方法的测试
        calculator = null;
    }
    @Test
    public void TestAdd() {
        int result = calculator.add(1,2);
//        设置期望值，如果不同则报错
//        绿色，证明通过
        Assert.assertEquals("加法有问题",3,result);
    }
    @Test
    public void TestSub() {
        int result = calculator.sub(5,2);
        Assert.assertEquals("减法有问题",3,result);
    }
    @Test
    public void TestMul() {
        int result = calculator.mul(2,5);
        Assert.assertEquals("乘法有问题",12,result); // 故意写错，报错
    }
    @Test
    public void TestDiv() {
        int result = calculator.div(10,5);
        Assert.assertEquals("除法有问题",2,result);
//        Assert.assertEquals("除法有问题",2.0,result);
    }

}

class Calculator {
    public int add(int addNnm1 , int addNum2) {
        return addNnm1 + addNum2;
    }
    public int sub(int subNum1 , int subNum2) {
        return subNum1 - subNum2;
    }
    public int mul(int mulNum1 , int mulNum2) {
        return mulNum1 * mulNum2;
    }
    public int div(int divNum1 , int divNum2) {
        return divNum1 / divNum2;
    }
}