package practise;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class four {
    public static void main(String[] args) throws Exception{
//        获取class对象
        Class ps = Class.forName("practise.PrintString");
        Constructor constructor = ps.getConstructor(String.class);
        PrintString printString = (PrintString) constructor.newInstance("打印反射的字符串");
        printString.printString();

        PrintString printString1 = new PrintString();
        Class p = printString1.getClass();
        Method psMethod = p.getMethod("printString");  // 根据方法名和参数获取public修饰的方法
        Field field = p.getDeclaredField("string"); // 根据成员变量名获取不受修饰限制的变量
        field.set(printString1,"打印第二种方式反射的字符串");
        psMethod.invoke(printString1);  // 由于打印方法没有参数，因此无法在这里直接设置字符串参数打印，需要另外反射字符串成员变量
    }
}

class PrintString {
    String string = "打印一个字符串";

    public PrintString() {
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public PrintString(String string) {
        this.string = string;
    }

    public void printString() {
        System.out.println(string);
    }
}