package practise;

import java.io.*;
import java.util.Properties;

public class Nine {
    public static void main(String[] args) throws Exception{
        InputStream inputStream = new FileInputStream("day12/src/practise/file/nine.properties");
        Properties properties = new Properties();
        properties.load(new BufferedInputStream(inputStream));
        OutputStream outputStream = new FileOutputStream("day12/src/practise/file/nine.properties");
        int count = 0;
        if (!properties.stringPropertyNames().contains("count")) {
            properties.setProperty("count","0");
            properties.store(outputStream,"");
        }else {
            count = Integer.valueOf(properties.getProperty("count"));
        }
        if (count < 3) {
            System.out.println("当前已经运行次数"+count);
            count++;
            properties.setProperty("count",String.valueOf(count));
        }else {
            System.out.println("已经运行3次了");
        }
        properties.store(outputStream,"");

        outputStream.close();
        inputStream.close();
    }
}
