package practise;

import java.lang.reflect.Method;
import java.util.ArrayList;
/*
* 利用反射机制在这个泛型为Integer的ArrayList中存放一个String类型的对象*/
public class 案例二 {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(111);
        integers.add(222);
        integers.add(333);

        Class o = integers.getClass();  // 通过对象获取类对象
        Method method = o.getMethod("add", Object.class);  // 获取类中的指定成员方法,根据方法名和参数获取，只能获取public修饰的
        method.invoke(integers,"erere");    // 添加字符串元素
        System.out.println(integers);
    }
}
