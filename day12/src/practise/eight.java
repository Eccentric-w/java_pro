package practise;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class eight {
    public static void main(String[] args) throws Exception{
        Properties properties = new Properties();
        properties.load(new BufferedInputStream(
                new FileInputStream("day12/src/practise/file/properties.properties")));
        properties.setProperty("DemoClass_path","practise.DemoClass");
        String DemoClass_path = properties.getProperty("DemoClass_path");
        Class c = Class.forName(DemoClass_path);
        Method rumMethod = c.getDeclaredMethod("run");
        rumMethod.invoke((DemoClass)c.newInstance());

    }
}
class DemoClass {
    public void run() {
        System.out.println("welcome to MyWorld");
    }
}