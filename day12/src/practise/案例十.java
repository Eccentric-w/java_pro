package practise;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 自定义注解
public class 案例十 {

}

@Target(ElementType.TYPE)   // 用于修饰类和接口（声明范围）
@Retention(RetentionPolicy.RUNTIME)   // 只在源码和字节码阶段有效（生命周期）
@interface MyAnno1 {
//    这个不需要写属性
//    如果只有一个属性时，属性名定义为value
}

@Target(ElementType.FIELD)  // 只修饰字段
@Retention(RetentionPolicy.SOURCE)  // 只在源码阶段生效
@interface MyAnno2 {
    public String type() default "java" ;// 定义默认值为：java
}

@Target(ElementType.METHOD) // 只修饰方法
@Retention(RetentionPolicy.CLASS)   // 源码和字节码阶段生效
@interface MyAnno3 {
    public String type() ;
    int[] intArr() ;
}