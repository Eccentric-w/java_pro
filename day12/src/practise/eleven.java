package practise;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class eleven {
    public static void main(String[] args) throws Exception{
        Class c = Class.forName("practise.Ait");    // 获取类对象
        Object o = c.getConstructor().newInstance();    // 获取Constructor对象并获取对应类对象
        Method[] methods = c.getDeclaredMethods();  // 获取所有方法（不受修饰符限制）
        for (Method method : methods) { // 便利所有方法
            if (method.isAnnotationPresent(MyTest.class)) { // 如果方法有@MyTest修饰，则执行
                method.invoke(o);
            }
        }

    }
}

@Target(ElementType.METHOD) // 只应用于方法
@Retention(RetentionPolicy.RUNTIME) // 在运行时可用
@interface MyTest { // 自定义注解类型

}

class Ait {

    @MyTest
    public void show1() {
        System.out.println("i am show 1");
    }

    @MyTest
    public void show2() {
        System.out.println("i am show 2");
    }
}