package 练习题.关卡三;

import org.w3c.dom.ls.LSOutput;

import java.lang.reflect.Array;
import java.security.spec.RSAOtherPrimeInfo;
import java.util.*;

public class 案例一 {

    static List<Integer> redArraryList = new ArrayList<>();
    static List<Integer> blueArraryList = new ArrayList<>();

    static {
        for (int i = 1 ; i < 17 ; i++) {
            blueArraryList.add(i);
        }
        redArraryList.addAll(blueArraryList);
        for (int i = 17 ; i < 34 ; i++){
            redArraryList.add(i);
        }

    }
    public static void main(String[] args) {

        System.out.println(redArraryList);
        System.out.println(blueArraryList);

//        打乱顺序
        Collections.shuffle(redArraryList);
        Collections.shuffle(blueArraryList);
        System.out.println(redArraryList);
        System.out.println(blueArraryList);

        System.out.println("随机生成的号码:");
        System.out.println("红色球:"+redArraryList.subList(0,6));
        System.out.println("蓝色球:"+blueArraryList.get(2));

    }
}
