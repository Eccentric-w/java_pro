package 练习题.关卡三;

import javax.print.attribute.standard.PresentationDirection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class test {
    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();

        Collections.addAll(list1,1,2,3,4,5,6);

//        此方法截取的集合，会随主集合元素顺序的变化，截取的子集合也变化
        list2 = list1.subList(0,3);

        System.out.println(list1);
        System.out.println(list2);

        Collections.shuffle(list1);

        System.out.println(list1);
        System.out.println(list2);
    }
}
