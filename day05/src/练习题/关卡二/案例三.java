package 练习题.关卡二;

import java.util.*;

public class 案例三 {

    static List<Character> characterArrayList = new ArrayList<>();
    static Set<Character> characterSet = new HashSet<>();

    static {
        characterArrayList.add('a');
        characterArrayList.add('b');
        characterArrayList.add('c');
        characterArrayList.add('b');
        characterArrayList.add('a');
    }

    public static void main(String[] args) {

//        characterArrayList集合中的元素全部导入characterSet集合中
        characterSet.addAll(characterArrayList);
//        清空characterArrayList集合
        characterArrayList.clear();

        characterArrayList.addAll(characterSet);

        Iterator iterator = characterArrayList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }
}
