package 练习题.关卡二;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class 案例一 {
    public static void main(String[] args) {

        Student student01 = new Student("nick",23,76);
        Student student02 = new Student("jack",12,89);
        Student student03 = new Student("lucy",32,98);
        Student student04 = new Student("liming",21,87);
        Student student05 = new Student("adi",18,96);

        List<Student> list = new ArrayList<>();
        list.add(student01);
        list.add(student02);
        list.add(student03);
        list.add(student04);
        list.add(student05);

        int totalScore = 0 ;
        double averageScore = 0;
        int maxScore = 0 ;
        int minScore = 100 ;

//        对象的迭代器
        Iterator iterator = list.iterator();

        System.out.println("学生信息：");
        System.out.println("姓名\t 年龄");
        while (iterator.hasNext()) {
            Student student = (Student) iterator.next();
            System.out.println(student.name+"\t "+student.age);
            totalScore += student.score;
            if (student.score < minScore) {
                minScore = (int) student.score;
            } else if (student.score > maxScore){
                maxScore = (int) student.score;
            }
        }

        System.out.println("总分\t 平均分\t 最高分\t 最低分");
        averageScore = totalScore/list.size();
        System.out.println(totalScore+"\t\t "+averageScore+"\t "+maxScore+"\t\t "+minScore);


    }
}


//学生类
class Student {
    String name ;
    int age ;
    double score ;

    public Student() {}

    public Student(String name, int age, double score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}