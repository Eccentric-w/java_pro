package 练习题.关卡二;

import javax.crypto.spec.PSource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class 案例二 {

    static List<Person> personList = new ArrayList<>();

    static {

        Person person1 = new Person("jack",23,1.87);
        Person person2 = new Person("nick",35,1.78);
        Person person3 = new Person("lucy",18,1.50);
        Person person4 = new Person("mary",32,1.69);
        Person person5 = new Person("adi",21,1.80);

        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);

    }

    public static void main(String[] args) {

        Iterator<Person> personIterator = personList.iterator();

        Person maxHighPerson = personList.get(0);
        Person minHighPerson = personList.get(0);

        while (personIterator.hasNext()) {
            Person person = personIterator.next();
            if (person.getHigh() > maxHighPerson.getHigh()) {
                maxHighPerson = person;
            }else if (person.getHigh() < minHighPerson.getHigh()) {
                minHighPerson = person;
            }
        }

        System.out.printf("最高的人是"+maxHighPerson.getName()+",身高%.2f",maxHighPerson.getHigh());
        System.out.println();
        System.out.printf("最低的人是"+minHighPerson.getName()+",身高%.2f",minHighPerson.getHigh());

    }
}

class Person {
    private String name ;
    private int age;
    private double high ;

    public Person() {}

    public Person(String name, int age, double high) {
        this.name = name;
        this.age = age;
        this.high = high;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }
}