package 练习题.关卡一;

import java.util.LinkedList;
import java.util.List;

public class 案例三_LinkedList {
    public static void main(String[] args) {

        List<String> linkedList = new LinkedList();

//        1. 添加元素
        linkedList.add("test");
        linkedList.add("test1");
        linkedList.add("test2");
        linkedList.add("test3");

        System.out.println(linkedList);

        System.out.println("---------------------------------------");
//        2. 指定位置元素赋值
        linkedList.set(1,"test1.1");
        System.out.println(linkedList);
        System.out.println("---------------------------------------");

//        3. 获取指定元素
        System.out.println(linkedList.get(2));
        System.out.println("---------------------------------------");

//        4. 删除指定索引元素
        linkedList.remove(3);

        System.out.println("---------------------------------------");

//        5. 获取集合大小
        System.out.println(linkedList.size());

        System.out.println(linkedList);
        System.out.println("---------------------------------------");

//        6. 清除集合元素
        linkedList.clear();
        System.out.println(linkedList);
        System.out.println("---------------------------------------");

    }
}
