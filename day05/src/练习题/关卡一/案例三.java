package 练习题.关卡一;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 ArraryList集合中，迭代器的使用
 */

public class 案例三 {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("abcd1");
        list.add("abcd2");
        list.add("abcd3");
        list.add("abcd4");

//        获取容器的迭代器对象
        Iterator iterator = list.iterator();

//        判断有没有下一个元素
        while (iterator.hasNext()) {

//                              获取下一个元素
            System.out.println(iterator.next());
        }
    }
}
