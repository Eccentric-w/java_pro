package 集合遍历方式;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class 迭代器 {
    public static void main(String[] args) {

//        遵循多态的写法
//        定义String的泛型
        Collection<String> arrayList = new ArrayList();
        arrayList.add("java");
        arrayList.add("python");
        arrayList.add("C");
        arrayList.add("go");

        System.out.println("直接输出：");
        System.out.println(arrayList);

//        得到集合的迭代器对象
//        指定String类型
        Iterator<String> iterator = arrayList.iterator();

        System.out.println("迭代器方式输出:");
//        利用迭代器判断集合下一个元素不为空
        while (iterator.hasNext()) {
//            刚开始指向集合开头，调用方法使它指向一个元素，然后逐个向后移动取值
            String s = iterator.next();
            System.out.println(s);
        }
    }
}
