package 集合遍历方式;

import java.util.ArrayList;
import java.util.Collection;

public class lambda表达式 {
    public static void main(String[] args) {

//        因为此处collection接口没有索引，索引无法对arrayList用get方法获取指定位置的元素
        Collection<String> arrayList = new ArrayList();
        arrayList.add("java");
        arrayList.add("python");
        arrayList.add("C");
        arrayList.add("go");

        System.out.println("直接用lambda方法:");
        arrayList.forEach(s -> {
            System.out.print(s+",");
        });
        System.out.println("\b");

        System.out.println("简写方法:");
        arrayList.forEach(s -> System.out.print(s+","));
        System.out.println("\b");


        System.out.println("超级简写方法:");
        arrayList.forEach( System.out::println );
    }
}
