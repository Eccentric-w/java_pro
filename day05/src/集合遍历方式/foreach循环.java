package 集合遍历方式;

import java.util.ArrayList;
import java.util.Collection;

public class foreach循环 {
    public static void main(String[] args) {

        Collection<String> arrayList = new ArrayList();
        arrayList.add("java");
        arrayList.add("python");
        arrayList.add("C");
        arrayList.add("go");
        System.out.println("直接输出集合:");
        System.out.println(arrayList);

        System.out.println("foreach循环输出集合:");
//        快捷键：集合名.for
        for (String s : arrayList) {
            System.out.print(s+",");
        }
        System.out.println("\b");

        System.out.println();

        Integer[] integers = new Integer[] {10,23,5,6,32};
        System.out.println("直接输出数组:");
//        数组没有重写toString方法，因此输出的是数组地址
        System.out.println(integers);

        System.out.println("foreach循环输出数组:");
        for (Integer integer : integers) {
            System.out.print(integer+",");
        }
        System.out.println("\b");

    }
}
