package 斗地主练习;

import java.util.Comparator;

public class Cards {
    private String number ; //点数
    private String colors ; //花色
    private int index ; //牌索引，方便后续排序

    public Cards(String number, String colors, int index) {
        this.number = number;
        this.colors = colors;
        this.index = index;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
//        只返回点数和花色
        return number + colors;
    }

}
