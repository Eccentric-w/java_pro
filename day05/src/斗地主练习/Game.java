package 斗地主练习;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 创建一副牌
 点数："3","4","5","6","7","8","9","10","J","Q","K","A","2"
 花色："♤","♡","♧","♢"
 特殊的大小王："♛","♚"

 洗牌
 发牌
 对牌进行排序
 看牌

 */

public class Game {

    static public final List<Cards> CARDS_LIST = new ArrayList<>();

    static {

//        已知长度和类型用数组
        String[] numbers = new String[] {"3","4","5","6","7","8","9","10","J","Q","K","A","2"};
        String[] colors = new String[] {"♤","♡","♧","♢"};

//        定义初始索引为0，每张牌的大小逐个递增
        int index = 0;

        for (String number : numbers) {
            for (String color : colors) {
//                索引每次运行完后递增
                CARDS_LIST.add(new Cards(number,color,index++));
            }
        }
//        最后替另加入大小王
        CARDS_LIST.add(new Cards("","♚",index++));
        CARDS_LIST.add(new Cards("","♛",index++));

        System.out.println("新的一副牌:"+CARDS_LIST);

//        洗牌:随机打乱牌，使牌变得无序
        Collections.shuffle(CARDS_LIST);
        System.out.println("洗后的牌:"+CARDS_LIST);
    }

    public static void main(String[] args) {

//        创建三个玩家
        List<Cards> diZhu = new ArrayList();
        List<Cards> nongMin1 = new ArrayList();
        List<Cards> nongMin2 = new ArrayList();

//        对三名玩家发牌
        dealCards(diZhu,nongMin1,nongMin2);

//        输出三名玩家排序后的牌
        sortCards(diZhu);
        sortCards(nongMin1);
        sortCards(nongMin2);
        System.out.println("地主的牌:"+diZhu);
        System.out.println("农民1号的牌:"+nongMin1);
        System.out.println("农民2号的牌:"+nongMin2);

//        切割集合最后三个元素
        System.out.println("三张底牌:"+CARDS_LIST.subList(CARDS_LIST.size()-3,CARDS_LIST.size()));

    }

    private static void sortCards(List<Cards> cards) {
        Collections.sort(cards, new Comparator<Cards>() {
            @Override
//            牌降序排列
            public int compare(Cards o1, Cards o2) {
                return o2.getIndex() - o1.getIndex();
            }
        });
    }

    //    发牌方法
    //    可变参数
    static void dealCards(List<Cards>...cards) {

//        发牌剩下最后三张底牌
        for (int i = 0;i<CARDS_LIST.size()-3;i++) {
            if (i % 3 == 0) {
                cards[0].add(CARDS_LIST.get(i));
            }else if (i % 3 == 1) {
                cards[1].add(CARDS_LIST.get(i));
            }else {
                cards[2].add(CARDS_LIST.get(i));
            }
        }
    }



}
