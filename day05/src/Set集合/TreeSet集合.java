package Set集合;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSet集合 {
    public static void main(String[] args) {

        Test test = new Test("张三","gk50z",23);
        Test test1 = new Test("李四","y7000",43);
        Test test2 = new Test("王五","p300",19);

//        第二种方法：在定义集合时，调用自带的比较器
        Set<Test> list2 = new TreeSet<>(new Comparator<Test>() {
            @Override
//            此处写法同下
//            按照id升序排列
//            排序规则冲突时，以类自带的比较方法为主（此规则）
            public int compare(Test o1, Test o2) {
//                比较字符串大小，返回ascll差值
                return o1.id.compareTo(o2.id);
            }
        });
        list2.add(test);
        list2.add(test1);
        list2.add(test2);
        System.out.println(list2);

//        直接输出会报错，因为默认的排序排不了
//        System.out.println(list2);






//        插入的字符串按照ascll值对比，默认升序排序
//        对自定义类绪不能直接排序，需要调性比较器
        Set<String> list1 = new TreeSet();
        list1.add("zhangsan");
        list1.add("lisi");
        list1.add("wangwu");
        System.out.println(list1);

    }
}

//类实现比较器接口
//默认是object类型，没有Test类的成员变量，修改泛型方便重写函数
class Test implements Comparable<Test>{
    String name ;
    String id ;
    int age ;

    public Test(String name, String id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", age=" + age;
    }

    @Override
    public int compareTo(Test o) {

//        自定义比较项
//        如果this某项大于o的同一项，则返回0，其他同理
//        简单写为：
//        此时按照年龄升序排列
        return this.age - o.age;
    }
}
