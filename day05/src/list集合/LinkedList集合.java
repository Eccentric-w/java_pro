package list集合;

import java.util.LinkedList;

/**
 可以用来实现栈和队列
 */

public class LinkedList集合 {
    public static void main(String[] args) {

//        因为需要用到大量LinkedList集合特有的方法，所以不能用多态的方式，定义父类类型

        LinkedList list = new LinkedList();

//        1. 实现队列
        System.out.println("实现队列");

        LinkedList queue = new LinkedList();
//        入队,在末尾加入元素
        queue.addLast("第一号");
        queue.addLast("第二号");
        queue.addLast("第三号");
        queue.addLast("第四号");

        System.out.println(queue);

//        出队,在头部移除并返回元素
        System.out.println(queue.removeFirst());
        System.out.println(queue.removeFirst());

        System.out.println("------------------------------------");

//        2. 实现栈
        System.out.println("实现栈:");

        LinkedList stack = new LinkedList();

//        如栈
//        push方法底层就是调用addFirst方法
        stack.push("第一颗子弹");
        stack.addFirst("第二颗子弹");
        stack.addFirst("第三颗子弹");
        stack.addFirst("第四颗子弹");
        stack.addFirst("第五颗子弹");

        System.out.println(stack);

//        出栈,先进后出
//        pop方法底层就是调用removeFirst方法
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        System.out.println(stack);
    }
}
