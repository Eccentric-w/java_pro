package list集合;

import java.util.ArrayList;
import java.util.List;

public class ArrayLisy集合 {
    public static void main(String[] args) {

//        List集合有索引，可以查找指定位置的元素
        List<String> list = new ArrayList();

//        1. 添加元素
        list.add("java");
        list.add("python");
        list.add("C");
        list.add("go");

        System.out.println(list);

//        2. 获取指定元素
        System.out.println(list.get(1));

//        3. 移除指定位置元素
        System.out.println(list.remove(2));

//        4. 修改指定位置元素，返回修改前的值
        System.out.println(list.set(2,"scala"));

        System.out.println(list);
    }
}
