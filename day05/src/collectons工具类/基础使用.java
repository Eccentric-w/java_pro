package collectons工具类;

import java.util.*;

public class 基础使用 {
    public static void main(String[] args) {

        Test test1 = new Test("zhangsan");
        Test test2 = new Test("lisi");
        Test test3 = new Test("wangwu");

        List<Test> list = new ArrayList();

//        批量加入元素
        Collections.addAll(list,test1,test2,test3);
        System.out.println(list);

//        打乱元素顺序
        Collections.shuffle(list);
        System.out.println(list);

//        元素排序
        List<Double> list2 = new ArrayList<>();
        Collections.addAll(list2, 12.8, 34.8, 45.3, 34.4);
//        如果要排序自定义类型，需要在第二个参数位置定义构造器
        Collections.sort(list2);
        System.out.println(list2);


    }

}

class Test {
    String name ;

    public Test(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Test{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
