package TCP多线程通信;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class 服务端 {
    public static void main(String[] args) throws IOException {
        System.out.println("-------------------服务端启动-------------------");

        ServerSocket serverSocket = new ServerSocket(12345);

        while (true) {
            Socket socket = serverSocket.accept();
            new ServerSocketThread(socket).start();
        }
    }
}

class ServerSocketThread extends Thread {
    private Socket socket ;

    public ServerSocketThread(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

            byte[] bytes = new byte[1024*64];
            int len ;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                System.out.println(new String(bytes,0,len));
            }
        }catch (Exception e) {
            e.getStackTrace();
        }
    }
}