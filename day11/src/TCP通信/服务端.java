package TCP通信;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class 服务端 {
    public static void main(String[] args) throws Exception {
        System.out.println("-----------------服务端启动-----------------");

//        1. 创建服务端管道,注册端口
        ServerSocket serverSocket = new ServerSocket(12306);

//        2. 等待客户端的连接
        Socket socket = serverSocket.accept();
//        3. 通过socket对象，获得字节输入流
        InputStream inputStream =socket.getInputStream();
//        4. 字节缓冲流，高级流
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        byte[] bytes = new byte[1024*32];
        int len ;
        while ((len = bufferedInputStream.read(bytes)) != -1) {
            System.out.println(new String(bytes,0,len));
        }
    }
}
