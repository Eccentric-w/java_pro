package TCP通信;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class 客户端 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        System.out.println("-----------------客户端启动-----------------");

//        1. 建立客户端请求管道
        Socket socket = new Socket("localhost",12306);
//          2. 通过管道，创建低级字节输出流
        OutputStream os = socket.getOutputStream();
//        3. 将字节输出流封装成打印流
        PrintStream printStream = new PrintStream(os);


        printStream.println("hello wrold");
        printStream.flush();    //刷新使打印流的数据生效

        String s = null;
//        循环输入
        while (true) {
            s = scanner.nextLine();
            printStream.print(s);
            if (s == "end\n") {
                break;
            }
        }
        printStream.close();    //关闭资源
    }
}
