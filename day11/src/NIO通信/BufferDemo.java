package NIO通信;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.util.UUID;

public class BufferDemo {
    public static void main(String[] args) throws  Exception{
//        低级的字节输入流，源文件
        FileInputStream fileInputStream = new FileInputStream("day11/src/file/NIO/img.jpg");
//        目标文件，低级的字节输出流
        FileOutputStream fileOutputStream =
                new FileOutputStream("day11/src/file/NIO/"+ UUID.randomUUID().toString()+".jpg");

        /* 根据字节输出输入流，创建Channel通道 */
        FileChannel inputChannel = fileInputStream.getChannel();
        FileChannel outputChannel = fileOutputStream.getChannel();

//        Buffer 数组，字节类型，大小是2048字节
        ByteBuffer buffer = ByteBuffer.allocate(2048);

        int len ;
        while ((len = inputChannel.read(buffer)) != -1) {   // 通道向Buffer中读取数据
            buffer.flip();  // 由读模式切换到写模式，重置Buffer，让指针回到起始位置
            outputChannel.write(buffer);    // Buffer中的数据写入输出通道中
            buffer.clear(); //清除Buffer中的数据，循环写入
        }

//        关闭资源
        fileInputStream.close();
        fileOutputStream.close();
        inputChannel.close();
        outputChannel.close();

    }
}
