package TCP通信_线程池;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//线程池处理类
public class SocketThreadPool  {
//    定义线程池对象变量
    private ExecutorService executorService;
//                      构造方法    同时存在的最多线程数，阻塞队列
//    构造方法没有返回类型
    public SocketThreadPool(int maxPoolSize ,int queueSize) {
        executorService = new ThreadPoolExecutor(   //创建线程池对象
                maxPoolSize,    //最大同时运行的线程数
                maxPoolSize,    //最多支持的线程数，
                120L,   //最多阻塞时间，阻塞队列中的任务等待此时长还不被执行就死掉
                TimeUnit.SECONDS,   //阻塞时间单位
                new ArrayBlockingQueue<>(queueSize)     //阻塞对象对象，阻塞队列的长度
        );
    }

//    任务提交到线程池
    public void excute(Runnable task) {this.executorService.submit(task);}

}
