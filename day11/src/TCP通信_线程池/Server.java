package TCP通信_线程池;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

public class Server {
    public static void main(String[] args) {
        System.out.println("--------------------服务端启动--------------------");
        try(
                ServerSocket serverSocket = new ServerSocket(12345);
        ) {
//        1. 创建线程池  最多同时处理三个线程任务，阻塞队列大小50
            SocketThreadPool socketThreadPool = new SocketThreadPool(3,50);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("有人上线了");
                socketThreadPool.excute(new TCPRunnable(socket));
            }
        }catch (Exception e) {
            e.getStackTrace();
            System.out.println("有人下线了");
        }
    }
}
