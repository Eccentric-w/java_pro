package TCP通信_线程池;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        System.out.println("-------------------客户端启动-------------------");

        try(
                Socket socket = new Socket("localhost",12345);
                OutputStream outputStream = socket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);
        ) {
            Scanner scanner = new Scanner(System.in);
            String s ;
            while (true) {
                s = scanner.nextLine();
                printStream.print(s);
                printStream.flush();
            }

        }catch (Exception e) {

        }
    }
}
