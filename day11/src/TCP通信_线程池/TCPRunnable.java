package TCP通信_线程池;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.Socket;

public class TCPRunnable implements Runnable {
    private Socket socket;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public TCPRunnable(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

            byte[] bytes = new byte[1024*64];
            int len ;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                System.out.printf(socket.getInetAddress()+"发来数据:");
                System.out.println(new String(bytes,0,len));
            }
        }catch (Exception e) {
            e.getStackTrace();
            System.out.println("有人下线了");
        }
    }
}
