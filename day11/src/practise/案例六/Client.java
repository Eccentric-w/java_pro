package practise.案例六;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws Exception{
//        建立客户端请求管道：服务器地址，服务器端口号
        Socket socket = new Socket("localhost",12345);
//        通过字节传输，所以使用字符转换流，进而封装成高级的字符缓冲流
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        Scanner scanner = new Scanner(System.in);
//        String s = scanner.nextLine();
        String s = "hello";
        while (!s.equals("exit")) { // 这种循环方式，必须要双方都exit才能退出，一方exit后要一直等待对方exit
//            可以在循环内部判断，如果输入exit，就立刻退出循环
            System.out.print("我说:");
            s = scanner.nextLine(); // 我发给对对方的消息
            writer.write(s);    // 写入进管道发送
            writer.newLine();   // 换行
            writer.flush(); // 刷新：使写入的数据生效
            System.out.println("对方说:"+reader.readLine());   // 读取对方发送过来的消息数据
        }
//        关闭资源
        writer.close();
        reader.close();
    }
}
