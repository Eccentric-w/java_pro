package practise.案例八;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Service {
    public static void main(String[] args) throws Exception{
        System.out.println("==================服务端启动=========");
        ServerSocket serverSocket = new ServerSocket(55555);    // 注册端口号
        while (true) {  // 一直接受连接，可连接多个客户端
            Socket socket = serverSocket.accept();  //　接受客户端请求连接
            new Thread(new MyRunnable(socket)).start(); // 多线程启动
        }

    }
}

// 处理线程任务，处理客户端请求服务
class MyRunnable implements Runnable {
    private Socket socket ;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public MyRunnable(Socket socket) {
        this.socket = socket;
    }

    /*
    创建与user.txt的输入输出连接，用来读取/创建用户信息，字符转换流
    创建与客户端的字节输入输出流通信
     */
    @Override
    public void run() {
        try {
//            序列化读取文件
//            ObjectInputStream objectInputStream = new ObjectInputStream(
//                    new FileInputStream("day11/src/practise/案例八/file/user.txt"));
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
//                    new FileOutputStream("day11/src/practise/案例八/file/user.txt"));
//            低级字符输入流
            FileReader fileReader = new FileReader("day11/src/practise/案例八/file/user.txt");
            Properties properties = new Properties();   // 保存属性数据，用户名=密码
            properties.load(new BufferedReader(fileReader));    // 读取文件内容
            Set<String> strings = properties.stringPropertyNames(); // 获取文件中所有键的集合
//            Map<String ,String > map = (HashMap<String, String>) objectInputStream.readObject();
            InputStream inputStream = socket.getInputStream();  // 从客户端接受数据
            OutputStream outputStream = socket.getOutputStream();   // 发送给客户端数据
            byte[] bytes = new byte[1024*64];   // 字节数据充当数据包
            int len = inputStream.read(bytes);  // 读取客户端发送过来的数据，并且记录字节长度
            System.out.print("客户端发来数据:");
//            while ((len = inputStream.read(bytes)) != -1) {
//                System.out.print(new String(bytes,0,len));
//            }
            String str = new String(bytes,0,len);   // 将客户端发送来的字节数据转换成字符串
            System.out.println(str);    // 打印客户端发送过来的数据
            String[] sts = str.split("=");  // 拆解客户端发送来的数据，用户名=密码
            if (strings.contains(sts[0])) { // 判断用户名是否存在属性集合中，键集合中是否已存在客户端请求的用户名
                if (properties.getProperty(sts[0]).equals(sts[1])) {    // 根据用户名键获取属性文件中对应的值（密码）
                    outputStream.write("登陆成功".getBytes());  // 如果用户名和密码匹配，登陆成功
                }else {
                    outputStream.write("密码错误".getBytes());  // 用户名存在，但是密码与已存在的用户名不匹配
                }
            }else {
//                用户名不存在，按照客户端请求的数据，在属性集合文件中，存储创建新的键值对
                BufferedWriter bufferedWriter = new BufferedWriter( // 创建要存储的目标属性文件
                        new FileWriter("day11/src/practise/案例八/file/user.txt"));
                outputStream.write("用户不存在，将注册用户".getBytes());   // 向客户端发送消息
                properties.setProperty(sts[0],sts[1]);  // 保存一对属性
                properties.store(bufferedWriter,"注册用户");    // 加载属性文件的数据到属性集合对象中去
            }
            outputStream.flush();   // 刷新向客户端发送数据的管道，使发送的数据生效
//            objectOutputStream.writeObject(map);

            // 关闭资源
            inputStream.close();
            outputStream.close();
//            objectInputStream.close();
//            objectOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}