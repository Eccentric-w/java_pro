package practise.案例八;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        System.out.println("==================客户端启动=========");
        try {
            Socket socket = new Socket("localhost",55555);  // 连接服务器，指定IP地址和端口号
            InputStream inputStream = socket.getInputStream();  // 接受服务器的数据
            OutputStream outputStream = socket.getOutputStream();   // 向服务器发送数据
            Scanner scanner = new Scanner(System.in);   // 接受键盘的数据输入
            System.out.print("请输入你的账号:");
            String user = scanner.nextLine();   // 输入请求的账号
            user += "=";    // 按照服务器要求拼接字符串
            System.out.print("请输入你的密码:");
            user += scanner.nextLine(); // 输入请求的密码
            outputStream.write(user.getBytes());    // 把请求的字符串发送给服务器
            outputStream.flush();   // 刷新使数据生效
            byte[] bytes = new byte[1024*24];   // 字节数组数据包
            int len = inputStream.read(bytes);  // 服务器发送来的数据字节长度
            System.out.println(new String(bytes,0,len));    // 转换服务器发送来的字节数据为字符串，并打印

//            关闭资源
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
