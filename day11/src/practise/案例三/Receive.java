package practise.案例三;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Receive {
    public static void main(String[] args) throws Exception{

//        1. 创建接受数据包
        byte[] bytes = new byte[2014*64];   // UDP最大支持64k的数据包
        DatagramPacket datagramPacket = new DatagramPacket(bytes,bytes.length); // 创建数据包对象
//        2. 创建码头对象
        DatagramSocket datagramSocket = new DatagramSocket(12306);  //
//        3. 接收数据
        datagramSocket.receive(datagramPacket);
//        4. 输出数据
//        获取接受到的数据长度
        int len = datagramPacket.getLength();
        String s = new String(bytes,0,len);
        System.out.println(s);
//        5. 打印发送端信息
        System.out.println(datagramPacket.getAddress().getHostName());  // 发送端名字
        System.out.println(datagramPacket.getAddress().getHostAddress());   // 发送端IP地址
        System.out.println(datagramPacket.getAddress().getAddress()); // 发送端完整地址
        System.out.println(datagramPacket.getPort()); // 发送端端口
        datagramSocket.close();
    }
}
