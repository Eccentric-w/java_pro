package practise.案例三;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Send {
    public static void main(String[] args) throws Exception{
        byte[] bytes = "hello world !".getBytes();
        DatagramPacket datagramPacket =
//                    数据包：字节数据数组，字节数据长度，服务端（就是本机）的IP地址，服务端的端口号
                new DatagramPacket(bytes,bytes.length, InetAddress.getLocalHost(),12306);
//        创建码头对象
        DatagramSocket datagramSocket = new DatagramSocket();
//        通过码头发送数据包
        datagramSocket.send(datagramPacket);
        datagramSocket.close(); // 关闭码头资源
    }
}
