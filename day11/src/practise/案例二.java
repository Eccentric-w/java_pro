package practise;
/*
请简述UDP协议和TCP协议各自的特点。
 */
public class 案例二 {
    /*
    UDP：直接连接，不管服务器是否在线，不可靠传输，用于视频直播等
        不面向连接
        数据包限制64k以内
        无需连接，速度快

    TCP：三次握手，可靠传输，
        面向连接
        安全性高
        数据包大小不受限制
        效率稍微低
     */
}
