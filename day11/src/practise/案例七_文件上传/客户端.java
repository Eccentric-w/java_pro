package practise.案例七_文件上传;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class 客户端 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = "";
        while (true) {
            s = scanner.nextLine();
            if (s.equals("exit")) {
                break;
            }
            try {
                Socket socket = new Socket("localhost", 55555);
                OutputStream outputStream = socket.getOutputStream();
                InputStream inputStream = new FileInputStream("day11/src/practise/案例七_文件上传/file/"+s+".jpg");
                byte[] bytes = new byte[1024*64];
                int len ;
                while ((len = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes,0,len);
                }
                outputStream.flush();
                socket.shutdownOutput();    // 通知服务器文件上传完成，否则服务器会一直等待

                System.out.print("对方反馈:");
                InputStream inputStream1 = socket.getInputStream();
                len = inputStream1.read(bytes);
                System.out.println(new String(bytes,0,len));
//                while ((len = inputStream.read(bytes)) != -1) {
//                    System.out.print(new String(bytes,0,len));
//                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
}
