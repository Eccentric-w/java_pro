package practise.案例九_文件下载;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import java.util.UUID;

public class Client {
    public static void main(String[] args) throws Exception{
        Socket socket = new Socket("localhost",55555);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                new FileOutputStream("day11/src/practise/案例九_文件下载/file/"+ UUID.randomUUID()+".jpg"));
        byte[] bytes = new byte[1024*64];
        int len ;
        while ((len = bufferedInputStream.read(bytes)) != -1) {
            bufferedOutputStream.write(bytes,0,len);
        }
        socket.shutdownInput();
        bufferedOutputStream.flush();
//        len = bufferedInputStream.read(bytes);
//        while ((len = bufferedInputStream.read(bytes)) != -1) {
//            System.out.print(new String(bytes,0,len));
//        }
//        System.out.print(new String(bytes,0,len));

        bufferedInputStream.close();
        bufferedOutputStream.close();
    }
}
