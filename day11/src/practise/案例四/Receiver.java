package practise.案例四;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Receiver {
//    TCP通过字节管道接受数据
    public static void main(String[] args) throws Exception{
        ServerSocket serverSocket = new ServerSocket(12306);    // 创建服务端管道，注册端口号
        Socket socket = serverSocket.accept();  // 等待客户端连接
        InputStream inputStream = socket.getInputStream();  // 生成字节输入流，接受数据
//        创建高级的字节缓冲流
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        byte[] bytes = new byte[1024*64];   // 接受的数据包
        int len ;   // 数据包中数据长度
        while ((len = bufferedInputStream.read(bytes)) != -1) {
            System.out.print(new String(bytes,0,len));
        }
        bufferedInputStream.close();    // 关闭资源
    }
}
