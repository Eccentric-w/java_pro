package practise.案例四;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

public class Sender {
//    TCP协议通过字节流传输数据
    public static void main(String[] args) throws Exception{

        Socket socket = new Socket("localhost",12306);  // 创建码头，服务端IP地址，服务端端口
        OutputStream outputStream = socket.getOutputStream();   // 生成字节输出流
        PrintStream printStream = new PrintStream(outputStream);    // 创建打印流，方便使用
        printStream.println("hello world !");   // 打印并传输数据
        printStream.flush();    // 刷新数据，使数据生效
        printStream.close();    // 关闭资源

    }
}
