package BS架构;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(12345);
            while (true) {
                Socket socket = serverSocket.accept();
                new ServerThread(socket).start();
            }
        }catch (Exception e) {
            e.getStackTrace();  // 获取异常栈信息
            e.printStackTrace();    // 打印异常栈信息
        }


    }
}

class ServerThread extends Thread {
    private Socket socket ;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            OutputStream outputStream = socket.getOutputStream();
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            PrintStream printStream = new PrintStream(bufferedOutputStream);    // 封装打印流
            printStream.println("HTTP/1.1 200 OK"); // 请求头，通知服务器，用的是HTTP协议，并且传输的是200正确的数据
            printStream.println("Content-Type:text/html;charset=UTF-8");    // 通知服务器：传输的出文本或网页，编码是utf8
            printStream.println();
//            html网页源码
            printStream.println("<span style='color=red;font-size=50px;'>这是bs架构的网页，通过IP地址:端口号<span>");

//            Thread.sleep(1000);   // 防止网页信息还没打印，打印流就关闭了
            printStream.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}