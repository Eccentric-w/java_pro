package InetAddress类;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class 方法 {
    public static void main(String[] args) throws Exception {

//        1. 获取本地IP地址对象
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println(inetAddress.getHostAddress());   //本地IP地址
        System.out.println(inetAddress.getHostName());  //本地主机名称
//        输出：主机名/ip地址字符串
        System.out.println(inetAddress);
        System.out.println("----------------------------------------------");

//        2. 获取百度域名IP对象
        InetAddress inetAddress1 = InetAddress.getByName("www.baidu.com");
        System.out.println(inetAddress1.getAddress());  //输出IP地址
        System.out.println(inetAddress1.getHostAddress());  //输出IP地址字符串
        System.out.println(inetAddress1.getHostName());     //输出主机名称
        System.out.println(inetAddress1);
        System.out.println("----------------------------------------------");

//        3. 获取公网IP地址对象
        InetAddress inetAddress2 = InetAddress.getByName("182.61.200.7");
        System.out.println(inetAddress2.getHostName());
        System.out.println(inetAddress2.getHostAddress());
        System.out.println(inetAddress2.isReachable(5000)); //测试5s内能否ping通

        System.out.println("----------------------------------------------");
    }
}
