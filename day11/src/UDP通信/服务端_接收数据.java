package UDP通信;

/**
 * 启动后会一起等待接收数据
 */

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.net.SocketException;

public class 服务端_接收数据 {
    public static void main(String[] args) throws Exception {
        System.out.println("-------------------------服务端接收数据-------------------------");

//        1. 创建接收数据包
        byte[] bytes = new byte[1024*64];
        DatagramPacket datagramPacket = new DatagramPacket(bytes,bytes.length);

//        2. 创建接收码头
        DatagramSocket datagramSocket = new DatagramSocket(12306);

//        3. 接收数据
        datagramSocket.receive(datagramPacket);

//        4. 输出数据
        int len = datagramPacket.getLength();   //获得接收的数据包长度，数据量
        String s = new String(bytes,0,len); //接收的字节包转换成字符串
        System.out.println(s);  //输出接收的数据

        System.out.println("------------------------------------------------");
//        5. 获取发送端（客户端）的信息
        String ip = datagramPacket.getAddress().getHostAddress();   //IP地址
        String name = datagramPacket.getAddress().getHostName();    //客户端名字
        SocketAddress data = datagramPacket.getSocketAddress(); //输出客户端信息：名字/IP地址/端口号
        int port = datagramPacket.getPort();     //端口号
        System.out.println("客户端信息:"+ip+" "+name+" "+port);
        System.out.println(data);


    }
}
