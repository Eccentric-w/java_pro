package UDP通信;

import java.net.*;

public class 客户端_发送数据 {
    public static void main(String[] args) throws Exception {
        System.out.println("-------------------------客户端发送数据-------------------------");

//        1. 准备要发送的数据
        byte[] bytes = "hello world".getBytes();
//        2. 创建数据包
//        字节数据包，数据包长度，服务端IP地址，服务端端口号
        DatagramPacket datagramPacket =
                new DatagramPacket(bytes,bytes.length, InetAddress.getLocalHost(),12306);

//        3.创建码头对象
        DatagramSocket datagramSocket = new DatagramSocket();

//        4. 发送数据
        datagramSocket.send(datagramPacket);

//        5. 关闭资源
        datagramSocket.close();
    }
}
