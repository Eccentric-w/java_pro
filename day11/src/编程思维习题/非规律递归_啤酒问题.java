package 编程思维习题;

/**
 啤酒2元一瓶
 2个瓶子可以换一瓶酒
 4个瓶盖可以换一瓶酒

 解决思维：将瓶子和瓶盖都换算成钱，再去买酒
 */

public class 非规律递归_啤酒问题 {
    private static int totalBeer ;  //买的啤酒总数
    private static int lastMoney ;
    private static int lastCover ; //剩余瓶盖数
    private static int lastBottle ;    //剩余瓶子数

    public static void main(String[] args) {
        totalBeer = buyBeer(11);
//                                有多少瓶盖，有多少瓶子
        totalBeer += exchangeBeer(totalBeer,totalBeer);

        System.out.println("可以得到"+totalBeer+"瓶酒");
        System.out.println("剩余:"+lastMoney+"钱");
        System.out.println("剩余:"+lastCover+"个瓶盖");
        System.out.println("剩余:"+lastBottle+"个瓶子");


    }

//  买酒的方法
    public static int buyBeer(int money) {
        lastMoney = money % 2; //剩余钱
        return money / 2; //能买多少瓶酒
    }

//  兑换酒的方法
    public static int exchangeBeer(int cover , int bottle) {
        if (cover >= 4 || bottle >=2) {
//          剩余瓶盖 = 瓶盖可以换几瓶酒 + 换酒换剩的瓶盖 + 瓶子可以换的酒数
            lastCover = cover / 4 + cover % 4 + bottle / 2 ;
            lastBottle = bottle / 2 + bottle % 2 +cover /4 ;
            return cover / 4 + bottle / 2 + exchangeBeer(lastCover,lastBottle);
        }
        return 0;
    }
}
