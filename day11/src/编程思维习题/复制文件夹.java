package 编程思维习题;

import javax.sql.rowset.BaseRowSet;
import java.io.*;

/**
 * 经典面试题
 *
 * 有除了文本以外的文件（图片等），不能用字符流，否则文件损害
 * 字符流只能读写文本文件
 * 其他文件要用字节流
 * 因此，不适合用 字符转换流
 */

public class 复制文件夹 {
    public static void main(String[] args) {
//        调用复制方法，参数是File对象
        copyDir(new File("day11/src/编程思维习题/source"),
                new File("day11/src/编程思维习题/target"));


    }

//    复制文件夹的方法
    public static void copyDir(File sourceDir , File targetDir) {
//        1. 判断文件夹是否存在
            if (sourceDir.exists()) {
//                System.out.println(sourceDir.exists());    //判断源文件夹是否存在
//                2. 如果目标文件夹不存在，酒创建
                if (!(targetDir.exists())) {
                    targetDir.mkdirs();
//                    System.out.println(targetDir.exists());  //判断目标文件夹是否存在
                }
//                3. 得到一级目录和文件对象
                File[] files = sourceDir.listFiles();
//                6. 判断一级名录是否为空
                if (files != null && files.length > 0) {
//                5. 遍历files数组
                    for (File file : files) {
                        if (file.isDirectory()) {
//                            6. 如果还是文件夹，递归调用本方法
                            copyDir(file,new File(targetDir,file.getName()));
                        }else {
//                            7. 如果使文件，直接复制
                            copyFile(file,new File(targetDir,file.getName()));
                        }
                    }
                }
            }
    }

//    复制文件的方法
    public static void copyFile(File sourceFile ,File targetFile ) {
        try(
//                1. 创建低级字节输入流，封装进字符转换输入流中，再封装在缓存输入流中
                BufferedInputStream bufferedInputStream =
                        new BufferedInputStream(new FileInputStream(sourceFile));

//                2. 创建低级字节输入流，封装进字符转换输出流中，再封装在缓存输出流中
                BufferedOutputStream bufferedOutputStream =
                        new BufferedOutputStream(new FileOutputStream(targetFile));
                ){
//            3. 创建字节数组，用数组存储读取的字节数据，定义len长度，记录读取多长的字节
            byte[] bytes = new byte[2048];
            int len ;
//            4. 循环读取，并写入
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                bufferedOutputStream.write(bytes,0,len);
            }
        }catch (Exception e) {
            e.getStackTrace();
        }
    }
}
