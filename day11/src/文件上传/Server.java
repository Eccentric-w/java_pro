package 文件上传;

import java.net.ServerSocket;
import java.net.Socket;

// 服务器类
public class Server {
    public static void main(String[] args) {
        try(
                ServerSocket serverSocket = new ServerSocket(Constant.Port); //创建服务器管道,占用端口
        ) {
//            创建线程池，最多同时执行 3 个线程，阻塞队列最多10个
            SocketThreadPool socketThreadPool = new SocketThreadPool(2,10);
            while (true) {
//                接收客户端申请的通信管道
               Socket socket = serverSocket.accept();
//               向线程池中添加线程任务，
               socketThreadPool.excute(new ServerRunnable(socket));
           }
        }catch (Exception e) {
            e.getStackTrace();
        }


    }
}
