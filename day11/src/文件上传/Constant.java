package 文件上传;

/**
 * 常用变量
 */
public class Constant {
//    设置成静态的才可以通过类名调用
    public static int Port = 12345 ;
    public static String ServerIP = "127.0.0.1";
//    public static String ClientIP = "localhost";
    public static String SourceFile = "day11/src/file/img.jpg";
    public static String TargetFile = "day11/src/file/server/";
}
