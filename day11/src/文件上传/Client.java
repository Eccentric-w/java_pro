package 文件上传;

import java.io.*;
import java.net.Socket;

// 客户端类
public class Client {
    public static void main(String[] args) {

        try(
                Socket socket = new Socket("127.0.0.1",Constant.Port);  //建立与服务器连接的通道
                OutputStream outputStream = socket.getOutputStream();   //创建字节输出流
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

                InputStream inputStream1 = new FileInputStream(Constant.SourceFile);    //读取本地文件
                BufferedInputStream bufferedInputStream1 = new BufferedInputStream(inputStream1);   //封装高级缓冲流
        ) {
            byte[] bytes = new byte[1024*4];
            int len ;
            while ((len = bufferedInputStream1.read(bytes)) != -1) {    //通过高级缓冲输入流，读取本地图片文件
                outputStream.write(bytes,0,len);    //通过通信管道发送给服务器
            }
            outputStream.flush();   // 刷新输入流，使写入服务器的数据生效

//            通信管道 与 传输流 不同，如果不通知服务器，服务器会一直等待客户端传输数据，导致系统阻塞
            socket.shutdownOutput();    // 通知已经通信完成，不需要继续读取数据了

            BufferedReader bufferedReader =     //字节缓冲输入流，获得服务器发送来的消息数据
                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("收到服务器信息:"+bufferedReader.readLine());   //输出服务器发送来的数据

        }catch (Exception e) {
            e.getStackTrace();
        }
    }
}