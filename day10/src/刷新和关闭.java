import java.io.FileWriter;
import java.io.IOException;

public class 刷新和关闭 {
    public static void main(String[] args) throws IOException {

        FileWriter fileWriter = new FileWriter("day10/src/test3");

        fileWriter.write("abc");
        fileWriter.write(729);

//        刷新流，还能继续使用
        fileWriter.flush();

        fileWriter.write(123);

//        最后必须调用close方法关闭资源，close关闭前也会调用刷新方法
        fileWriter.close();
    }
}
