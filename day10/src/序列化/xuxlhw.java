package 序列化;

import java.io.*;

public class xuxlhw {
    public static void main(String[] args) throws IOException {

//    1. 创建对象
        Test test = new Test("liming",21,"man");

//    2.创建低级的字节输出流，并封装成高级的字节输出流
        ObjectOutputStream objectInputStream =
                new ObjectOutputStream(new FileOutputStream("day10/src/file/xulxhw.dat"));

//        3. 封装对象
        objectInputStream.writeObject(test);

//        4. 释放资源
        objectInputStream.close();
        System.out.println("success");
    }

}
