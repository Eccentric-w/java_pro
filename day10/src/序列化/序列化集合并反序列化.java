package 序列化;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class 序列化集合并反序列化 {
//    序列化和反序列化都可以直接操作对象集合

    public static void main(String[] args) throws Exception {

//        1. 创建对象，存入集合
        List<Test> testList = new ArrayList<>();
        testList.add(new Test("nick",34,"man"));
        testList.add(new Test("frank",40,"man"));
        testList.add(new Test("lucy",23,"woman"));

//        2. 序列化集合中的对象
        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(new FileOutputStream("day10/src/file/xulxhwjihe.dat"));

        objectOutputStream.writeObject(testList);

//        3. 反序列化集合中的对象,并输出
        ObjectInputStream objectInputStream =
                new ObjectInputStream(new FileInputStream("day10/src/file/xulxhwjihe.bat"));

        ArrayList<Test> arrayList = (ArrayList<Test>) objectInputStream.readObject();
//        简化写法
        arrayList.forEach(System.out::println);

    }
}
