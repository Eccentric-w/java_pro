package 序列化;

import java.io.Serializable;

//必须实现Serializable接口
public class Test implements Serializable {
    private String name ;
    private int age ;
    private String sex ;

    @Override
    public String toString() {
        return "test{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Test(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
}
