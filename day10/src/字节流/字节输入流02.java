package 字节流;

import java.io.*;

public class 字节输入流02 {
    public static void main(String[] args) throws IOException {

//        创建字节输入管道
        InputStream inputStream = new FileInputStream("day10/src/file/test");

        byte[] bytes = new byte[4]; //避免出现截断中文编码的问题，用集合接
        int len ;
//        read抛出异常
        while ((len = inputStream.read(bytes)) != -1) {
//            读取字节数组的0的len的内容
            String sre = new String(bytes,0,len);
            System.out.print(sre);
        }
        System.out.println();

//        读取过一次之后，流就空了
        InputStream inputStream1 = new FileInputStream("day10/src/file/test");
//        避免循环过去麻烦，一下子读取所有内容到字节集合中
        byte[] bytes1 = inputStream1.readAllBytes();
//        直接把集合转换成字符串
        String s = new String(bytes1);
        System.out.println(s);
    }
}
