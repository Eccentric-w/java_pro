package 字节流;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class 字节输入流01 {
    public static void main(String[] args) {

        File file = new File("day10/src/file/test");

        try(
//                在try（）小括号内创建资源，会自动关闭通道，只能创建资源
                InputStream inputStream = new FileInputStream(file);
                ) {
            int c;  //存储字节
//            read方法一直读取下一个元素，直到读取到没有数据返回-1
            while ((c=inputStream.read()) != -1) {
//                ASCII转换成字符
                System.out.print((char)c);
            }
        }catch (Exception e) {
            e.getStackTrace();
        }
    }
}
