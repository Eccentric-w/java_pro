package 缓冲流;

import java.io.*;

public class 复制test5 {

    public static void main(String[] args) {

        try(
                Reader reader = new FileReader("day10/src/test4");
                BufferedReader bufferedReader = new BufferedReader(reader);
                Writer writer = new FileWriter("day10/src/file/test5");
                BufferedWriter bufferedWriter = new BufferedWriter(writer);
        ) {
            String s ;
            while ((s = bufferedReader.readLine()) != null ) {
                bufferedWriter.write(s);
                bufferedWriter.newLine();
                System.out.println(s);
            }
        }catch (Exception e) {
            e.getStackTrace();
        }
    }
}
