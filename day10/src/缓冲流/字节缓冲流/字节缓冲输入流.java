package 缓冲流.字节缓冲流;

import java.io.*;

public class 字节缓冲输入流 {
    public static void main(String[] args) throws IOException {

        InputStream inputStream = new FileInputStream("day10/src/test4");

//        封装字节输入流，提高性能
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

//        字节输入流，要用字节数组
        byte[] chars = new byte[64];
        int len;
        while ((len = bufferedInputStream.read(chars)) != -1) {
//            由于utf8编码中中文占用3个字节，所以可能造成截断读取中文字符，造成乱码
            System.out.println(new String(chars,0,len));
        }

    }
}
