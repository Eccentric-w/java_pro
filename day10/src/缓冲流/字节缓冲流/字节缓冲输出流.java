package 缓冲流.字节缓冲流;

import java.io.*;

public class 字节缓冲输出流 {

    public static void main(String[] args) throws IOException {
//        追加写文件，不然会覆盖管道数据
        OutputStream outputStream = new FileOutputStream("day10/src/test4",true);

//       封装成缓冲流
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

        bufferedOutputStream.write(1);
        bufferedOutputStream.write('a');
        bufferedOutputStream.write("\n作者：李白".getBytes());   //转换成字节数组
//        最后关闭缓冲流资源
        bufferedOutputStream.close();
    }

}
