package 缓冲流.字符缓冲流;

import java.io.*;

public class 字符缓冲输入流 {
//    相比低级的字符输入流，多了一个 readline 方法，读取一行数据

    public static void main(String[] args) throws IOException {

        Reader reader = new FileReader("day10/src/test4");
//                                  缓冲类也是Reader的实现类
        BufferedReader bufferedReader = new BufferedReader(reader);

        String s ;
        //  经典代码
        while ((s = bufferedReader.readLine()) != null) {   //一次读取一行字符
            System.out.println(s);
        }

    }

}
