package 缓冲流.字符缓冲流;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class 字符缓冲输出流 {

    public static void main(String[] args) {

        try(
//                专门第一资源，会自动关闭
                Writer writer = new FileWriter("day10/src/test5",true);  //追加数据
                BufferedWriter bufferedWriter = new BufferedWriter(writer);
        ) {
            bufferedWriter.write("abc");
            bufferedWriter.newLine();   //换行
            bufferedWriter.write("My name is liming!");
            bufferedWriter.newLine();   //换行
            bufferedWriter.write("12306".toCharArray(),0,2);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
