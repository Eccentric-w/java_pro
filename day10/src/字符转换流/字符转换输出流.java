package 字符转换流;

import java.io.*;

public class 字符转换输出流 {

    public static void main(String[] args) throws IOException {

//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("day10/src/file/test6",true));

        BufferedWriter bufferedWriter =
                new BufferedWriter(new OutputStreamWriter(new FileOutputStream("day10/src/file/test6",true),"gbk"));

        bufferedWriter.newLine();
        bufferedWriter.write("i am rather");
        bufferedWriter.newLine();
        bufferedWriter.write("abcdefghijklm",0,9);  //英文不会乱码
        bufferedWriter.newLine();
        bufferedWriter.write("中文乱码问题");


        bufferedWriter.close();


    }

}
