package 字符转换流;

import java.io.*;

public class 字符转换输入流 {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader("day10/src/file/test6"));

        String a ;
        while ((a = bufferedReader.readLine()) != null) {
            System.out.println(a);
//            字母和数字不受字符编码的影响
//            但是中文情况下，代码编码和文件不同则会乱码
        }
        bufferedReader.close();

        System.out.println("-----------------------------------------");

//        字节流转换到字符流的桥梁
        BufferedReader bufferedReader1 = new BufferedReader
                (new InputStreamReader(new FileInputStream("day10/src/file/test6"),"GBK"));
//                指定GBK编码（文件的编码格式）
        while ((a = bufferedReader1.readLine()) != null) {
            System.out.println(a);
        }
        bufferedReader1.close();
    }
}
