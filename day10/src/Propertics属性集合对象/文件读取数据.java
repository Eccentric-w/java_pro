package Propertics属性集合对象;

import java.io.*;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class 文件读取数据 {
    public static void main(String[] args) throws IOException {

        Reader reader = new FileReader("day10/src/file/properties.properties");

        Properties properties = new Properties();

        properties.load(reader);

        System.out.println(properties.getClass().getName());    // 获取变量所在类的名称
        System.out.println("--------------------------------------");

        properties.forEach((a,b) -> System.out.println(a+"=>"+b));  // 遍历输出

        System.out.println("--------------------------------------");
        System.out.println(properties.getProperty("Root"));

        System.out.println("--------------------------------------");
        Set<String> set = properties.stringPropertyNames();     //获取所有键的列表
        System.out.println(set);
    }
}
