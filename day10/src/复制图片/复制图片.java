package 复制图片;

import java.io.*;

public class 复制图片 {
    public static void main(String[] args) throws IOException {

        InputStream inputStream = new FileInputStream("day10/src/image.jpg");

        OutputStream outputStream = new FileOutputStream("day10/src/img/test.jpg");

        byte[] bytes = new byte[1024];
        int len ;

        while ((len = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes);
        }
        inputStream.close();
        outputStream.close();
    }
}
