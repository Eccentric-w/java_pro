package practise;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class 案例五 {
    public static void main(String[] args) throws Exception{
        OutputStreamWriter outputStreamWriter =
                new OutputStreamWriter(
                        new FileOutputStream(  // 指定存储为gbk编码的我的文件，utf8打开会乱码
                                new File("day10/src/practise/file/a.txt")),"gbk");
        outputStreamWriter.write("我爱java");
        outputStreamWriter.close();
    }
}