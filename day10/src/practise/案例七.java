package practise;

import java.io.*;

/*
对象的序列化
 */
public class 案例七 {

    public static void main(String[] args) throws Exception{
        Student student = new Student("nick","08",23);
        Student student1 = new Student("frank","01",38);

        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream( //对象序列化流
                        new BufferedOutputStream(   // 高级字节输出流
                                new FileOutputStream(   //　低级字节输出流
                                    new File("day10/src/practise/file/student.txt"))));    // 文件对象

        objectOutputStream.writeObject(student);
        objectOutputStream.writeObject(student1);

        objectOutputStream.close(); // 关闭资源
    }
}

// 被序列化的对象必须实现序列化接口
class Student implements Serializable {
//    transient 修饰的属性不被序列化
    private String name ;
    private String ID ;
    private transient int age ; // 这个属性不被序列化,用默认值填充 0；

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", ID='" + ID + '\'' +
                ", age=" + age +
                '}';
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student(String name, String ID, int age) {
        this.name = name;
        this.ID = ID;
        this.age = age;
    }
}