package practise;

import com.sun.jdi.ByteType;

import java.io.*;

public class 案例三 {
    public static void main(String[] args) throws Exception{
//        低级输出、输入流
        InputStream inputStream = new  FileInputStream("day10/src/practise/file/e.txt");
        OutputStream outputStream = new FileOutputStream("day10/src/practise/file/copy_e.txt");
//          高级输出、输入流
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

        byte[] bytes = new byte[1024];  //　字节容器
        int len ;   // 读取字节长度
        while ((len=bufferedInputStream.read(bytes))!=-1) { // 读取的字节长度不是-1（读取结束）
            bufferedOutputStream.write(bytes,0,len);
        }
//        关闭资源
        bufferedInputStream.close();
        bufferedOutputStream.close();
    }
}
