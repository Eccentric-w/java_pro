package practise;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class 案例十三 {
    public static void main(String[] args) throws Exception{

//        第一种方法，list方式直接排序
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader("day10/src/practise/file/thirteen.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter("day10/src/practise/file/thirteen_order_1.txt"));

        List<String> list = new ArrayList<>();  // 所有段落存储在list集合中
        String s ;
        while ((s = bufferedReader.readLine()) != null) {
            list.add(s);
        }
        Collections.sort(list); // 根据第一个字符排序，由于是数字，可以直接排序
        for (String s1 : list) {
            bufferedWriter.write(s1);   //　写入一行数据
            bufferedWriter.newLine();   // 写入一个空行
        }
        bufferedWriter.close();     // 关闭资源
        bufferedReader.close(); //关闭资源

//        第二种方法,自定义排序
        BufferedWriter writer = new BufferedWriter(
                new FileWriter("day10/src/practise/file/thirteen_order_2.txt"));
        BufferedReader reader = new BufferedReader(
                new FileReader("day10/src/practise/file/thirteen_unorder_2.txt"));
        List<Character> characters = new ArrayList<>();
        Collections.addAll(characters,'零','一','二','三','四','五','六','七','八','九','十'); // 数组中批量添加数据
        List<String> strings = new ArrayList<>();   // 存储读取的段落
        String st ;
        while ((st = reader.readLine()) != null) {
            strings.add(st);
        }
//        自定义排序器，lambda简略写法
//        每段第一个字符是中文，不能直接排序
//        获取段落第一个字符，对应characters数组字符元素的索引位置，对比大小
        Collections.sort(strings,(s1,s2) -> characters.indexOf(s1.charAt(0))-characters.indexOf(s2.charAt(0)));
        for (String string : strings) {
            writer.write(string);
            writer.newLine();
        }
//        关闭资源
        reader.close();
        writer.close();


//        第三种方法
        BufferedWriter writer1 = new BufferedWriter(
                new FileWriter("day10/src/practise/file/thirteen_order_3.txt"));
        BufferedReader reader1 = new BufferedReader(
                new FileReader("day10/src/practise/file/thirteen_unorder_2.txt"));
        Map<Integer,String> map = new HashMap<>();
        String str ;
        while ((str = reader1.readLine()) != null) {
//            去除字符串前后的空格，获取第一个字符，在characters数组中对应的索引位置，用于添加到key的位置，value是一个完整的段落
            map.put(characters.indexOf(str.trim().charAt(0)),str);
        }
//        System.out.println(map);
        for (int key = 1 ; key < map.size()+1 ; key++) {    // 便利map数组
            writer1.write(map.get(key));    // 通过key获取value（段落数据）
            writer1.newLine();  // 换行
        }
//        关闭资源
        reader1.close();
        writer1.close();
    }
}
