package practise;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class 案例十 {
    public static void main(String[] args) throws Exception{
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader("day10/src/practise/file/ten.txt"));

        ArrayList<String> list = new ArrayList<>(); // 存储读取到的元素
        String s ;
        while ((s=bufferedReader.readLine()) != null) { // 读取一行内容，直到读取的内容不为空
            list.add(s);
        }
        Collections.reverse(list);  // 反转元素顺序
        BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter("day10/src/practise/file/ten_reverse.txt"));
        for (String s1 : list) {
            bufferedWriter.write(s1);
            bufferedWriter.newLine();   // 换行
        }
        bufferedReader.close();
        bufferedWriter.close(); // 关闭前会自动刷新
    }
}
