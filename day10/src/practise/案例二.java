package practise;

import java.io.*;

public class 案例二 {
    public static void main(String[] args) throws Exception {
//        低级字节输出流
        OutputStream file = new FileOutputStream("day10/src/practise/file/e.txt");
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(file); // 高级字节输出流
//        转换utf8格式
        OutputStreamWriter writer = new OutputStreamWriter(bufferedOutputStream,"utf8");
        bufferedOutputStream.write("i love you".getBytes());    // 写入字节，字符串转字节数组
        bufferedOutputStream.flush();   // 写入刷新
        bufferedOutputStream.close();
    }
}
