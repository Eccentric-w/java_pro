package practise;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/*
字节打印流
 */
public class 案例九 {
    public static void main(String[] args) throws Exception{
        PrintStream printStream = new PrintStream(new BufferedOutputStream( // 字节打印流
                new FileOutputStream("day10/src/practise/file/nine.txt")));
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        printStream.println(s); // 数据打印到控制台，并写入到文件中
        printStream.close();    // 关闭资源
    }
}
