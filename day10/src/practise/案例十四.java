package practise;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Properties;
import java.util.Set;

public class 案例十四 {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(
                new FileReader("day10/src/practise/file/fourteen.txt"));
        Properties properties = new Properties();
        properties.load(reader);

        Set<String> strings = properties.stringPropertyNames();
        for (String string : strings) {
            if (string.equals("lisi")) {
                properties.setProperty(string,"100");
            }
        }
        FileOutputStream fileOutputStream = new FileOutputStream("day10/src/practise/file/fourteen.txt");
        properties.store(fileOutputStream,"修改lisi的数据为100，然后提交");
        fileOutputStream.close();
    }
}
