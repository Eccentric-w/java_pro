package practise;

import java.io.BufferedReader;
import java.io.FileReader;

public class 案例十二 {
    public static void main(String[] args) throws Exception{
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader("day10/src/practise/file/twelve.txt"));
        String s ;
        while ((s = bufferedReader.readLine()) != null) {
            System.out.println(s);
        }
        bufferedReader.close();
    }
}
