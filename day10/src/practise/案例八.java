package practise;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class 案例八 {
    public static void main(String[] args) throws Exception{
//        ObjectInputStream objectInputStream =
//                new ObjectInputStream(    // 反序列化
//                    new BufferedInputStream(    //  高级字节输入流
//                        new FileInputStream(    //  低级字节输入流
//                                new File("day10/src/practise/file/student.txt")))); //文件对象
        FileInputStream fileInputStream =
                new FileInputStream(new File("day10/src/practise/file/student.txt")); // 低级字节输入流
        ObjectInputStream objectInputStream =
                new ObjectInputStream(new BufferedInputStream(fileInputStream));    // 反序列化对象
//        File file = new File("day10/src/practise/file/student.txt");

        List<Student> students = new ArrayList<>();
        try {
//        students.add(new Student("nick","34",23));
//        Student[] students = new Student[10];
            System.out.println(fileInputStream.available());    // 查看字节数
            while (fileInputStream.available() > 0) {   // 判断是否是有数据未读取
                System.out.println("进入循环"); // 判断时候进入循环
                students.add((Student) objectInputStream.readObject());    // 转换成学生对象
            }
        }catch (Exception e) {

        }
        System.out.println(students.size());    // 集合中有多少个学生对象
        for (Student student : students) {  // 遍历读取的学生集合
            System.out.println(student);
        }

//        直接输出
        Student student = (Student)objectInputStream.readObject();
        Student student1 = (Student)objectInputStream.readObject();
        System.out.println(student);
        System.out.println(student1);

//        try {
//            while (true) {
//                Student s = (Student)objectInputStream.readObject();
//                System.out.println(s);
//                System.out.println("di");
//            }
//        }catch (Exception e) {
//
//        }

        objectInputStream.close();  // 关闭资源
        fileInputStream.close();
    }
}
