package practise;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class 案例十一 {
    public static void main(String[] args) throws Exception{
        List<Student> students = new ArrayList<>();
        Student student = new Student("nick","01",1);
        Student student2 = new Student("frank","02",2);
        students.add(student);
        students.add(student2);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("day10/src/practise/file/eleven.txt"));
        objectOutputStream.writeObject(students);

        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("day10/src/practise/file/eleven.txt"));
        List<Student> students1 = (List<Student>) objectInputStream.readObject();
        for (Student student1 : students1) {
            System.out.println(student1);
        }
        objectInputStream.close();
        objectOutputStream.close();
    }
}
