package practise;

import java.io.*;

//利用高效字节输出流往file文件夹下的d.txt文件输出一个字节数
public class 案例一 {
    public static void main(String[] args) throws Exception {
        OutputStream outputStream = new FileOutputStream("day10/src/practise/file/d.txt");  // 低级字节输出流
        System.out.println("read successful");
//        高级字节输出流
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
        OutputStreamWriter outputStreamWriter = // 字符转换流
                new OutputStreamWriter(bufferedOutputStream,"utf-8");
        byte b = 3;
        outputStreamWriter.write(b);
        outputStreamWriter.flush(); // 刷新，使字符写入
        outputStreamWriter.close();
        System.out.println("success");
    }
}
