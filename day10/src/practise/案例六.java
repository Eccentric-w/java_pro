package practise;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/*
转换流：将gbk编码的文件读取出来，不能乱码
 */
public class 案例六 {
    public static void main(String[] args) throws Exception{
        InputStreamReader inputStreamReader =
                new InputStreamReader(  //字符转换流
                        new BufferedInputStream(    // 高级字符输入流
                                new FileInputStream(    //　低级字符输入流
                                        new File("day10/src/practise/file/a.txt"))), // 文件对象
                        "gbk");  // 以gbk编码的方式读取
        char[] chars = new char[1024];
        int len ;
        while ((len = inputStreamReader.read(chars)) != -1) {
            System.out.print(new String(chars,0,len));    // 新建字符串
        }
        inputStreamReader.close();  // 关闭资源
    }
}
