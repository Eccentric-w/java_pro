package 字符流;

import java.io.FileWriter;
import java.io.IOException;

public class 字符输出流 {
    public static void main(String[] args) throws IOException {

        FileWriter fileWriter = new FileWriter("day10/src/test2");

        fileWriter.write(95);
        fileWriter.write("abc");
        fileWriter.write(12306);

//        未调用close方法，数据只是保存到了缓冲区，并未写出到文件中。
        fileWriter.close();
    }
}
