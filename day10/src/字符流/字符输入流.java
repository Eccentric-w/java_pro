package 字符流;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class 字符输入流 {
    public static void main(String[] args) throws IOException {

//        1. 创建字节流通道,创建这个对象需要抛出异常
        FileReader fileReader = new FileReader("day10/src/test1");

//        2. while循环读取字符
        int i ;
        while ((i = fileReader.read()) != -1) {
            System.out.print((char)i);
        }
        fileReader.close();
    }
}
