package 字符流;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class 字节输入流数组 {
    public static void main(String[] args) throws IOException {

        FileReader fileReader = new FileReader("day10/src/test1");

        char[] c = new char[2]; //每次取出两个字符
        int i ;
//        read方法返回读取的字符长度
        while ((i = fileReader.read(c) )!= -1) {
            System.out.println(new String(c,0,i));
//            将c数组的0到i的字符封装成字符串
        }
    }
}
