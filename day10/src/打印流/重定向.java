package 打印流;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class 重定向 {
    public static void main(String[] args) throws FileNotFoundException {

//        默认会覆盖之前的数据，想追加只能包装低级输出流
        PrintStream printStream = new PrintStream("day10/src/file/daynlq.txt");

        System.out.println("这里在空制台输出");
        System.setOut(printStream);    //重定向后，系统输出流向打印流

        System.out.println("接下来的数据都会流向打印流printStream");
        System.out.println("不会打印在控制台");

        printStream.close();
    }
}
