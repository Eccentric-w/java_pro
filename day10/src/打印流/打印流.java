package 打印流;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

public class 打印流 {
    public static void main(String[] args) throws FileNotFoundException {

        System.out.println("-----------------------打印流-----------------------");

        FileOutputStream fileOutputStream = new FileOutputStream("day10/src/file/daynlq.txt");
        PrintStream printStream = new PrintStream("day10/src/file/printStream");
        PrintWriter printWriter = new PrintWriter(fileOutputStream);

        printStream.println("hello world");
        System.setOut(printStream); // 使系统的输出流指向打印流
        printStream.println("\n hello wrold");

        printWriter.println(12306);
        printWriter.println("my name is liming");

        printStream.close();
        printWriter.close();
    }
}
