package XPath;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.junit.*;
import java.io.InputStream;
import java.util.List;

public class 绝对和相对路径 {

    Document document;

    @Before // 在每一个实例方法前执行一次
    public void vpbzDocument() throws Exception {
        SAXReader saxReader = new SAXReader();
        InputStream inputStream = 绝对和相对路径.class.getResourceAsStream("/xml/Contacts.xml");
        Document document = saxReader.read(inputStream);
        System.out.println("before方法执行了");
    }

    @Test
    public void testAbsolute() throws Exception {
        String xPath = "/contactList/contact/name";
        if (document != null) {
        List<Node> nodeName = document.selectNodes(xPath);
        for (Node node : nodeName) {
            System.out.println(node.getText());
        }
    }else {
            System.out.println(document);
        }
    }
}
