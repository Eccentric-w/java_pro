package CommonsIO包;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Test {
    public static void main(String[] args) throws Exception{

//        一行复制文件
        IOUtils.copy(new FileInputStream("day13/file/books.xml"),
                new FileOutputStream("day13/file/test1.xml"));

//        java自带的一行复制文件
//                    路径格式                             字节输出流
        Files.copy(Paths.get("day13/file/books.xml"), new FileOutputStream("day13/file/test1.xml"));
        
    }
}
