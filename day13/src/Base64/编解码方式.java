package Base64;

import java.util.Base64;
import java.util.UUID;

public class 编解码方式 {
    public static void main(String[] args) {

        String s = "折纸之泪";
        String url = "?name=ecctntric&p=294";
        StringBuilder stringBuilder = new StringBuilder();
        for ( int i = 0 ; i < 10 ; i++ ) {
            stringBuilder.append(UUID.randomUUID().toString());
        }
        String sc = stringBuilder.toString();

//        1. 基本方式
        String s1 = Base64.getEncoder().encodeToString(s.getBytes());
        System.out.println(s1);
        byte[] bytes = Base64.getDecoder().decode(s1);
        System.out.println(new String(bytes));

        System.out.println("--------------------------------------");

//        2. URL方式
        String s2 = Base64.getUrlEncoder().encodeToString(url.getBytes());
        System.out.println(s2);
        byte[] bytes1 = Base64.getUrlDecoder().decode(s2);
        System.out.println(new String(bytes1));

        System.out.println("--------------------------------------");

//        3. MIME方式
        String s3 = Base64.getMimeEncoder().encodeToString(sc.getBytes());
        System.out.println(s3);
        byte[] bytes2 = Base64.getMimeDecoder().decode(s3);
        System.out.println(new String(bytes2));

    }
}
