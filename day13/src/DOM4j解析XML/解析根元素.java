package DOM4j解析XML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.File;
import java.io.InputStream;

public class 解析根元素 {
    public static void main(String[] args) throws Exception{

//        1. 创建dom4j解析器，代表dom4j框架
        SAXReader saxReader = new SAXReader();
//          2. 解析构建document文档树对象
        Document document = saxReader.read(new File("day13/src/xml/books.xml"));    //项目下的绝对路径
//        两种写法，后者好一些（创建字节输入流）
        InputStream inputStream = 解析根元素.class.getResourceAsStream("/xml/books.xml");    //src路径下
        Document document1 = saxReader.read(inputStream);   // 构建文档
//          3. 输出文档树名称
        System.out.println(document.getName()); // 文档名称
//        4.获取根元素对象，Element是一个标签对象
        Element element = document.getRootElement();
//          5. 输出根元素名称
        System.out.println(element.getName());

    }
}