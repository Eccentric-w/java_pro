package DOM4j解析XML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

public class 获取文本信息 {
    public static void main(String[] args) throws Exception {

        SAXReader saxReader = new SAXReader();
        InputStream inputStream = 获取文本信息.class.getResourceAsStream("/xml/Contact.xml");
        Document document = saxReader.read(inputStream);

        Element root = document.getRootElement();   // 得到根节点
        List<Element> elements = root.elements();   // 得到根节点下所有一级元素的对象集合
        for (Element element : elements) {
            List<Element> elements1 = element.elements();   // 得到根节点一级元素下一级元素的集合
            for (Element element1 : elements1) {                // 获取指定子元素的文本内容
                System.out.println(element1.getName()+"=>"+element.elementText(element1.getName()));
            }
            System.out.println("-----------------------");
        }

    }
}
