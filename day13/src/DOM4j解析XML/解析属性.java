package DOM4j解析XML;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

public class 解析属性 {
    public static void main(String[] args) throws Exception{

        SAXReader saxReader = new SAXReader();

        InputStream inputStream = 解析属性.class.getResourceAsStream("/xml/books.xml");
        Document document = saxReader.read(inputStream);

        Element root = document.getRootElement();   // 获取根元素

        Element book = root.element("book");
        Attribute id = book.attribute("id");    // 根据属性名，得到对象
        System.out.println(id);
        System.out.println(id.getName());
        System.out.println(id.getValue());
        System.out.println("--------------------------------");

        String idValue = book.attributeValue("id");
        System.out.println(idValue);
        System.out.println("--------------------------------");

        List<Attribute> bookLisit = book.attributes();
        for (Attribute attribute : bookLisit) {
            System.out.println(attribute);
        }
        System.out.println("--------------------------------");

    }
}
