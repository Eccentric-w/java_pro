package DOM4j解析XML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.io.InputStream;
import java.util.List;

public class 解析子元素 {
    public static void main(String[] args) throws Exception {
//        1. 创建DOM4j对象的，充当dom4j框架
        SAXReader saxReader = new SAXReader();

//        2. 根据当前类创建字节输入流
        InputStream inputStream = 解析子元素.class.getResourceAsStream("/xml/books.xml");
        Document document = saxReader.read(inputStream);    // 解析为文档dom树
        System.out.println("--------------输出根元素--------------");
//                          文档.获取根元素对象.获取名字
        System.out.println(document.getRootElement().getName());  // 根元素名称

        System.out.println("--------------输出根元素下子元素--------------");
        List<Element> elements =  document.getRootElement().elements(); // 获取根元素下所有元素对象，List列表存储
        for (Element element : elements) {
            System.out.print(element.getName()+",");    // 遍历输出名字
        }
        System.out.println("\b");

        System.out.println("--------------输出book元素的子元素--------------");
//                                                获取根元素下所有名字是 name 的元素对象
        List<Element> elements1 = document.getRootElement().elements("name");
        for (Element element : elements1) {
            System.out.print(element.getName()+",");
        }
        System.out.println("\b");

        System.out.println("--------------输出根元素下第一个user元素--------------");
//                                      获取根元素下第一个名字是 book 的元素对象
        Element element = document.getRootElement().element("book");
        List<Element> elements2 = element.elements();   // 获取第一book元素对象下的所用一级元素对象
        for (Element element1 : elements2) {
            System.out.print(element1.getName()+",");
        }
        System.out.println("\b");
        System.out.println(element.getName());  // 输出名字是 book 的第一个元素对象的名字
        System.out.println(element.attributeValue("author"));   // 输出第一个book元素对象下的author属性的属性值
    }
}
