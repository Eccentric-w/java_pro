package DOM4j解析XML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class 解析Contact_xml案例 {
    public static void main(String[] args) throws Exception{

        SAXReader saxReader = new SAXReader();
        InputStream inputStream = 解析Contact_xml案例.class.getResourceAsStream("/xml/Contact.xml");
        Document document = saxReader.read(inputStream);

        Element root = document.getRootElement();

        List<Element> elementList  = root.elements();
        List<Contact> contacts = new ArrayList<>();
        for (Element element : elementList) {
            Contact contact = new Contact();
            contact.setId(Integer.valueOf(element.attributeValue("id")));
            contact.setName(element.elementTextTrim("name"));
            contact.setGender(element.elementTextTrim("gender"));
            contact.setEmail(element.elementTextTrim("email"));
            contacts.add(contact);
        }
        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }
}



class Contact {
    private int id ;
    private String  name ;
    private String gender ;
    private String email ;

    public Contact() {
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Contact(int id, String name, String gender, String email) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.email = email;
    }
}