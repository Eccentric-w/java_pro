package 工厂设计模式;

public class 工厂设计模式 {

    public static void main(String[] args) {
        Factory factory = new Factory();
        Hero hero = factory.Create("kasha");
        hero.introduction();
    }

}

class Hero {
    void introduction() {
        System.out.println("英雄介绍");
    }
}
class KaSha extends Hero {
    @Override
    void introduction() {
        System.out.println("I am kasha");
    }
}

class DaoMei extends Hero {
    @Override
    void introduction() {
        System.out.println("I am daomei");
    }
}

// 通过工厂生产类，
class Factory {

    public Hero Create(String hero) {
        if (hero.equals("daomei")) {
            return new DaoMei();
        }else if (hero.equals("kasha")) {
            return new KaSha();
        }
        return null;
    }
}
