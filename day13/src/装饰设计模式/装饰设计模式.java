package 装饰设计模式;

public class 装饰设计模式 {

    public static void main(String[] args) {
        Cat cat1 = new WhiteCat();
        cat1.run();
        cat1.eat();
        Cat cat2 = new BlackCat(cat1);
        cat2.run();
        cat2.eat();
    }
}

interface Cat {
    void run();
    void eat();
}

class WhiteCat implements Cat {

    @Override
    public void run() {
        System.out.println("这只猫跑得贼快");
    }

    @Override
    public void eat() {
        System.out.println("白猫爱吃罐头");
    }
}

class BlackCat implements Cat{
    Cat cat ;

    public BlackCat(Cat cat) {
        this.cat = cat;
    }

    @Override
    public void run() {
        cat.run();  // 不需要装饰的方法，用原来的对象调用
    }

    @Override
    public void eat() {
        System.out.println("这只猫吃罐头吃得特别香");
    }
}