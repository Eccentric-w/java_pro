package 多态;


public class 强制类型转换 {
    public static void main(String[] args) {
        G h = new H();
        h.run();    //调用重写的方法
        G j = new J();
        j.run();
        go(j);
        go(h);
    }


    public static void go(G g) {
//        如果错误转换会报错
        if(g instanceof H){
            H h = (H) g;    //强制类型转换
            h.special_H();  //调用自己的独有方法
        }else if(g instanceof J){
            J j = (J) g;
            j.special_J();
        }
    }
}


class G {
    public void run() {
        System.out.println("我是G君");
    }
}


class H extends G {
    public void run() {
        System.out.println("我是H君");
    }
    public void special_H() {
        System.out.println("我是H君，这是我的独有功能");
    }
}


class J extends G {
    public void run() {
        System.out.println("我是J君");
    }

    public void special_J() {
        System.out.println("我是J君，这是我的独有功能");
    }
}