package 多态;

public class 概念 {
    public static void main(String[] args) {
        A test = new B();
        test.run();     //调用方法时，编译看左边(左边A类中有run方法)，运行看右边(run方法的结果看B类中的run方法)
        System.out.println(test.name);  //调用变量时，编译看左边，运行看左边
        // 方法存在重写，变量不存在重写
    }
}

class A {
    public String name = "我是动物";
    public void run() {
        System.out.println("我是小动物");
    }
}

class B extends A {
    public String name = "我是猫";
    @Override
    public void run() {
        System.out.println("我是一只猫");
    }
}