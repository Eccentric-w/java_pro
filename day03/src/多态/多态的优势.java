package 多态;

public class 多态的优势 {
    public static void main(String[] args) {
        C d = new D();
        d.run();
        go(d);
//        d.happy();


        C e = new E();
        e.run();
        go(e);
    }



    public static void go(C c) {
//        父类类型是C的变量都可接收到
        System.out.println("比赛开始了——————————");
        c.run();
        System.out.println("比赛结束了");
    }
}

class C {
    public void run() {
        System.out.println("我是C君");
    }
}

class D extends C {
    @Override
    public void run() {
        System.out.println("我是D君");
    }

    public void happy() {
        System.out.println("我是D君，这是我独有的方法");
    }
//    多态缺点：不能直接调用父类没有自己独有的方法，
}

class E extends C {
    @Override
    public void run() {
        System.out.println("我是E君");
    }
}