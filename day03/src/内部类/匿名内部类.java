package 内部类;

public class 匿名内部类 {
    public static void main(String[] args) {
        Fu fu = new Fu() {
            @Override
            public void explain() {
                System.out.println("我是Fu类的匿名内部类");
            }
        };
        yes(fu);
        yes(new Fu() {
            @Override
            public void explain() {
                System.out.println("我也是Fu类的匿名内部类，不过这样写简单一点");
            }
        });
    }

    static void yes(Fu fu) {
        fu.explain();
    }
}

interface Fu {
    public void explain();
}