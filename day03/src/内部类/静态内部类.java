package 内部类;

public class 静态内部类 {
    public static void main(String[] args) {
        Outer.Inter inter = new Outer.Inter();
        System.out.println(inter.name);
        System.out.println(inter.age);
        System.out.println(inter);

    }


}

class Outer {
    String wname = "我是外部类的实例成员变量";
    static String wage = "外部类的静态成员变量";

    void test() {
//        静态内部类不能访问，必须用外部类象访问
        System.out.println("外部类的实例方法放we");
    }
    public static class Inter {
        String name = "我是内部类的实例成员";
//        静态内部类中不能有静态方法
        static String age = "内部类的静态成员";
    }
}
