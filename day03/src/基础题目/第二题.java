package 基础题目;

public class 第二题 {
    public static void main(String[] args) {

//        简化代码
//        不用创建子类
        Employee employee = new Employee("十佳员工","id23",34) {
            @Override
            void work() {
                System.out.println("工号为:"+id+"的"+name+"，他的薪水是"+salary+"元正在工作");
            }

            @Override
            void meet() {
                System.out.println("工号为:"+id+"的"+name+"，他的薪水是"+salary+"元正在开会");
            }
        };

        employee.meet();
        employee.work();
    }
}

abstract class Employee {
    String name = null;
    String id = null;
    int salary = 0;

    public Employee() {
    }

    public Employee(String name, String id, int salary) {
        this.name = name;
        this.id = id;
        this.salary = salary;
    }

    abstract void work() ;
    abstract void meet() ;

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取
     * @return salary
     */
    public int getSalary() {
        return salary;
    }

    /**
     * 设置
     * @param salary
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String toString() {
        return "Employee{name = " + name + ", id = " + id + ", salary = " + salary + "}";
    }
}
