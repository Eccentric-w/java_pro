package 基础题目;

public class 第四题 {
    public static void main(String[] args) {

        Player player = new Player() {
            @Override
            public void playBasketball() {
                System.out.println("运动员打篮球");
            }

            @Override
            public void playPingpong() {
                System.out.println("运动员打乒乓球");
            }
        };

        player.playBasketball();
        player.playPingpong();

    }
}

interface Player {
    void playBasketball();
    void playPingpong();
}

