package 基础题目;

import java.util.Calendar;

public class 第五题 {
    public static void main(String[] args) {

//        多态的写法
        Animal cat = new Cat("猫",2);
        Animal dog = new Dog("狗",8);
        dog.eat();
        cat.eat();

//        不能通过对象直接调用子类独有的方法
        run(dog);
        run(cat);

    }

//    判断属于哪种类型，进行向下类型转换，然后执行特有方法
    static void run(Animal animal) {
        if (animal instanceof Dog) {
            Dog dog = (Dog) animal;
            dog.lookHome();
        }else if (animal instanceof Cat) {
            Cat cat = (Cat) animal;
            cat.catchMouse();
        }
    }
}

abstract class Animal {
    String name ;
    int weight ;

    public Animal(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    abstract void eat ();
}

class Cat extends Animal {

    public Cat(String name, int weight) {
        super(name, weight);
    }

    @Override
    void eat() {
        System.out.println(name+"吃鱼");
    }

    void catchMouse() {
        System.out.println("努力抓老鼠");
    }
}

class Dog extends Animal {

//    继承的变量
    public Dog(String name, int weight) {
//        调用父类构造器
        super(name, weight);
    }

    @Override
    void eat() {
        System.out.println(name+"吃骨头");
    }

    void lookHome() {
        System.out.println("老老实实看家");
    }

}