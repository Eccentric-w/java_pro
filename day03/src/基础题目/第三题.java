package 基础题目;

public class 第三题 {
    public static void main(String[] args) {

        Phone phone = new Phone() {
            @Override
            public void call() {
                System.out.println("手机打电话");
            }

            @Override
            public void message() {
                System.out.println("手机发短信");
            }
        };

        phone.call();
        phone.message();

    }
}

interface Phone {
    void call();
    void message();
}
