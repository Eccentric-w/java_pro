package 接口与多态的综合案例;

import javax.imageio.stream.MemoryCacheImageOutputStream;
import java.security.Key;

public class usb设备 {
    public static void main(String[] args) {
        Computer computer = new Computer();
        Usb mouse = new Mouse("小米鼠标");
        Usb keyBoard = new KeyBoard("机械键盘");
        computer.useful(mouse);
        computer.useful(keyBoard);
    }

}

class Computer {
    public void useful(Usb usb) {
        usb.connection();

        if (usb instanceof Mouse) {
            Mouse mouse = (Mouse) usb;
            mouse.click();
        }else if (usb instanceof KeyBoard) {
            KeyBoard keyboard = (KeyBoard) usb;
            keyboard.input();
        }

        usb.unConnection();
    }
}


interface Usb {
    void connection();
    void unConnection();
}


class Mouse implements Usb {
    private String name ;

    public Mouse(String name) {
        this.name = name;
    }

    public void click() {
        System.out.println(name+"点击了屏幕");
    }

    @Override
    public void connection() {
        System.out.println(name+"插入了");
    }

    @Override
    public void unConnection() {
        System.out.println(name+"拔出了");
    }
}


class KeyBoard implements Usb {
    private String name ;

    public KeyBoard(String name) {
        this.name = name;
    }

    public void input() {
        System.out.println(name+"开始敲击键盘输入内容");
    }

    @Override
    public void connection() {
        System.out.println(name+"插入了");
    }

    @Override
    public void unConnection() {
        System.out.println(name+"拔出了");
    }
}