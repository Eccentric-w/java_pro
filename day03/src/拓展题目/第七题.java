package 拓展题目;

public class 第七题 {
    public static void main(String[] args) {

        Employee manager = new Manager("张晓强",9000);
        Employee coder = new Coder("李小良",5000);

        Company company = new Company();
        company.paySalary(manager);
        company.paySalary(coder);
    }
}

class Employee {
    String name ;
    int salary ;
}

class Manager extends Employee {
    String name ;
    int salary ;

    public Manager(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }
}

class Coder extends Employee {
    String name ;
    int salary ;

    public Coder(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }
}

interface Money {
    void paySalary(Employee employee);
}

class Company implements Money{
    int assets = 1000000;

    @Override
    public void paySalary(Employee employee) {
        if (employee instanceof Manager) {
            Manager manager = (Manager) employee;
            assets -= manager.salary;
            System.out.println("给"+manager.name+"发工资:"+manager.salary+",公司剩余:"+assets);

        }else if (employee instanceof Coder) {
            Coder coder = (Coder) employee;
            assets -= coder.salary;
            System.out.println("给"+coder.name+"发工资:"+coder.salary+",公司剩余:"+assets);
        }
    }
}