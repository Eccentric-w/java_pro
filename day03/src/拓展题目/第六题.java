package 拓展题目;

public class 第六题 {
    public static void main(String[] args) {
//        会打篮球的类型
        Sport sportTeacher = new SportTeacher("大姚",35);
        Sport sportStudent = new SportStudent("网众望",21);
        playBasketball(sportStudent);
        playBasketball(sportTeacher);
    }

//        这里要接收的对象类型要是Sport类型，因为只有会打篮球的人（实现Sport接口）才能调用打篮球方法
    static void playBasketball(Sport person) {
        if (person instanceof SportStudent) {
            SportStudent sportStudent = (SportStudent) person;
            sportStudent.playBasketball();
        }else if (person instanceof SportTeacher) {
            SportTeacher sportTeacher = (SportTeacher) person;
            sportTeacher.playBasketball();
        }
    }
}

//普通的抽象人 类
abstract class Person {
    String name ;
    int age ;

    abstract void eat();
}

//普通学生
class Student extends Person {

    void study() {
        System.out.println(age+"岁的"+name+"同学在学习");
    }

    @Override
    void eat() {
        System.out.println(age+"岁的"+name+"同学在吃学生餐");
    }
}

//普通老师
class Teacher extends Person {

    void teach() {
        System.out.println(age+"岁的"+name+"老师在教学");
    }

    @Override
    void eat() {
        System.out.println(age+"岁的"+name+"老师在吃工作餐");
    }
}

//会打篮球的老师
class SportTeacher extends Teacher implements Sport {

    public SportTeacher(String name,int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public void playBasketball() {
        System.out.println(age+"岁的"+name+"老师会打篮球");
    }
}

//会打篮球的学生
class SportStudent extends Student implements Sport {

    public SportStudent(String name,int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public void playBasketball() {
        System.out.println(age+"岁的"+name+"同学会打篮球");
    }
}

//运动接口，谁会打篮球就实现这个接口
interface Sport {
    void playBasketball();
}
