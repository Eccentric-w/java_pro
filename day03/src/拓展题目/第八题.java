package 拓展题目;

import java.util.LinkedList;

public class 第八题 {
    public static void main(String[] args) {
        Goods apple = new Fruit("g20000","苹果",50);
        Goods computer = new Electron("g10000","笔记本",10000);
        Goods phone = new Electron("g10001","手机",5000);

//        实例化购物车对象
        Shooping shooping = new Shooping();

//        向购物车里添加商品
        shooping.add(apple);
        shooping.add(computer);
        shooping.add(phone);

//        打印所有商品信息
        shooping.print();

//        删除id是g10001的手机商品
        shooping.remove("g10001");

        shooping.print();

//        输出结算价格
        shooping.count();
    }
}

class Goods {
    String id ;
    String name ;
    double price ;
}

class Electron extends Goods {

    public Electron(String id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}

class Fruit extends Goods {
    public Fruit(String id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}

class Shooping {
    LinkedList<Goods> linkedList = new LinkedList<>();
    double count ;

    void add(Goods goods) {
        linkedList.addLast(goods);
        System.out.println("加入"+goods.name+"成功");
    }

    void remove(String id) {
        for (Goods goods : linkedList) {
            if (goods.id == id) {
                linkedList.remove(goods);
            }
        }
    }

    void print() {
        System.out.println("==========================打印商品==========================\n您选购的商品:");
        for (Goods goods : linkedList) {
            System.out.println("\t"+goods.id+","+goods.name+","+goods.price);
        }
    }

    void count() {
        System.out.println("-----------");

        for (Goods goods : linkedList) {
            count += goods.price;
        }
        System.out.println("原价为:"+count);
        System.out.println("折后价为:"+Math.round(count*8.8));
    }
}