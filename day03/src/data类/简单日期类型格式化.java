package data类;

import java.text.SimpleDateFormat;
import java.util.Date;

public class 简单日期类型格式化 {
    public static void main(String[] args) {

//        获取当前时间的美式形式
        Date date = new Date();

//        获取当前时间的毫秒值
        long time = date.getTime();
        time += 61*1000;

//        创建简单日期格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss a EEE");

//        输出当前格式化的日期,格式化日期类型
        String current_date = simpleDateFormat.format(date);
        System.out.println(current_date);

//        输出61s后格式化毫秒值的日期
        String future_date = simpleDateFormat.format(time);
        System.out.println(future_date);



    }
}
