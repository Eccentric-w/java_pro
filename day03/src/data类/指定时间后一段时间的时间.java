package data类;

// 面试题：请问 “2019-11-04 09:30:30” 往后 1天15小时，30分29s后的时间是多少

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class 指定时间后一段时间的时间 {
//    抛出解析字符串提醒的异常
    public static void main(String[] args) throws ParseException {

//    1. 解析指定日期的字符串，转换成日期类型，然后转换成毫秒值
    SimpleDateFormat simpleDateFormat01 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//    解析字符串
    Date appointDate = simpleDateFormat01.parse("2019-11-04 09:30:30");
    long valueAppointDate = appointDate.getTime();


//    2. 解析一段时间的字符串，转换成日期类型，然后转换成毫秒值
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
//        解析一段时间的字符串
//        时间起点是1970年1月1日8：00:00
        Date time = simpleDateFormat2.parse("1970-1-02-23:30:29");
//        转换成毫秒值
        long valueTime = time.getTime();


//    3. 毫秒值计算，结果格式化成指定日期类型
        long valueDate = valueAppointDate + valueTime ;
//        转换成美式日期格式
        Date finalDate = new Date(valueDate);

        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
        String result_date = simpleDateFormat3.format(finalDate);

        System.out.println(result_date);
    }
}
