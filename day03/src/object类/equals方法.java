package object类;

import java.util.Objects;

public class equals方法 {
    public static void main(String[] args) {
        Test test1 = new Test("张三",12);
        Test test2 = new Test("张三",12);
        System.out.println(test1.equals(test2));

    }
}

class Test {
    String name ;
    int age ;

    public Test(String name, int age) {
        this.name = name;
        this.age = age;
    }

//    原方法用来比较两个对象地址是否相同，意义不大
//    通常用来重写，比较对象内容是否相同
    @Override
    public boolean equals(Object o) {
//        如果和本身比较，直接返回true
        if (this == o) return true;
//        如果进行比较的对象为空或者比较的两个对象不是同一类型，直接返回true
        if (o == null || getClass() != o.getClass()) return false;
//        强制转换成Test类型
        Test test = (Test) o;
//        进行内容比较并直接返回结果
        return age == test.age &&
                Objects.equals(name, test.name);
    }

}
