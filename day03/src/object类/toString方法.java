package object类;

public class toString方法 {
    public static void main(String[] args) {
        A a = new A("张三",13);
        System.out.println(a.test());       //输出a对象的地址，格式：类名.对象名@地址
        System.out.println(a.toString());   //输出a对象的内容

    }
}

class A {
    String name ;
    int age ;

    public A(String name, int age) {
        this.name = name;
        this.age = age;
    }

    String test() {
        return super.toString();
    }

    //直接输出地址意义不大，更多的是输出信息
    @Override
    public String toString() {
        return "A{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
