import java.math.BigDecimal;

public class BigDecimal大数据类 {
    public static void main(String[] args) {
        double a = 0.1;
        double b = 0.2;
        double c = a+b;
//        这样直接运算存在失真问题
        System.out.println(c);

//        把浮点型a转换成大数据对象
        BigDecimal a1 = BigDecimal.valueOf(a);
        BigDecimal b1 = BigDecimal.valueOf(b);
        BigDecimal c1 = a1.add(b1);


//        直接输出还是失真
        System.out.println(c);

//        double只是解决精度问题的手段，结果还需要double类型
        double c2 = c1.doubleValue();
        System.out.println(c2);

    }
}
