package 正则表达式;

public class 结合方法做分割 {
    public static void main(String[] args) {

        String names = "黑马1234完美3fudiof结果";
        String names1 = "第一名，第二名，第三名";

//        根据”，“做分割
        String[] nameList1 = names1.split("，");
//        System.out.println(nameList1);    //输出地址
        for (int i=0 ; i<nameList1.length;i++) {
            System.out.println(nameList1[i]);
        }


//        根据正则表达式做分割
        String[] nameList2 = names.split("\\w+");
        for (int i=0 ; i<nameList2.length ; i++) {
            System.out.println(nameList2[i]);
        }


    }
}
