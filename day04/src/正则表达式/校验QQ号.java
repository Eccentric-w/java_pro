package 正则表达式;

/**
 校验QQ号：
 1. 必须全部都是数字
 2. 长度必须大于4位
 */

public class 校验QQ号 {
    public static void main(String[] args) {
        String qqId01 = "12343234";
        String qqId02 = "123";
        System.out.println(checkQq(qqId01));
        System.out.println(checkQq(qqId02));
    }

    public static boolean checkQq(String qq) {
//       \d:表示数字
//        \\:如果单斜杠加d表示转义字符，双斜杠表示是真的斜杠
//        {4,}:表示长度最低4位
        return qq.matches("\\d{4,}");
    }
}
