package 泛型;

import java.util.ArrayList;

public class 自定义泛型方法 {

    public static void main(String[] args) {

        String[] song = {"手心","林俊杰","邓紫棋"};
        run(song);

        Number[] numbers = {1,12,2,3,34,44};
        run(numbers);

    }

    public  static <T> void run(T[] t) {

//        字符串拼接类型
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");

        if (t != null && t.length >0) {
            for (int i = 0; i<t.length-1 ; i++){
//                if判断句
                stringBuilder.append(i==t.length-1 ? t[i]:t[i]+",");
            }
        }
        stringBuilder.append("]");
        System.out.println(stringBuilder);
    }

}
