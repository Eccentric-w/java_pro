package 集合;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

public class 集合的特点 {
    public static void main(String[] args) {
        Collection hashSet = new HashSet();
//        无序，不可重复，无索引
        hashSet.add("java");
        hashSet.add("python");
        hashSet.add("C");
        hashSet.add("python");
        System.out.println(hashSet);

        Collection linkedList = new LinkedList<>();
//        有序，可重复，有索引
        linkedList.add("java");
        linkedList.add("python");
        linkedList.add("C");
        linkedList.add("python");
        System.out.println(linkedList);


    }
}
