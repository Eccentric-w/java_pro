package Calendar日历类;

import java.util.Calendar;

import static java.util.Calendar.YEAR;

public class 日历类 {
    public static void main(String[] args) {

//        直接通过类名调用方法创建对象
        Calendar calendar = Calendar.getInstance();
//        直接打印除日历信息，说明默认重写了toString方法
        System.out.println(calendar);

//        查看当前日期
        System.out.println(calendar.getTime());

//        查看当前毫秒值
        System.out.println(calendar.getTimeInMillis());

//        查看5年后的时间
        calendar.add(YEAR,5);
        System.out.println(calendar.get(YEAR));
    }
}
