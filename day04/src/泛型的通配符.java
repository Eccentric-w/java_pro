import java.util.ArrayList;

public class 泛型的通配符 {
    public static void main(String[] args) {

        ArrayList<Mouse> mouseArrayList =new ArrayList<>();
        Mouse mouse1 = new Mouse();
        Mouse mouse2 = new Mouse();
        Mouse mouse3 = new Mouse();
        mouseArrayList.add(mouse1);
        mouseArrayList.add(mouse2);
        mouseArrayList.add(mouse3);
        run(mouseArrayList);

        ArrayList<KeyBoard> keyBoardArrayList = new ArrayList<>();
        keyBoardArrayList.add(new KeyBoard());
        keyBoardArrayList.add(new KeyBoard());
        keyBoardArrayList.add(new KeyBoard());
        run(keyBoardArrayList);
}

    public static void run(ArrayList<? extends Computer> c) {
//                                    Computer本身及其所有子类才能接收

    }
}

class Computer {}

class Mouse extends Computer {}

class KeyBoard extends Computer {}