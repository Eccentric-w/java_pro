public class Math数学类 {
    public static void main(String[] args) {

//        取绝对值
        System.out.println(Math.abs(-23.3));

//        向上取整
        System.out.println(Math.ceil(3.000000000001));

//        向下取整
        System.out.println(Math.floor(4.999999));

//        指数次方
        System.out.println(Math.pow(2,3));

//        四舍五入
        System.out.println(Math.round(4.5));


    }
}
