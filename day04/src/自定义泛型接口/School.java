package 自定义泛型接口;

public interface School<E> {
    void add(E e);
    void delete(E e);
    void update(E e);
    void search(int i);

}
