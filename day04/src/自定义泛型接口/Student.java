package 自定义泛型接口;

public class Student implements School<Student> {

    @Override
    public void add(Student student) {
        System.out.println("添加学生");
    }

    @Override
    public void delete(Student student) {
        System.out.println("删除学生");
    }

    @Override
    public void update(Student student) {
        System.out.println("修改学生信息");
    }

    @Override
    public void search(int i) {
        System.out.println("查找学生");
    }
}
