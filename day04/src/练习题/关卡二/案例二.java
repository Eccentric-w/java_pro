package 练习题.关卡二;

import java.util.Date;

/**
 * 计算程序执行所需时间
 * 1. 创建String对象，使用for循环对字符串进行拼接，计算执行10000次for循环花费的时间。
 * 2. 创建StringBuilder对象，使用for循环对字符串进行拼接，计算执行10000次for循环花费的时间
 */

public class 案例二 {
    public static void main(String[] args) {

        Date date = new Date();
        long start01 = System.currentTimeMillis();
        String s = new String();
        for (int i = 0; i<10000 ; i++) {
            s += "i";
        }
        long end01 = System.currentTimeMillis();
        long start02 = System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0 ; i<10000; i++) {
            stringBuilder.append("i");
        }
        long end02 = System.currentTimeMillis();

        double time01 = (double)(end01 - start01)/1000;
        System.out.println("String对象循环用时："+time01+"秒");

        double time02 = (double)(end02 - start02)/1000;
        System.out.println("StringBuilder对象循环用时："+time02+"秒");


    }
}
