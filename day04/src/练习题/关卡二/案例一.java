package 练习题.关卡二;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 计算迄今为止活了多少天
 */

public class 案例一 {
    public static void main(String[] args) throws ParseException {
        Date date = new Date();
        long l = date.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdayDate = simpleDateFormat.parse("1999-05-19");
        long birthdayValue = birthdayDate.getTime();
        long liveValue = l - birthdayValue;
        long dayValue = liveValue/(1000*60*60*24);
        System.out.println("迄今位置已经活了"+dayValue+"天了");
    }
}
