package 练习题.关卡二;

public class 案例三 {
    public static void main(String[] args) {
        String[] list1 = {"a","b","c","d"};
        swap(list1,1,3);
        for (String s : list1) {
            System.out.print(s+", ");
        }
        System.out.println("\b\b");

        Integer[] list2 = {1,2,3,4};
        swap(list2,1,3);
        for (Integer integer : list2) {
            System.out.print(integer+", ");
        }
        System.out.println("\b\b");
    }

//                各种类型都可以接收的泛型
    public static <T> void swap(T[] list, int a, int b) {
        T temp = list[a];
        list[a] = list[b];
        list[b] = temp;
    }
}
