package 练习题.关卡三;

import java.util.*;

public class 案例二_超市购物系统 {

    static List<Goods> goodsList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);
    static Map<String,Integer> goodsCar = new HashMap();
    static  {
        Goods goods001 = new Goods("001","少林核桃",15.5,"斤");
        Goods goods002 = new Goods("002","尚康饼干",14.5,"包");
        Goods goods003 = new Goods("003","移动硬盘",369.0,"个");
        Goods goods004 = new Goods("004","高清电视",399.0,"G");

        goodsList.add(goods001);
        goodsList.add(goods002);
        goodsList.add(goods003);
        goodsList.add(goods004);
    }

    public static void main(String[] args) {
        System.out.println("\t\t欢迎使用超市购物系统");
        menu();
    }

    public static void buyGoods() {

        System.out.println("=======================================");
        System.out.println("商品id\t 名称\t 单位\t 计价单位");
        for (Goods goods : goodsList) {
            System.out.printf(goods.id+"\t   "+goods.name+"\t "+goods.price+"\t "+goods.unit);
            System.out.println();
        }
        System.out.println("=======================================");
        System.out.println("请输入你要购买的商品项，（输入3格式：商品id-数量），输入end表示购买结束");
        while (true) {
            String purchase = scanner.nextLine();
            String[] purchaselist = purchase.split("-");
            if (purchaselist.length != 2) {
                System.out.println("你的输入有误，请重写输入，（输入格式：商品id-数量）");
                continue;
            }
            int purchaseNumber = Integer.parseInt(purchaselist[1]);
            if (purchase.equals("end")) {
                System.out.println("购买结束");
                break;
            }
            else if ( judgeId(purchaselist[0]) && Integer.parseInt(purchaselist[1]) >= 0 ) {
                goodsCar.put(purchaselist[0],Integer.parseInt(purchaselist[1]));
            }else {
                System.out.println("你的输入有误，请重写输入，（输入格式：商品id-数量）");
            }
        }
    }

    public static boolean judgeId(String id) {
        for (Goods goods : goodsList) {
            if (goods.id.equals(id)) {
                return true;
            }
        }
        return false;
    }

    public static void settle() {
        System.out.println("======================================");
        System.out.println("\t\t\t欢迎光临");
        System.out.println("名称\t 售价\t 数量\t 金额\t ");
        System.out.println("--------------------------------------");
        int count = 0;
        double countMonery = 0.0;
        goodsCar.forEach((str,inter)->{
            for (Goods goods : goodsList) {
                if (str.equals(goods.id)) {
                    goods.number = inter;
                    System.out.printf(goods.name,goods.price,goods.number,goods.monery);
                }
            }
        });
        System.out.println("--------------------------------------");
        System.out.println(goodsCar.size()+"项商品");
        System.out.println();
        System.out.println("======================================");
    }

    public static void menu() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("请输入你要进行的操作:");
            System.out.println("1.购买商品\t 2.结算并打印小票\t 3.退出系统");

            String operation = scanner.nextLine();
            switch (operation) {
                case "1":
                    buyGoods();
                    break;
                case "2":
                    settle();
                    break;
                case "3":
                    System.out.println("感谢您使用本系统！");
                    System.exit(0);
                default:
                    System.out.print("您输入的指令有无，请重写输入:");
            }
        }

    }
}

//商品类
class Goods {

    String id ;
    String name ;
    double price ;
    String unit ;
    int number = 0;    //选购数量
    double monery = price * number;

    public Goods(String id, String name, double price, String unit) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getMonery() {
        return monery;
    }

    public void setMonery(double monery) {
        this.monery = monery;
    }
}

