package 练习题.关卡三;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class 案例一 {
    public static void main(String[] args) throws ParseException {
        String thatDate = "2016-12-18";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(thatDate);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy年MM月dd日");
        String thisDate = simpleDateFormat1.format(date);
        System.out.println(thisDate);
    }
}
