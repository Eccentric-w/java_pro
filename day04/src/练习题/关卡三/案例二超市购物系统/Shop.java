package 练习题.关卡三.案例二超市购物系统;

import java.util.*;

public class Shop {

//    商品列表
    static List<Goods> goodsList = new ArrayList<>() ;

//    购物车
    static Map<String,Integer> shopCar = new HashMap();

//    扫描器，扫描输入
    static Scanner scanner  = new Scanner(System.in);

//    添加商品进列表
    static {
        Goods goods001 = new Goods("001","少林核桃",15.5,"斤");
        Goods goods002 = new Goods("002","尚康饼干",14.5,"包");
        Goods goods003 = new Goods("003","移动硬盘",369.0,"个");
        Goods goods004 = new Goods("004","高清电视",399.0,"G");

        goodsList.add(goods001);
        goodsList.add(goods002);
        goodsList.add(goods003);
        goodsList.add(goods004);
    }

//    系统菜单
    public static void menu() {

//        循环在菜单中，不退出系统则系统一直跑
        while (true) {
            System.out.println("请输入你要进行的操作:");
            System.out.println("1.购买商品\t 2.结算并打印小票\t 3.退出系统");

            int operation = scanner.nextInt();
            switch (operation) {
                case 1:
                    buyGoods(); //购买商品
                    break;
                case 2:
                    settle();   //打印小票
                    break;
                case 3:       //退出系统
                    System.out.println("感谢您使用本系统！");
                    System.exit(0);
                default:
                    System.out.print("您输入的指令有无，请重写输入:");
            }
        }
    }

//    购买商品
    public static void buyGoods() {

//        展示所有商品信息
        for (Goods goods : goodsList) {
            System.out.println(goods.id+"\t "+goods.name+"\t "+goods.price+"\t "+goods.unit);
        }

//        输入购买信息
        while (true) {
            String purchase = scanner.nextLine();
            int number = 0;
            if (purchase.equals("end")) {
                System.out.println("购买结束");
                return;
            }else {
                String[] purchaseList = purchase.split("-");
                try {
//                    防止转换异常，导致系统崩溃
                    number = Integer.parseInt(purchaseList[1]);
                } catch (Exception e) {
                    System.err.println("输入的商品数量不合法");
                }

//                判读输入格式正确吗
//                数组长度为二（商品id和购买数量）、商品id是否存在、购买数量是否合法
                if (purchaseList.length == 2 && checkIdInGoodsList(purchaseList[0]) && number > 0 ) {
//                    添加到购物车集合
                    addGoodsToShopCar(purchaseList[0],number);
                }else {
//                    输入格式不对，重写输入
                    System.out.println("输入的购买信息有误，请重写输入:");
                }
            }
        }
    }

//    打印购买小票
    public static void settle() {
        if (shopCar.size() == 0) {
            System.out.println("还没有购买任何商品");
            return;
        }
//        总数量
        int totalNumber = 0;
//        总价格
        double totalMonery = 0.0;

//        遍历购物车集合：先转换成Set集合
        Set<Map.Entry<String,Integer>> entries = shopCar.entrySet();

        for (Map.Entry<String, Integer> entry : entries) {
            for (Goods goods : goodsList) {
                if (goods.id.equals(entry.getKey())) {
//                    数量赋值给商品
                    goods.number = entry.getValue();
//                    输出商品信息
                    System.out.printf(goods.name,goods.price,goods.number,goods.number*goods.price);
                    System.out.println();
//                    循环累计总数量
                    totalNumber += entry.getValue();
//                    循环累计总价格
                    totalMonery += goods.number*goods.price;
//                    最后把商品对象的数量属性归零
                    goods.number = 0;
                }
            }
        }
//        输出购买信息
        System.out.println(shopCar.size()+"项商品");
        System.out.println("共计:"+totalNumber+"件");
        System.out.println("共:"+totalMonery+"元");
    }

//    检查id是否存在商品列表中
    public static boolean checkIdInGoodsList(String id) {
        for (Goods goods : goodsList) {
            if (goods.id.equals(id)) {
                return true;
            }
        }
        return false;
    }

//    商品添加购物车
    public static void addGoodsToShopCar(String id,int number) {
//        多次购买同一商品，数量叠加
//        Map集合的不重复特征，再次添加则覆盖之前的数量信息
        if (shopCar.containsKey(id)) {
            int value = shopCar.get(id);
            number += value;
        }
        shopCar.put(id,number);
    }


    public static void main(String[] args) {
//        程序入口，调用系统菜单
        menu();
    }

}
