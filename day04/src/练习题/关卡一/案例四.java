package 练习题.关卡一;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class 案例四 {
//    指定字符串按照格式转换成日期类型
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("1999-05-19");
        System.out.println(date);
    }
}
