package 练习题.关卡一;

import java.util.ArrayList;
import java.util.Collection;

public class 案例六 {
    public static void main(String[] args) {

//        1.创建Colection对象.Collection是接口.所以创建子类ArrayList对象
        Collection<String> arrayList = new ArrayList<>();

//        2.往集合中添加对象元素
        arrayList.add("黄四郎");
        arrayList.add("张麻子");
        arrayList.add("汤师爷");

//        3.删除元素
        arrayList.remove("张麻子");

//        4.获取集合大小
        System.out.println(arrayList.size());

//        6.输出集合内容
        for (String s : arrayList) {
            System.out.println(s);
        }
        System.out.println(arrayList);

//        5.清空集合
        arrayList.clear();

    }
}
