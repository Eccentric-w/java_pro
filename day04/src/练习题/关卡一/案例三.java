package 练习题.关卡一;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//将日期类型转换成，指定格式的字符串类型

public class 案例三 {
    public static void main(String[] args) throws ParseException {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        直接调用把date类型的数据传入format方法中
        String s = simpleDateFormat.format(date);
        System.out.println(s);
    }
}
