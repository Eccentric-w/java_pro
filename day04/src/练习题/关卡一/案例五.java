package 练习题.关卡一;

import java.util.Calendar;

public class 案例五 {
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();
        System.out.println("当前日期是:"+calendar.get(Calendar.YEAR)+"年"+calendar.get(Calendar.MONTH)+"月"+calendar.get(Calendar.DAY_OF_MONTH)+"日");

        calendar.add(Calendar.DAY_OF_MONTH,500);
        System.out.println("500天之后的日期是:"+calendar.get(Calendar.YEAR)+"年"+calendar.get(Calendar.MONTH)+"月"+calendar.get(Calendar.DAY_OF_MONTH)+"日");

    }
}
